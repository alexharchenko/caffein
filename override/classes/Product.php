<?php

/**
 * @deprecated 1.5.0.1
 */
// define('_CUSTOMIZE_FILE_', 0);
/**
 * @deprecated 1.5.0.1
 */
// define('_CUSTOMIZE_TEXTFIELD_', 1);

class Product extends ProductCore
{
    public static function getFrontFeaturesStatic($id_lang, $id_product)
    {
        if (!Feature::isFeatureActive()) {
            return array();
        }
        if (!array_key_exists($id_product.'-'.$id_lang, self::$_frontFeaturesCache)) {
            self::$_frontFeaturesCache[$id_product.'-'.$id_lang] = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
				SELECT name, value, pf.id_feature, ind.url_name
				FROM '._DB_PREFIX_.'feature_product pf
				LEFT JOIN '._DB_PREFIX_.'feature_lang fl ON (fl.id_feature = pf.id_feature AND fl.id_lang = '.(int)$id_lang.')
				LEFT JOIN '._DB_PREFIX_.'feature_value_lang fvl ON (fvl.id_feature_value = pf.id_feature_value AND fvl.id_lang = '.(int)$id_lang.')
				LEFT JOIN '._DB_PREFIX_.'feature f ON (f.id_feature = pf.id_feature AND fl.id_lang = '.(int)$id_lang.')
                LEFT JOIN '._DB_PREFIX_.'layered_indexable_feature_lang_value ind ON (ind.id_feature = f.id_feature AND ind.id_lang = '.(int)$id_lang.')
				'.Shop::addSqlAssociation('feature', 'f').'
				WHERE pf.id_product = '.(int)$id_product.'
				ORDER BY f.position ASC'
            );
        }
        return self::$_frontFeaturesCache[$id_product.'-'.$id_lang];
    }
}