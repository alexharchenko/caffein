<?php

class ObjectModel extends ObjectModelCore
{
    public static function formatValue($value, $type, $with_quotes = false, $purify = true, $allow_null = false)
    {
    	if ($type == self::TYPE_HTML && $purify) {
    		if ($allow_null && $value === null) {
	            return array('type' => 'sql', 'value' => 'NULL');
	        }
                // comment line for enable custom attributes
    		//$value = Tools::purifyHTML($value, null, true);
    		if ($with_quotes) {
                return '\''.pSQL($value, true).'\'';
            }
            
			return pSQL($value, true);
    	} else {
    		return parent::formatValue($value, $type, $with_quotes, $purify, $allow_null);
    	}
    }
}