<?php

class FrontController extends FrontControllerCore
{
    protected function canonicalRedirection($canonical_url = '')
    {
        if (Tools::getValue('q')) {
            return;
        }
        parent::canonicalRedirection($canonical_url);
    }

    protected function updateQueryString(array $extraParams = null)
    {
        $uriWithoutParams = explode('?', $_SERVER['REQUEST_URI'])[0];
        $url = Tools::getCurrentUrlProtocolPrefix().$_SERVER['HTTP_HOST'].$uriWithoutParams;

        $params = array();
        parse_str($_SERVER['QUERY_STRING'], $params);

        if (null !== $extraParams) {
            foreach ($extraParams as $key => $value) {
                if (null === $value) {
                    unset($params[$key]);
                } else {
                    $params[$key] = $value;
                }
            }
        }
        if (array_key_exists('page', $params) && is_int($params['page'])) {
            $return = $url.'?page='.$params['page'].(!empty($params['order']) ? '&order='.$params['order'] : '');
            return $return;
        }
	
        $m = [];
        if (isset($params['q'])) {
            $m['q'] = $params['q'];
        }
        $queryString = str_replace('%2F', '/', http_build_query($m, '', '&'));

        //$queryString_sort = str_replace('%2F', '/', http_build_query($sorting, '', '&'));
        $queryString = str_replace('=', '/', $queryString);
        if (Tools::getValue('q')) {
        	$url_arr = explode('/q/', $url);
        	$url = $url_arr[0];
        }

        unset($params['q']);
        unset($params['from-xhr']);
        if (preg_match('/q\/$/', $queryString)) {
            $queryString = substr($queryString, 0, -3);
        }
        $queryString .= (http_build_query($params, '', '&') != '' ? '?'.http_build_query($params, '', '&') : '');

        $return = $url.($queryString ? "/$queryString" : '');
        if (preg_match('/\/q\/$/', $return)) {
            $return = substr($return, 0, -3);
        }
        $return = str_replace("/?", "?", $return);
        $re = '/-₴-([0-9]+)-([0-9]+)/m';
        $subst = '-₴f$1t$2';
        $return = preg_replace($re, $subst, $return);
        $return = preg_replace('/q\/{2,}/', 'q/', $return);
        $return = str_replace("%E2%82%B4", "₴", $return);
        return $return;
    }
}

