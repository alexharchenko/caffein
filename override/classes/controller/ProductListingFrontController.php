    <?php

use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery;
use PrestaShop\PrestaShop\Core\Product\Search\Pagination;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchContext;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchResult;
use PrestaShop\PrestaShop\Core\Product\Search\Facet;
use PrestaShop\PrestaShop\Core\Product\Search\SortOrder;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchProviderInterface;
use PrestaShop\PrestaShop\Core\Product\Search\FacetsRendererInterface;

abstract class ProductListingFrontController extends ProductListingFrontControllerCore
{
    /**
     * Converts a Facet to an array with all necessary
     * information for templating.
     *
     * @param Facet $facet
     *
     * @return array ready for templating
     */
    protected function prepareFacetForTemplate($facet)
    {
    	if (is_array($facet)) {
    		$facetsArray = $facet;
    	} else {
    		$facetsArray = $facet->toArray();
    	}

        foreach ($facetsArray['filters'] as &$filter) {
            $filter['facetLabel'] = $facetsArray['label'];
            if ($filter['nextEncodedFacets']) {
            	$tmp_facets = explode('/',$filter['nextEncodedFacets']);
            	$nextEncodedFacets = [];
            	foreach($tmp_facets as $tmp_facet)
            	{
            		$tmp_filters = explode('-',$tmp_facet);
            		$nextEncodedFilters = [];
            		foreach($tmp_filters as $key => $tmp_filter)
            		{
	            		if ($key == 0) {
	            			$nextEncodedFilters[] = $this->getFacetsRewriteByName($tmp_filter);
	            		} else {
	            			if ($tmp_filters[0] == $this->trans('Categories', array(), 'Modules.Facetedsearch.Shop')) {
	            				$nextEncodedFilters[] = str_replace("-", ".", Db::getInstance()->getValue('SELECT cl.`link_rewrite`
																					FROM `'._DB_PREFIX_.'category_lang` cl
																					WHERE `id_lang` = '.(int) $this->context->language->id.'
																					'.Shop::addSqlRestrictionOnLang('cl').'
																					AND cl.`name` = "'.pSQL($tmp_filter).'"'));
	            			} elseif ($tmp_filters[0] == $this->trans('Price', array(), 'Modules.Facetedsearch.Shop')) {
	            				// $cena = $this->getFacetsRewriteByName($tmp_filters[0]);
	            				# preg_match("/$cena-(.*)\/?/",Tools::getValue('q'),$search);
	            				// $nextEncodedFilters[] = $search[1];
                 //                var_dump($tmp_filter); echo '<br>';
                                $nextEncodedFilters[] = $tmp_filter;
	            				break;
                            // } elseif ($tmp_filters[0] == $this->trans('Weight', array(), 'Modules.Facetedsearch.Shop')) {
                            //     $weightValue = substr($tmp_filter, 0, -2);
                            //     $nextEncodedFilters[] = $this->getFiltersRewriteByName($weightValue);
                            //     // var_dump($nextEncodedFilters); echo '<br>';
                            //     break;
	            			} else {
	            				$nextEncodedFilters[] = $this->getFiltersRewriteByName($tmp_filter);
	            			}
	            		}
	            	}
	            	$nextEncodedFacets[] = implode('-',$nextEncodedFilters);
            	}
                $filter['nextEncodedFacetsURL'] = $this->updateQueryString(array(
                    'q' => implode('/',$nextEncodedFacets),
                    'page' => null,
                ));
            } else {
                $filter['nextEncodedFacetsURL'] = $this->updateQueryString(array(
                    'q' => null,
                ));
            }
        }
        unset($filter);

        return $facetsArray;
    }

    protected function prepareProductForTemplate(array $rawProduct)
    {
        $product = (new ProductAssembler($this->context))
            ->assembleProduct($rawProduct)
        ;

        if (!$product) {
            Tools::redirect($this->context->link->getPageLink('pagenotfound'));
        }

        $presenter = $this->getProductPresenter();
        $settings = $this->getProductPresentationSettings();

        return $presenter->present(
            $settings,
            $product,
            $this->context->language
        );
    }

    public function getFacetsRewriteByName($name) {
    	
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
			SELECT rewrite
			FROM '._DB_PREFIX_.'layered_product_facets_rewrite
			WHERE name="'.pSQL($name).'"'
        );
    }

    public function getFiltersRewriteByName($name) {
    	
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
			SELECT rewrite
			FROM '._DB_PREFIX_.'layered_product_filters_rewrite
			WHERE name="'.pSQL($name).'"'
        );
    }

    public function getFacetsNameByRewrite($rewrite) {
    	
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
			SELECT name
			FROM '._DB_PREFIX_.'layered_product_facets_rewrite
			WHERE rewrite="'.pSQL($rewrite).'"'
        );
    }

    public function getFiltersNameByRewrite($rewrite) {
    	
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
			SELECT name
			FROM '._DB_PREFIX_.'layered_product_filters_rewrite
			WHERE rewrite="'.pSQL($rewrite).'"'
        );
    }

    /**
     * Renders an array of facets.
     *
     * @param array $facets
     *
     * @return string the HTML of the facets
     */
    protected function renderFacets(ProductSearchResult $result)
    {
        $facetCollection = $result->getFacetCollection();
        // not all search providers generate menus
        if (empty($facetCollection)) {
            return '';
        }

        $facetsArray = $this->getFacetsArray($facetCollection);

        $facetsVar = array_map(
            array($this, 'prepareFacetForTemplate'),
            $facetsArray
        );

        $activeFilters = array();
        foreach ($facetsVar as $facet) {
            foreach ($facet['filters'] as $filter) {
                if ($filter['active']) {
                    $activeFilters[] = $filter;
                }
            }
        }

        return $this->render('catalog/_partials/facets', array(
            'facets' => $facetsVar,
            'js_enabled' => $this->ajax,
            'activeFilters' => $activeFilters,
            'sort_order' => $result->getCurrentSortOrder()->toString(),
            'clear_all_link' => $this->updateQueryString(array('q' => null, 'page' => null))
        ));
    }

    public function getFacetsArray($facetCollection) {

    	$facets = $facetCollection->getFacets();
        $array_facets = array();
        $array_filters = array();

        foreach($facets as $facet_position => $facet) {
        	$facetsArray = $facet->toArray();
        	$array_facets[$facet_position] = $facetsArray['label'];
        	$array_filters[$facet_position] = array();
        	foreach($facetsArray['filters'] as $filter_position => $filter)
        	{
        		$array_filters[$facet_position][$filter_position] = $filter['label'];
        	}
        }
        $facetsArray = array();
        $facetsArraynames = array();
        $price = '';
        // $weight = '';
        $name_of_price = '';
        // $name_of_weight = '';
        $count_p = 0;
        $count_w = 0;
        foreach($facets as $facet_position => $facet) {
        	$facetsArray[$facet_position] = $facet->toArray();
        	foreach($facetsArray[$facet_position]['filters'] as $filter_position => $filter) {
        		$tmp_facets = explode('/',$filter['nextEncodedFacets']);
            	$new_tmp_facets = array();
            	foreach($tmp_facets as $q => $tmp_facet)
            	{
            		$tmp_filters = explode('-',$tmp_facet);
            		$new_tmp_filters = array();
            		foreach($tmp_filters as $key => $tmp_filter)
            		{
            			if ($key > 0)
            			{
            				if($tmp_filters[0] == $this->trans('Price', array(), 'Modules.Facetedsearch.Shop') 
            					) {
            					$new_tmp_filters[] = $tmp_filters[1].'f'.$tmp_filters[2].'t'.$tmp_filters[3];
            					if ($tmp_filters[0] == $this->trans('Price', array(), 'Modules.Facetedsearch.Shop') && $count_p == 0 && strpos(Tools::getValue('q'), $this->getFacetsRewriteByName($this->trans('Price', array(), 'Modules.Facetedsearch.Shop'))) !== false) {
            						$price = $tmp_filters[1].'f'.$tmp_filters[2].'t'.$tmp_filters[3];
                                    Media::addJsDef(array('rangeFrom_p' => $tmp_filters[2]));
                                    Media::addJsDef(array('rangeTo_p' => $tmp_filters[3]));
            						$name_of_price = $tmp_filters[0];
            						$count_p++;
            					}
                 //                 elseif ($tmp_filters[0] == $this->trans('Weight', array(), 'Modules.Facetedsearch.Shop') && $count_w == 0 && strpos(Tools::getValue('q'), $this->getFacetsRewriteByName($this->trans('Weight', array(), 'Modules.Facetedsearch.Shop'))) !== false) {
            					// 	// $weight = $tmp_filters[1].'f'.$tmp_filters[2].'t'.$tmp_filters[3];
                 //                    $weight = $tmp_filters[1];
                 //                    Media::addJsDef(array('rangeFrom_w' => $tmp_filters[2]));
                 //                    Media::addJsDef(array('rangeTo_w' => $tmp_filters[3]));
            					// 	$name_of_weight = $tmp_filters[0];
            					// 	$count_w++;
            					// }
            					break;
            				} else {
            					$new_tmp_filters[array_search($tmp_filter,$array_filters[array_search($tmp_filters[0],$array_facets)])] = $tmp_filter;
            				}
            			}
            		}
            		ksort($new_tmp_filters);
            		array_unshift($new_tmp_filters,$tmp_filters[0]);
            		$new_tmp_facets[array_search($tmp_filters[0],$array_facets)] = implode('-',$new_tmp_filters);
            	}
            	if ($count_p > 0) {
            		$new_tmp_facets[array_search($name_of_price,$array_facets)] = $name_of_price.'-'.$price;
            	// } elseif ($count_w > 0) {
            	// 	$new_tmp_facets[array_search($name_of_weight,$array_facets)] = $name_of_weight.'-'.$weight;
            	}
            	
            	ksort($new_tmp_facets);

         		$facetsArray[$facet_position]['filters'][$filter_position]['nextEncodedFacets'] = implode('/',$new_tmp_facets);
         		$facetsArraynames[] = implode('/',$new_tmp_facets);
        	}
        }
        if (Tools::getValue('q'))
		{

			$tmp_facets_rewrite = explode('/',Tools::getValue('q'));
            if (count($tmp_facets_rewrite) == 0 || (count($tmp_facets_rewrite) == 1 && $tmp_facets_rewrite[0] == "-")) {
                header("HTTP/1.1 301 Moved Permanently");
                header("Location: ".$this->context->link->getCategoryLink(Tools::getValue('id_category')));
            }
			$nextEncodedFacets =  array();
    		foreach($tmp_facets_rewrite as $tmp_facet_rewrite)
    		{
    			$tmp_filters_rewrite = explode('-',$tmp_facet_rewrite);
            	$nextEncodedFilters = [];
            	foreach($tmp_filters_rewrite as $key => $tmp_filter_rewrite)
    			{
	    			if ($key == 0) {
	        			$nextEncodedFilters[] = $this->getFacetsNameByRewrite(str_replace('+', ' ', $tmp_filter_rewrite));
	        		} else {
	        			if ($tmp_filters_rewrite[0] == $this->getFacetsRewriteByName($this->trans('Categories', array(), 'Modules.Facetedsearch.Shop'))) {
                            $tmp_filter_rewrite = str_replace(".", "-", $tmp_filter_rewrite);
	        				$nextEncodedFilters[] = Db::getInstance()->getValue('SELECT cl.`name`
																				FROM `'._DB_PREFIX_.'category_lang` cl
																				WHERE `id_lang` = '.(int) $this->context->language->id.'
																				'.Shop::addSqlRestrictionOnLang('cl').'
																				AND cl.`link_rewrite` = "'.pSQL($tmp_filter_rewrite).'"');
	        			} elseif ($tmp_filters_rewrite[0] == $this->getFacetsRewriteByName($this->trans('Price', array(), 'Modules.Facetedsearch.Shop'))) {
	        				$nextEncodedFilters[] = $tmp_filter_rewrite;
	        				break;
	        			} else {
	        				$nextEncodedFilters[] = $this->getFiltersNameByRewrite(str_replace('+', ' ', $tmp_filter_rewrite));
	        			}
	        		}
	        	}
	        	$nextEncodedFacets[] = implode('-',$nextEncodedFilters);
    		}
    		$current_name = implode('/',$nextEncodedFacets);
    		$flag = 0;

            if (strpos($current_name, $this->trans('Price', array(), 'Modules.Facetedsearch.Shop')) !== false) {
                $checked_current_name = preg_replace('/'.$this->trans('Price', array(), 'Modules.Facetedsearch.Shop').'-(.*?)\//is', $this->trans('Price', array(), 'Modules.Facetedsearch.Shop').'-/', $current_name);  
            } else {
                $checked_current_name = $current_name;
            }
            
            foreach($facetsArraynames as $facetsArrayname)
            {
                    if (strpos($current_name, $this->trans('Price', array(), 'Modules.Facetedsearch.Shop')) !== false) {
                        $checked_facetsArrayname = preg_replace('/'.$this->trans('Price', array(), 'Modules.Facetedsearch.Shop').'-(.*?)\//is', $this->trans('Price', array(), 'Modules.Facetedsearch.Shop').'-/', $facetsArrayname);  
                    } else {
                        $checked_facetsArrayname = $facetsArrayname;
                    }
                    
              if (strpos($checked_facetsArrayname,$checked_current_name) !== false) {
                $flag = 1;
                break;
              }
            }

			if ($flag == 0) {
				$redirect_facets = explode('/',$current_name);
            	$new_redirect_facets = array();
            	foreach($redirect_facets as $redirect_facet)
            	{
            		$redirect_filters = explode('-',$redirect_facet);
            		$new_redirect_filters = array();
            		foreach($redirect_filters as $key => $redirect_filter)
            		{
            			if ($key > 0)
            			{
            				if ($redirect_filters[0] == $this->trans('Categories', array(), 'Modules.Facetedsearch.Shop')) {
	            				$new_redirect_filters[array_search($redirect_filter,$array_filters[array_search($redirect_filters[0],$array_facets)])] = str_replace("-", ".", Db::getInstance()->getValue('SELECT cl.`link_rewrite`
																					FROM `'._DB_PREFIX_.'category_lang` cl
																					WHERE `id_lang` = '.(int) $this->context->language->id.'
																					'.Shop::addSqlRestrictionOnLang('cl').'
																					AND cl.`name` = "'.pSQL($redirect_filter).'"'));
	            			} elseif ($redirect_filters[0] == $this->trans('Price', array(), 'Modules.Facetedsearch.Shop')) {
	            				$new_redirect_filters[array_search($redirect_filter,$array_filters[array_search($redirect_filters[0],$array_facets)])] = $redirect_filter;
	            				break;
	            			} else {
            					$new_redirect_filters[array_search($redirect_filter,$array_filters[array_search($redirect_filters[0],$array_facets)])] = $this->getFiltersRewriteByName($redirect_filter);
            				}
            			}
            		}
            		ksort($new_redirect_filters);
            		array_unshift($new_redirect_filters,$this->getFacetsRewriteByName($redirect_filters[0]));
            		$new_redirect_facets[array_search($redirect_filters[0],$array_facets)] = implode('-',str_replace(' ', '+', $new_redirect_filters));
            	}
            	ksort($new_redirect_facets);
                $flag_n = 0;
                foreach($facetsArraynames as $facetsArrayname)
                {
                    if (strpos($facetsArrayname,$new_redirect_facets) !== false) {
                        $flag_n = 1;
                        break;
                    }
                }
                header("HTTP/1.1 301 Moved Permanently");
                if ($flag_n == 1) {
                    header("Location: ".$this->context->link->getCategoryLink(Tools::getValue('id_category')).'/q/'.implode('/',$new_redirect_facets));
                } else {
                    header("Location: ".$this->context->link->getPageLink('pagenotfound'));
                }
                exit;
			}
		}
        return $facetsArray;
    }

    /**
     * Renders an array of active filters.
     *
     * @param array $facets
     *
     * @return string the HTML of the facets
     */
    protected function renderActiveFilters(ProductSearchResult $result)
    {
        $facetCollection = $result->getFacetCollection();
        // not all search providers generate menus
        if (empty($facetCollection)) {
            return '';
        }

        $facetsArray = $this->getFacetsArray($facetCollection);

        $facetsVar = array_map(
            array($this, 'prepareFacetForTemplate'),
            $facetsArray
        );

        $activeFilters = array();
        foreach ($facetsVar as $facet) {
            foreach ($facet['filters'] as $filter) {
                if ($filter['active']) {
                    $activeFilters[] = $filter;
                }
            }
        }

        return $this->render('catalog/_partials/active_filters', array(
            'activeFilters' => $activeFilters,
            'clear_all_link' => $this->updateQueryString(array('q' => null, 'page' => null))
        ));
    }

    /**
     * This returns all template variables needed for rendering
     * the product list, the facets, the pagination and the sort orders.
     *
     * @return array variables ready for templating
     */
    protected function getProductSearchVariables()
    {
        /*
         * To render the page we need to find something (a ProductSearchProviderInterface)
         * that knows how to query products.
         */

        // the search provider will need a context (language, shop...) to do its job
        $context = $this->getProductSearchContext();

        // the controller generates the query...
        $query = $this->getProductSearchQuery();

        // ...modules decide if they can handle it (first one that can is used)
        $provider = $this->getProductSearchProviderFromModules($query);

        // if no module wants to do the query, then the core feature is used
        if (null === $provider) {
            $provider = $this->getDefaultProductSearchProvider();
        }

        $resultsPerPage = (int) Tools::getValue('resultsPerPage');
        if ($resultsPerPage <= 0 || $resultsPerPage > 36) {
            $resultsPerPage = Configuration::get('PS_PRODUCTS_PER_PAGE');
        }

        // we need to set a few parameters from back-end preferences
        $query
            ->setResultsPerPage($resultsPerPage)
            ->setPage(max((int) Tools::getValue('page'), 1))
        ;

        // set the sort order if provided in the URL
        if (($encodedSortOrder = Tools::getValue('order'))) {
            $query->setSortOrder(SortOrder::newFromString(
                $encodedSortOrder
            ));
        } else {
        	$query->setSortOrder(SortOrder::newFromString(
                'product.price.desc'
            ));
        }

        // get the parameters containing the encoded facets from the URL
        $encodedFacets = Tools::getValue('q');

        $encodedFacets = str_replace('+', ' ', $encodedFacets);

        if($encodedFacets) {
        	$tmp_facets = explode('/',$encodedFacets);
	    	$nextEncodedFacets = [];
	    	foreach($tmp_facets as $tmp_facet)
	    	{
	    		$tmp_filters = explode('-',$tmp_facet);
	    		$nextEncodedFilters = [];
	    		foreach($tmp_filters as $key => $tmp_filter)
	    		{
	        		if ($key == 0) {
	        			$nextEncodedFilters[] = $this->getFacetsNameByRewrite($tmp_filter);
	        		} else {
	        			if ($tmp_filters[0] == $this->getFacetsRewriteByName($this->trans('Categories', array(), 'Modules.Facetedsearch.Shop'))) {
                            $tmp_filter = str_replace(".", "-", $tmp_filter);
	        				$nextEncodedFilters[] = Db::getInstance()->getValue('SELECT cl.`name`
																				FROM `'._DB_PREFIX_.'category_lang` cl
																				WHERE `id_lang` = '.(int) $this->context->language->id.'
																				'.Shop::addSqlRestrictionOnLang('cl').'
																				AND cl.`link_rewrite` = "'.pSQL($tmp_filter).'"');
	        			} elseif ($tmp_filters[0] == $this->getFacetsRewriteByName($this->trans('Price', array(), 'Modules.Facetedsearch.Shop'))) {
            				$new_tmp_filter = str_replace('f','-',$tmp_filter);
            				$new_tmp_filter = str_replace('t','-',$new_tmp_filter);
            				$nextEncodedFilters[] = $new_tmp_filter;
            				break;
            			} else {
	        				$nextEncodedFilters[] = $this->getFiltersNameByRewrite($tmp_filter);
	        			}
	        		}
	        	}
	        	$nextEncodedFacets[] = implode('-',$nextEncodedFilters);
	    	}
	    	$encodedFacets = implode('/',$nextEncodedFacets);
        }
        /*
         * The controller is agnostic of facets.
         * It's up to the search module to use /define them.
         *
         * Facets are encoded in the "q" URL parameter, which is passed
         * to the search provider through the query's "$encodedFacets" property.
         */

        $query->setEncodedFacets($encodedFacets);

        // We're ready to run the actual query!

        $result = $provider->runQuery(
            $context,
            $query
        );

        // sort order is useful for template,
        // add it if undefined - it should be the same one
        // as for the query anyway
        if (!$result->getCurrentSortOrder()) {
            $result->setCurrentSortOrder($query->getSortOrder());
        }

        $r = $result->getProducts();
        if (count($r[0]) < 2) {
            if (strrpos($_SERVER['REQUEST_URI'], "page=") !== false) {
                $re = '/\?page=[0-9]+$/m';
                $subst = '';
                $redirect = preg_replace($re, $subst, $_SERVER['REQUEST_URI']);
                $re = '/page=[0-9]+&/m';
                $redirect = preg_replace($re, $subst, $redirect);
                $re = '/&page=[0-9]+/m';
                $redirect = preg_replace($re, $subst, $redirect);
                $url = Tools::getShopProtocol() . Tools::getShopDomain() . $redirect;
                header("HTTP/1.1 301 Moved Permanently");
                header("Location: " . $url);
                exit;
            }
        }

        // prepare the products
        $products = $this->prepareMultipleProductsForTemplate(
            $r
        );

        // render the facets
        if ($provider instanceof FacetsRendererInterface) {
            // with the provider if it wants to
            $rendered_facets = $provider->renderFacets(
                $context,
                $result
            );
            $rendered_active_filters = $provider->renderActiveFilters(
                $context,
                $result
            );
        } else {
            // with the core
            $rendered_facets = $this->renderFacets(
                $result
            );
            $rendered_active_filters = $this->renderActiveFilters(
                $result
            );
        }
        
        $pagination = $this->getTemplateVarPagination(
            $query,
            $result
        );
        
        // prepare the sort orders
        // note that, again, the product controller is sort-orders
        // agnostic
        // a module can easily add specific sort orders that it needs
        // to support (e.g. sort by "energy efficiency")
        $sort_orders = $this->getTemplateVarSortOrders(
            $result->getAvailableSortOrders(),
            $query->getSortOrder()->toString()
        );

        $sort_selected = false;
        if (!empty($sort_orders)) {
            foreach ($sort_orders as $order) {
                if (isset($order['current']) && true === $order['current']) {
                    $sort_selected = $order['label'];
                    break;
                }
            }
            $current_url = Tools::getCurrentUrlProtocolPrefix().$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            $ultra_str = $current_url;
            if (strpos($current_url, 'order') !== false) {
                $new_order_del = explode('order', $current_url);
                if (strpos($new_order_del[1], '&') !== false) {
                    $supa_new = stristr($new_order_del[1], '&');
                    $supa_new = substr( $supa_new, 1);
                    $ultra_str = $new_order_del[0] . $supa_new;
                } else {
                    $supa_new = substr( $new_order_del[0], 0, -1);
                    $ultra_str = $supa_new;
                }
                
            }
            foreach ($sort_orders as $pos => $order) {
                if (strpos($current_url, '?') === false || strpos($current_url, '?order') !== false && strpos($ultra_str, '?') === false) {
                    $sort_orders[$pos]['url'] = $ultra_str . '?order=' . $order['urlParameter'];
                } else {
                    $sort_orders[$pos]['url'] = $ultra_str . '&order=' . $order['urlParameter'];
                }
            }
        }
        $query = $result->getEncodedFacets();
        $facets = explode('/', $query);

        $true_facets = array();
        foreach ($facets as &$facet) {
            $re = '/-₴-([0-9]+)-([0-9]+)/m';
            $subst = '-₴f$1t$2';
            $facet = preg_replace($re, $subst, $facet);
            $filters = explode('-', $facet);
            $true_filter = array();
            foreach ($filters as $key => $filter) {
                if ($key == 0) {
                    $true_filter[] = $this->getFacetsRewriteByName($filter);
                } else {
                    if ($filters[0] == $this->trans('Categories', array(), 'Modules.Facetedsearch.Shop')) {
                        $true_filter[] = str_replace("-", ".", Db::getInstance()->getValue('SELECT cl.`link_rewrite`
                                                                            FROM `'._DB_PREFIX_.'category_lang` cl
                                                                            WHERE `id_lang` = '.(int) $this->context->language->id.'
                                                                            '.Shop::addSqlRestrictionOnLang('cl').'
                                                                            AND cl.`name` = "'.pSQL($filter).'"'));
                    } elseif ($filters[0] == $this->trans('Price', array(), 'Modules.Facetedsearch.Shop')) {
                        $true_filter[] = $filter;
                        break;
                    } else {
                        $true_filter[] = $this->getFiltersRewriteByName($filter);
                    }
                }
            }
            $true_facets[] = implode('-', $true_filter);
        }
        $result_url = implode('/', $true_facets);

        $searchVariables = array(
            'label' => $this->getListingLabel(),
            'products' => $products,
            'sort_orders' => $sort_orders,
            'sort_selected' => $sort_selected,
            'pagination' => $pagination,
            'rendered_facets' => $rendered_facets,
            'rendered_active_filters' => $rendered_active_filters,
            'js_enabled' => $this->ajax,
            'current_url' => $this->updateQueryString(array(
                'q' => $result_url,
            )),
        );

        Hook::exec('filterProductSearch', array('searchVariables' => &$searchVariables));
        Hook::exec('actionProductSearchAfter', $searchVariables);

        return $searchVariables;
    }
}
