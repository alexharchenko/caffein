<?php

class Ps_FacetedsearchOverride extends Ps_Facetedsearch
{
    const KL_CONST_FOR_COUNT = 9999999; // Need const cause presta closed zero value of filters see modules/ps_facetedsearch/src/Ps_FacetedsearchProductSearchProvider.php:181
    // if u change this value change this in themes/kl_clix/templates/catalog/_partials/facets.tpl:137,149,164

/*
    public function __construct()
    {
        $this->installProductFacetsRewrite();
        $this->installProductFiltersRewrite();
        echo  'allok';
        exit;
    }
*/

    public function install()
    {
        if(parent::install()) {
            $this->installProductFacetsRewrite();
            $this->installProductFiltersRewrite();
        }
    }



    public function uninstall()
    {
        Db::getInstance()->execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'layered_product_facets_rewrite');
        Db::getInstance()->execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'layered_product_filters_rewrite');
        parent::uninstall();
    }

    public function installProductFacetsRewrite()
    {
        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'layered_product_facets_rewrite`');
        Db::getInstance()->execute('
        CREATE TABLE `'._DB_PREFIX_.'layered_product_facets_rewrite` (
        `name` varchar(150) NOT NULL,
        `rewrite` varchar(150) NOT NULL,
        PRIMARY KEY (`name`)
        ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');
    }

    public function installProductFiltersRewrite()
    {
        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'layered_product_filters_rewrite`');
        Db::getInstance()->execute('
        CREATE TABLE `'._DB_PREFIX_.'layered_product_filters_rewrite` (
        `name` varchar(150) NOT NULL,
        `rewrite` varchar(150) NOT NULL,
        PRIMARY KEY (`name`)
        ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');
    }

    public function getContent()
    {
        global $cookie;
        $message = '';
        if (Tools::isSubmit('SubmitFilter')) {
            if (!Tools::getValue('layered_tpl_name')) {
                $message = $this->displayError($this->trans('Filter template name required (cannot be empty)', array(), 'Modules.Facetedsearch.Admin'));
            } elseif (!Tools::getValue('categoryBox')) {
                $message = $this->displayError($this->trans('You must select at least one category.', array(), 'Modules.Facetedsearch.Admin'));
            } else {
                if (Tools::getValue('id_layered_filter')) {
                    Db::getInstance()->execute('
                        DELETE FROM '._DB_PREFIX_.'layered_filter
                        WHERE id_layered_filter = '.(int) Tools::getValue('id_layered_filter')
                    );
                    $this->buildLayeredCategories();
                }

                if (Tools::getValue('scope') == 1) {
                    Db::getInstance()->execute('TRUNCATE TABLE '._DB_PREFIX_.'layered_filter');
                    $categories = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
                        SELECT id_category
                        FROM '._DB_PREFIX_.'category'
                    );

                    foreach ($categories as $category) {
                        $_POST['categoryBox'][] = (int) $category['id_category'];
                    }
                }

                $id_layered_filter = (int) Tools::getValue('id_layered_filter');

                if (!$id_layered_filter) {
                    $id_layered_filter = (int) Db::getInstance()->Insert_ID();
                }

                $shop_list = array();

                if (isset($_POST['checkBoxShopAsso_layered_filter'])) {
                    foreach ($_POST['checkBoxShopAsso_layered_filter'] as $id_shop => $row) {
                        $assos[] = array('id_object' => (int) $id_layered_filter, 'id_shop' => (int) $id_shop);
                        $shop_list[] = (int) $id_shop;
                    }
                } else {
                    $shop_list = array(Context::getContext()->shop->id);
                }

                Db::getInstance()->execute('
                    DELETE FROM '._DB_PREFIX_.'layered_filter_shop
                    WHERE `id_layered_filter` = '.(int) $id_layered_filter
                );

                if (count($_POST['categoryBox'])) {
                    /* Clean categoryBox before use */
                    if (isset($_POST['categoryBox']) && is_array($_POST['categoryBox'])) {
                        foreach ($_POST['categoryBox'] as &$category_box_tmp) {
                            $category_box_tmp = (int) $category_box_tmp;
                        }
                    }

                    $filter_values = array();

                    foreach ($_POST['categoryBox'] as $idc) {
                        $filter_values['categories'][] = (int) $idc;
                    }

                    $filter_values['shop_list'] = $shop_list;
                    $values = false;

                    foreach ($_POST['categoryBox'] as $id_category_layered) {
                        foreach ($_POST as $key => $value) {
                            if (substr($key, 0, 17) == 'layered_selection' && $value == 'on') {
                                $values = true;
                                $type = 0;
                                $limit = 0;

                                if (Tools::getValue($key.'_filter_type')) {
                                    $type = Tools::getValue($key.'_filter_type');
                                }
                                if (Tools::getValue($key.'_filter_show_limit')) {
                                    $limit = Tools::getValue($key.'_filter_show_limit');
                                }

                                $filter_values[$key] = array(
                                    'filter_type' => (int) $type,
                                    'filter_show_limit' => (int) $limit,
                                );
                            }
                        }
                    }

                    $values_to_insert = array(
                        'name' => pSQL(Tools::getValue('layered_tpl_name')),
                        'filters' => pSQL(serialize($filter_values)),
                        'n_categories' => (int) count($filter_values['categories']),
                        'date_add' => date('Y-m-d H:i:s'), );

                    if (isset($_POST['id_layered_filter']) && $_POST['id_layered_filter']) {
                        $values_to_insert['id_layered_filter'] = (int) Tools::getValue('id_layered_filter');
                    }

                    $id_layered_filter = isset($values_to_insert['id_layered_filter']) ? (int) $values_to_insert['id_layered_filter'] : 'NULL';
                    $sql = 'INSERT INTO '._DB_PREFIX_.'layered_filter (name, filters, n_categories, date_add, id_layered_filter) VALUES ("'.pSQL($values_to_insert['name']).'", "'.$values_to_insert['filters'].'",'.(int) $values_to_insert['n_categories'].',"'.pSQL($values_to_insert['date_add']).'",'.$id_layered_filter.')';
                    Db::getInstance()->execute($sql);
                    $id_layered_filter = (int) Db::getInstance()->Insert_ID();

                    if (isset($assos)) {
                        foreach ($assos as $asso) {
                            Db::getInstance()->execute('
                            INSERT INTO '._DB_PREFIX_.'layered_filter_shop (`id_layered_filter`, `id_shop`)
                            VALUES('.$id_layered_filter.', '.(int) $asso['id_shop'].')'
                        );
                        }
                    }

                    $this->buildLayeredCategories();
                    $message = $this->displayConfirmation($this->trans('Your filter', array(), 'Modules.Facetedsearch.Admin').' "'.Tools::safeOutput(Tools::getValue('layered_tpl_name')).'" '.
                        ((isset($_POST['id_layered_filter']) && $_POST['id_layered_filter']) ? $this->trans('was updated successfully.', array(), 'Modules.Facetedsearch.Admin') : $this->trans('was added successfully.', array(), 'Modules.Facetedsearch.Admin')));
                }
                if (Tools::getValue('layered_product_facets')) {

                    foreach (Tools::getValue('layered_product_facets') as $name => $rewrite)
                    {
                        $sql='INSERT INTO '._DB_PREFIX_.'layered_product_facets_rewrite (name,rewrite) VALUES ("'.$name.'","'.$rewrite.'")
                            ON DUPLICATE KEY UPDATE name=VALUES(name), rewrite=VALUES(rewrite)';

                        Db::getInstance()->execute($sql);
                    }
                }
                if (Tools::getValue('layered_product_filters')) {

                    foreach (Tools::getValue('layered_product_filters') as $name => $rewrite)
                    {
                        $sql='INSERT INTO '._DB_PREFIX_.'layered_product_filters_rewrite (name,rewrite) VALUES ("'.$name.'","'.$rewrite.'")
                            ON DUPLICATE KEY UPDATE name=VALUES(name), rewrite=VALUES(rewrite)';

                        Db::getInstance()->execute($sql);
                    }
                }
            }
        } elseif (Tools::isSubmit('submitLayeredSettings')) {
            Configuration::updateValue('PS_LAYERED_SHOW_QTIES', (int) Tools::getValue('ps_layered_show_qties'));
            Configuration::updateValue('PS_LAYERED_FULL_TREE', (int) Tools::getValue('ps_layered_full_tree'));
            Configuration::updateValue('PS_LAYERED_FILTER_PRICE_USETAX', (int) Tools::getValue('ps_layered_filter_price_usetax'));
            Configuration::updateValue('PS_LAYERED_FILTER_CATEGORY_DEPTH', (int) Tools::getValue('ps_layered_filter_category_depth'));
            Configuration::updateValue('PS_LAYERED_FILTER_PRICE_ROUNDING', (int) Tools::getValue('ps_layered_filter_price_rounding'));

            $this->ps_layered_full_tree = (int) Tools::getValue('ps_layered_full_tree');

            if (version_compare(_PS_VERSION_, '1.6.0', '>=') === true) {
                $message = '<div class="alert alert-success">'.$this->trans('Settings saved successfully', array(), 'Modules.Facetedsearch.Admin').'</div>';
            } else {
                $message = '<div class="conf">'.$this->trans('Settings saved successfully', array(), 'Modules.Facetedsearch.Admin').'</div>';
            }
        } elseif (Tools::getValue('deleteFilterTemplate')) {
            $layered_values = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
                SELECT filters
                FROM '._DB_PREFIX_.'layered_filter
                WHERE id_layered_filter = '.(int) Tools::getValue('id_layered_filter')
            );

            if ($layered_values) {
                Db::getInstance()->execute('
                    DELETE FROM '._DB_PREFIX_.'layered_filter
                    WHERE id_layered_filter = '.(int) Tools::getValue('id_layered_filter').' LIMIT 1'
                );
                $this->buildLayeredCategories();
                $message = $this->displayConfirmation($this->trans('Filter template deleted, categories updated (reverted to default Filter template).', array(), 'Modules.Facetedsearch.Admin'));
            } else {
                $message = $this->displayError($this->trans('Filter template not found', array(), 'Modules.Facetedsearch.Admin'));
            }
        }

        $category_box = array();
        $attribute_groups = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
            SELECT ag.id_attribute_group, ag.is_color_group, agl.name, COUNT(DISTINCT(a.id_attribute)) n
            FROM '._DB_PREFIX_.'attribute_group ag
            LEFT JOIN '._DB_PREFIX_.'attribute_group_lang agl ON (agl.id_attribute_group = ag.id_attribute_group)
            LEFT JOIN '._DB_PREFIX_.'attribute a ON (a.id_attribute_group = ag.id_attribute_group)
            WHERE agl.id_lang = '.(int) $cookie->id_lang.'
            GROUP BY ag.id_attribute_group'
        );

        $attribute_groups_names = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
            SELECT a.id_attribute_group, a.id_attribute, al.name
            FROM '._DB_PREFIX_.'attribute a
            LEFT JOIN '._DB_PREFIX_.'attribute_lang al ON (a.id_attribute = al.id_attribute)
            WHERE al.id_lang = '.(int) $cookie->id_lang
        );

        $features = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
            SELECT fl.id_feature, fl.name, COUNT(DISTINCT(fv.id_feature_value)) n
            FROM '._DB_PREFIX_.'feature_lang fl
            LEFT JOIN '._DB_PREFIX_.'feature_value fv ON (fv.id_feature = fl.id_feature)
            WHERE (fv.custom IS NULL OR fv.custom = 0) AND fl.id_lang = '.(int) $cookie->id_lang.'
            GROUP BY fl.id_feature'
        );

        $features_names = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
            SELECT f.id_feature, fl.value, f.id_feature_value
            FROM '._DB_PREFIX_.'feature_value f
            LEFT JOIN '._DB_PREFIX_.'feature_value_lang fl ON (fl.id_feature_value = f.id_feature_value)
            WHERE fl.id_lang = '.(int) $cookie->id_lang
        );

        $brands_names = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
            SELECT id_manufacturer, name
            FROM '._DB_PREFIX_.'manufacturer'
        );

        $product_facets = $this->getFacetsRewrite();
        $product_filters = $this->getFiltersRewrite();
        $layered_product_facets_rewrite = [];
        $layered_product_filters_rewrite = [];
        foreach($product_facets as $product_facet)
        {
            $layered_product_facets_rewrite[$product_facet['name']] = $product_facet['rewrite'];
        }

        foreach($product_filters as $product_filter)
        {
            $layered_product_filters_rewrite[$product_filter['name']] = $product_filter['rewrite'];
        }
        if (Shop::isFeatureActive() && count(Shop::getShops(true, null, true)) > 1) {
            $helper = new HelperForm();
            $helper->id = Tools::getValue('id_layered_filter', null);
            $helper->table = 'layered_filter';
            $helper->identifier = 'id_layered_filter';
            $this->context->smarty->assign('asso_shops', $helper->renderAssoShop());
        }

        if (version_compare(_PS_VERSION_, '1.6.0', '>=') === true) {
            $tree_categories_helper = new HelperTreeCategories('categories-treeview');
            $tree_categories_helper->setRootCategory((Shop::getContext() == Shop::CONTEXT_SHOP ? Category::getRootCategory()->id_category : 0))
                ->setUseCheckBox(true);
        } else {
            if (Shop::getContext() == Shop::CONTEXT_SHOP) {
                $root_category = Category::getRootCategory();
                $root_category = array('id_category' => $root_category->id_category, 'name' => $root_category->name);
            } else {
                $root_category = array('id_category' => '0', 'name' => $this->trans('Root', array(), 'Modules.Facetedsearch.Admin'));
            }

            $tree_categories_helper = new Helper();
        }

        $module_url = Tools::getProtocol(Tools::usingSecureMode()).$_SERVER['HTTP_HOST'].$this->getPathUri();

        if (method_exists($this->context->controller, 'addJquery')) {
            $this->context->controller->addJS('/override'.$this->_path.'js/ps_facetedsearchadmin.js');
            if (version_compare(_PS_VERSION_, '1.6.0.3', '>=') === true) {
                $this->context->controller->addjqueryPlugin('sortable');
            } elseif (version_compare(_PS_VERSION_, '1.6.0', '>=') === true) {
                $this->context->controller->addJS(_PS_JS_DIR_.'jquery/plugins/jquery.sortable.js');
            } else {
                $this->context->controller->addJS($this->_path.'js/jquery.sortable.js');
            }
        }

        $this->context->controller->addCSS($this->_path.'css/ps_facetedsearch_admin.css');

        if (Tools::getValue('add_new_filters_template')) {
            $this->context->smarty->assign(array(
                'current_url' => $this->context->link->getAdminLink('AdminModules').'&configure=ps_facetedsearch&tab_module=front_office_features&module_name=ps_facetedsearch',
                'uri' => $this->getPathUri(),
                'id_layered_filter' => 0,
                'template_name' => sprintf($this->trans('My template - %s', array(), 'Modules.Facetedsearch.Admin'), date('Y-m-d')),
                'attribute_groups' => $attribute_groups,
                'attribute_groups_names' => $attribute_groups_names,
                'features' => $features,
                'layered_product_facets_rewrite' => $layered_product_facets_rewrite,
                'layered_product_filters_rewrite' => $layered_product_filters_rewrite,
                'features_names' => $features_names,
                'brands_names' => $brands_names,
                'total_filters' => 6 + count($attribute_groups) + count($features),
            ));

            if (version_compare(_PS_VERSION_, '1.6.0', '>=') === true) {
                $this->context->smarty->assign('categories_tree', $tree_categories_helper->render());
            } else {
                $this->context->smarty->assign('categories_tree', $tree_categories_helper->renderCategoryTree(
                    $root_category, array(), 'categoryBox'));
            }

            return $this->display(__FILE__, 'views/templates/admin/add.tpl');
        } elseif (Tools::getValue('edit_filters_template')) {
            $template = Db::getInstance()->getRow('
                SELECT *
                FROM `'._DB_PREFIX_.'layered_filter`
                WHERE id_layered_filter = '.(int) Tools::getValue('id_layered_filter')
            );

            $filters = Tools::unSerialize($template['filters']);

            if (version_compare(_PS_VERSION_, '1.6.0', '>=') === true) {
                $tree_categories_helper->setSelectedCategories($filters['categories']);
                $this->context->smarty->assign('categories_tree', $tree_categories_helper->render());
            } else {
                $this->context->smarty->assign('categories_tree', $tree_categories_helper->renderCategoryTree(
                    $root_category, $filters['categories'], 'categoryBox'));
            }

            $select_shops = $filters['shop_list'];
            unset($filters['categories']);
            unset($filters['shop_list']);

            $this->context->smarty->assign(array(
                'current_url' => $this->context->link->getAdminLink('AdminModules').'&configure=ps_facetedsearch&tab_module=front_office_features&module_name=ps_facetedsearch',
                'uri' => $this->getPathUri(),
                'id_layered_filter' => (int) Tools::getValue('id_layered_filter'),
                'template_name' => $template['name'],
                'attribute_groups' => $attribute_groups,
                'attribute_groups_names' => $attribute_groups_names,
                'features' => $features,
                'features_names' => $features_names,
                'brands_names' => $brands_names,
                'layered_product_facets_rewrite' => $layered_product_facets_rewrite,
                'layered_product_filters_rewrite' => $layered_product_filters_rewrite,
                'filters' => Tools::jsonEncode($filters),
                'total_filters' => 6 + count($attribute_groups) + count($features),
            ));

            return $this->display(__FILE__, 'views/templates/admin/add.tpl');
        } else {
            $this->context->smarty->assign(array(
                'message' => $message,
                'uri' => $this->getPathUri(),
                'PS_LAYERED_INDEXED' => Configuration::getGlobalValue('PS_LAYERED_INDEXED'),
                'current_url' => Tools::safeOutput(preg_replace('/&deleteFilterTemplate=[0-9]*&id_layered_filter=[0-9]*/', '', $_SERVER['REQUEST_URI'])),
                'id_lang' => Context::getContext()->cookie->id_lang,
                'token' => substr(Tools::encrypt('ps_facetedsearch/index'), 0, 10),
                'base_folder' => urlencode(_PS_ADMIN_DIR_),
                'price_indexer_url' => $module_url.'ps_facetedsearch-price-indexer.php'.'?token='.substr(Tools::encrypt('ps_facetedsearch/index'), 0, 10),
                'full_price_indexer_url' => $module_url.'ps_facetedsearch-price-indexer.php'.'?token='.substr(Tools::encrypt('ps_facetedsearch/index'), 0, 10).'&full=1',
                'attribute_indexer_url' => $module_url.'ps_facetedsearch-attribute-indexer.php'.'?token='.substr(Tools::encrypt('ps_facetedsearch/index'), 0, 10),
                'filters_templates' => Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT * FROM '._DB_PREFIX_.'layered_filter ORDER BY date_add DESC'),
                'show_quantities' => Configuration::get('PS_LAYERED_SHOW_QTIES'),
                'full_tree' => $this->ps_layered_full_tree,
                'category_depth' => Configuration::get('PS_LAYERED_FILTER_CATEGORY_DEPTH'),
                'price_use_tax' => (bool) Configuration::get('PS_LAYERED_FILTER_PRICE_USETAX'),
                'limit_warning' => $this->displayLimitPostWarning(21 + count($attribute_groups) * 3 + count($features) * 3),
                'price_use_rounding' => (bool) Configuration::get('PS_LAYERED_FILTER_PRICE_ROUNDING'),
            ));

            return $this->display(__FILE__, 'views/templates/admin/view.tpl');
        }
    }

    public function getFacetsRewrite() {
        
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
            SELECT *
            FROM '._DB_PREFIX_.'layered_product_facets_rewrite'
        );
    }

    public function getFiltersRewrite() {
        
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
            SELECT *
            FROM '._DB_PREFIX_.'layered_product_filters_rewrite'
        );
    }

    public function getFilterBlock(
        $selected_filters = array(),
        $compute_range_filters = true
    ) {
        global $cookie;

        // Remove all empty selected filters
        foreach ($selected_filters as $key => $value) {
            switch ($key) {
                case 'price':
                // case 'weight':
                    if ($value[0] === '' && $value[1] === '') {
                        unset($selected_filters[$key]);
                    }
                    break;
                default:
                    if ($value == '' || $value == array()) {
                        unset($selected_filters[$key]);
                    }
                    break;
            }
        }

        static $latest_selected_filters = null;
        static $productCache = array();
        $context = Context::getContext();

        $id_lang = $context->language->id;
        $currency = $context->currency;
        $id_shop = (int) $context->shop->id;
        $alias = 'product_shop';

        $id_parent = (int) Tools::getValue('id_category', Tools::getValue('id_category_layered', Configuration::get('PS_HOME_CATEGORY')));

        $parent = new Category((int) $id_parent, $id_lang);

        /* Get the filters for the current category */
        $filters = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
            SELECT type, id_value, filter_show_limit, filter_type FROM '._DB_PREFIX_.'layered_category
            WHERE id_category = '.(int) $id_parent.'
                AND id_shop = '.$id_shop.'
            GROUP BY `type`, id_value ORDER BY position ASC'
        );

        $catRestrictionDerivedTable = '(SELECT DISTINCT cp.id_product, p.id_manufacturer, product_shop.condition, p.weight FROM '._DB_PREFIX_.'category c
                                             STRAIGHT_JOIN '._DB_PREFIX_.'category_product cp ON (c.id_category = cp.id_category AND
                                             '.($this->ps_layered_full_tree ? 'c.nleft >= '.(int) $parent->nleft.'
                                             AND c.nright <= '.(int) $parent->nright : 'c.id_category = '.(int) $id_parent).'
                                             AND c.active = 1)
                                             STRAIGHT_JOIN '._DB_PREFIX_.'product_shop product_shop ON (product_shop.id_product = cp.id_product
                                             AND product_shop.id_shop = '.(int) $context->shop->id.')
                                             STRAIGHT_JOIN '._DB_PREFIX_.'product p ON (p.id_product=cp.id_product)
                                             WHERE product_shop.`active` = 1 AND product_shop.`visibility` IN ("both", "catalog"))';

        $filter_blocks = array();
        $filter_blocks_full = array();
        foreach ($filters as $filter) {
            $cacheKey = $filter['type'] . '-' . $filter['id_value'];
            if ($latest_selected_filters == $selected_filters && isset($productCache[$cacheKey])) {
                $products = $productCache[$cacheKey];
            } else {
                $sql_query = array('select' => '', 'from' => '', 'join' => '', 'where' => '', 'group' => '');
                switch ($filter['type']) {
                    case 'price':
                        $sql_query['select'] = 'SELECT p.`id_product`, psi.price_min, psi.price_max ';
                        // price slider is not filter dependent
                        $sql_query['from'] = '
                        FROM ' . $catRestrictionDerivedTable . ' p';
                        $sql_query['join'] = 'INNER JOIN `' . _DB_PREFIX_ . 'layered_price_index` psi
                                    ON (psi.id_product = p.id_product AND psi.id_currency = ' . (int)$context->currency->id . ' AND psi.id_shop=' . (int)$context->shop->id . ')';
                        $sql_query['where'] = 'WHERE 1';
                        break;
                    // case 'weight':
                    //     $sql_query['select'] = 'SELECT p.`id_product`, p.`weight` ';
                    //     // price slider is not filter dependent
                    //     $sql_query['from'] = '
                    //     FROM ' . $catRestrictionDerivedTable . ' p';
                    //     $sql_query['where'] = 'WHERE 1';
                    //     break;
                    case 'condition':
                        $sql_query['select'] = 'SELECT DISTINCT p.`id_product`, product_shop.`condition` ';
                        $sql_query['from'] = '
                        FROM ' . $catRestrictionDerivedTable . ' p';
                        $sql_query['where'] = 'WHERE 1';
                        $sql_query['from'] .= Shop::addSqlAssociation('product', 'p');
                        break;
                    case 'quantity':
                        $sql_query['select'] = 'SELECT DISTINCT p.`id_product`, sa.`quantity`, sa.`out_of_stock` ';

                        $sql_query['from'] = '
                        FROM ' . $catRestrictionDerivedTable . ' p';

                        $sql_query['join'] .= 'LEFT JOIN `' . _DB_PREFIX_ . 'stock_available` sa
                            ON (sa.id_product = p.id_product AND sa.id_product_attribute=0 ' . StockAvailable::addSqlShopRestriction(
                                null,
                                null,
                                'sa'
                            ) . ') ';
                        $sql_query['where'] = 'WHERE 1';
                        break;

                    case 'manufacturer':
                        $sql_query['select'] = 'SELECT COUNT(DISTINCT p.id_product) nbr, m.id_manufacturer, m.name ';
                        $sql_query['from'] = '
                        FROM ' . $catRestrictionDerivedTable . ' p
                        INNER JOIN ' . _DB_PREFIX_ . 'manufacturer m ON (m.id_manufacturer = p.id_manufacturer) ';
                        $sql_query['where'] = 'WHERE 1';
                        $sql_query['group'] = ' GROUP BY p.id_manufacturer ORDER BY m.name';
                        break;
                    case 'id_attribute_group':// attribute group
                        $sql_query['select'] = '
                        SELECT COUNT(DISTINCT lpa.id_product) nbr, lpa.id_attribute_group,
                        a.color, al.name attribute_name, agl.public_name attribute_group_name , lpa.id_attribute, ag.is_color_group,
                        liagl.url_name name_url_name, liagl.meta_title name_meta_title, lial.url_name value_url_name, lial.meta_title value_meta_title';
                        $sql_query['from'] = '
                        FROM ' . _DB_PREFIX_ . 'layered_product_attribute lpa
                        INNER JOIN ' . _DB_PREFIX_ . 'attribute a
                        ON a.id_attribute = lpa.id_attribute
                        INNER JOIN ' . _DB_PREFIX_ . 'attribute_lang al
                        ON al.id_attribute = a.id_attribute
                        AND al.id_lang = ' . (int)$id_lang . '
                        INNER JOIN ' . $catRestrictionDerivedTable . ' p
                        ON p.id_product = lpa.id_product
                        INNER JOIN ' . _DB_PREFIX_ . 'attribute_group ag
                        ON ag.id_attribute_group = lpa.id_attribute_group
                        INNER JOIN ' . _DB_PREFIX_ . 'attribute_group_lang agl
                        ON agl.id_attribute_group = lpa.id_attribute_group
                        AND agl.id_lang = ' . (int)$id_lang . '
                        LEFT JOIN ' . _DB_PREFIX_ . 'layered_indexable_attribute_group_lang_value liagl
                        ON (liagl.id_attribute_group = lpa.id_attribute_group AND liagl.id_lang = ' . (int)$id_lang . ')
                        LEFT JOIN ' . _DB_PREFIX_ . 'layered_indexable_attribute_lang_value lial
                        ON (lial.id_attribute = lpa.id_attribute AND lial.id_lang = ' . (int)$id_lang . ') ';

                        $sql_query['where'] = 'WHERE lpa.id_attribute_group = ' . (int)$filter['id_value'];
                        $sql_query['where'] .= ' AND lpa.`id_shop` = ' . (int)$context->shop->id;
                        $sql_query['group'] = '
                        GROUP BY lpa.id_attribute
                        ORDER BY ag.`position` ASC, a.`position` ASC';
                        break;

                    case 'id_feature':

                        $id_lang = (int)$id_lang;

                        $sql_query['select'] = 'SELECT fl.name feature_name, fp.id_feature, fv.id_feature_value, fvl.value,
                        COUNT(DISTINCT p.id_product) nbr,
                        lifl.url_name name_url_name, lifl.meta_title name_meta_title, lifvl.url_name value_url_name, lifvl.meta_title value_meta_title ';
                        $sql_query['from'] = '
                        FROM ' . _DB_PREFIX_ . 'feature_product fp
                        INNER JOIN ' . $catRestrictionDerivedTable . ' p
                        ON p.id_product = fp.id_product
                        LEFT JOIN ' . _DB_PREFIX_ . 'feature_lang fl ON (fl.id_feature = fp.id_feature AND fl.id_lang = ' . $id_lang . ')
                        INNER JOIN ' . _DB_PREFIX_ . 'feature_value fv ON (fv.id_feature_value = fp.id_feature_value AND (fv.custom IS NULL OR fv.custom = 0))
                        LEFT JOIN ' . _DB_PREFIX_ . 'feature_value_lang fvl ON (fvl.id_feature_value = fp.id_feature_value AND fvl.id_lang = ' . $id_lang . ')
                        LEFT JOIN ' . _DB_PREFIX_ . 'layered_indexable_feature_lang_value lifl
                        ON (lifl.id_feature = fp.id_feature AND lifl.id_lang = ' . $id_lang . ')
                        LEFT JOIN ' . _DB_PREFIX_ . 'layered_indexable_feature_value_lang_value lifvl
                        ON (lifvl.id_feature_value = fp.id_feature_value AND lifvl.id_lang = ' . $id_lang . ') ';
                        $sql_query['where'] = 'WHERE fp.id_feature = ' . (int)$filter['id_value'];
                        $sql_query['group'] = 'GROUP BY fv.id_feature_value ';
                        break;

                    case 'category':
                        if (Group::isFeatureActive()) {
                            $this->user_groups = ($this->context->customer->isLogged() ? $this->context->customer->getGroups() : array(
                                Configuration::get(
                                    'PS_UNIDENTIFIED_GROUP'
                                )
                            ));
                        }

                        $depth = Configuration::get('PS_LAYERED_FILTER_CATEGORY_DEPTH');
                        if ($depth === false) {
                            $depth = 1;
                        }

                        $sql_query['select'] = '
                        SELECT c.id_category, c.id_parent, cl.name, (SELECT count(DISTINCT p.id_product) # ';
                        $sql_query['from'] = '
                        FROM ' . _DB_PREFIX_ . 'category_product cp
                        LEFT JOIN ' . _DB_PREFIX_ . 'product p ON (p.id_product = cp.id_product) ';
                        $sql_query['where'] = '
                        WHERE cp.id_category = c.id_category
                        AND ' . $alias . '.active = 1 AND ' . $alias . '.`visibility` IN ("both", "catalog")';
                        $sql_query['group'] = ') count_products
                        FROM ' . _DB_PREFIX_ . 'category c
                        LEFT JOIN ' . _DB_PREFIX_ . 'category_lang cl ON (cl.id_category = c.id_category AND cl.`id_shop` = ' . (int)Context::getContext()->shop->id . ' and cl.id_lang = ' . (int)$id_lang . ') ';

                        if (Group::isFeatureActive()) {
                            $sql_query['group'] .= 'RIGHT JOIN ' . _DB_PREFIX_ . 'category_group cg ON (cg.id_category = c.id_category AND cg.`id_group` IN (' . implode(
                                    ', ',
                                    $this->user_groups
                                ) . ')) ';
                        }

                        $sql_query['group'] .= 'WHERE c.nleft > ' . (int)$parent->nleft . '
                        AND c.nright < ' . (int)$parent->nright . '
                        ' . ($depth ? 'AND c.level_depth <= ' . ($parent->level_depth + (int)$depth) : '') . '
                        AND c.active = 1
                        GROUP BY c.id_category ORDER BY c.nleft, c.position';

                        $sql_query['from'] .= Shop::addSqlAssociation('product', 'p');
                }

                /*
                 * Loop over the filters again to add their restricting clauses to the sql
                 * query being built.
                 */
                $sql_query_full_all = $sql_query;
                foreach ($filters as $filter_tmp) {
                    $method_name = 'get' . ucfirst($filter_tmp['type']) . 'FilterSubQuery';
                    if (method_exists('Ps_Facetedsearch', $method_name)) {
                        $no_subquery_necessary = ($filter['type'] == $filter_tmp['type'] && $filter['id_value'] == $filter_tmp['id_value'] && ($filter['id_value'] || $filter['type'] === 'category' || $filter['type'] === 'condition' || $filter['type'] === 'quantity'));

                        if ($no_subquery_necessary) {
                            // Do not apply the same filter twice, i.e. when the primary filter
                            // and the sub filter have the same type and same id_value.
                            $sub_query_filter = array();
                        } else {
                            // The next part is hard to follow, but here's what I think this
                            // bit of code does:

                            // It checks whether some filters in the current facet
                            // (our current iterator, $filter_tmp), which
                            // is part of the "template" for this category, were selected by the
                            // user.

                            // If so, it formats the current facet
                            // in yet another strange way that is appropriate
                            // for calling get***FilterSubQuery.

                            // For instance, if inside $selected_filters I have:

                            // [id_attribute_group] => Array
                            //   (
                            //      [8] => 3_8
                            //      [11] => 3_11
                            //   )

                            // And $filter_tmp is:
                            // Array
                            // (
                            //   [type] => id_attribute_group
                            //   [id_value] => 3
                            //   [filter_show_limit] => 0
                            //   [filter_type] => 0
                            //  )

                            // Then $selected_filters_cleaned will be:
                            // Array
                            // (
                            //   [0] => 8
                            //   [1] => 11
                            // )

                            // The strategy employed is different whether we're dealing with
                            // a facet with an "id_value" (this is the most complex case involving
                            // the usual underscore-encoded values deserialization witchcraft)
                            // such as "id_attribute_group" or with a facet without id_value.
                            // In the latter case we're in luck because we can just use the
                            // facet in $selected_filters directly.

                            if (!is_null($filter_tmp['id_value'])) {
                                $selected_filters_cleaned = $this->cleanFilterByIdValue(
                                    @$selected_filters[$filter_tmp['type']],
                                    $filter_tmp['id_value']
                                );
                            } else {
                                $selected_filters_cleaned = @$selected_filters[$filter_tmp['type']];
                            }
                            $ignore_join = ($filter['type'] == $filter_tmp['type']);
                            // Prepare the new bits of SQL query.
                            // $ignore_join is set to true when the sub-facet
                            // is of the same "type" as the main facet. This way
                            // the method ($method_name) knows that the tables it needs are already
                            // there and don't need to be joined again.
                            $sub_query_filter = self::$method_name(
                                $selected_filters_cleaned,
                                $ignore_join
                            );

                            $sub_query_filter_full_all = self::$method_name(
                                [],
                                $ignore_join
                            );

                            if ($method_name == 'getManufacturerFilterSubQuery') {

                                $sub_query_filter_all_manufacturer = self::$method_name(
                                    $selected_filters_cleaned,
                                    $ignore_join,
                                    true
                                );
                            } else {
                                $sub_query_filter_all_manufacturer = $sub_query_filter;
                            }
                        }

                        $sql_query_all_manufacturer = $sql_query;

                        if (strstr($sql_query_all_manufacturer['where'], ' AND p.id_manufacturer IN (') !== false) {
                            $sql_query_all_manufacturer['where'] = preg_replace('/ AND p\.id_manufacturer IN \(.*?\)/', '', $sql_query_all_manufacturer['where']);
                        } 

                        if (isset($sub_query_filter_full_all)) {
                            foreach ($sub_query_filter_full_all as $key => $value) {
                                $sql_query_full_all[$key] .= $value;
                            }
                        }

                        if (isset($sub_query_filter_all_manufacturer)) {
                            foreach ($sub_query_filter_all_manufacturer as $key => $value) {
                                $sql_query_all_manufacturer[$key] .= $value;
                            }
                        }
                        // Now we "merge" the query from the subfilter with the main query
                        foreach ($sub_query_filter as $key => $value) {
                            $sql_query[$key] .= $value;
                        }
                    }
                }

                $products = false;
                if (!empty($sql_query['from'])) {
                    $assembled_sql_query = implode(
                        "\n",
                        array(
                            $sql_query['select'],
                            $sql_query['from'],
                            $sql_query['join'],
                            $sql_query['where'],
                            $sql_query['group'],
                        )
                    );
                    $products = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($assembled_sql_query);
                }

                $products_full_all = false;
                if (!empty($sql_query['from'])) {
                    $assembled_sql_query_full_all = implode(
                        "\n",
                        array(
                            $sql_query_full_all['select'],
                            $sql_query_full_all['from'],
                            $sql_query_full_all['join'],
                            $sql_query_full_all['where'],
                            $sql_query_full_all['group'],
                        )
                    );
                    $products_full_all = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($assembled_sql_query_full_all);
                }

                $products_manufacturer = false;
                if (!empty($sql_query_all_manufacturer['from'])) {
                    $assembled_sql_query_all_manufacturer = implode(
                        "\n",
                        array(
                            $sql_query_all_manufacturer['select'],
                            $sql_query_all_manufacturer['from'],
                            $sql_query_all_manufacturer['join'],
                            $sql_query_all_manufacturer['where'],
                            $sql_query_all_manufacturer['group'],
                        )
                    );
                    $products_manufacturer = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($assembled_sql_query_all_manufacturer);
                }

                // price & weight have slidebar, so it's ok to not complete recompute the product list
                if (!empty($selected_filters['price']) && $filter['type'] != 'price') {
                    $products = self::filterProductsByPrice(@$selected_filters['price'], $products);
                    $products_manufacturer = $products;
                    $products_full_all = $products;
                }
                $productCache[$cacheKey] = $products;
            }

            switch ($filter['type']) {
                case 'price':
                    if ($this->showPriceFilter()) {
                        $price_array = array(
                            'type_lite' => 'price',
                            'type' => 'price',
                            'id_key' => 0,
                            'name' => $this->trans('Price', array(), 'Modules.Facetedsearch.Shop'),
                            'slider' => true,
                            'max' => '0',
                            'min' => null,
                            'unit' => $currency->sign,
                            'format' => $currency->format,
                            'filter_show_limit' => $filter['filter_show_limit'],
                            'filter_type' => $filter['filter_type'],
                            'list_of_values' => array(),
                        );
                        $price_array_full = $price_array;
                        if ($compute_range_filters && isset($products) && $products) {
                            $rangeAggregator = new Ps_FacetedsearchRangeAggregator();
                            $aggregatedRanges = $rangeAggregator->aggregateRanges(
                                $products,
                                'price_min',
                                'price_max'
                            );
                            $price_array['min'] = $aggregatedRanges['min'];
                            $price_array['max'] = $aggregatedRanges['max'];

                            $mergedRanges = $rangeAggregator->mergeRanges(
                                $aggregatedRanges['ranges'],
                                10
                            );

                            $price_array['list_of_values'] = array_map(function (array $range) {
                                return array(
                                    0 => $range['min'],
                                    1 => $range['max'],
                                    'nbr' => $range['count'],
                                );
                            }, $mergedRanges);

                            $price_array['values'] = array($price_array['min'], $price_array['max']);
                        }
                        if ($compute_range_filters && isset($products_full_all) && $products_full_all) {
                            $rangeAggregator_full = new Ps_FacetedsearchRangeAggregator();
                            $aggregatedRanges_full = $rangeAggregator_full->aggregateRanges(
                                $products_full_all,
                                'price_min',
                                'price_max'
                            );
                            $price_array_full['min'] = $aggregatedRanges_full['min'];
                            $price_array_full['max'] = $aggregatedRanges_full['max'];

                            $mergedRanges_full = $rangeAggregator_full->mergeRanges(
                                $aggregatedRanges_full['ranges'],
                                10
                            );

                            $price_array_full['list_of_values'] = array_map(function (array $range_full) {
                                return array(
                                    0 => $range_full['min'],
                                    1 => $range_full['max'],
                                    'nbr' => self::KL_CONST_FOR_COUNT,
                                );
                            }, $mergedRanges_full);

                            $price_array_full['values'] = array($price_array_full['min'], $price_array_full['max']);
                        }
                        $filter_blocks[] = $price_array;
                        $filter_blocks_full[] = $price_array_full;
                        //var_dump($filter_blocks, $filter_blocks_full);exit;
                    }
                    break;

                // case 'weight':
                //     $weight_array = array(
                //         'type_lite' => 'weight',
                //         'type' => 'weight',
                //         'id_key' => 0,
                //         'name' => $this->trans('Weight', array(), 'Modules.Facetedsearch.Shop'),
                //         'slider' => true,
                //         'max' => '0',
                //         'min' => null,
                //         'unit' => Configuration::get('PS_WEIGHT_UNIT'),
                //         'format' => 5, // Ex: xxxxx kg
                //         'filter_show_limit' => $filter['filter_show_limit'],
                //         'filter_type' => $filter['filter_type'],
                //         'list_of_values' => array(),
                //     );
                //     if ($compute_range_filters && isset($products) && $products) {
                //         $rangeAggregator = new Ps_FacetedsearchRangeAggregator();
                //         $aggregatedRanges = $rangeAggregator->getRangesFromList(
                //             $products,
                //             'weight'
                //         );
                //         $weight_array['min'] = $aggregatedRanges['min'];
                //         $weight_array['max'] = $aggregatedRanges['max'];

                //         $mergedRanges = $rangeAggregator->mergeRanges(
                //             $aggregatedRanges['ranges'],
                //             10
                //         );

                //         $weight_array['list_of_values'] = array_map(function (array $range) {
                //             return array(
                //                 0 => $range['min'],
                //                 1 => $range['max'],
                //                 'nbr' => $range['count'],
                //             );
                //         }, $mergedRanges);

                //         if (empty($weight_array['list_of_values']) && isset($selected_filters['weight'])) {
                //             // in case we don't have a list of values,
                //             // add the original one.
                //             // This may happen when e.g. all products
                //             // weigh 0.
                //             $weight_array['list_of_values'] = array(
                //                 array(
                //                     0 => $selected_filters['weight'][0],
                //                     1 => $selected_filters['weight'][1],
                //                     'nbr' => count($products),
                //                 ),
                //             );
                //         }

                //         $weight_array['values'] = array($weight_array['min'], $weight_array['max']);
                //     }
                //     $filter_blocks[] = $weight_array;
                //     $filter_blocks_full[] = $weight_array;
                //     break;

                case 'condition':
                    $condition_array = array(
                        'new' => array('name' => $this->trans('New', array(), 'Modules.Facetedsearch.Shop'), 'nbr' => 0),
                        'used' => array('name' => $this->trans('Used', array(), 'Modules.Facetedsearch.Shop'), 'nbr' => 0),
                        'refurbished' => array('name' => $this->trans('Refurbished', array(), 'Modules.Facetedsearch.Shop'),
                            'nbr' => 0,),
                    );
                    $condition_array_full = $condition_array;
                    if (isset($products) && $products) {
                        foreach ($products as $product) {
                            if (isset($selected_filters['condition']) && in_array($product['condition'], $selected_filters['condition'])) {
                                $condition_array[$product['condition']]['checked'] = true;
                            }
                        }
                    }
                    foreach ($condition_array as $key => $condition) {
                        if (isset($selected_filters['condition']) && in_array($key, $selected_filters['condition'])) {
                            $condition_array[$key]['checked'] = true;
                        }
                    }
                    if (isset($products) && $products) {
                        foreach ($products as $product) {
                            if (isset($condition_array[$product['condition']])) {
                                ++$condition_array[$product['condition']]['nbr'];
                            }
                        }
                    }

                    if (isset($products_full_all) && $products_full_all) {
                        foreach ($products_full_all as $product) {
                            if (isset($condition_array_full[$product['condition']])) {
                                $condition_array_full[$product['condition']]['nbr']=self::KL_CONST_FOR_COUNT;
                            }
                        }
                    }

                    $filter_blocks[] = array(
                        'type_lite' => 'condition',
                        'type' => 'condition',
                        'id_key' => 0,
                        'name' => $this->trans('Condition', array(), 'Modules.Facetedsearch.Shop'),
                        'values' => $condition_array,
                        'filter_show_limit' => $filter['filter_show_limit'],
                        'filter_type' => $filter['filter_type'],
                    );

                    $filter_blocks_full[] = array(
                        'type_lite' => 'condition',
                        'type' => 'condition',
                        'id_key' => 0,
                        'name' => $this->trans('Condition', array(), 'Modules.Facetedsearch.Shop'),
                        'values' => $condition_array_full,
                        'filter_show_limit' => $filter['filter_show_limit'],
                        'filter_type' => $filter['filter_type'],
                    );
                    break;

                case 'quantity':
                    $quantity_array = array(
                        0 => array('name' => $this->trans('Not available', array(), 'Modules.Facetedsearch.Shop'), 'nbr' => 0),
                        1 => array('name' => $this->trans('In stock', array(), 'Modules.Facetedsearch.Shop'), 'nbr' => 0),
                    );
                    $quantity_array_full = $quantity_array;
                    foreach ($quantity_array as $key => $quantity) {
                        if (isset($selected_filters['quantity']) && in_array($key, $selected_filters['quantity'])) {
                            $quantity_array[$key]['checked'] = true;
                        }
                    }
                    if (isset($products) && $products) {
                        foreach ($products as $product) {
                            //If oosp move all not available quantity to available quantity
                            if ((int)$product['quantity'] > 0 || Product::isAvailableWhenOutOfStock($product['out_of_stock'])) {
                                ++$quantity_array[1]['nbr'];
                            } else {
                                ++$quantity_array[0]['nbr'];
                            }
                        }
                    }

                    if (isset($products_full_all) && $products_full_all) {
                        foreach ($products_full_all as $product) {
                            //If oosp move all not available quantity to available quantity
                            if ((int)$product['quantity'] > 0 || Product::isAvailableWhenOutOfStock($product['out_of_stock'])) {
                                $quantity_array_full[1]['nbr'] = self::KL_CONST_FOR_COUNT;
                            } else {
                                $quantity_array_full[0]['nbr']= self::KL_CONST_FOR_COUNT;
                            }
                        }
                    }

                    $filter_blocks[] = array(
                        'type_lite' => 'quantity',
                        'type' => 'quantity',
                        'id_key' => 0,
                        'name' => $this->trans('Availability', array(), 'Modules.Facetedsearch.Shop'),
                        'values' => $quantity_array,
                        'filter_show_limit' => $filter['filter_show_limit'],
                        'filter_type' => $filter['filter_type'],
                    );

                    $filter_blocks_full[] = array(
                        'type_lite' => 'quantity',
                        'type' => 'quantity',
                        'id_key' => 0,
                        'name' => $this->trans('Availability', array(), 'Modules.Facetedsearch.Shop'),
                        'values' => $quantity_array_full,
                        'filter_show_limit' => $filter['filter_show_limit'],
                        'filter_type' => $filter['filter_type'],
                    );

                    break;

                case 'manufacturer':
                    
                    if (!isset($products_manufacturer)) {
                        $products_manufacturer = $products;
                    }
                    if (isset($products) && $products) {
                        $manufaturers_array = array();
                        foreach ($products_manufacturer as $manufacturer) {
                            if (!isset($manufaturers_array[$manufacturer['id_manufacturer']])) {
                                $manufaturers_array[$manufacturer['id_manufacturer']] = array('name' => $manufacturer['name'], 'nbr' => $manufacturer['nbr']);
                            }
                            if (isset($selected_filters['manufacturer']) && in_array((int)$manufacturer['id_manufacturer'], $selected_filters['manufacturer'])) {
                                $manufaturers_array[$manufacturer['id_manufacturer']]['checked'] = true;
                            }
                        }
                        $filter_blocks[] = array(
                            'type_lite' => 'manufacturer',
                            'type' => 'manufacturer',
                            'id_key' => 0,
                            'name' => $this->trans('Brand', array(), 'Modules.Facetedsearch.Shop'),
                            'values' => $manufaturers_array,
                            'filter_show_limit' => $filter['filter_show_limit'],
                            'filter_type' => $filter['filter_type'],
                        );
                    }
                    if (isset($products_full_all) && $products_full_all) {
                        $manufaturers_array_full = array();
                        foreach ($products_full_all as $manufacturer) {
                            if (!isset($manufaturers_array_full[$manufacturer['id_manufacturer']])) {
                                $manufaturers_array_full[$manufacturer['id_manufacturer']] = array('name' => $manufacturer['name'], 'nbr' => self::KL_CONST_FOR_COUNT);
                            }
                        }
                        $filter_blocks_full[] = array(
                            'type_lite' => 'manufacturer',
                            'type' => 'manufacturer',
                            'id_key' => 0,
                            'name' => $this->trans('Brand', array(), 'Modules.Facetedsearch.Shop'),
                            'values' => $manufaturers_array_full,
                            'filter_show_limit' => $filter['filter_show_limit'],
                            'filter_type' => $filter['filter_type'],
                        );
                    }
                    break;

                case 'id_attribute_group':
                    $attributes_array = array();
                    $attributes_array_full = array();
                    if (isset($products) && $products) {
                        foreach ($products as $attributes) {
                            if (!isset($attributes_array[$attributes['id_attribute_group']])) {
                                $attributes_array[$attributes['id_attribute_group']] = array(
                                    'type_lite' => 'id_attribute_group',
                                    'type' => 'id_attribute_group',
                                    'id_key' => (int)$attributes['id_attribute_group'],
                                    'name' => $attributes['attribute_group_name'],
                                    'is_color_group' => (bool)$attributes['is_color_group'],
                                    'values' => array(),
                                    'url_name' => $attributes['name_url_name'],
                                    'meta_title' => $attributes['name_meta_title'],
                                    'filter_show_limit' => $filter['filter_show_limit'],
                                    'filter_type' => $filter['filter_type'],
                                );
                            }

                            if (!isset($attributes_array[$attributes['id_attribute_group']]['values'][$attributes['id_attribute']])) {
                                $attributes_array[$attributes['id_attribute_group']]['values'][$attributes['id_attribute']] = array(
                                    'color' => $attributes['color'],
                                    'name' => $attributes['attribute_name'],
                                    'nbr' => (int)$attributes['nbr'],
                                    'url_name' => $attributes['value_url_name'],
                                    'meta_title' => $attributes['value_meta_title'],
                                );
                            }

                            if (isset($selected_filters['id_attribute_group'][$attributes['id_attribute']])) {
                                $attributes_array[$attributes['id_attribute_group']]['values'][$attributes['id_attribute']]['checked'] = true;
                            }
                        }

                        $filter_blocks = array_merge($filter_blocks, $attributes_array);
                    }
                    if (isset($products_full_all) && $products_full_all) {
                        foreach ($products_full_all as $attributes) {
                            if (!isset($attributes_array_full[$attributes['id_attribute_group']])) {
                                $attributes_array_full[$attributes['id_attribute_group']] = array(
                                    'type_lite' => 'id_attribute_group',
                                    'type' => 'id_attribute_group',
                                    'id_key' => (int)$attributes['id_attribute_group'],
                                    'name' => $attributes['attribute_group_name'],
                                    'is_color_group' => (bool)$attributes['is_color_group'],
                                    'values' => array(),
                                    'url_name' => $attributes['name_url_name'],
                                    'meta_title' => $attributes['name_meta_title'],
                                    'filter_show_limit' => $filter['filter_show_limit'],
                                    'filter_type' => $filter['filter_type'],
                                );
                            }

                            if (!isset($attributes_array_full[$attributes['id_attribute_group']]['values'][$attributes['id_attribute']])) {
                                $attributes_array_full[$attributes['id_attribute_group']]['values'][$attributes['id_attribute']] = array(
                                    'color' => $attributes['color'],
                                    'name' => $attributes['attribute_name'],
                                    'nbr' => self::KL_CONST_FOR_COUNT,
                                    'url_name' => $attributes['value_url_name'],
                                    'meta_title' => $attributes['value_meta_title'],
                                );
                            }

                            if (isset($selected_filters['id_attribute_group'][$attributes['id_attribute']])) {
                                $attributes_array_full[$attributes['id_attribute_group']]['values'][$attributes['id_attribute']]['checked'] = true;
                            }
                        }
                        $filter_blocks_full = array_merge($filter_blocks_full, $attributes_array_full);
                    }
                    break;
                case 'id_feature':
                    $feature_array = array();
                    if (isset($products) && $products) {
                        foreach ($products as $feature) {
                            if (!isset($feature_array[$feature['id_feature']])) {
                                $feature_array[$feature['id_feature']] = array(
                                    'type_lite' => 'id_feature',
                                    'type' => 'id_feature',
                                    'id_key' => (int)$feature['id_feature'],
                                    'values' => array(),
                                    'name' => $feature['feature_name'],
                                    'url_name' => $feature['name_url_name'],
                                    'meta_title' => $feature['name_meta_title'],
                                    'filter_show_limit' => $filter['filter_show_limit'],
                                    'filter_type' => $filter['filter_type'],
                                );
                            }

                            if (!isset($feature_array[$feature['id_feature']]['values'][$feature['id_feature_value']])) {
                                $feature_array[$feature['id_feature']]['values'][$feature['id_feature_value']] = array(
                                    'nbr' => (int)$feature['nbr'],
                                    'name' => $feature['value'],
                                    'url_name' => $feature['value_url_name'],
                                    'meta_title' => $feature['value_meta_title'],
                                );
                            }
                        }

                        //Natural sort
                        foreach ($feature_array as $key => $value) {
                            $temp = array();
                            foreach ($feature_array[$key]['values'] as $keyint => $valueint) {
                                $temp[$keyint] = $valueint['name'];
                            }

                            natcasesort($temp);
                            $temp2 = array();

                            foreach ($temp as $keytemp => $valuetemp) {
                                $temp2[$keytemp] = $feature_array[$key]['values'][$keytemp];
                            }

                            $feature_array[$key]['values'] = $temp2;
                        }

                        $filter_blocks = array_merge($filter_blocks, $feature_array);
                    }
                    $feature_array_full = array();
                    if (isset($products_full_all) && $products_full_all) {
                        foreach ($products_full_all as $feature) {
                            if (!isset($feature_array_full[$feature['id_feature']])) {
                                $feature_array_full[$feature['id_feature']] = array(
                                    'type_lite' => 'id_feature',
                                    'type' => 'id_feature',
                                    'id_key' => (int)$feature['id_feature'],
                                    'values' => array(),
                                    'name' => $feature['feature_name'],
                                    'url_name' => $feature['name_url_name'],
                                    'meta_title' => $feature['name_meta_title'],
                                    'filter_show_limit' => $filter['filter_show_limit'],
                                    'filter_type' => $filter['filter_type'],
                                );
                            }

                            if (!isset($feature_array_full[$feature['id_feature']]['values'][$feature['id_feature_value']])) {
                                $feature_array_full[$feature['id_feature']]['values'][$feature['id_feature_value']] = array(
                                    'nbr' => self::KL_CONST_FOR_COUNT,
                                    'name' => $feature['value'],
                                    'url_name' => $feature['value_url_name'],
                                    'meta_title' => $feature['value_meta_title'],
                                );
                            }
                        }

                        //Natural sort
                        foreach ($feature_array_full as $key => $value) {
                            $temp = array();
                            foreach ($feature_array_full[$key]['values'] as $keyint => $valueint) {
                                $temp[$keyint] = $valueint['name'];
                            }

                            natcasesort($temp);
                            $temp2 = array();

                            foreach ($temp as $keytemp => $valuetemp) {
                                $temp2[$keytemp] = $feature_array_full[$key]['values'][$keytemp];
                            }

                            $feature_array_full[$key]['values'] = $temp2;
                        }

                        $filter_blocks_full = array_merge($filter_blocks_full, $feature_array_full);
                    }
                    break;

                case 'category':
                    $tmp_array = array();
                    if (isset($products) && $products) {
                        $categories_with_products_count = 0;
                        foreach ($products as $category) {
                            $tmp_array[$category['id_category']] = array(
                                'name' => $category['name'],
                                'nbr' => (int)$category['count_products'],
                            );

                            if ((int)$category['count_products']) {
                                ++$categories_with_products_count;
                            }

                            if (isset($selected_filters['category']) && in_array($category['id_category'], $selected_filters['category'])) {
                                $tmp_array[$category['id_category']]['checked'] = true;
                            }
                        }
                        if ($categories_with_products_count) {
                            $filter_blocks[] = array(
                                'type_lite' => 'category',
                                'type' => 'category',
                                'id_key' => 0, 'name' => $this->trans('Categories', array(), 'Modules.Facetedsearch.Shop'),
                                'values' => $tmp_array,
                                'filter_show_limit' => $filter['filter_show_limit'],
                                'filter_type' => $filter['filter_type'],
                            );
                        }
                    }
                    $tmp_array_full = array();
                    if (isset($products_full_all) && $products_full_all) {
                        $categories_with_products_count = 0;
                        foreach ($products_full_all as $category) {
                            $tmp_array_full[$category['id_category']] = array(
                                'name' => $category['name'],
                                'nbr' => self::KL_CONST_FOR_COUNT,
                            );

                            if ((int)$category['count_products']) {
                                ++$categories_with_products_count;
                            }
                        }
                        if ($categories_with_products_count) {
                            $filter_blocks_full[] = array(
                                'type_lite' => 'category',
                                'type' => 'category',
                                'id_key' => 0, 'name' => $this->trans('Categories', array(), 'Modules.Facetedsearch.Shop'),
                                'values' => $tmp_array_full,
                                'filter_show_limit' => $filter['filter_show_limit'],
                                'filter_type' => $filter['filter_type'],
                            );
                        }
                    }
                    break;
            }
        }
        // var_dump($filter_blocks_full);echo '<br>';echo '<br>';echo '<br>';
        $latest_selected_filters = $selected_filters;
        if (Tools::getValue('q') === false) {
            $filter_blocks_full = $filter_blocks;
        } else {
            foreach ($filter_blocks_full as $full_key => $filter_block_full) {
                foreach ($filter_blocks as $key => $filter_block) {
                    if ($filter_block_full['type'] === $filter_block['type'] && $filter_block['type'] != 'price') {
                        foreach ($filter_block_full['values'] as $all_key => $one_filter_all) {
                            foreach ($filter_block['values'] as $key_one => $one_filter) {
                                if ($one_filter_all['name'] === $one_filter['name']) {
                                    $filter_blocks_full[$full_key]['values'][$all_key] = $one_filter;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            foreach ($filter_blocks_full as $full_key => $filter_block_full) {
                if (isset($filter_block_full['values'])) {
                    foreach ($filter_block_full['values'] as $all_key => $one_filter_all) {
                        if ($one_filter_all['nbr'] === self::KL_CONST_FOR_COUNT) {
                            $temp_arr = $filter_blocks_full[$full_key]['values'][$all_key];
                            unset($filter_blocks_full[$full_key]['values'][$all_key]);
                            $filter_blocks_full[$full_key]['values'][] = $temp_arr;
                        }
                    }
                }
            }
        }

        return array(
            'filters' => $filter_blocks_full,
        );
    }

    public function getProductByFilters(
        $products_per_page,
        $page,
        $order_by,
        $order_way,
        $id_lang,
        $selected_filters = array()
    )
    {
        $products_per_page = (int)$products_per_page;

        if (!Validate::isOrderBy($order_by)) {
            $order_by = 'cp.position';
        }

        if (!Validate::isOrderWay($order_way)) {
            $order_way = 'ASC';
        }

        $order_clause = $order_by . ' ' . $order_way;

        $home_category = Configuration::get('PS_HOME_CATEGORY');
        /* If the current category isn't defined or if it's homepage, we have nothing to display */
        $id_parent = (int)Tools::getValue('id_category', Tools::getValue('id_category_layered', $home_category));

        $alias_where = 'p';
        if (version_compare(_PS_VERSION_, '1.5', '>')) {
            $alias_where = 'product_shop';
        }

        $query_filters_where = ' AND ' . $alias_where . '.`active` = 1 AND ' . $alias_where . '.`visibility` IN ("both", "catalog")';
        $query_filters_from = '';

        $parent = new Category((int)$id_parent);

        foreach ($selected_filters as $key => $filter_values) {
            if (!count($filter_values)) {
                continue;
            }

            preg_match('/^(.*[^_0-9])/', $key, $res);
            $key = $res[1];

            switch ($key) {
                case 'id_feature':
                    $sub_queries = array();
                    foreach ($filter_values as $filter_value) {
                        $filter_value_array = explode('_', $filter_value);
                        if (!isset($sub_queries[$filter_value_array[0]])) {
                            $sub_queries[$filter_value_array[0]] = array();
                        }
                        $sub_queries[$filter_value_array[0]][] = 'fp.`id_feature_value` = ' . (int)$filter_value_array[1];
                    }
                    foreach ($sub_queries as $sub_query) {
                        $query_filters_where .= ' AND p.id_product IN (SELECT `id_product` FROM `' . _DB_PREFIX_ . 'feature_product` fp WHERE ';
                        $query_filters_where .= implode(' OR ', $sub_query) . ') ';
                    }
                    break;

                case 'id_attribute_group':
                    $sub_queries = array();

                    foreach ($filter_values as $filter_value) {
                        $filter_value_array = explode('_', $filter_value);
                        if (!isset($sub_queries[$filter_value_array[0]])) {
                            $sub_queries[$filter_value_array[0]] = array();
                        }
                        $sub_queries[$filter_value_array[0]][] = 'pac.`id_attribute` = ' . (int)$filter_value_array[1];
                    }
                    foreach ($sub_queries as $sub_query) {
                        $query_filters_where .= ' AND p.id_product IN (SELECT pa.`id_product`
                        FROM `' . _DB_PREFIX_ . 'product_attribute_combination` pac
                        LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute` pa
                        ON (pa.`id_product_attribute` = pac.`id_product_attribute`)' .
                            Shop::addSqlAssociation('product_attribute', 'pa') . '
                        WHERE ' . implode(' OR ', $sub_query) . ') ';
                    }
                    break;

                case 'category':
                    $query_filters_where .= ' AND p.id_product IN (SELECT id_product FROM ' . _DB_PREFIX_ . 'category_product cp WHERE ';
                    foreach ($selected_filters['category'] as $id_category) {
                        $query_filters_where .= 'cp.`id_category` = ' . (int)$id_category . ' OR ';
                    }
                    $query_filters_where = rtrim($query_filters_where, 'OR ') . ')';
                    break;

                case 'quantity':
                    if (count($selected_filters['quantity']) == 2) {
                        break;
                    }

                    $query_filters_where .= ' AND sa.quantity ' . (!$selected_filters['quantity'][0] ? '<=' : '>') . ' 0 ';
                    $query_filters_from .= 'LEFT JOIN `' . _DB_PREFIX_ . 'stock_available` sa ON (sa.id_product = p.id_product ' . StockAvailable::addSqlShopRestriction(null, null, 'sa') . ') ';
                    break;

                case 'manufacturer':
                    $selected_filters['manufacturer'] = array_map('intval', $selected_filters['manufacturer']);
                    $query_filters_where .= ' AND p.id_manufacturer IN (' . implode($selected_filters['manufacturer'], ',') . ')';
                    break;

                case 'condition':
                    if (count($selected_filters['condition']) == 3) {
                        break;
                    }
                    $query_filters_where .= ' AND ' . $alias_where . '.condition IN (';
                    foreach ($selected_filters['condition'] as $cond) {
                        $query_filters_where .= '\'' . pSQL($cond) . '\',';
                    }
                    $query_filters_where = rtrim($query_filters_where, ',') . ')';
                    break;

                // case 'weight':
                //     if ($selected_filters['weight'][0] != 0 || $selected_filters['weight'][1] != 0) {
                //         $query_filters_where .= ' AND p.`weight` BETWEEN ' . (float)($selected_filters['weight'][0] - 0.001) . ' AND ' . (float)($selected_filters['weight'][1] + 0.001);
                //     }
                //     break;

                case 'price':
                    if (isset($selected_filters['price'])) {
                        if ($selected_filters['price'][0] !== '' || $selected_filters['price'][1] !== '') {
                            $price_filter = array();
                            $price_filter['min'] = (float)($selected_filters['price'][0]);
                            $price_filter['max'] = (float)($selected_filters['price'][1]);
                        }
                    } else {
                        $price_filter = false;
                    }
                    break;
            }
        }

        $context = Context::getContext();
        $id_currency = (int)$context->currency->id;

        $price_filter_query_in = ''; // All products with price range between price filters limits
        $price_filter_query_out = ''; // All products with a price filters limit on it price range
        if (isset($price_filter) && $price_filter) {
            $price_filter_query_in = 'INNER JOIN `' . _DB_PREFIX_ . 'layered_price_index` psi
            ON
            (
                psi.price_min <= ' . (int)$price_filter['max'] . '
                AND psi.price_max >= ' . (int)$price_filter['min'] . '
                AND psi.`id_product` = p.`id_product`
                AND psi.`id_shop` = ' . (int)$context->shop->id . '
                AND psi.`id_currency` = ' . $id_currency . '
            )';

            $price_filter_query_out = 'INNER JOIN `' . _DB_PREFIX_ . 'layered_price_index` psi
            ON
                ((psi.price_min < ' . (int)$price_filter['min'] . ' AND psi.price_max > ' . (int)$price_filter['min'] . ')
                OR
                (psi.price_max > ' . (int)$price_filter['max'] . ' AND psi.price_min < ' . (int)$price_filter['max'] . '))
                AND psi.`id_product` = p.`id_product`
                AND psi.`id_shop` = ' . (int)$context->shop->id . '
                AND psi.`id_currency` = ' . $id_currency;
        }

        $query_filters_from .= Shop::addSqlAssociation('product', 'p');
        $extraWhereQuery = '';

        if (!empty($selected_filters['category'])) {
            $categories = array_map('intval', $selected_filters['category']);
        }

        if (isset($price_filter) && $price_filter) {
            static $ps_layered_filter_price_usetax = null;
            static $ps_layered_filter_price_rounding = null;

            if ($ps_layered_filter_price_usetax === null) {
                $ps_layered_filter_price_usetax = Configuration::get('PS_LAYERED_FILTER_PRICE_USETAX');
            }

            if ($ps_layered_filter_price_rounding === null) {
                $ps_layered_filter_price_rounding = Configuration::get('PS_LAYERED_FILTER_PRICE_ROUNDING');
            }

            if (empty($selected_filters['category'])) {
                $all_products_out = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
                    SELECT p.`id_product` id_product
                    FROM `' . _DB_PREFIX_ . 'product` p JOIN ' . _DB_PREFIX_ . 'category_product cp USING (id_product)
                    INNER JOIN ' . _DB_PREFIX_ . 'category c ON (c.id_category = cp.id_category AND
                        ' . ($this->ps_layered_full_tree ? 'c.nleft >= ' . (int)$parent->nleft . '
                        AND c.nright <= ' . (int)$parent->nright : 'c.id_category = ' . (int)$id_parent) . '
                        AND c.active = 1)
                    ' . $price_filter_query_out . '
                    ' . $query_filters_from . '
                    WHERE 1 ' . $query_filters_where . ' GROUP BY cp.id_product');
            } else {
                $all_products_out = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
                    SELECT p.`id_product` id_product
                    FROM `' . _DB_PREFIX_ . 'product` p JOIN ' . _DB_PREFIX_ . 'category_product cp USING (id_product)
                    ' . $price_filter_query_out . '
                    ' . $query_filters_from . '
                    WHERE cp.`id_category` IN (' . implode(',', $categories) . ') ' . $query_filters_where . ' GROUP BY cp.id_product');
            }

            /* for this case, price could be out of range, so we need to compute the real price */
            foreach ($all_products_out as $product) {
                $price = Product::getPriceStatic($product['id_product'], $ps_layered_filter_price_usetax);
                if ($ps_layered_filter_price_rounding) {
                    $price = (int)$price;
                }
                if ($price < $price_filter['min'] || $price > $price_filter['max']) {
                    // out of range price, exclude the product
                    $product_id_delete_list[] = (int)$product['id_product'];
                }
            }
            if (!empty($product_id_delete_list)) {
                $extraWhereQuery = ' AND p.id_product NOT IN (' . implode(',', $product_id_delete_list) . ') ';
            }
        }
        if (empty($selected_filters['category'])) {
            $catFilterRestrictionDerivedTable = ' ((SELECT cp.id_product, MIN(cp.position) position FROM ' . _DB_PREFIX_ . 'category c
                                                         STRAIGHT_JOIN ' . _DB_PREFIX_ . 'category_product cp ON (c.id_category = cp.id_category AND
                                                         c.id_category = ' . (int)$id_parent . '
                                                         AND c.active = 1)
                                                         STRAIGHT_JOIN `' . _DB_PREFIX_ . 'product` p ON (p.id_product=cp.id_product)
                                                         ' . $price_filter_query_in . '
                                                         ' . $query_filters_from . '
                                                         WHERE 1 ' . $query_filters_where . $extraWhereQuery . '
                                                         GROUP BY cp.id_product)';
            if ($this->ps_layered_full_tree) {
                // add other products in subcategories, but not present in the main cat!
                $catFilterRestrictionDerivedTable .= ' UNION ALL (SELECT cp.id_product, MIN(cp.position) position FROM ' . _DB_PREFIX_ . 'category c
                                                         STRAIGHT_JOIN ' . _DB_PREFIX_ . 'category_product cp ON (c.id_category = cp.id_category AND
                                                         c.id_category != ' . (int)$id_parent . '
                                                         AND c.nleft >= ' . (int)$parent->nleft . '
                                                         AND c.nright <= ' . (int)$parent->nright.'
                                                         AND c.active = 1)
                                                         STRAIGHT_JOIN `' . _DB_PREFIX_ . 'product` p ON (p.id_product=cp.id_product)
                                                         ' . $price_filter_query_in . '
                                                         ' . $query_filters_from . '
                                                         WHERE NOT EXISTS(SELECT * FROM ' . _DB_PREFIX_ . 'category_product cpe 
                                                                            WHERE cp.id_product=cpe.id_product AND cpe.id_category = ' . (int)$id_parent . ')
                                                         ' . $query_filters_where . $extraWhereQuery . '
                                                         GROUP BY cp.id_product)';
            }
            $catFilterRestrictionDerivedTable .= ')';
        } else {
            $catFilterRestrictionDerivedTable = ' (SELECT cp.id_product, MIN(cp.position) position FROM ' . _DB_PREFIX_ . 'category_product cp
                                                         STRAIGHT_JOIN `' . _DB_PREFIX_ . 'product` p ON (p.id_product=cp.id_product)
                                                         ' . $price_filter_query_in . '
                                                         ' . $query_filters_from . '
                                                         WHERE cp.`id_category` IN (' . implode(',', $categories) . ') ' . $query_filters_where . $extraWhereQuery . '
                                                         GROUP BY cp.id_product)';
        }

        $this->nbr_products = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT COUNT(*) FROM ' . $catFilterRestrictionDerivedTable . ' ps');

        if ($this->nbr_products == 0) {
            $products = array();
        } else {
            $nb_day_new_product = (Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20);

            if (version_compare(_PS_VERSION_, '1.6.1', '>=') === true) {
                $products = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
                    SELECT
                        p.*,
                        ' . ($alias_where == 'p' ? '' : 'product_shop.*,') . '
                        ' . $alias_where . '.id_category_default,
                        pl.*,
                        image_shop.`id_image` id_image,
                        il.legend,
                        m.name manufacturer_name,
                        ' . (Combination::isFeatureActive() ? 'product_attribute_shop.id_product_attribute id_product_attribute,' : '') . '
                        DATEDIFF(' . $alias_where . '.`date_add`, DATE_SUB("' . date('Y-m-d') . ' 00:00:00", INTERVAL ' . (int)$nb_day_new_product . ' DAY)) > 0 AS new,
                        stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity' . (Combination::isFeatureActive() ? ', product_attribute_shop.minimal_quantity AS product_attribute_minimal_quantity' : '') . '
                    FROM '.$catFilterRestrictionDerivedTable.' cp
                    LEFT JOIN `' . _DB_PREFIX_ . 'product` p ON p.`id_product` = cp.`id_product`
                    ' . Shop::addSqlAssociation('product', 'p') .
                    (Combination::isFeatureActive() ?
                        ' LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute_shop` product_attribute_shop
                        ON (p.`id_product` = product_attribute_shop.`id_product` AND product_attribute_shop.`default_on` = 1 AND product_attribute_shop.id_shop=' . (int)$context->shop->id . ')' : '') . '
                    LEFT JOIN ' . _DB_PREFIX_ . 'product_lang pl ON (pl.id_product = p.id_product' . Shop::addSqlRestrictionOnLang('pl') . ' AND pl.id_lang = ' . (int)$id_lang . ')
                    LEFT JOIN `' . _DB_PREFIX_ . 'image_shop` image_shop
                        ON (image_shop.`id_product` = p.`id_product` AND image_shop.cover=1 AND image_shop.id_shop=' . (int)$context->shop->id . ')
                    LEFT JOIN `' . _DB_PREFIX_ . 'image_lang` il ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = ' . (int)$id_lang . ')
                    LEFT JOIN ' . _DB_PREFIX_ . 'manufacturer m ON (m.id_manufacturer = p.id_manufacturer)
                    ' . Product::sqlStock('p', 0) . '
                    WHERE ' . $alias_where . '.`active` = 1 AND ' . $alias_where . '.`visibility` IN ("both", "catalog")
                    ORDER BY ' . $order_clause . ' , cp.id_product' .
                    ' LIMIT ' . (((int)$page - 1) * $products_per_page . ',' . $products_per_page));
            } else {
                $products = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
                    SELECT
                        p.*,
                        ' . ($alias_where == 'p' ? '' : 'product_shop.*,') . '
                        ' . $alias_where . '.id_category_default,
                        pl.*,
                        MAX(image_shop.`id_image`) id_image,
                        il.legend,
                        m.name manufacturer_name,
                        ' . (Combination::isFeatureActive() ? 'MAX(product_attribute_shop.id_product_attribute) id_product_attribute,' : '') . '
                        DATEDIFF(' . $alias_where . '.`date_add`, DATE_SUB("' . date('Y-m-d') . ' 00:00:00", INTERVAL ' . (int)$nb_day_new_product . ' DAY)) > 0 AS new,
                        stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity' . (Combination::isFeatureActive() ? ', MAX(product_attribute_shop.minimal_quantity) AS product_attribute_minimal_quantity' : '') . '
                    FROM '.$catFilterRestrictionDerivedTable.' cp
                    LEFT JOIN `' . _DB_PREFIX_ . 'product` p ON p.`id_product` = cp.`id_product`
                    ' . Shop::addSqlAssociation('product', 'p') .
                    (Combination::isFeatureActive() ?
                        'LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute` pa ON (p.`id_product` = pa.`id_product`)
                    ' . Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1 AND product_attribute_shop.id_shop=' . (int)$context->shop->id) : '') . '
                    LEFT JOIN ' . _DB_PREFIX_ . 'product_lang pl ON (pl.id_product = p.id_product' . Shop::addSqlRestrictionOnLang('pl') . ' AND pl.id_lang = ' . (int)$id_lang . ')
                    LEFT JOIN `' . _DB_PREFIX_ . 'image` i  ON (i.`id_product` = p.`id_product`)' .
                    Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1') . '
                    LEFT JOIN `' . _DB_PREFIX_ . 'image_lang` il ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = ' . (int)$id_lang . ')
                    LEFT JOIN ' . _DB_PREFIX_ . 'manufacturer m ON (m.id_manufacturer = p.id_manufacturer)
                    ' . Product::sqlStock('p', 0) . '
                    WHERE ' . $alias_where . '.`active` = 1 AND ' . $alias_where . '.`visibility` IN ("both", "catalog")
                    GROUP BY product_shop.id_product
                    ORDER BY ' . $order_clause . ' , cp.id_product' .
                    ' LIMIT ' . (((int)$page - 1) * $products_per_page . ',' . $products_per_page));
            }
        }

        if ($order_by == 'p.price') {
            Tools::orderbyPrice($products, $order_way);
        }
        $product_arrays_ids = '';
        foreach ($products as $key => $product)
        {
            if(end($products) == $product) {
                $product_arrays_ids .= $product['id_product'];
            } else {
                $product_arrays_ids .= $product['id_product'].', ';
            }
            // $attributes = Product::getProductAttributesIds($product['id_product']);
            // $prices = [];
            // foreach ($attributes as $attribute) {
            //     $prices[] = Product::getPriceStatic($product['id_product'],true,$attribute['id_product_attribute'],2);
            // }
            // $min_price = isset($prices[0]) ? $prices[0] : 0;
            // $min_price = $prices[0];
            // foreach ($prices as $price) {
            //     if ($min_price == 0) {
            //         $min_price = $price;
            //     } else {
            //         if ($price < $min_price && $price != 0) {
            //             $min_price = $price;
            //         }
            //     }
            // }
            // $products[$key]['min_price_for_product'] = $min_price;

                $products[$key]['min_price_for_product'] = $this->getMinPriceByProductId($product['id_product']);
                $products[$key]['max_price_for_product'] = $this->getMaxPriceByProductId($product['id_product']);
        }

        $product_without_price = $this->getProductByFiltersWithoutPrice($products_per_page, $page, $order_by, $order_way, $id_lang, $selected_filters);

        foreach ($product_without_price as $key => $product)
        {
            $min_price = $this->getMinPriceByProductId($product['id_product']);
            $max_price = $this->getMaxPriceByProductId($product['id_product']);
            
            if (!isset($minimum_price_for_filt) || $min_price < $minimum_price_for_filt) {
                $minimum_price_for_filt = $min_price;
            } 
            if (!isset($maximum_price_for_filt) || $max_price > $maximum_price_for_filt) {
                $maximum_price_for_filt = $max_price;
            }
        }
        if (Tools::getValue('q')) {
            Media::addJsDef(array('onlyFrom_p' => $minimum_price_for_filt));
            Media::addJsDef(array('onlyTo_p' => $maximum_price_for_filt)); 
        }
        
        if ($this->nbr_products != 0) {
            $get_all_products_ids = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
                        SELECT p.id_product
                        FROM '.$catFilterRestrictionDerivedTable.' cp
                        LEFT JOIN `' . _DB_PREFIX_ . 'product` p ON p.`id_product` = cp.`id_product`
                        ' . Shop::addSqlAssociation('product', 'p') .
                        (Combination::isFeatureActive() ?
                            ' LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute_shop` product_attribute_shop
                            ON (p.`id_product` = product_attribute_shop.`id_product` AND product_attribute_shop.`default_on` = 1 AND product_attribute_shop.id_shop=' . (int)$context->shop->id . ')' : '') . '
                        LEFT JOIN ' . _DB_PREFIX_ . 'product_lang pl ON (pl.id_product = p.id_product' . Shop::addSqlRestrictionOnLang('pl') . ' AND pl.id_lang = ' . (int)$id_lang . ')
                        LEFT JOIN `' . _DB_PREFIX_ . 'image_shop` image_shop
                            ON (image_shop.`id_product` = p.`id_product` AND image_shop.cover=1 AND image_shop.id_shop=' . (int)$context->shop->id . ')
                        LEFT JOIN `' . _DB_PREFIX_ . 'image_lang` il ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = ' . (int)$id_lang . ')
                        LEFT JOIN ' . _DB_PREFIX_ . 'manufacturer m ON (m.id_manufacturer = p.id_manufacturer)
                        ' . Product::sqlStock('p', 0) . '
                        WHERE ' . $alias_where . '.`active` = 1 AND p.id_product NOT IN ('.$product_arrays_ids.') AND ' . $alias_where . '.`visibility` IN ("both", "catalog")
                        ORDER BY ' . $order_clause . ' , cp.id_product');

            $products[0]['get_all_products_ids'] = $get_all_products_ids;
        }

        return array(
            'products' => $products,
            'count' => $this->nbr_products,
        );
    }

    private function getMinPriceByProductId($product_id)
    {
        $attributes = Product::getProductAttributesIds($product_id);
        $prices = [];
        if ($attributes && !empty($attributes)) {
            foreach ($attributes as $attribute) {
                $prices[] = Product::getPriceStatic($product_id,true,$attribute['id_product_attribute'],2);
            }
        } else {
            $prices[] = Product::getPriceStatic($product_id,true,null,2);
        }
        $min_price = $prices[0];

        foreach ($prices as $price) {
            if ($min_price == 0) {
                $min_price = $price;
            } else {
                if ($price < $min_price && $price != 0) {
                    $min_price = $price;
                }
            }
        }

        return $min_price;
    }

    private function getMaxPriceByProductId($product_id)
    {
        $attributes = Product::getProductAttributesIds($product_id);
        $prices = [];
        if ($attributes && !empty($attributes)) {
            foreach ($attributes as $attribute) {
                $prices[] = Product::getPriceStatic($product_id,true,$attribute['id_product_attribute'],2);
            }
        } else {
            $prices[] = Product::getPriceStatic($product_id,true,null,2);
        }
        $max_price = $prices[0];

        foreach ($prices as $price) {
            if ($price > $max_price) {
                $max_price = $price;
            }
        }

        return $max_price;
    }

    protected static function getManufacturerFilterSubQuery($filter_value, $ignore_join = false, $all_manufacturer = false)
    {
        if (empty($filter_value) || $all_manufacturer == true) {
            $query_filters = '';
        } else {
            array_walk($filter_value, create_function('&$id_manufacturer', '$id_manufacturer = (int)$id_manufacturer;'));
            $query_filters = ' AND p.id_manufacturer IN ('.implode($filter_value, ',').')';
        }
        if ($ignore_join) {
            return array('where' => $query_filters);
        } else {
            return array('where' => $query_filters, 'join' => 'LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (m.id_manufacturer = p.id_manufacturer) ');
        }
    }

    public function getProductByFiltersWithoutPrice(
        $products_per_page,
        $page,
        $order_by,
        $order_way,
        $id_lang,
        $selected_filters = array()
    )
    {
        $products_per_page = (int)$products_per_page;

        if (!Validate::isOrderBy($order_by)) {
            $order_by = 'cp.position';
        }

        if (!Validate::isOrderWay($order_way)) {
            $order_way = 'ASC';
        }

        $order_clause = $order_by . ' ' . $order_way;

        $home_category = Configuration::get('PS_HOME_CATEGORY');
        /* If the current category isn't defined or if it's homepage, we have nothing to display */
        $id_parent = (int)Tools::getValue('id_category', Tools::getValue('id_category_layered', $home_category));

        $alias_where = 'p';
        if (version_compare(_PS_VERSION_, '1.5', '>')) {
            $alias_where = 'product_shop';
        }

        $query_filters_where = ' AND ' . $alias_where . '.`active` = 1 AND ' . $alias_where . '.`visibility` IN ("both", "catalog")';
        $query_filters_from = '';

        $parent = new Category((int)$id_parent);

        foreach ($selected_filters as $key => $filter_values) {
            if (!count($filter_values)) {
                continue;
            }

            preg_match('/^(.*[^_0-9])/', $key, $res);
            $key = $res[1];

            switch ($key) {
                case 'id_feature':
                    $sub_queries = array();
                    foreach ($filter_values as $filter_value) {
                        $filter_value_array = explode('_', $filter_value);
                        if (!isset($sub_queries[$filter_value_array[0]])) {
                            $sub_queries[$filter_value_array[0]] = array();
                        }
                        $sub_queries[$filter_value_array[0]][] = 'fp.`id_feature_value` = ' . (int)$filter_value_array[1];
                    }
                    foreach ($sub_queries as $sub_query) {
                        $query_filters_where .= ' AND p.id_product IN (SELECT `id_product` FROM `' . _DB_PREFIX_ . 'feature_product` fp WHERE ';
                        $query_filters_where .= implode(' OR ', $sub_query) . ') ';
                    }
                    break;

                case 'id_attribute_group':
                    $sub_queries = array();

                    foreach ($filter_values as $filter_value) {
                        $filter_value_array = explode('_', $filter_value);
                        if (!isset($sub_queries[$filter_value_array[0]])) {
                            $sub_queries[$filter_value_array[0]] = array();
                        }
                        $sub_queries[$filter_value_array[0]][] = 'pac.`id_attribute` = ' . (int)$filter_value_array[1];
                    }
                    foreach ($sub_queries as $sub_query) {
                        $query_filters_where .= ' AND p.id_product IN (SELECT pa.`id_product`
                        FROM `' . _DB_PREFIX_ . 'product_attribute_combination` pac
                        LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute` pa
                        ON (pa.`id_product_attribute` = pac.`id_product_attribute`)' .
                            Shop::addSqlAssociation('product_attribute', 'pa') . '
                        WHERE ' . implode(' OR ', $sub_query) . ') ';
                    }
                    break;

                case 'category':
                    $query_filters_where .= ' AND p.id_product IN (SELECT id_product FROM ' . _DB_PREFIX_ . 'category_product cp WHERE ';
                    foreach ($selected_filters['category'] as $id_category) {
                        $query_filters_where .= 'cp.`id_category` = ' . (int)$id_category . ' OR ';
                    }
                    $query_filters_where = rtrim($query_filters_where, 'OR ') . ')';
                    break;

                case 'quantity':
                    if (count($selected_filters['quantity']) == 2) {
                        break;
                    }

                    $query_filters_where .= ' AND sa.quantity ' . (!$selected_filters['quantity'][0] ? '<=' : '>') . ' 0 ';
                    $query_filters_from .= 'LEFT JOIN `' . _DB_PREFIX_ . 'stock_available` sa ON (sa.id_product = p.id_product ' . StockAvailable::addSqlShopRestriction(null, null, 'sa') . ') ';
                    break;

                case 'manufacturer':
                    $selected_filters['manufacturer'] = array_map('intval', $selected_filters['manufacturer']);
                    $query_filters_where .= ' AND p.id_manufacturer IN (' . implode($selected_filters['manufacturer'], ',') . ')';
                    break;

                case 'condition':
                    if (count($selected_filters['condition']) == 3) {
                        break;
                    }
                    $query_filters_where .= ' AND ' . $alias_where . '.condition IN (';
                    foreach ($selected_filters['condition'] as $cond) {
                        $query_filters_where .= '\'' . pSQL($cond) . '\',';
                    }
                    $query_filters_where = rtrim($query_filters_where, ',') . ')';
                    break;

                case 'weight':
                    if ($selected_filters['weight'][0] != 0 || $selected_filters['weight'][1] != 0) {
                        $query_filters_where .= ' AND p.`weight` BETWEEN ' . (float)($selected_filters['weight'][0] - 0.001) . ' AND ' . (float)($selected_filters['weight'][1] + 0.001);
                    }
                    break;
            }
        }

        $context = Context::getContext();
        $id_currency = (int)$context->currency->id;

        $query_filters_from .= Shop::addSqlAssociation('product', 'p');
        $extraWhereQuery = '';

        if (!empty($selected_filters['category'])) {
            $categories = array_map('intval', $selected_filters['category']);
        }

        if (empty($selected_filters['category'])) {
            $catFilterRestrictionDerivedTable = ' ((SELECT cp.id_product, MIN(cp.position) position FROM ' . _DB_PREFIX_ . 'category c
                                                         STRAIGHT_JOIN ' . _DB_PREFIX_ . 'category_product cp ON (c.id_category = cp.id_category AND
                                                         c.id_category = ' . (int)$id_parent . '
                                                         AND c.active = 1)
                                                         STRAIGHT_JOIN `' . _DB_PREFIX_ . 'product` p ON (p.id_product=cp.id_product)
                                                         ' . $query_filters_from . '
                                                         WHERE 1 ' . $query_filters_where . $extraWhereQuery . '
                                                         GROUP BY cp.id_product)';
            if ($this->ps_layered_full_tree) {
                // add other products in subcategories, but not present in the main cat!
                $catFilterRestrictionDerivedTable .= ' UNION ALL (SELECT cp.id_product, MIN(cp.position) position FROM ' . _DB_PREFIX_ . 'category c
                                                         STRAIGHT_JOIN ' . _DB_PREFIX_ . 'category_product cp ON (c.id_category = cp.id_category AND
                                                         c.id_category != ' . (int)$id_parent . '
                                                         AND c.nleft >= ' . (int)$parent->nleft . '
                                                         AND c.nright <= ' . (int)$parent->nright.'
                                                         AND c.active = 1)
                                                         STRAIGHT_JOIN `' . _DB_PREFIX_ . 'product` p ON (p.id_product=cp.id_product)
                                                         ' . $query_filters_from . '
                                                         WHERE NOT EXISTS(SELECT * FROM ' . _DB_PREFIX_ . 'category_product cpe 
                                                                            WHERE cp.id_product=cpe.id_product AND cpe.id_category = ' . (int)$id_parent . ')
                                                         ' . $query_filters_where . $extraWhereQuery . '
                                                         GROUP BY cp.id_product)';
            }
            $catFilterRestrictionDerivedTable .= ')';
        } else {
            $catFilterRestrictionDerivedTable = ' (SELECT cp.id_product, MIN(cp.position) position FROM ' . _DB_PREFIX_ . 'category_product cp
                                                         STRAIGHT_JOIN `' . _DB_PREFIX_ . 'product` p ON (p.id_product=cp.id_product)
                                                         ' . $query_filters_from . '
                                                         WHERE cp.`id_category` IN (' . implode(',', $categories) . ') ' . $query_filters_where . $extraWhereQuery . '
                                                         GROUP BY cp.id_product)';
        }

        $this->nbr_products = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT COUNT(*) FROM ' . $catFilterRestrictionDerivedTable . ' ps');


        if ($this->nbr_products == 0) {
            $products = array();
        } else {
            $nb_day_new_product = (Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20);

            if (version_compare(_PS_VERSION_, '1.6.1', '>=') === true) {
                $products = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
                    SELECT
                        p.*,
                        ' . ($alias_where == 'p' ? '' : 'product_shop.*,') . '
                        ' . $alias_where . '.id_category_default,
                        pl.*,
                        image_shop.`id_image` id_image,
                        il.legend,
                        m.name manufacturer_name,
                        ' . (Combination::isFeatureActive() ? 'product_attribute_shop.id_product_attribute id_product_attribute,' : '') . '
                        DATEDIFF(' . $alias_where . '.`date_add`, DATE_SUB("' . date('Y-m-d') . ' 00:00:00", INTERVAL ' . (int)$nb_day_new_product . ' DAY)) > 0 AS new,
                        stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity' . (Combination::isFeatureActive() ? ', product_attribute_shop.minimal_quantity AS product_attribute_minimal_quantity' : '') . '
                    FROM '.$catFilterRestrictionDerivedTable.' cp
                    LEFT JOIN `' . _DB_PREFIX_ . 'product` p ON p.`id_product` = cp.`id_product`
                    ' . Shop::addSqlAssociation('product', 'p') .
                    (Combination::isFeatureActive() ?
                        ' LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute_shop` product_attribute_shop
                        ON (p.`id_product` = product_attribute_shop.`id_product` AND product_attribute_shop.`default_on` = 1 AND product_attribute_shop.id_shop=' . (int)$context->shop->id . ')' : '') . '
                    LEFT JOIN ' . _DB_PREFIX_ . 'product_lang pl ON (pl.id_product = p.id_product' . Shop::addSqlRestrictionOnLang('pl') . ' AND pl.id_lang = ' . (int)$id_lang . ')
                    LEFT JOIN `' . _DB_PREFIX_ . 'image_shop` image_shop
                        ON (image_shop.`id_product` = p.`id_product` AND image_shop.cover=1 AND image_shop.id_shop=' . (int)$context->shop->id . ')
                    LEFT JOIN `' . _DB_PREFIX_ . 'image_lang` il ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = ' . (int)$id_lang . ')
                    LEFT JOIN ' . _DB_PREFIX_ . 'manufacturer m ON (m.id_manufacturer = p.id_manufacturer)
                    ' . Product::sqlStock('p', 0) . '
                    WHERE ' . $alias_where . '.`active` = 1 AND ' . $alias_where . '.`visibility` IN ("both", "catalog")
                    ORDER BY ' . $order_clause . ' , cp.id_product' .
                    ' LIMIT ' . (((int)$page - 1) * $products_per_page . ',' . $products_per_page));
            } else {
                $products = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
                    SELECT
                        p.*,
                        ' . ($alias_where == 'p' ? '' : 'product_shop.*,') . '
                        ' . $alias_where . '.id_category_default,
                        pl.*,
                        MAX(image_shop.`id_image`) id_image,
                        il.legend,
                        m.name manufacturer_name,
                        ' . (Combination::isFeatureActive() ? 'MAX(product_attribute_shop.id_product_attribute) id_product_attribute,' : '') . '
                        DATEDIFF(' . $alias_where . '.`date_add`, DATE_SUB("' . date('Y-m-d') . ' 00:00:00", INTERVAL ' . (int)$nb_day_new_product . ' DAY)) > 0 AS new,
                        stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity' . (Combination::isFeatureActive() ? ', MAX(product_attribute_shop.minimal_quantity) AS product_attribute_minimal_quantity' : '') . '
                    FROM '.$catFilterRestrictionDerivedTable.' cp
                    LEFT JOIN `' . _DB_PREFIX_ . 'product` p ON p.`id_product` = cp.`id_product`
                    ' . Shop::addSqlAssociation('product', 'p') .
                    (Combination::isFeatureActive() ?
                        'LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute` pa ON (p.`id_product` = pa.`id_product`)
                    ' . Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1 AND product_attribute_shop.id_shop=' . (int)$context->shop->id) : '') . '
                    LEFT JOIN ' . _DB_PREFIX_ . 'product_lang pl ON (pl.id_product = p.id_product' . Shop::addSqlRestrictionOnLang('pl') . ' AND pl.id_lang = ' . (int)$id_lang . ')
                    LEFT JOIN `' . _DB_PREFIX_ . 'image` i  ON (i.`id_product` = p.`id_product`)' .
                    Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1') . '
                    LEFT JOIN `' . _DB_PREFIX_ . 'image_lang` il ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = ' . (int)$id_lang . ')
                    LEFT JOIN ' . _DB_PREFIX_ . 'manufacturer m ON (m.id_manufacturer = p.id_manufacturer)
                    ' . Product::sqlStock('p', 0) . '
                    WHERE ' . $alias_where . '.`active` = 1 AND ' . $alias_where . '.`visibility` IN ("both", "catalog")
                    GROUP BY product_shop.id_product
                    ORDER BY ' . $order_clause . ' , cp.id_product' .
                    ' LIMIT ' . (((int)$page - 1) * $products_per_page . ',' . $products_per_page));
            }
        }

        if ($order_by == 'p.price') {
            Tools::orderbyPrice($products, $order_way);
        }

        return $products;
    }
}
