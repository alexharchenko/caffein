<?php

class TmGoogleMapOverride extends TmGoogleMap
{
	public function hookActionModuleRegisterHookAfter($params)
    {
    	if ($this->settings->getSetting($params['hook_name'], 'TMGOOGLE_STYLE') === false) {
    		$this->settings->setDefaultSettings($params['hook_name']);
    	}
    }
}