<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

class TmmegamenuOverride extends Tmmegamenu
{
	protected function makeMenuTop($hookName)
    {
        if (!is_array($this->menu)) {
            $this->menu = array();
        }
        if ($top_items = $this->megamenu->getTopItems($hookName)) {
            $this->menu[$hookName] = '<ul class="menu clearfix top-level-menu tmmegamenu_item">';

            $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://';
            $currentUrl = $protocol . $_SERVER['HTTP_HOST']  . $_SERVER['REQUEST_URI'];

            foreach ($top_items as $key => $top) {
                $item_num = $key + 1;
                $simple_class = '';
                $badge = '';

                if ($top['is_simple']) {
                    $simple_class = ' simple';
                }

                if ($top['badge']) {
                    $badge = '<span class="menu_badge '.$top['unique_code'].' top-level-badge tmmegamenu_item">'.$top['badge'].'</span>';
                }

                if (!$top['is_custom_url']) {
                    $top_item_url = $this->generateTopItemUrl($top['url']);
                } else {
                    $top_item_url = array('url' =>$top['url'], 'selected' => '');
                }

                $this->menu[$hookName] .= '<li class="'.$top['specific_class'].$simple_class.$top_item_url['selected'].' top-level-menu-li tmmegamenu_item '.$top['unique_code'].'">';
                if (!Tools::isEmpty($top_item_url['url'])) {
                    if ($top_item_url['url'] == $currentUrl) {
                        $this->menu[$hookName] .= '<span class="'.$top['unique_code'].' top-level-menu-li-a tmmegamenu_item">'.$top['title'].$badge.'</span>';
                    } else {
                        $this->menu[$hookName] .= '<a class="'.$top['unique_code'].' top-level-menu-li-a tmmegamenu_item" href="'.$top_item_url['url'].'">'.$top['title'].$badge.'</a>';
                    }
                } else {
                    $this->menu[$hookName] .= $top['title'];
                }

                if (!$top['is_mega']) {
                    $subitems = $this->megamenu->getMenuItem((int)$top['id_item'], 0, true);
                    if ($subitems) {
                        $this->menu[$hookName] .= '<ul class="is-simplemenu tmmegamenu_item first-level-menu '.$top['unique_code'].'">';
                        $this->menu[$hookName] .= $this->makeMenu($hookName, $subitems);
                        $this->menu[$hookName] .= '</ul>';
                    }
                } else {
                    if ($rows = $this->megamenu->getMegamenuRow((int)$top['id_item'])) {
                        $this->menu[$hookName] .= '<div class="is-megamenu tmmegamenu_item first-level-menu '.$top['unique_code'].'">';
                        foreach ($rows as $row) {
                            $this->menu[$hookName] .= '<div id="megamenu-row-'.$item_num.'-'.$row.'" class="megamenu-row row megamenu-row-'.$row.'">';
                            if ($cols = $this->megamenu->getMegamenuRowCols((int)$top['id_item'], $row)) {
                                $sp_class = '';
                                foreach ($cols as $col) {
                                    if ($col['class']) {
                                        $sp_class = ' '.$col['class'];
                                    }
                                    $this->menu[$hookName] .= '<div id="column-'.$item_num.'-'.$row.'-'.$col['col'].'"
                                                    class="megamenu-col megamenu-col-'.$row.'-'.$col['col'].' col-sm-'.$col['width'].' '.$sp_class.'">';
                                    $this->menu[$hookName] .= '<ul class="content">';
                                    $this->menu[$hookName] .= $this->makeMenu($hookName, explode(',', $col['settings']));
                                    $this->menu[$hookName] .= '</ul>';
                                    $this->menu[$hookName] .= '</div>';
                                    $sp_class = '';
                                }
                            }
                            $this->menu[$hookName] .= '</div>';
                        }
                        $this->menu[$hookName] .= '</div>';
                    }
                }
                $this->menu[$hookName] .= '</li>';
            }
            $this->menu[$hookName] .= '</ul>';
        }
    }

    protected function generateCategoriesMenu($categories)
    {
        $html = '';

        $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://';
        $currentUrl = $protocol . $_SERVER['HTTP_HOST']  . $_SERVER['REQUEST_URI'];

        foreach ($categories as $category) {
            if ($category['level_depth'] > 1) {
                $cat = new Category($category['id_category']);
                $link = Tools::HtmlEntitiesUTF8($cat->getLink());
            } else {
                $link = $this->context->link->getPageLink('index');
            }

            $html .= '<li'.(($this->page_name == 'category'
                && (int)Tools::getValue('id_category') == (int)$category['id_category']) ? ' class="sfHoverForce category"' : ' class="category"').'>';

            if ($link == $currentUrl) {
                $html .= '<span>'.$category['name'].'</span';
            } else {
                $html .= '<a href="'.$link.'" title="'.$category['name'].'">'.$category['name'].'</a>';
            }

            if (isset($category['children']) && !empty($category['children'])) {
                $html .= '<ul>';
                    $html .= $this->generateCategoriesMenu($category['children'], 1);
                $html .= '</ul>';
            }

            $html .= '</li>';
        }

        return $html;
    }

    protected function makeMenu($hookName, $subitems)
    {
        $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://';
        $currentUrl = $protocol . $_SERVER['HTTP_HOST']  . $_SERVER['REQUEST_URI'];

        $id_lang = (int)$this->context->language->id;

        foreach ($subitems as $item) {
            if (!$item) {
                continue;
            }

            preg_match($this->pattern, $item, $value);
            $id = (int)Tools::substr($item, Tools::strlen($value[1]), Tools::strlen($item));

            switch (Tools::substr($item, 0, Tools::strlen($value[1]))) {
                case 'CAT':
                    $this->menu[$hookName] .= $this->generateCategoriesMenu(Category::getNestedCategories($id, $id_lang, true, $this->user_groups));
                    break;

                case 'PRD':
                    $selected = ($this->page_name == 'product' && (Tools::getValue('id_product') == $id)) ? ' class="sfHover product"' : ' class="product"';
                    $product = new Product((int)$id, true, (int)$id_lang);
                    if (!is_null($product->id)) {
                        $this->menu[$hookName] .= '<li'.$selected.'>
                                            <a href="'.Tools::HtmlEntitiesUTF8($product->getLink()).'" title="'.$product->name.'">'.$product->name.'</a>
                                        </li>'.PHP_EOL;
                    }
                    break;

                case 'PRDI':
                    $selected = ($this->page_name == 'product' && (Tools::getValue('id_product') == $id)) ?
                                ' class="sfHover product-info"' :
                                ' class="product-info"';
                    $product = new Product((int)$id, true, (int)$id_lang);
                    if (!is_null($product->id)) {
                        $this->menu[$hookName] .= '<li'.$selected.'>'.$this->generateProductInfo($hookName, $id).'</li>'.PHP_EOL;
                    }
                    break;

                case 'CMS':
                    $selected = ($this->page_name == 'cms' && (Tools::getValue('id_cms') == $id)) ? ' class="sfHover cms-page"' : ' class="cms-page"';
                    $cms = CMS::getLinks((int)$id_lang, array($id));
                    if (count($cms)) {
                        if (Tools::HtmlEntitiesUTF8($cms[0]['link']) == $currentUrl) {
                            $this->menu[$hookName] .= '<li'.$selected.'>
                                            <span>'.Tools::safeOutput($cms[0]['meta_title']).
                                            '</span>
                                        </li>'.PHP_EOL;
                        } else {
                            $this->menu[$hookName] .= '<li'.$selected.'>
                                            <a href="'.Tools::HtmlEntitiesUTF8($cms[0]['link']).'" title="'
                                                .Tools::safeOutput($cms[0]['meta_title']).'">'.Tools::safeOutput($cms[0]['meta_title']).
                                            '</a>
                                        </li>'.PHP_EOL;
                        }
                    }
                    break;

                case 'CMS_CAT':
                    $category = new CMSCategory((int)$id, (int)$id_lang);
                    $selected = ($this->page_name == 'cms' && ((int)Tools::getValue('id_cms_category') == $category->id)) ?
                                ' class="sfHoverForce cms-category"' :
                                ' class="cms-category"';
                    if (count($category)) {
                        $this->menu[$hookName] .= '<li'.$selected.'>
                            <a href="'.Tools::HtmlEntitiesUTF8($category->getLink()).'" title="'.$category->name.'">'.$category->name.'</a>';
                            $this->getCMSMenuItems($hookName, $category->id);
                        $this->menu[$hookName] .= '</li>'.PHP_EOL;
                    }
                    break;

                case 'ALLMAN':
                    $link = new Link;
                    $this->menu[$hookName] .= '<li class="all-manufacturers">
                                        <a href="'.$link->getPageLink('manufacturer').'" title="'.$this->l('All manufacturers').'">'.$this->l('All manufacturers').'</a>
                                        <ul>'.PHP_EOL;
                    $manufacturers = Manufacturer::getManufacturers();
                    foreach ($manufacturers as $manufacturer) {
                        $selected = ($this->page_name == 'manufacturer' && (Tools::getValue('id_supplier') == (int)$manufacturer['id_manufacturer'])) ?
                                    ' class="sfHoverForce manufacturer"' :
                                    ' class="manufacturer"';
                        $this->menu[$hookName] .= '<li'.$selected.'>
                                            <a href="'.$link->getManufacturerLink((int)$manufacturer['id_manufacturer'], $manufacturer['link_rewrite']).'"
                                                title="'.Tools::safeOutput($manufacturer['name']).'">'.Tools::safeOutput($manufacturer['name']).'</a>
                                        </li>'.PHP_EOL;
                    }
                    $this->menu[$hookName] .= '</ul>';
                    break;

                case 'MAN':
                    $selected = ($this->page_name == 'manufacturer' && (Tools::getValue('id_manufacturer') == $id)) ?
                                ' class="sfHover manufacturer"' :
                                ' class="manufacturer"';
                    $manufacturer = new Manufacturer((int)$id, (int)$id_lang);
                    if (!is_null($manufacturer->id)) {
                        if ((int)Configuration::get('PS_REWRITING_SETTINGS')) {
                            $manufacturer->link_rewrite = Tools::link_rewrite($manufacturer->name);
                        } else {
                            $manufacturer->link_rewrite = 0;
                        }
                        $link = new Link;
                        $this->menu[$hookName] .= '<li'.$selected.'>
                                            <a href="'.Tools::HtmlEntitiesUTF8($link->getManufacturerLink((int)$id, $manufacturer->link_rewrite)).'"
                                                title="'.Tools::safeOutput($manufacturer->name).'">'.Tools::safeOutput($manufacturer->name).'</a>
                                            </li>'.PHP_EOL;
                    }
                    break;

                case 'ALLSUP':
                    $link = new Link;
                    $this->menu[$hookName] .= '<li class="all-suppliers">
                                        <a href="'.$link->getPageLink('supplier').'" title="'.$this->l('All suppliers').'">'.$this->l('All suppliers').'</a>
                                        <ul>'.PHP_EOL;
                    $suppliers = Supplier::getSuppliers();
                    foreach ($suppliers as $supplier) {
                        $selected = ($this->page_name == 'supplier' && (Tools::getValue('id_supplier') == (int)$supplier['id_supplier'])) ?
                                    ' class="sfHoverForce supplier"' :
                                    ' class="supplier"';
                        $this->menu[$hookName] .= '<li'.$selected.'>
                                            <a href="'.$link->getSupplierLink((int)$supplier['id_supplier'], $supplier['link_rewrite']).'"
                                            title="'.Tools::safeOutput($supplier['name']).'">'.Tools::safeOutput($supplier['name']).'</a>
                                        </li>'.PHP_EOL;
                    }
                    $this->menu[$hookName] .= '</ul>';
                    break;

                case 'SUP':
                    $selected = ($this->page_name == 'supplier' && (Tools::getValue('id_supplier') == $id)) ?
                                ' class="sfHover supplier"' :
                                ' class="supplier"';
                    $supplier = new Supplier((int)$id, (int)$id_lang);
                    if (!is_null($supplier->id)) {
                        $link = new Link;
                        $this->menu[$hookName] .= '<li'.$selected.'>
                                            <a href="'.Tools::HtmlEntitiesUTF8($link->getSupplierLink((int)$id, $supplier->link_rewrite)).'"
                                            title="'.$supplier->name.'">'.$supplier->name.'</a>
                                        </li>'.PHP_EOL;
                    }
                    break;

                case 'SHOP':
                    $selected = ($this->page_name == 'index' && ($this->context->shop->id == $id)) ? ' class="sfHover shop"' : ' class="shop"';
                    $shop = new Shop((int)$id);
                    if (Validate::isLoadedObject($shop)) {
                        $link = new Link;
                        $this->menu[$hookName] .= '<li'.$selected.'>
                                            <a href="'.Tools::HtmlEntitiesUTF8($shop->getBaseURL()).'" title="'.$shop->name.'">'.$shop->name.'</a>
                                        </li>'.PHP_EOL;
                    }
                    break;

                case 'HTML':
                    $this->menu[$hookName] .= $this->generateCustomHtml($id);
                    break;
                case 'LNK':
                    $this->menu[$hookName] .= $this->generateCustomLink($id);
                    break;
                case 'BNR':
                    $this->menu[$hookName] .= $this->generateBanner($hookName, $id);
                    break;
                case 'VID':
                    $this->menu[$hookName] .= $this->generateVideo($hookName, $id);
                    break;
                case 'MAP':
                    $this->menu[$hookName] .= $this->generateMap($hookName, $id);
                    break;
            }
        }
    }

    protected function generateCustomLink($id_item)
    {
        $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://';
        $currentUrl = $protocol . $_SERVER['HTTP_HOST']  . $_SERVER['REQUEST_URI'];

        $output = '';
        $link = new MegaMenuLink((int)$id_item);
        if ($link) {
            if ($link->url[(int)$this->context->language->id] == $currentUrl) {
            $output .= '<li '.($link->specific_class?'class="'.$link->specific_class.' custom-link"':'class="custom-link"').'>
                            <span>'
                                .$link->title[(int)$this->context->language->id].
                            '</span>
                        </li>';
            } else {
                $output .= '<li '.($link->specific_class?'class="'.$link->specific_class.' custom-link"':'class="custom-link"').'>
                            <a '.($link->blank?'target="_blank"':'').' href="'.$link->url[(int)$this->context->language->id].'">'
                                .$link->title[(int)$this->context->language->id].
                            '</a>
                        </li>';
            }
        }
        return $output;
    }
}
