<?php
/**
 * PrestaShop PS IT Solution Module
 *
 * @author    PS IT Solution http://www.psitsolution.com/
 * @copyright 2010-2017 PS IT Solution
 * @license   This software is distributed under MIT License
 *
 * CONTACT US http://psitsolution.com
 * info@psitsolution.com
 */
use PrestaShop\PrestaShop\Adapter\Cart\CartPresenter;
class CartController extends CartControllerCore
{
    
    /*
    * module: psitclean
    * date: 2018-05-14 16:41:10
    * version: 2.0.3
    */
    public function displayAjaxProductRefresh()
    {
        if ($this->id_product)
        {
          $url = $this->context->link->getProductLink(
              $this->id_product,
              null,
              null,
              null,
              $this->context->language->id,
              null,
              (int)Product::getIdProductAttributesByIdAttributes($this->id_product, Tools::getValue('group'), true),
              false,
              false,
              true,
              ['quantity_wanted' => (int)$this->qty]
          );
          $url .= (parse_url($url, PHP_URL_QUERY) ? '&' : '?') . 'idpa=' . (int)Product::getIdProductAttributesByIdAttributes($this->id_product, Tools::getValue('group'), true);
        }
        else
        {
            $url = false;
        }
        ob_end_clean();
        header('Content-Type: application/json');
        $this->ajaxDie(Tools::jsonEncode([
            'success' => true,
            'productUrl' => $url
        ]));
    }
}
?>
