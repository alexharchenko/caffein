<?php

class OrderConfirmationController extends OrderConfirmationControllerCore
{
	public function displayOrderConfirmation($order)
    {
        session_start();
        if (isset($_SESSION['confirmedOrders'])) {
            if (in_array($order->id, $_SESSION['confirmedOrders'])) {
                $this->context->smarty->assign(array(
                    'displayOrderConfirmedMessage' => false
                ));
            } else {
                $this->context->smarty->assign(array(
                    'displayOrderConfirmedMessage' => true
                ));
                $_SESSION['confirmedOrders'][] = $order->id;
            }
        } else {
            $this->context->smarty->assign(array(
                'displayOrderConfirmedMessage' => true
            ));
            $_SESSION['confirmedOrders'] = array($order->id);
        }

        return parent::displayOrderConfirmation($order);
    }
}