<?php
/**
 * PrestaShop PS IT Solution Module
 *
 * @author    PS IT Solution http://www.psitsolution.com/
 * @copyright 2010-2017 PS IT Solution
 * @license   This software is distributed under MIT License
 *
 * CONTACT US http://psitsolution.com
 * info@psitsolution.com
 */
 use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery;
 use PrestaShop\PrestaShop\Core\Product\Search\SortOrder;
 use PrestaShop\PrestaShop\Adapter\Manufacturer\ManufacturerProductSearchProvider;
class ManufacturerController extends ManufacturerControllerCore
{
    /*
    * module: psitclean
    * date: 2018-05-14 16:41:12
    * version: 2.0.3
    */
    public function init()
    {
        {
            $link_pattern = Tools::safeOutput(urldecode(Tools::getValue('manufacturer_rewrite')));
            $manufacturer_pattern = '@.*?([0-9]+)\_([/_a-zA-Z0-9\pL\pS-]*)@';
            preg_match($manufacturer_pattern, $_SERVER['REQUEST_URI'], $Array);
            if (isset($Array[2]) && $Array[2] != "")
            {
                $link_pattern = $Array[2];
            }
            if ($link_pattern)
            {
              $sqlall = 'SELECT * FROM `' . _DB_PREFIX_ . 'manufacturer` m
          LEFT JOIN `' . _DB_PREFIX_ . 'manufacturer_shop` s ON (m.`id_manufacturer` = s.`id_manufacturer`) WHERE 1=1 ';
              if (Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_SHOP){
                  $sqlall .= ' AND s.`id_shop` = ' . (int)Shop::getContextShopID();
              }
                $allmanufacturers = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sqlall);
                foreach ($allmanufacturers as $key => $manufacturer)
                {
                    if ($link_pattern == Tools::str2url($manufacturer['name']))
                    {
                        $id_manufacturer = $manufacturer['id_manufacturer'];
                    }
                }
                if ($id_manufacturer != "")
                {
                    $_POST['id_manufacturer'] = $id_manufacturer;
                    $_GET['id_manufacturer'] = $id_manufacturer;
                    $_GET['manufacturer_rewrite'] = '';
                }
            }
            parent::init();
        }
    }
}
