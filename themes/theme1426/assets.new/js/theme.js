/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);


/***/ }),
/* 1 */
/***/ (function(module, exports) {

	/**
	 * 2007-2016 PrestaShop
	 *
	 * NOTICE OF LICENSE
	 *
	 * This source file is subject to the Open Software License (OSL 3.0)
	 * that is bundled with this package in the file LICENSE.txt.
	 * It is also available through the world-wide-web at this URL:
	 * http://opensource.org/licenses/osl-3.0.php
	 * If you did not receive a copy of the license and are unable to
	 * obtain it through the world-wide-web, please send an email
	 * to license@prestashop.com so we can send you a copy immediately.
	 *
	 * DISCLAIMER
	 *
	 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
	 * versions in the future. If you wish to customize PrestaShop for your
	 * needs please refer to http://www.prestashop.com for more information.
	 *
	 * @author    PrestaShop SA <contact@prestashop.com>
	 * @copyright 2007-2016 PrestaShop SA
	 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
	 * International Registered Trademark & Property of PrestaShop SA
	 */
	import 'expose?Tether!tether';
	import 'bootstrap/dist/js/bootstrap.min';
	import "swiper/dist/js/swiper.jquery.min";
	import 'flexibility';
	import 'bootstrap-touchspin';
	
	import '../css/theme';
	import './responsive';
	import './checkout';
	import './customer';
	import './listing';
	import './product';
	import './cart';
	
	import DropDown from './components/drop-down';
	import Form from './components/form';
	import ProductMinitature from './components/product-miniature';
	import ProductSelect from './components/product-select';
	import TopMenu from './components/top-menu';
	
	import prestashop from 'prestashop';
	import EventEmitter from 'events';
	
	import './lib/bootstrap-filestyle.min';
	import './lib/jquery.scrollbox.min';
	
	import './components/block-cart';
	
	// "inherit" EventEmitter
	for (var i in EventEmitter.prototype) {
	  prestashop[i] = EventEmitter.prototype[i];
	}
	
	$(document).ready(() => {
	  let dropDownEl = $('.js-dropdown');
	  const form = new Form();
	  let topMenuEl = $('.js-top-menu ul[data-depth="0"]');
	  let dropDown = new DropDown(dropDownEl);
	  let topMenu = new TopMenu(topMenuEl);
	  let productMinitature = new ProductMinitature();
	  let productSelect = new ProductSelect();
	  dropDown.init();
	  form.init();
	  topMenu.init();
	  productMinitature.init();
	  productSelect.init();
	});

/***/ })
/******/ ]);
//# sourceMappingURL=theme.js.map