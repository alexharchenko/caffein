/*
 * Custom code goes here.
 * A template should always ship with an empty custom.js
 */
function n(e){(0,s.default)("#search_filters").replaceWith(e.rendered_facets),(0,s.default)("#js-active-search-filters").replaceWith(e.rendered_active_filters),(0,s.default)("#js-product-list-top").replaceWith(e.rendered_products_top),(0,s.default)("#js-product-list").replaceWith(e.rendered_products),(0,s.default)("#js-product-list-bottom").replaceWith(e.rendered_products_bottom);var t=new d.default;t.init();my_slider()}
$(document).ready(function($) {
    var products_carousel = new Swiper('.products-carousel',{
        slidesPerView: 4,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerGroup: 1
    });
});
