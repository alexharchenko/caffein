{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
 {literal}
<!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution=setup_tool
  page_id="184673708607880">
</div>
 {/literal}
<div class="footer-container">
  <div class="container">
    <div class="footer-before">
      {hook h='displayFooterBefore'}
    </div>
    <div class="footer-main">
      {hook h='displayFooter'}
    </div>
    <div class="footer-bottom">
      <div class="row">
        <div class="col-md-9">
            {l s='%copyright% 2007 &mdash; %year%' sprintf=['%year%' => 'Y'|date, '%copyright%' => '©'] d='Shop.Theme'}
        </div>
        <div class="col-md-3 text-right">
          <div class="small">
            Разработка и поддержка сайта - <a href="http://klookva.com.ua">kLooKva</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{literal}
<noindex>
<link rel="stylesheet" href="//cabinet.salesupwidget.com/widget/tracker.css">
<script type="text/javascript" src="//cabinet.salesupwidget.com/php/1.js" id="salesupwidget1js" data-uid="1035" charset="UTF-8" async></script>
</noindex>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/ru_RU/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
{/literal}