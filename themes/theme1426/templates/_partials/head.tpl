{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">

{block name='head_seo'}
  <title>{block name='head_seo_title'}{$page.meta.title}{/block}</title>
  <meta name="description" content="{block name='head_seo_description'}{$page.meta.description}{/block}">
  <meta name="keywords" content="{block name='head_seo_keywords'}{$page.meta.keywords}{/block}">
  {if $page.meta.robots !== 'index'}
    <meta name="robots" content="{$page.meta.robots}">
  {/if}
  {if $page.canonical && $page.page_name !== "category"}
    <link rel="canonical" href="{$page.canonical}">
  {/if}
{/block}

<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="icon" type="image/vnd.microsoft.icon" href="{$shop.favicon}?{$shop.favicon_update_time}">
<link rel="shortcut icon" type="image/x-icon" href="{$shop.favicon}?{$shop.favicon_update_time}">

{block name='stylesheets'}
  {include file="_partials/stylesheets.tpl" stylesheets=$stylesheets}
{/block}

{block name='javascript_head'}
  {include file="_partials/javascript.tpl" javascript=$javascript.head vars=$js_custom_vars}
{/block}

{block name='hook_header'}
  {$HOOK_HEADER nofilter}
{/block}
{literal}
<script>
  window.dataLayer = window.dataLayer || []
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PMGBHFV');</script>
<!-- End Google Tag Manager -->
{/literal}
{literal}
<script>var amo_social_button = {id: 4380, hash: "f9f7d0cbebf86b9ebd7e8a27e50290cca8e9625d8d06372d8b665594f21d95e0", locale: "ru"};</script><script id="amo_social_button_script" async="async" src="https://gso.amocrm.ru/js/button.js"> </script>
{/literal}

{* BEGIN Posternak *}
{block name='head_canonical'}
{/block}

{if $page.page_name == 'index'}
<script type="application/ld+json">
{literal}{{/literal}
  "@context": "http://schema.org",
  "@type": "Organization",
  "url": "{$urls.shop_domain_url}",
  "logo": "{$urls.shop_domain_url}{$shop.logo}"
{literal}}{/literal}
</script>
{/if}
{if $page.page_name == 'cms' && $cms.id == 6}
<script type="application/ld+json">
{literal}{{/literal}
  "@context": "http://schema.org",
  "@type": "Course",
  "name": "Школа бариста",
  "description": "Если Вы хотите повысить свой квалификационный уровень и стать мастером своего дела - приглашаем пройти наши курсы бариста. В процессе занятий Вы узнаете массу интересных нюансов и профессиональных секретов настоящих гуру кофейных традиций. Подобный опыт нарабатывают многие годы. Наши тренера готовы поделиться с Вами опытом на однодневном курсе в школе Caffein.",
  "provider": {literal}{{/literal}
    "@type": "Organization",
    "name": "{$shop.name}",
    "sameAs": "{$urls.shop_domain_url}"
  {literal}}{/literal}
{literal}}{/literal}
</script>
{/if}
<link rel="author" href="https://plus.google.com/u/0/115205397490224301799">

{if isset($listing) && is_array($listing)}
  {foreach from=$listing.pagination.pages item="page"}
    {if $page.type !== 'spacer' && $page.url !== $urls.current_url}
      {if $page.type === 'previous' && strpos($urls.current_url, 'page=')}

      <link rel="prev" href="{$page.url}"/>
      {elseif $page.type === 'next'}
      <link rel="next" href="{$page.url}"/>
      {/if}
    {/if}
  {/foreach}
{/if}
{if stripos($urls.current_url, '/tmsearch?') || stripos($urls.current_url, '/search?')}
<meta name="robots" content="noindex"/>
{/if}
{* END Posternak *}

<script type="text/javascript">
  (function(d, w, s) {
  var widgetHash = 'fe10x656shjmwpb3quja', gcw = d.createElement(s); gcw.type = 'text/javascript'; gcw.async = true;
  gcw.src = '//widgets.binotel.com/getcall/widgets/'+ widgetHash +'.js';
  var sn = d.getElementsByTagName(s)[0]; sn.parentNode.insertBefore(gcw, sn);
  })(document, window, 'script');
</script>
