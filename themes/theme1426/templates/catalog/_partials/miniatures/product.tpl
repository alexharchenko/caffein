{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<article class="col-xs-6 col-md-4 col-xl-4 product-miniature js-product-miniature swiper-slide" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}">
  <a name="product{{$product.id_product}}"></a>
  <div class="thumbnail-container" itemscope itemtype="http://schema.org/Product">
    <div class="product-heading">
      
      <div class="quick-view-wrap hidden-xs-down">
        <a
          href="#"
          class="quick-view"
          data-link-action="quickview"
          >
          <i class="material-icons search">&#xE8B6;</i>
        </a>
      </div>
    </div>
    {block name='product_thumbnail'}
      <div class="product_image">
        <a href="{$product.url}" class="thumbnail product-thumbnail" onclick="EEproductClick({$product.id_product})">
          <img
            itemprop="image"
            class="img-fluid"
            src = "{$product.cover.bySize.home_default.url}"
            alt = "{$product.cover.legend}"
            data-full-size-image-url = "{$product.cover.large.url}"
          >
        </a>
        {block name='product_flags'}
          <ul class="product-flags hidden-xs-down">
            {foreach from=$product.flags item=flag}
              <li class="{$flag.type}">{$flag.label}</li>
            {/foreach}
          </ul>
        {/block}
      </div>
    {/block}
    <div class="product-heading text-center">
      {block name='product_name'}
        <div class="h1 product-title" itemprop="name">
          <a href="{$product.url}" onclick="EEproductClick({$product.id_product})">{$product.name|truncate:100:'...'}</a>
        </div>
      {/block}
    </div>
    <div class="product-taste-info">
        {foreach from=$product.features item=feature}
        {if $feature.url_name|strpos:"kl-dot-" !== false}
          <div>
               <span>{$feature.name}</span>
                <form action="#">
                  <fieldset class="rating" value="{$feature.value}">
                    <input type="radio" id="{$feature.url_name}-star5" name="rating" value="5" {if $feature.value eq 5} checked {/if} /><label class = "full" for="{$feature.url_name}-star5"></label>
                    <input type="radio" id="{$feature.url_name}-star4" name="rating" value="4" {if $feature.value eq 4} checked {/if} /><label class = "full" for="{$feature.url_name}-star4"></label>
                    <input type="radio" id="{$feature.url_name}-star3" name="rating" value="3" {if $feature.value eq 3} checked {/if} /><label class = "full" for="{$feature.url_name}-star3"></label>
                    <input type="radio" id="{$feature.url_name}-star2" name="rating" value="2" {if $feature.value eq 2} checked {/if} /><label class = "full" for="{$feature.url_name}-star2"></label>
                    <input type="radio" id="{$feature.url_name}-star1" name="rating" value="1" {if $feature.value eq 1} checked {/if} /><label class = "full" for="{$feature.url_name}-star1"></label>
                  </fieldset>
                </form>
          </div>
        {/if}
      {/foreach}
    </div>
    <div class="product-description">
      {block name='product_price_and_shipping'}
        <div class="product-pricing-data">
        {if $product.show_price}
          {if $product.min_price_for_product && $product.max_price_for_product}
            <div class='product-weight'>
              {if $product.min_price_for_product != $product.max_price_for_product}<label>{l s='250 гр.' d='Shop.Theme.Catalog'}</label>{/if}
              <div class="product-price-and-shipping">
                {hook h='displayProductPriceBlock' product=$product type="before_price"}

                <span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                  <meta itemprop="priceCurrency" content="{$currency.iso_code}" />
                  <span itemprop="price" class="price" content="{$product.price_amount}">{round($product.min_price_for_product)} {l s='грн.' d='Shop.Theme.Catalog'}</span>
                  {if $product.has_discount}
                    {hook h='displayProductPriceBlock' product=$product type="old_price"}
                </span>

                  <span class="regular-price">{$product.regular_price}</span>
                  {if $product.discount_type === 'percentage'}
                    <span class="discount-percentage">{$product.discount_percentage}</span>
                  {/if}
                {/if}

                {hook h='displayProductPriceBlock' product=$product type='unit_price'}

                {hook h='displayProductPriceBlock' product=$product type='weight'}
              </div>
            </div>
            {if $product.min_price_for_product != $product.max_price_for_product}
              <div class="product-weight">
                <label>{l s='1 кг.' d='Shop.Theme.Catalog'}</label>
                <div class="product-price-and-shipping">
                  {hook h='displayProductPriceBlock' product=$product type="before_price"}

                  <span>
                    <span class="price">{round($product.max_price_for_product)} {l s='грн.' d='Shop.Theme.Catalog'}</span>
                    {if $product.has_discount}
                      {hook h='displayProductPriceBlock' product=$product type="old_price"}
                  </span>

                    <span class="regular-price">{$product.regular_price}</span>
                    {if $product.discount_type === 'percentage'}
                      <span class="discount-percentage">{$product.discount_percentage}</span>
                    {/if}
                  {/if}

                  {hook h='displayProductPriceBlock' product=$product type='unit_price'}

                  {hook h='displayProductPriceBlock' product=$product type='weight'}
                </div>
              </div>
            {/if}
          {else}
            <div class="product-price-and-shipping">
              {hook h='displayProductPriceBlock' product=$product type="before_price"}

              <span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                <meta itemprop="priceCurrency" content="{$currency.iso_code}" />
                <span itemprop="price" class="price" content="{$product.price_amount}">{$product.price}</span>
                {if $product.has_discount}
                  {hook h='displayProductPriceBlock' product=$product type="old_price"}
              </span>

                <span class="regular-price">{$product.regular_price}</span>
                {if $product.discount_type === 'percentage'}
                  <span class="discount-percentage">{$product.discount_percentage}</span>
                {/if}
              {/if}

              {hook h='displayProductPriceBlock' product=$product type='unit_price'}

              {hook h='displayProductPriceBlock' product=$product type='weight'}
            </div>
          {/if}
        {/if}
        </div>
      {/block}

      

      <div class="product-add-to-cart">
        {if !$configuration.is_catalog && {$product.minimal_quantity} < {$product.quantity}}
          <form action="{$urls.pages.cart}" method="post" id="add-to-cart-or-refresh">
            <div class="product-quantity" style="display:none;">
              <input type="hidden" name="token" value="{$static_token}">
              <input type="hidden" name="id_product" value="{$product.id_product}" id="product_page_product_id">
              <input type="hidden" name="id_customization" value="0" id="product_customization_id">
              <input type="hidden" name="qty" id="quantity_wanted" value="1" class="input-group"  min="1"  />
            </div>
            <a href="javascript:void(0);" class="ajax_add_to_cart_button add-to-cart btn- btn-primary- btn-sm-" data-button-action="add-to-cart" onclick="EEaddToCart ({$product.id_product}, 1);">
              <i class="material-icons">&#xE8CB;</i>
              <!-- {l s='Buy' d='Shop.Theme.Catalog'} -->
            </a>
          </form>
        {/if}
      </div>
    </div>
  </div>
</article>
