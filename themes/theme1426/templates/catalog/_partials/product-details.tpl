<div class="tab-pane fade{if $product.description} in active{/if}"
     id="product-details"
     data-product="{$product.embedded_attributes|json_encode}"
  >
  {block name='product_reference'}
    {if isset($product_manufacturer->id)}
      <div class="product-manufacturer">
        {if isset($manufacturer_image_url)}
          <a href="{$product_brand_url}">
            <img src="{$manufacturer_image_url}" class="img img-thumbnail manufacturer-logo" />
          </a>
        {else}
          <label class="label">{l s='Brand' d='Shop.Theme.Catalog'}</label>
          <span>
            <a href="{$product_brand_url}">{$product_manufacturer->name}</a>
          </span>
        {/if}
      </div>
    {/if}
    {if isset($product.reference_to_display)}
      <div class="product-reference">
        <label class="label">{l s='Reference' d='Shop.Theme.Catalog'} </label>
        <span itemprop="sku">{$product.reference_to_display}</span>
      </div>
    {/if}
    {/block}
    {block name='product_quantities'}
      {if $product.show_quantities}
        <div class="product-quantities">
          <label class="label">{l s='In stock' d='Shop.Theme.Catalog'}</label>
          <span>{$product.quantity} {$product.quantity_label}</span>
        </div>
      {/if}
    {/block}
    {block name='product_availability_date'}
      {if $product.availability_date}
        <div class="product-availability-date">
          <label>{l s='Availability date:' d='Shop.Theme.Catalog'} </label>
          <span>{$product.availability_date}</span>
        </div>
      {/if}
    {/block}
    {block name='product_out_of_stock'}
      <div class="product-out-of-stock">
        {hook h='actionProductOutOfStock' product=$product}
      </div>
    {/block}

    {block name='product_features'}
      {if $product.features}
        <section class="product-features">
          <!--<h3 class="h2">{l s='Data sheet' d='Shop.Theme.Catalog'}</h3>-->
          <dl class="data-sheet">
            {foreach from=$product.features item=feature}
              <dt class="name">{$feature.name}</dt>
              {if $feature.url_name|strpos:"kl-dot-" !== false}
                <dd class="value">
                  <form action="#">
                    <fieldset class="rating" value="{$feature.value}">
                      <input type="radio" id="{$feature.url_name}-star5" name="rating" value="5" {if $feature.value eq 5} checked {/if} /><label class = "full" for="{$feature.url_name}-star5" title="5 stars"></label>
                      <input type="radio" id="{$feature.url_name}-star4" name="rating" value="4" {if $feature.value eq 4} checked {/if} /><label class = "full" for="{$feature.url_name}-star4" title="4 stars"></label>
                      <input type="radio" id="{$feature.url_name}-star3" name="rating" value="3" {if $feature.value eq 3} checked {/if} /><label class = "full" for="{$feature.url_name}-star3" title="3 stars"></label>
                      <input type="radio" id="{$feature.url_name}-star2" name="rating" value="2" {if $feature.value eq 2} checked {/if} /><label class = "full" for="{$feature.url_name}-star2" title="2 stars"></label>
                      <input type="radio" id="{$feature.url_name}-star1" name="rating" value="1" {if $feature.value eq 1} checked {/if} /><label class = "full" for="{$feature.url_name}-star1" title="1 star"></label>
                    </fieldset>
                  </form>
                </dd>
              {else}
                <dd class="value">{$feature.value}</dd>
              {/if}
            {/foreach}
          </dl>
        </section>
      {/if}
    {/block}

    {* if product have specific references, a table will be added to product details section *}
    {block name='product_specific_references'}
      {if isset($product.specific_references)}
        <section class="product-features">
          <h3 class="h6">{l s='Specific References' d='Shop.Theme.Catalog'}</h3>
            <dl class="data-sheet">
              {foreach from=$product.specific_references item=reference key=key}
                <dt class="name">{$key}</dt>
                <dd class="value">{$reference}</dd>
              {/foreach}
            </dl>
        </section>
      {/if}
    {/block}

    {block name='product_condition'}
      {if $product.condition}
        <div class="product-condition">
          <label class="label">{l s='Condition' d='Shop.Theme.Catalog'} </label>
          <link itemprop="itemCondition" href="{$product.condition.schema_url}"/>
          <span>{$product.condition.label}</span>
        </div>
      {/if}
    {/block}
</div>
