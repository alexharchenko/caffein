{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
  <div id="search_filters">

    {foreach from=$facets item="facet"}
      {if $facet.displayed}
        {assign var=_collapse2 value=true}
        {foreach from=$facet.filters item="filter"}
          {if $filter.active}
            {assign var=_collapse2 value=false}
          {/if}
        {/foreach}
        {if !$_collapse2}
          {break}
        {/if}
      {/if}
    {/foreach}
    {$count = 0}
    {if isset($smarty.post.q)}
      {$arr_qs = explode('/', $smarty.post.q)}
      {foreach $arr_qs as $key => $arr_q}
        {$arr_q = str_replace("%", "%25", $arr_q)}
        {$arr_q = str_replace(",", "%2C", $arr_q)}
        {$arr_qs[$key] = explode('-', $arr_q)}
        {$count = $count + count($arr_qs[$key]) - 1}
      {/foreach}
    {/if}

    <div class="filter-wrapper collapse {if isset($_collapse2) && !$_collapse2} in{/if}" id="collapseFilter">
      <div class="filter-inner">
        {foreach from=$facets item="facet"}
          {if $facet.displayed}
            <section class="facet">
              <div class="h6 facet-title">{$facet.label}</div>
                {if $facet.type === 'price'}
                  <div class="row">
                  <div class="col-md-6">
                    <div class="input-group rounded">
                      <input type="text" class="form-control rounded" id="price-from" value="{$facet.properties.min}" name="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-group rounded">
                      <input type="text" class="form-control rounded" id="price-to" value="{$facet.properties.max}" name="">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div id="slider-range"></div>
                  </div>
                </div>
                <script type="text/javascript">
                window.addEventListener('load', function (e) {
                  var from_p = $('#price-from');
                  var to_p = $('#price-to');
                  var flag_f;
                  var flag_t;

                  from_p.keydown(function () {
                    flag_f = from_p.val().length;
                  });

                  from_p.keyup(function(e) {
                    e = e || event;
                    if (e.keyCode == 13) {
                      if (from_p.val().length != flag_f) {
                        return;
                      } else {
                        urlPriceChange();
                      }
                    }
                  });

                  to_p.keydown(function () {
                    flag_t = to_p.val().length;
                  });

                  to_p.keyup(function(e) {
                    e = e || event;
                    if (e.keyCode == 13) {
                      if (to_p.val().length != flag_t) {
                        return;
                      } else {
                        urlPriceChange();
                      }
                    }
                  });
                  my_slider();
                });
                function urlPriceChange() {
                  var s = '{$facet.filters[0].nextEncodedFacetsURL}';
                  var q_q = s.split('/q/');
                  arr_s = q_q[1].split('/');
                  var obj = {};
                  for (var j=0;j<arr_s.length;j++) {
                    var str = arr_s[j];
                    obj[str] = true;
                  }
                  arr_s = Object.keys(obj);
                  s = q_q[0]+'/q/'+arr_s.join('/');
                  
                  if (arr_s[arr_s.length-1].indexOf('tsena-') == 0) {
                    s = s.replace(/tsena\-.*$/g,'tsena-{$facet.filters[0].properties.symbol}f'+$( "#price-from" ).val()+'t'+$( "#price-to" ).val());
                  } else {
                    s = s.replace(/tsena\-.*?\//g,'tsena-{$facet.filters[0].properties.symbol}f'+$( "#price-from" ).val()+'t'+$( "#price-to" ).val()+'/');
                  }
                  
                  location.href = s;
                }
                function my_slider(minimum={$facet.properties.min},maximum={$facet.properties.max},range_small={$facet.properties.min},range_big={$facet.properties.max}) {
                  //console.log(minimum+" "+maximum+" "+range_small+" "+range_big);
                  if (typeof rangeFrom_p !== 'undefined') {
                    range_small = rangeFrom_p;
                  }
                  if (typeof rangeTo_p !== 'undefined') {
                    range_big = rangeTo_p;
                  }
                  if (typeof onlyFrom_p !== 'undefined') {
                    minimum = onlyFrom_p;
                  }
                  if (typeof onlyTo_p !== 'undefined') {
                    maximum = onlyTo_p;
                  }
                  $( "#slider-range" ).slider({
                    range: true,
                    min: minimum,
                    max: maximum,
                    values: [ range_small, range_big ],
                    slide: function( event, ui ) {
                      //console.log(ui);
                      $( "#price-from" ).val( ui.values[ 0 ]);
                      $( "#price-to" ).val( ui.values[ 1 ]);
                    },
                    stop: function( event, ui ) {
                      urlPriceChange();
                    }

                  });
                  $( "#price-from" ).val( $( "#slider-range" ).slider( "values", 0 ));
                  $( "#price-to" ).val( $( "#slider-range" ).slider( "values", 1 ));
                }
                function getUrlVars(href)
                {
                  var vars = [], hash;
                  var hashes = href.slice(href.indexOf('?') + 1).split('&');
                  for(var i = 0; i < hashes.length; i++)
                  {
                    hash = hashes[i].split('=');
                    vars.push(hash[0]);
                    vars[hash[0]] = hash[1];
                  }
                  return vars;
                }
                
                  // window.addEventListener('load', function (e) {
                  //   // alert(location.href);
                  //   lhref = location.href;
                  //   regex = /tsena-%E2%82%B4f[0-9]+t[0-9]+/gm;
                  //   // let range_small = 
                  //   // alert(lhref);
                  //   // alert(regex.exec(lhref));
                  //   if (regex.exec(lhref)) {
                  //     regex = /tsena-%E2%82%B4f([0-9]+)t([0-9]+)/gm;
                  //     intervalArray = lhref.replace(regex, ` $1 $2 `).split(" ");
                  //     range_small = intervalArray[1];
                  //     range_big = intervalArray[2];
                  //   } else {
                  //     range_small = {$facet.properties.min};
                  //     range_big = {$facet.properties.max};
                  //   }
                  //   my_slider({$facet.properties.min}, {$facet.properties.max}, range_small, range_big);
                  // });

                  // function my_slider(minimum={$facet.properties.min},maximum={$facet.properties.max},range_small={$facet.properties.min},range_big={$facet.properties.max}) {
                  //   // console.log(minimum+" "+maximum+" "+range_small+" "+range_big);
                        
                  //   $( "#slider-range" ).slider({
                  //     range: true,
                  //     min: minimum,
                  //     max: maximum,
                  //     values: [ range_small, range_big ],
                  //     slide: function( event, ui ) {
                  //       //console.log(ui);
                  //       $( "#price-from" ).val( ui.values[ 0 ]);
                  //       $( "#price-to" ).val( ui.values[ 1 ]);
                  //     },
                  //     stop: function( event, ui ) {
                  //       var s = '{$facet.filters[0].nextEncodedFacetsURL}';
                  //       // let from = $( "#price-from" ).val();
                  //       // let to = $( "#price-to" ).val();
                  //       // let newPrice = "-₴f" + from + "t" + to;
                  //       // s = s.replace(/-₴f[0-9]+t[0-9]+/gm, newPrice);

                  //       s = s.replace(/\?.*?q=/g, "?q=");
                  //       //s = s.replace(/\&amp;to=.*?&/g, "&");
                  //       s = s.split('-%')[0];
                  //       s += '-{$facet.filters[0].properties.symbol}-'+$( "#price-from" ).val()+'-'+$( "#price-to" ).val();


                  //       $.ajax({
                  //         url: s+'?from-xhr',
                  //         type: "get",
                  //         dataType: "json",
                  //         contentType: "application/json; charset=utf-8",
                  //         // data: {
                  //         //   'min': parseInt(minimum),
                  //         //   'max': parseInt(maximum),
                  //         //   'from': parseInt($( "#price-from" ).val()),
                  //         //   'to': parseInt($( "#price-to" ).val()),
                  //         // },
                  //         success: function(data){
                  //           window.history.pushState(data, undefined, data.current_url);
                  //           // location.href=s;
                  //           //updateProductListDOM(data);
                  //           // $('#search_filters').replaceWith(data.rendered_facets);
                  //           // $('#search_filters a').attr('href', $('#search_filters a').attr('href'));
                  //           $('#search_filters a').each(function(indx){
                  //             let oldHref = $(this).attr('href');
                  //             regex = /tsena-₴f[0-9]+t[0-9]+/gm;
                  //             if (regex.exec(oldHref)) {
                  //               regex = /tsena-₴f([0-9]+)t([0-9]+)/gm;
                  //               newHref = oldHref.replace(regex, 'tsena-₴f' + from + 't' + to);
                  //               $(this).attr('href', newHref);
                  //             }
                  //           });
                  //           $('#js-active-search-filters').replaceWith(data.rendered_active_filters);
                  //           $('#js-product-list-top').replaceWith(data.rendered_products_top);
                  //           $('#js-product-list').replaceWith(data.rendered_products);
                  //           $('#js-product-list-bottom').replaceWith(data.rendered_products_bottom);
                  //           my_slider(parseInt({$facet.properties.min}),parseInt({$facet.properties.max}),parseInt(from),parseInt(to));
                  //           /*let productMinitature = new ProductMinitature();
                  //           productMinitature.init();*/
                  //         }
                  //       });
                  //     }

                  //   });
                  //   $( "#price-from" ).val( $( "#slider-range" ).slider( "values", 0 ));
                  //   $( "#price-to" ).val( $( "#slider-range" ).slider( "values", 1 ));
                  // }
                  // function getUrlVars(href)
                  // {
                  //   var vars = [], hash;
                  //   var hashes = href.slice(href.indexOf('?') + 1).split('&');
                  //   for(var i = 0; i < hashes.length; i++)
                  //   {
                  //     hash = hashes[i].split('=');
                  //     vars.push(hash[0]);
                  //     vars[hash[0]] = hash[1];
                  //   }
                  //   return vars;
                  // }
                </script>
              {elseif $facet.widgetType !== 'dropdown'}
                <ul id="facet_list">
                  {foreach from=$facet.filters item="filter"}
                    {if $filter.displayed}
                      {if $filter.facetLabel == 'Вес'}
                        {$filter.properties = array()}
                      {/if}
                      <li>
                        <label class="facet-label{if $filter.active} active {/if}">
                          {if $facet.multipleSelectionAllowed}
                            <span class="custom-checkbox">
                              <input
                                data-search-url="{$filter.nextEncodedFacetsURL}"
                                type="checkbox"
                                {if $filter.active && $filter.magnitude != 9999999} checked {/if}{if $filter.magnitude == 9999999} disabled{/if}
                              >
                              {if isset($filter.properties.color)}
                                <span class="color" style="background-color:{$filter.properties.color}"></span>
                                {elseif isset($filter.properties.texture)}
                                  <span class="color texture" style="background-image:url({$filter.properties.texture})"></span>
                                {else}
                                <span {if !$js_enabled} class="ps-shown-by-js" {/if}><i class="material-icons checkbox-checked">&#xE5CA;</i></span>
                              {/if}
                            </span>
                          {else}
                            <span class="custom-radio">
                              <input
                                data-search-url="{$filter.nextEncodedFacetsURL}"
                                type="radio"
                                name="filter {$facet.label}"
                                {if $filter.active } checked {/if}
                              >
                              <span {if !$js_enabled} class="ps-shown-by-js" {/if}></span>
                            </span>
                          {/if}
                          {if $filter.magnitude == 9999999}
                          <span class="_gray-darker search-link js-search-link empty">
                            {$filter.label}
                            {if $filter.magnitude}
                              <span class="magnitude">({if $filter.magnitude == 9999999}0{else}{$filter.magnitude}{/if})</span>
                            {/if}
                          </span>
                          {else}
                          <a
                            href="{$filter.nextEncodedFacetsURL}"
                            class="_gray-darker search-link js-search-link"
                            {if $count >= 2}rel="nofollow"{/if}
                          >
                            {$filter.label}
                            {if $filter.magnitude}
                              <span class="magnitude">({if $filter.magnitude == 9999999}0{else}{$filter.magnitude}{/if})</span>
                            {/if}
                          </a>
                          {/if}
                        </label>
                      </li>
                    {/if}
                  {/foreach}
                </ul>
              {else}
                <form>
                  <input type="hidden" name="order" value="{$sort_order}">
                  <select name="q">
                    <option disabled selected hidden>{l s='(no filter)' d='Shop.Theme'}</option>
                    {foreach from=$facet.filters item="filter"}
                      {if $filter.displayed}
                        <option
                          {if $filter.active}
                            selected
                            value="{$smarty.get.q}"
                          {else}
                            value="{$filter.nextEncodedFacets}"
                          {/if}
                        >
                          {$filter.label}
                          {if $filter.magnitude}
                            ({$filter.magnitude})
                          {/if}
                        </option>
                      {/if}
                    {/foreach}
                  </select>
                  {if !$js_enabled}
                    <button class="ps-hidden-by-js" type="submit">
                      {l s='Filter' d='Shop.Theme.Actions'}
                    </button>
                  {/if}
                </form>
              {/if}
            </section>
          {/if}
        {/foreach}
      </div>
    </div>
    <div class="h4" data-toggle="collapse" data-target="#collapseFilter">
      {l s='Filter By' d='Shop.Theme.Actions'}
      <span class="clear-all-wrapper">
        <span data-search-url="{$clear_all_link}" class="js-search-filters-clear-all">
          ({l s='Clear all' d='Shop.Theme.Actions'})
        </span>
      </span>
      <i class="down material-icons">&#xE5C5;</i>
      <i class="up material-icons">&#xE5C7;</i>
    </div>
  </div>

