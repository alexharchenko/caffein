{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file=$layout}

{block name='head_canonical'}
  {if isset($smarty.post.q)}
    {$arr_qs = explode('/', $smarty.post.q)}
    {$count = 0}
    {foreach $arr_qs as $key => $arr_q}
      {$arr_q = str_replace("%", "%25", $arr_q)}
      {$arr_q = str_replace(",", "%2C", $arr_q)}
      {$arr_qs[$key] = explode('-', $arr_q)}
      {$count = $count + count($arr_qs[$key]) - 1}
    {/foreach}
    {if $count >= 2}
      <link rel="canonical" href="{$page.canonical}/q/{$arr_qs[0][0]}-{$arr_qs[0][1]}{if isset($arr_qs[0][2])}-{$arr_qs[0][2]}{else}/{$arr_qs[1][0]}-{$arr_qs[1][1]}{/if}">
    {elseif $count == 1}
      {if !is_bool(strpos($smarty.post.q, 'kategorii-'))}
        {$p = preg_match('/kategorii-([0-9a-zA-Z%_+.]+)/', $smarty.post.q, $matches)}
        {$rewrite = str_replace('.', '-', $matches[1])}
        <link rel="canonical" href="{$urls.shop_domain_url}/{$rewrite}">
      {elseif $smarty.post.q == 'pomol+zerna-zerno'}
	<link rel="canonical" href="{$urls.base_url}content/kofe-v-zernakh">
      {else}
        <link rel="canonical" href="{$page.canonical}/q/{$arr_qs[0][0]}-{$arr_qs[0][1]}">
      {/if}
    {/if}
  {else}
    <link rel="canonical" href="{$page.canonical}">
  {/if}
<script type="application/ld+json">
{literal}{{/literal}
  "@context":"http://schema.org",
  "@type":"ItemList",
  "itemListElement":[
  {foreach from=$listing.products item="product" name="carousel"}
    {literal}{{/literal}
      "@type":"ListItem",
      "position":{$smarty.foreach.carousel.iteration},
      "item": {literal}{{/literal}
          "@type": "Product",
          "url": "{$urls.current_url}#product{$product.id_product}",
          "name": "{$product.name}",
          "image": "{$product.cover.large.url}"
      {literal}}{/literal}
    {literal}}{/literal}{if !$smarty.foreach.carousel.last},{/if}
  {/foreach}
  ]
{literal}}{/literal}
</script>
{/block}

{block name='content'}
  <section id="main">
    {block name='product_list_header'}
      <h2 class="h2">{$listing.label}</h2>
    {/block}
    <section id="products">
      <div class="row">
        {if $listing.rendered_facets !== ''}
        <div class="col-lg-3">

          <div class="block-category">
            {if $category.description}
              <div class="category-cover">
                <img class="img-fluid" src="{$category.image.large.url}" alt="{$category.image.legend}">
              </div>
            {/if}
            <h1>{$category.name}</h1>
          </div>

          {hook h='displayNavFullWidth'}
        </div>
        <div class="col-lg-9">
        {else}
        <div class="col-lg-12">
        {/if}
            {if $listing.products|count}

            <div id="">
              {block name='product_list_top'}
                {include file='catalog/_partials/products-top.tpl' listing=$listing}
              {/block}
            </div>

            {block name='product_list_active_filters'}
              <div id="" class="hidden-sm-down">
                {$listing.rendered_active_filters nofilter}
              </div>
            {/block}
            <div id="">
              {block name='product_list'}
                {include file='catalog/_partials/products.tpl' listing=$listing}
              {/block}
            </div>

            <div id="js-product-list-bottom">
              {block name='product_list_bottom'}
                {include file='catalog/_partials/products-bottom.tpl' listing=$listing}
              {/block}
            </div>

          {else}

            {include file='errors/not-found.tpl'}
          {/if}
        </div>
      </div>
      
    </section>
    {if $category.description }
        <div id="category-description" class="text-muted"><div class="hidden-lg-up">{$category.description|truncate:180:'...' nofilter}</div>
        {foreach from=$listing.pagination.pages item="page"}
          {if $page.current && $page.page == 1}
            <div class="hidden-md-down">{$category.description nofilter}</div>
          {/if}
        {/foreach}
        </div>
      {/if}
  </section>
{/block}

