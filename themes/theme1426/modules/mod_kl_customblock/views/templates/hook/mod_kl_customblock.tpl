{if $elements}
	{foreach from=$elements item=element}
		<div class="col-xs-12 customblock-product-inset">
			<div class="bg-gray-lightest">
				<div class="container">
					<div class="row content-module">
						{if isset($element.image)}
						<div class="col-md-2"><img src="{$element.image}" alt=" "></div>
						{/if}
						<div class="col-md-10">
							<div class="h3">{$element.title}</div>
							{$element.text nofilter}
							<p><a class="" href="{$element.url}">{$element.button_title}</a></p>	
						</div>
						
					</div>
				</div>
			</div>
		</div>
	{/foreach}
{/if}