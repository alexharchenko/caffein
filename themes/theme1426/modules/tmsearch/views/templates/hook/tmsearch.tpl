<div id="_desktop_tmsearch">
	<div id="tmsearch">
		<span class="expand-more" data-toggle="dropdown"><i class="material-icons">&#xE8B6;</i><i class="material-icons close-i">&#xE5CD;</i></span>
		<form id="tmsearchbox" method="get" action="{Tmsearch::getTMSearchLink('tmsearch')|escape:'htmlall':'UTF-8'}" >
			{if !Configuration::get('PS_REWRITING_SETTINGS')}
				<input type="hidden" name="fc" value="module" />
				<input type="hidden" name="controller" value="tmsearch" />
				<input type="hidden" name="module" value="tmsearch" />
			{/if}
			<div class="selector form-control">
				<select name="search_categories" class="form-control">
					{foreach from=$search_categories item=category}
						<option {if $active_category == $category.id}selected="selected"{/if} value="{$category.id|escape:'htmlall':'UTF-8'}">{if $category.id == 2}{l s='All Categories' mod='tmsearch'}{else}{$category.name|escape:'htmlall':'UTF-8'}{/if}</option>
					{/foreach}
				</select>
			</div>
			<input class="tm_search_query form-control" type="text" id="tm_search_query" name="search_query" placeholder="{l s='Search' mod='tmsearch'}" value="{$search_query|escape:'htmlall':'UTF-8'|stripslashes}" />
			<button type="submit" name="tm_submit_search" class="button-search">
				<i class="material-icons">&#xE8B6;</i>
			</button>
		</form>
	</div>
</div>