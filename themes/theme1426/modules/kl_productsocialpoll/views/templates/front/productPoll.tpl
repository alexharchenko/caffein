{if $poll.products}
	{if $poll.poll_photo}
		<img src="{$poll.poll_photo}"  class="item-img " title="" alt="" width="100%" height="100%"/>
	{/if}
	<div class="item-html">
		<div class="content_left">
			<form id="productSocialPoll" class="">
				<div class="h1">{$poll.name}</div>
				<p>{$poll.description}</p>
				<div>
				   {foreach from=$poll.products item=prod}
					<div>
						<input type="radio" name="products-to-vote" value="{$prod.product_id}" id="{$prod.name}">
						<label for="{$prod.name}">{$prod.name}</label>
					</div>
					{/foreach}
				</div>
				    
			</form>
		</div>
		<p class="button-wrap"><button type="submit" id="vote-button" class="btn btn-primary">Голосовать</button></p>
		
	</div>
{/if}