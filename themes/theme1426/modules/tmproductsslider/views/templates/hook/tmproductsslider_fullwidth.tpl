{*
* 2002-2017 TemplateMonster
*
* TM Products Slider
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
* @author     TemplateMonster
* @copyright  2002-2017 TemplateMonster
* @license    http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{*-------Information list-------*}

{$settings.online_only = false}                 {*display "Online only" in slide info*}
{$settings.reference = false}                   {*display "Reference name" in slide info*}
{$settings.new_sale_labels = false}             {*display "New Sale labels" in slide info*}
{$settings.condition = false}                   {*display "Condition" in slide info*}
{$settings.product_name = true}                 {*display "Product name" in slide info*}
{$settings.description_short = false}           {*display "Description short" in slide info*}
{$settings.description = false}                  {*display "Description" in slide info*}
{$settings.manufacturer = false}                 {*display "Manufacturer" in slide info*}
{$settings.supplier = true}                     {*display "Supplier" in slide info*}
{$settings.features = false}                    {*display "Features" in slide info*}
{$settings.prices = true}                       {*display "Prices" in slide info*}
{$settings.quantity = false}                    {*display "Quantity" in slide info*}
{$settings.cart_button = true}                  {*display "Add to cart button" in slide info*}

{*-------and Information list------*}


{if isset($slides) && $slides}
    <div id="tm-products-slider" class="fullwidth swiper-container">
        <div class="swiper-wrapper">
            {foreach from=$slides item=slide}
                <div class="swiper-slide">
                    <div class="slide-inner">
                        {if isset($slide.info_array.images) && $slide.info_array.images}
                            <div class="slide-image">
                                {if ($settings.fullwidth_extended_settings && $settings.fullwidth_images_gallery) || !$settings.fullwidth_extended_settings}
                                    {if isset($slide.info_array.images) && $slide.info_array.images}
                                        <div class="tm-products-slider-inner-{$slide.info_array.id_product|escape:'htmlall':'UTF-8'} swiper-container">
                                            <div class="swiper-wrapper">
                                                {foreach from=$slide.images item=img}
                                                    <div class="swiper-slide">
                                                        <img class="img-fluid" src="{$link->getImageLink($slide.info_array.name, $img.id_image, 'large_default')|escape:'htmlall':'UTF-8'}" alt="{$slide.info_array.name|escape:'htmlall':'UTF-8'}" />
                                                    </div>
                                                {/foreach}
                                            </div>
                                        </div>
                                    {/if}
                                {else}
                                    <a class="slide-image" href="{$slide.info_array.url|escape:'htmlall':'UTF-8'}" title="{$slide.info_array.name|escape:'htmlall':'UTF-8'}">
                                        <img class="img-fluid" src="{$slide.info_array.cover.bySize.large_default.url|escape:'htmlall':'UTF-8'}" alt="{$slide.info_array.name|escape:'htmlall':'UTF-8'}" />
                                    </a>
                                {/if}
                            </div>
                        {/if}
                        <div class="slide-info">
                            {if $slide.info_array.online_only && $settings.online_only}
                                <p class="online_only">{l s='Online only' mod='tmproductsslider'}</p>
                            {/if}
                            {if (!empty($slide.info_array.reference) || $slide.info_array.reference) && $settings.reference}
                                <p id="product_reference">
                                    <label>{l s='Reference:' mod='tmproductsslider'} </label>
                                    <span class="editable" {if !empty($slide.info_array.reference) && $slide.info_array.reference} content="{$slide.info_array.reference|escape:'htmlall':'UTF-8'}"{/if}>{if !isset($groups)}{$slide.info_array.reference|escape:'htmlall':'UTF-8'}{/if}</span>
                                </p>
                            {/if}
                            {if !$slide.info_array.is_virtual && $slide.info_array.embedded_attributes.condition && $settings.condition}
                                <p id="product_condition">
                                    <label>{l s='Condition:' mod='tmproductsslider'} </label>
                                    {if $slide.info_array.embedded_attributes.condition == 'new'}
                                        <span class="editable">{l s='New product' mod='tmproductsslider'}</span>
                                    {elseif $slide.info_array.embedded_attributes.condition == 'used'}
                                        <span class="editable">{l s='Used' mod='tmproductsslider'}</span>
                                    {elseif $slide.info_array.embedded_attributes.condition == 'refurbished'}
                                        <span class="editable">{l s='Refurbished' mod='tmproductsslider'}</span>
                                    {/if}
                                </p>
                            {/if}
                            {if $slide.info_array.price && $slide.info_array.show_price && $settings.prices && !isset($restricted_country_mode)}
                                {block name='product_price_and_shipping'}
                                    {if $slide.info_array.has_discount}
                                        {hook h='displayProductPriceBlock' product=$slide.info_array type="old_price"}
                                    {/if}
                                {/block}
                            {/if}
                            <h3>{l s='Hot deals today!' mod='tmproductsslider'}</h3>
                            {if $settings.product_name}<h2><a href="{$slide.info_array.url|escape:'htmlall':'UTF-8'}">{$slide.info_array.name|escape:'htmlall':'UTF-8'}</a></h2>{/if}
                            {if $slide.info_array.description_short && $settings.description_short}
                                <p class="slide-description des-short">{$slide.info_array.description_short|strip_tags:true|truncate:130:'...':false|escape:'htmlall':'UTF-8'}</p>
                            {/if}
                            {if $slide.info_array.description && $settings.description}
                                <p class="slide-description">{$slide.info_array.description|strip_tags:true|truncate:230:'...':false|escape:'htmlall':'UTF-8'}</p>
                            {/if}
                            {if $slide.info_array.manufacturer_name && $settings.manufacturer}
                                <div class="slide-manufacturer">
                                    <span>{l s='Brand:' mod='tmproductsslider'}</span>
                                    {$slide.info_array.manufacturer_name|escape:'htmlall':'UTF-8'}
                                </div>
                            {/if}
                            {if isset($slide.info_array.features) && $slide.info_array.features && $settings.features}
                                <div class="product-features">
                                    {foreach from=$slide.info_array.features item=feature}
                                        <div><span>{$feature.name|escape:'htmlall':'UTF-8'}:</span> {$feature.value|escape:'htmlall':'UTF-8'}</div>
                                    {/foreach}
                                </div>
                            {/if}
                            {if $slide.info_array.price && $slide.info_array.show_price && $settings.prices && !isset($restricted_country_mode)}
                                <div class="product-price-box">
                                    {block name='product_price_and_shipping'}
                                        <div class="product-price-and-shipping">
                                            {hook h='displayProductPriceBlock' product=$slide.info_array type="before_price"}
                                            <span class="product-price">{$slide.info_array.price}</span>
                                            {if $slide.info_array.has_discount}
                                                <span class="product-price product-price-old">{$slide.info_array.regular_price}</span>
                                            {/if}
                                            {hook h='displayProductPriceBlock' product=$slide.info_array type='unit_price'}
                                            {hook h='displayProductPriceBlock' product=$slide.info_array type='weight'}
                                        </div>
                                    {/block}
                                    {if ($slide.info_array.embedded_attributes.available_for_order && !$slide.info_array.embedded_attributes.quantity_all_versions <= 0 && $settings.quantity)}
                                        <!-- number of item in stock -->
                                        <p id="product-quantity">
                                            <span>{$slide.info_array.embedded_attributes.quantity_all_versions|intval}</span>
                                            {if $slide.info_array.embedded_attributes.quantity_all_versions == 1}
                                                <span>{l s='Item' mod='tmproductsslider'}</span>
                                            {else}
                                                <span>{l s='Items' mod='tmproductsslider'}</span>
                                            {/if}
                                        </p>
                                    {/if}
                                </div>
                                {if $slide.info_array.discount_type === 'percentage'}
                                    <h4>{l s='Save up to %1$s off' d='Shop.Theme.Catalog' sprintf=[$slide.info_array.discount_percentage]}</h4>
                                {/if}
                            {/if}
                            {if $settings.more_button || $settings.cart_button}
                                <div class="buttons-container">
                                    {if $slide.info_array.embedded_attributes.available_for_order && !isset($restricted_country_mode) && $settings.cart_button}
                                        {if (!isset($slide.info_array.embedded_attributes.customization_required) || !$slide.info_array.embedded_attributes.customization_required) && $slide.info_array.embedded_attributes.quantity_all_versions > 0}
                                            <a class="ajax_add_to_cart_button btn btn-md btn-default cart-button" href="{$link->getPageLink('cart', true, NULL, "qty=1&amp;id_product={$slide.info_array.id_product|intval}&amp;token={$static_token}&amp;add", false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart' mod='tmproductsslider'}" data-id-product="{$slide.info_array.id_product|intval}" data-minimal_quantity="{$slide.info_array.embedded_attributes.minimal_quantity|intval}">
                                                <span>{l s='Add to cart' mod='tmproductsslider'}</span>
                                            </a>
                                        {else}
                                            <a href="{$slide.info_array.url|escape:'htmlall':'UTF-8'}" class="btn btn-md btn-default cart-button">
                                                <span>{l s='Add to cart' mod='tmproductsslider'}</span>
                                            </a>
                                        {/if}
                                    {/if}
                                </div>
                            {/if}
                        </div>
                    </div>
                </div>
            {/foreach}
        </div>
    </div>
{/if}

