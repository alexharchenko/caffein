{include file="_partials/headerH1.tpl"}
<section class="featured-products clearfix">
  <div class="h2 products-section-title">
    {l s='Popular %1$s Products %2$s' d='Shop.Theme.Catalog' sprintf=['<span>', '</span>']}
  </div>
  <div class="products-carousel swiper-container">
    <div class="products swiper-wrapper">
      {foreach from=$products item="product"}
        {include file="catalog/_partials/miniatures/product.tpl" product=$product}
      {/foreach}
    </div>
    <div class="swiper-button-prev swiper-button-black"></div>
    <div class="swiper-button-next swiper-button-black"></div>
  </div>
</section>