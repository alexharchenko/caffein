{if isset($product_image) || isset($text1) || isset($text2)}
    <div class="kl_productcustomblock">
        {if isset($product_image)}
        <img src="{$product_image}" class="item-img " title="" alt="" width="100%" height="100%">
        {/if}
        <div class="item-html">
            <div class="content_bottom">
                {if isset($text1)}
                <div class="h2">{$text1 nofilter}</div>
                {/if}
                {if isset($text2)}
                <div class="h3 text-white js_custom_bl"><span>{$text2 nofilter}</span></div>
                {/if}
            </div>
        </div>
    </div>
{/if}
