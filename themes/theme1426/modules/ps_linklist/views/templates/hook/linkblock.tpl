{foreach $linkBlocks as $linkBlock}
  {if $linkBlock.hook != displayNav2} 
  {$url_to_cat = ''} 
  {foreach $linkBlock.links as $link}
    {if stristr($link.url, 'category') !== false}
      {$url_to_cat = $link.url}
    {/if}
  {/foreach}
    <div class="links">
      {assign var=_expand_id value=10|mt_rand:100000}
      <div class="title clearfix" data-target="#footer_sub_menu_{$_expand_id}" >
        {if $url_to_cat != ''}
          {if $url_to_cat === $urls.current_url}
          <span class="h3">{$linkBlock.title}</span>
          {else}
          <a href="{$url_to_cat}" class="h3">{$linkBlock.title}</a>
          {/if}
        {else}
          <span class="h3">{$linkBlock.title}</span>
        {/if}
        <!--<span class="pull-xs-right">
          <span class="navbar-toggler collapse-icons">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
        </span>-->
      </div>
      <ul id="footer_sub_menu_{$_expand_id}" class="collapse">
        {foreach $linkBlock.links as $link}
          {if $url_to_cat != $link.url}
            <li>
              {if $link.url === $urls.current_url}
              <span
                id="{$link.id}-{$linkBlock.id}"
                class="{$link.class}"
                title="{$link.description}">
                {$link.title}
              </span>
              {else}
              <a
                id="{$link.id}-{$linkBlock.id}"
                class="{$link.class}"
                href="{$link.url}"
                title="{$link.description}">
                {$link.title}
              </a>
              {/if}
            </li>
          {/if}
        {/foreach}
      </ul>
    </div>
  {else}
  <div id="_desktop_links" class="links nav-links">
    <ul>
      {foreach $linkBlock.links as $link}
        <li>
          {if $link.url === $urls.current_url}
          <span
            id="{$link.id}-{$linkBlock.id}"
            class="{$link.class}"
            title="{$link.description}">
            {$link.title}
          </span>
          {else}
          <a
            id="{$link.id}-{$linkBlock.id}"
            class="{$link.class}"
            href="{$link.url}"
            title="{$link.description}">
            {$link.title}
          </a>
          {/if}
        </li>
      {/foreach}
    </ul>
  </div>
  {/if}
{/foreach}

