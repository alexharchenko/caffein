{*
* 2002-2016 TemplateMonster
*
* TM Mosaic Products
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
* @author    TemplateMonster
* @copyright 2002-2016 TemplateMonster
* @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

<div id="block-container-{$row_name}" class="block-container-row block-container-{$row_name}">
  <div class="tmmp_popup_item">
    <div class="text-right btn-remove">
      <button type="button" class="btn btn-sm btn-danger button-remove-row">{l s='Remove row' mod='tmmosaicproducts'}</button>
    </div>
    <ul id="{$row_type}" class="clearfix {$row_type} items">
      <li class="col-xs-12 col-sm-6">
        <ul class="row">

          {foreach from=$row_content key=k item=items name=loop}
            {if $k == 1}
              <li class="col-xs-12 item">
                <div class="content">{include file=$partial_admin_path}</div>
              </li>
            {/if}
          {/foreach}

          <li class="col-xs-12">
             <ul class="row">
               {foreach from=$row_content key=k item=items name=loop}
                 {if $k > 1 && $k < 3}
                   <li class="col-xs-12 col-sm-6 item">
                     <div class="content">{include file=$partial_admin_path}</div>
                   </li>
                 {/if}
               {/foreach}
             </ul>
          </li>
        </ul>
      </li>


      <li class="col-xs-12 col-sm-6">
        <ul class="row">
          <li class="col-xs-12">
            <ul class="row">
              {foreach from=$row_content key=k item=items name=loop}
                {if $k > 3 && $k < 6}
                  <li class="col-xs-12 col-sm-6 item">
                    <div class="content">
                      {include file=$partial_admin_path}
                    </div>
                  </li>
                {/if}
              {/foreach}
            </ul>
          </li>

          {foreach from=$row_content key=k item=items name=loop}
            {if $k == 6}
              <li class="col-xs-12 item">
                <div class="content">{include file=$partial_admin_path}</div>
              </li>
            {/if}
          {/foreach}

        </ul>
      </li>

    </ul>
  </div>
  <input type="hidden" value="{literal}{{/literal}{$row_code}{literal}}{/literal}" name="row_content">
</div>