<?php
/**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

class KlPartialPaymentPrivatRedirectModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        $module = new klpartialpaymentprivat();
        $id_cart = Tools::GetValue('id_cart');
        $cart = new Cart((int)$id_cart);
        $total = $cart->getOrderTotal(true, 3);
        $customer = new Customer($cart->id_customer);
        $password = Configuration::get("PARTIALPRIVATPASSWORD");
        $url = 'http://' . Tools::getHttpHost(false) . __PS_BASE_URI__;
        $url .= 'index.php?controller=order-confirmation&id_cart=';
        $url .= (int)$cart->id . '&id_module=' . (int)$this->module->id . '&id_order=';
        $url .= Order::getIdByCartId($id_cart) . '&key=' . $customer->secure_key;
        try {
            $module->validateOrder(
                (int)$cart->id,
                Configuration::get('PS_OS_PREPARATION'),
                $total,
                $module->displayName
            );
        } catch (Exception $e) {
            echo($e->getMessage() . "\n");
        }

        $products = array_reduce($cart->getProducts(), function ($cummulative, $i) {
            $cummulative[] = array(
                'name' => $i['name'],
                'count' => (int)$i['cart_quantity'],
                'price' => round($i['price_wt'], 2)
            );
            return $cummulative;
        }, array());
        if ($shippingPrice = $cart->getOrderTotal(true, Cart::ONLY_SHIPPING)) {
            $products[] = array(
                'name' => 'Shipping',
                'count' => 1,
                'price' => $shippingPrice,
            );
        }


        $postData = new stdClass();
        $postData->storeId = Configuration::get("PARTIALPRIVATSTOREID");
        $postData->orderId = '000' . $id_cart;
        $postData->amount = $total;
        // $postData->partsCount = 25;
        $postData->partsCount = (int)Tools::GetValue('partsCount');
        $postData->merchantType = 'II';
        $postData->products = $products;
        $postData->responseUrl = $this->context->link->getModuleLink('klpartialpaymentprivat', 'validation');
        // $postData->responseUrl = $url;
        $postData->redirectUrl = $url;
        $module = $this->module;
        $postData->signature = $this->module::getSignature(
            $password,
            $postData->storeId,
            $postData->orderId,
            $this->module->withoutFloatingPoint($postData->amount),
            $postData->partsCount,
            $postData->merchantType,
            $postData->responseUrl,
            $postData->redirectUrl,
            array_reduce($postData->products, function ($cummulative, $i) use ($module) {
                $tmp_d = $cummulative.$i['name'].$i['count'].$module->withoutFloatingPoint($i['price']);
                return $tmp_d;
            }, ''),
            $password
        );

        // die(var_dump($postData));
        
        $options = array(
            CURLOPT_URL => 'https://payparts2.privatbank.ua/ipp/v2/payment/create',
            CURLOPT_POST => true,
            CURLOPT_HTTPHEADER => array(
                'Accept: application/json',
                'Accept-Encoding: UTF-8',
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode((array)$postData),
            CURLOPT_RETURNTRANSFER => true
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $result = json_decode(curl_exec($ch));
        curl_close($ch);

        if ($result->state == 'SUCCESS') {
            Tools::redirect("https://payparts2.privatbank.ua/ipp/v2/payment?token=" . $result->token);
        } else {
            if (isset($result->errorMessage)) {
                $this->context->smarty->assign(array('errorMsg' => $result->errorMessage));
            } else {
                $this->context->smarty->assign(array('errorMsg' => $result->message));
            }
            $this->setTemplate('module:klpartialpaymentprivat/views/templates/front/error.tpl');
        }
    }
}
