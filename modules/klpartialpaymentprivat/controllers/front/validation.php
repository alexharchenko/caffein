<?php
/**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

class KlPartialPaymentPrivatValidationModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        $data = json_decode(Tools::file_get_contents('php://input'), true);
        if (!$data) {
            $this->context->smarty->assign(array('errorMsg' => $this->module->l('No data response')));
            $this->setTemplate('module:klpartialpaymentprivat/views/templates/front/error.tpl');
        }
        $state = $this->getState($data);
        if ($state === 'SUCCESS') {
            $status = 'PARTIALPRIVATSUCCESS';
        } elseif ($state === 'FAIL') {
            $status = 'PARTIALPRIVATERROR';
        } else {
            $status = 'PARTIALPRIVATETC';
        }
        $orderId = Order::getIdByCartId($data['orderId']);
        $order = new Order($orderId);
        $order->setCurrentState(Configuration::get($status));
        $history = new OrderHistory();
        $history->id_order = $orderId;
        $history->id_order_state = Configuration::get($status);
        $history->add();
    }

    public function getState($data)
    {
        $password = Configuration::get('PARTIALPRIVATPASSWORD');
        $ResSignature = klpartialpaymentprivat::getSignature(
            $password,
            $data['storeId'],
            $data['orderId'],
            $data['paymentState'],
            $data['message'],
            $password
        );
        if ($ResSignature === $data['signature']) {
            return $data['paymentState'];
        }
    }
}
