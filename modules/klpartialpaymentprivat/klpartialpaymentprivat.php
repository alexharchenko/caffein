<?php
/**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

class KlPartialPaymentPrivat extends PaymentModule
{
    protected $supported_currencies = array('EUR','UAH','USD','RUB');

    public function __construct()
    {
        $this->name = 'klpartialpaymentprivat';
        $this->tab = 'payments_gateways';
        $this->version = '1.0.4';
        $this->author = 'klookva Antikov';
        $this->currencies = true;
        $this->currencies_mode = 'checkbox';

        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = 'Privat Partial Payment';
        $this->description = $this->l('Accept payments with partial payment via Privat Bank');
        $this->confirmUninstall = $this->l('Are you sure you want to delete Privat Partial Payment ?');
        $this->ps_versions_compliancy = array('min' => '1.6.1.0', 'max' => _PS_VERSION_);
        $this->templateFile = _PS_MODULE_DIR_ . $this->name . "/views/templates/hook/paymentForm.tpl";
    }

    public function install()
    {
        return parent::install()
            && $this->registerHook('payment')
            && $this->registerHook('displayHeader')
            && $this->registerHook('displayPrivatButton')
            && $this->registerHook('displayBackOfficeHeader')
            && $this->registerHook('paymentOptions')
            && $this->registerHook('paymentReturn');
    }

    public function uninstall()
    {
        return parent::uninstall()
            && Configuration::deleteByName('PARTIALPRIVATSTOREID')
            && Configuration::deleteByName('PARTIALPRIVATPASSWORD')
            && Configuration::deleteByName('PARTIALPRIVATSUCCESS')
            && Configuration::deleteByName('PARTIALPRIVATERROR')
            && Configuration::deleteByName('PARTIALPRIVATPRODUCTS')
            && Configuration::deleteByName('PARTIALPRIVATETC');
    }

    public function hookDisplayBackOfficeHeader()
    {
        if (Tools::getValue('controller') == 'AdminModules' && Tools::getValue('configure') == 'klpartialpaymentprivat') {
            $this->context->controller->addCSS($this->_path . 'views/css/select2.min.css', 'all');
            $this->context->controller->addJquery();
            $this->context->controller->addJS($this->_path . 'views/js/select2.min.js', 'all');
            $this->context->controller->addJS($this->_path . 'views/js/back.js', 'all');
        }
    }

    public function hookDisplayPrivatButton($params)
    {
        $id_product = Tools::getValue('id_product');
        $id_product = (int)($id_product ? $id_product : $params['id_product']);
        $products_id = explode(',', Configuration::get('PARTIALPRIVATPRODUCTS'));
        if (!in_array($id_product, $products_id)) {
            return;
        }
        $disabled = false;
        $products = $params['cart']->getProducts();
        foreach ($products as $product) {
            if (!in_array($product['id_product'], $products_id)) {
                $disabled = true;
                break;
            }
        }
        $this->context->smarty->assign(
            array(
                'disabled' => $disabled
            )
        );
        return $this->display(__FILE__, 'views/templates/hook/button.tpl');
    }

    public function hookDisplayHeader()
    {
        $this->context->controller->registerStylesheet(
            'modules-klpartialpaymentprivat-privat',
            'modules/'.$this->name.'/views/css/privat.css',
            array('position' => 'top', 'priority' => 150)
        );
        $this->context->controller->registerJavascript(
            'modules-klpartialpaymentprivat-front',
            'modules/'.$this->name.'/views/js/front.js',
            array('position' => 'bottom', 'priority' => 150)
        );
    }

    public function hookPaymentReturn($params)
    {
    }

    public function getPath()
    {
        return $this->_path;
    }

    public static function getSignature(...$params)
    {
        $string = '';
        foreach ($params as $i) {
            $string .= trim($i);
        }
        return base64_encode(sha1($string, true));
    }

    public function withoutFloatingPoint($total)
    {
        if (strpos($total, '.') !== false) {
            $tmp = explode('.', $total);
        } elseif (strpos($total, ',') !== false) {
            $tmp = explode(',', $total);
        } else {
            $tmp = array($total);
        }
        $decimals = '00';
        if (isset($tmp[1])) {
            $tmp[1] .= '0';
            $decimals = substr($tmp[1], 0, 2);
        }
        $result = implode('', $tmp);

        return $result . $decimals;
    }

    public function hookPaymentOptions($params)
    {
        if (!$this->checkCurrency($params['cart'])) {
            PrestaShopLogger::addLog(
                'Privat partial payment currency check failed',
                3,
                null,
                'PaymentOptions',
                (int)$params['cart']->id,
                true
            );
            return;
        }
        $products_id = explode(',', Configuration::get('PARTIALPRIVATPRODUCTS'));
        $products = $params['cart']->getProducts();
        foreach ($products as $product) {
            if (!in_array($product['id_product'], $products_id)) {
                return;
            }
        }
        $this->smarty->assign(array(
            'id' => (int)$params['cart']->id,
        ));
        $newOption = new PaymentOption();
        $newOption->setModuleName($this->name)
                ->setCallToActionText($this->l('Pay by installment plan via Privat'))
                ->setAction($this->context->link->getModuleLink(
                    $this->name,
                    'redirect',
                    array('id_cart' => $params['cart']->id)
                ))
                ->setAdditionalInformation($this->fetch($this->templateFile));

        return array($newOption);
    }

    public function checkCurrency($cart)
    {
        $currency_order = new Currency((int)($cart->id_currency));
        $currencies_module = $this->getCurrency((int)$cart->id_currency);
        if (is_array($currencies_module)) {
            foreach ($currencies_module as $currency_module) {
                if ($currency_order->id == $currency_module['id_currency']) {
                    return true;
                }
            }
        }
        return false;
    }

    public function getContent()
    {
        $output = '';
        if (Tools::isSubmit('submit' . $this->name)) {
            $output .= $this->updateSettings();
        }
        return $output . $this->displayForm();
    }

    private function updateSettings()
    {
        $storeId = (string)Tools::getValue('storeID');
        $password = (string)Tools::getValue('password');
        $sucID = (string)Tools::getValue('successStatID');
        $errID = (string)Tools::getValue('errorStatID');
        $etcID = (string)Tools::getValue('etcStatID');
        $productsId = implode(',', (array)Tools::getValue('productsId'));
        if (!$storeId || !$password || !$sucID || !$errID || !$etcID || !$productsId || $productsId == '') {
            return $this->displayError($this->l('Invalid Configuration value'));
        } else {
            Configuration::updateValue('PARTIALPRIVATSTOREID', $storeId);
            Configuration::updateValue('PARTIALPRIVATPASSWORD', $password);
            Configuration::updateValue('PARTIALPRIVATPRODUCTS', $productsId);
            Configuration::updateValue('PARTIALPRIVATSUCCESS', $sucID);
            Configuration::updateValue('PARTIALPRIVATERROR', $errID);
            Configuration::updateValue('PARTIALPRIVATETC', $etcID);
            return $this->displayConfirmation($this->l('Settings updated'));
        }
    }

    public function displayForm()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $fields_form = array();
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Settings'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Shop ID'),
                    'name' => 'storeID',
                    'size' => 20,
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Shop password'),
                    'name' => 'password',
                    'size' => 20,
                    'required' => true
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Status for success'),
                    'name' => 'successStatID',
                    'class' => 'input-lg',
                    'options' => array(
                        'query' => OrderState::getOrderStates($this->context->language->id),
                        'id' => 'id_order_state',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Status for error'),
                    'name' => 'errorStatID',
                    'class' => 'input-lg',
                    'options' => array(
                        'query' => OrderState::getOrderStates($this->context->language->id),
                        'id' => 'id_order_state',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Status for else'),
                    'name' => 'etcStatID',
                    'class' => 'input-lg',
                    'options' => array(
                        'query' => OrderState::getOrderStates($this->context->language->id),
                        'id' => 'id_order_state',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Display payment method on products'),
                    'name' => 'productsId[]',
                    'multiple' => true,
                    'class' => 'input-lg js_klookva_select2',
                    'options' => array(
                        'query' => Product::getProducts($this->context->language->id, 0, PHP_INT_MAX, 'name', 'asc'),
                        'id' => 'id_product',
                        'name' => 'name'
                    )
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'button'
            )
        );
        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->submit_action = 'submit' . $this->name;
        $helper->toolbar_btn = array(
            'save' => array(
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&save' . $this->name .
                '&token='.Tools::getAdminTokenLite('AdminModules'),
            ),
            'back' => array(
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );
        $array = Configuration::get('PARTIALPRIVATPRODUCTS');
        $helper->fields_value['storeID'] = Configuration::get('PARTIALPRIVATSTOREID');
        $helper->fields_value['password'] = Configuration::get('PARTIALPRIVATPASSWORD');
        $helper->fields_value['productsId[]'] = ($array ? explode(',', $array) : array());
        $helper->fields_value['successStatID'] = Configuration::get('PARTIALPRIVATSUCCESS');
        $helper->fields_value['errorStatID'] = Configuration::get('PARTIALPRIVATERROR');
        $helper->fields_value['etcStatID'] = Configuration::get('PARTIALPRIVATETC');
        return $helper->generateForm($fields_form);
    }
}
