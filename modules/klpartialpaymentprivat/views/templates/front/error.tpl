{**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}
<div class="module_error alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {$errorMsg}
</div>