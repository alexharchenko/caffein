{**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}
<label for="partsCountSelector">{l s='Number of parts to pay: ' mod='klpartialpaymentprivat'}</label>
<select name="partsCount" id="partsCountSelector" class="form-control">
    {foreach 1|range:25 as $n}
        <option value="{$n}" selected>{$n}</option>
    {/foreach}
</select>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(){
        var pc = document.getElementById("partsCountSelector");
        setPartsCount();
        pc.addEventListener("change", function() {
            setPartsCount();
        });

        function setPartsCount() {
            const forms = document.getElementsByTagName("form");
            for (let i = 0; i < forms.length; i++) {
                if (forms[i].action.indexOf("klpartialpaymentprivat") !== -1) {
                    if (forms[i].action.indexOf("partsCount") !== -1) {
                        let offset = forms[i].action.indexOf("partsCount") - forms[i].action.length;
                        forms[i].action = forms[i].action.slice(0, offset) + "partsCount=" + pc.options[pc.selectedIndex].value;
                    }
                    else {
                        forms[i].action = forms[i].action + "&partsCount=" + pc.options[pc.selectedIndex].value;
                    }
                    break;
                    console.log(forms[i].action);
                }
            }
        }
    });
</script>