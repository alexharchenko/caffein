{**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}
<button class="btn btn-primary add-to-cart privat-buy-button js-privat-buy-button" data-button-action="add-to-cart" onclick="localStorage.setItem('privatSelect', 1);" type="submit" {if $disabled}disabled{/if}>
	<img src="/modules/klpartialpaymentprivat/views/img/privat.png">
	{l s='Buy by installments' mod='klpartialpaymentprivat'}
</button>