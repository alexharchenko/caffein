/**
* @author    Antikov Evgeniy
* @copyright 2017-2019 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
$(document).ready(function() {
	$('.js_klookva_select2').select2({
    	sorter: function(results) {
      		var query = $('.select2-search__field').val().toLowerCase();
  			return results.sort(function(a, b) {
    			return a.text.toLowerCase().indexOf(query) - b.text.toLowerCase().indexOf(query);
    		});
		}
  	});
});
