/**
* @author    Antikov Evgeniy
* @copyright 2017-2019 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
$(document).ready(function() {
	if ($('.payment-options input[data-module-name="klpartialpaymentprivat"]').length > 0 && localStorage.getItem('privatSelect')) {
		$('.payment-options input[data-module-name="klpartialpaymentprivat"]').click();
		localStorage.removeItem('privatSelect');
	}
});
