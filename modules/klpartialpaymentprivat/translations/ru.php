<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{klpartialpaymentprivat}prestashop>klpartialpaymentprivat_211051e0259a89fd9e10b4f079b01ab7'] = 'Принимать платежи с частичной оплатой через Приват Банк';
$_MODULE['<{klpartialpaymentprivat}prestashop>klpartialpaymentprivat_582648d7c44376bdc618f2ab109ffe4f'] = 'Вы уверены, что хотите удалить частичный платеж Приват Банк?';
$_MODULE['<{klpartialpaymentprivat}prestashop>klpartialpaymentprivat_6d3a067b1316232b630c00e10186f51b'] = 'Оплата в рассрочку через Приват Банк';
$_MODULE['<{klpartialpaymentprivat}prestashop>klpartialpaymentprivat_fe5d926454b6a8144efce13a44d019ba'] = 'Неверное значение конфигурации';
$_MODULE['<{klpartialpaymentprivat}prestashop>klpartialpaymentprivat_c888438d14855d7d96a2724ee9c306bd'] = 'Настройки обновлены';
$_MODULE['<{klpartialpaymentprivat}prestashop>klpartialpaymentprivat_f4f70727dc34561dfde1a3c529b6205c'] = 'Настройки';
$_MODULE['<{klpartialpaymentprivat}prestashop>klpartialpaymentprivat_ccf107a5d46c6501c9f2f4345400dc2e'] = 'Ид магазина';
$_MODULE['<{klpartialpaymentprivat}prestashop>klpartialpaymentprivat_ecb77be87bda4dd34d51cfedbef9b6c3'] = 'Пароль магазина';
$_MODULE['<{klpartialpaymentprivat}prestashop>klpartialpaymentprivat_976508bd5f69750df1147c5a82a172d9'] = 'Статус заказа успешной оплаты';
$_MODULE['<{klpartialpaymentprivat}prestashop>klpartialpaymentprivat_d87226f7f5a4cfbbfef6b4b152aa068f'] = 'Статус заказа ошибки';
$_MODULE['<{klpartialpaymentprivat}prestashop>klpartialpaymentprivat_5da14ec0471ccb41237a2f5a66a47e97'] = 'Статус заказа прочее';
$_MODULE['<{klpartialpaymentprivat}prestashop>klpartialpaymentprivat_2a4ac5cfd9574a73835a7885cc3f2a72'] = 'Показать способ оплаты на продуктах';
$_MODULE['<{klpartialpaymentprivat}prestashop>klpartialpaymentprivat_c9cc8cce247e49bae79f15173ce97354'] = 'Сохранить';
$_MODULE['<{klpartialpaymentprivat}prestashop>klpartialpaymentprivat_630f6dc397fe74e52d5189e2c80f282b'] = 'Обратно к списку';
$_MODULE['<{klpartialpaymentprivat}prestashop>validation_6ed75b41a9c08a2423d8b2d9392983f4'] = 'Нет ответа на данные';
$_MODULE['<{klpartialpaymentprivat}prestashop>paymentform_699fb80734de4f1e1a2d85c50d30bcbc'] = 'Количество частей для оплаты:';
$_MODULE['<{klpartialpaymentprivat}prestashop>button_9a609f218dedea1844908f602805afe6'] = 'Купить в рассрочку';
