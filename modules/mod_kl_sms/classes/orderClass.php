<?php

class orderClass extends ObjectModel
{
    public $id;

    public $id_order;

    public $id_shop;

    public $date;

    public $text;

    public $text_admin;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'kl_sms_order',
        'primary' => 'id_sms',
        'multilang' => true,
        'fields' => array(
            'id_shop' =>            array('type' => self::TYPE_NOTHING, 'validate' => 'isUnsignedId'),
            'date' =>               array('type' => self::TYPE_DATE, 'validate' => 'isDate' ),
            'id_order' =>           array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
            // Lang fields
            'text' =>               array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString'),
        	'text_admin' =>         array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString'),
        )
    );    

    public static function getSmsByOrderId($id_lang,$id_order)
    {
        return  Db::getInstance()->executeS('
            SELECT r.`id_sms`, r.`id_order`, r.`id_shop`, r.`date`, rl.`text`, rl.`text_admin`
            FROM `'._DB_PREFIX_.'kl_sms_order` r
            LEFT JOIN `'._DB_PREFIX_.'kl_sms_order_lang` rl ON (r.`id_sms` = rl.`id_sms`)
            WHERE `id_order` = '.(int)$id_order.' AND `id_lang` = '.(int)$id_lang.' '.Shop::addSqlRestrictionOnLang());
    }
}
