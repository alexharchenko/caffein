<?php

if (!defined('_PS_VERSION_'))
    exit;

include_once _PS_MODULE_DIR_.'mod_kl_sms/classes/statusClass.php';
include_once _PS_MODULE_DIR_.'mod_kl_sms/classes/orderClass.php';

class mod_kl_sms extends Module {

    private $templateFile;

    public function __construct() {
        $this->name = 'mod_kl_sms';
        $this->version = '1.0.7';
        $this->author = 'Klookva Antikov';
        $this->bootstrap = TRUE;

        parent::__construct();
        
        $this->displayName = $this->l('Sms Sender');
        $this->description = $this->l('Send sms after confirm order,tracking number update and status update');
        $this->templateFile = 'module:mod_kl_sms/views/templates/hook/mod_kl_sms.tpl';
    }

    public function install()
    {
        return parent::install()
            && $this->installDB()
            && Configuration::updateValue('PHONE_SMS_KLOOKVA', Configuration::get('PS_SHOP_PHONE'))
            && $this->registerHook('displayAdminOrderTabOrder')
            && $this->registerHook('actionValidateOrder')
            && $this->registerHook('actionOrderStatusUpdate')
            && $this->registerHook('actionAdminOrdersTrackingNumberUpdate')
        ;
    }

    public function installDB()
    {
        $return = true;
        $return &= Db::getInstance()->execute('
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'kl_sms_status` (
                `id_sms` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                `id_status` int(10) unsigned DEFAULT NULL,
                `is_active` tinyint(1) unsigned NOT NULL default \'0\',
                `is_active_admin` tinyint(1) unsigned NOT NULL default \'0\',
                `id_shop` int(10) unsigned DEFAULT NULL,
                UNIQUE (`id_status`),
                PRIMARY KEY (`id_sms`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;'
        );

        $return &= Db::getInstance()->execute('
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'kl_sms_status_lang` (
                `id_sms` INT UNSIGNED NOT NULL,
                `id_lang` int(10) unsigned NOT NULL ,
                `text` text,
                `text_admin` text,
                PRIMARY KEY (`id_sms`, `id_lang`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;'
        );

        $return &= Db::getInstance()->execute('
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'kl_sms_order` (
                `id_sms` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                `id_order` int(10) unsigned DEFAULT NULL,
                `id_shop` int(10) unsigned DEFAULT NULL,
                `date` datetime NOT NULL,
                PRIMARY KEY (`id_sms`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;'
        );

        $return &= Db::getInstance()->execute('
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'kl_sms_order_lang` (
                `id_sms` INT UNSIGNED NOT NULL,
                `id_lang` int(10) unsigned NOT NULL ,
                `text` text,
                `text_admin` text,
                PRIMARY KEY (`id_sms`, `id_lang`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;'
        );

        return $return;
    }

    public function uninstall()
    {
        foreach (Language::getLanguages(false) as $lang) {
            Configuration::deleteByName('ORDER_SMS_KLOOKVA_'.(int)$lang['id_lang']);
            Configuration::deleteByName('TRACKING_SMS_KLOOKVA_'.(int)$lang['id_lang']);
            Configuration::deleteByName('ORDER_SMS_ADMIN_KLOOKVA_'.(int)$lang['id_lang']);
            Configuration::deleteByName('TRACKING_SMS_ADMIN_KLOOKVA_'.(int)$lang['id_lang']);
        }
        return 
            $this->uninstallDB() &&
            Configuration::deleteByName('LOGIN_SMS_KLOOKVA') &&
            Configuration::deleteByName('PASSWORD_SMS_KLOOKVA') &&
            Configuration::deleteByName('PHONE_SMS_KLOOKVA') &&
            Configuration::deleteByName('ACTIVE_ORDER_SMS_KLOOKVA') &&
            Configuration::deleteByName('ACTIVE_ORDER_SMS_ADMIN_KLOOKVA') &&
            Configuration::deleteByName('ACTIVE_TRACKING_SMS_KLOOKVA') &&
            Configuration::deleteByName('ACTIVE_TRACKING_SMS_ADMIN_KLOOKVA') &&
            parent::uninstall();
    }

    public function uninstallDB()
    {
        return Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'kl_sms_status`') 
            && Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'kl_sms_status_lang`') 
            && Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'kl_sms_order`') 
            && Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'kl_sms_order_lang`');
    }

    public function getContent()
    {
        $html = '';
        $html .= $this->adminDisplayInformation($this->l('Input %order% for show product order number')." | ".$this->l('Input %name% for show name and surname of customer'));
        $id_sms = (int)Tools::getValue('id_sms');
        if (Tools::isSubmit('mod_kl_sms_checkbalance')) {
            $html .= $this->checkbalance();
        }
        if (Tools::isSubmit('sendSmsForUsers')) {
            $phones = Tools::getValue('users');
            $text = Tools::getValue('text_sms');
            if (Tools::strpos($text, '%name%') !== false || Tools::strpos($text, '%order%') !== false) {
                if (is_array($phones) && !empty($phones)) {
                    $flag = true;
                    foreach($phones as $phone) {
                        $tmp_text = $text;
                        $customer_id = $this->getCustomerIdByPhone($phone);
                        if (Tools::strpos($tmp_text, '%name%') !== false) {
                            $customer = new Customer($customer_id);
                            $tmp_text = str_replace("%name%", $customer->firstname.' '.$customer->lastname, $tmp_text);
                        }
                        if (Tools::strpos($tmp_text, '%order%') !== false) {
                            $orders = Order::getCustomerOrders($customer_id);
                            if (empty($orders)) {
                                $tmp_text = str_replace("%order%", '', $tmp_text);
                            } else {
                                $tmp_text = str_replace("%order%", $orders[0]['reference'], $tmp_text);
                            }
                        }
                        if (!$this->sendSms($tmp_text, $phone, 0, false, false)) {
                            $flag = false;
                        }
                    }
                } else {
                    $flag = false;
                }
            } else {
                $phone_numbers = implode(',', $phones);
                $flag = $this->sendSms($text, $phone_numbers, 0, false, false);
            }

            if ($flag) {
                $html .= $this->displayConfirmation($this->l('Success send sms to').' '.count($phones).' '.$this->l('customers'));
            } else {
                $html .= $this->displayError($this->l('Cant send sms, check balance'));
            }
        }
        if (Tools::isSubmit('savemod_kl_sms_login')) {
            Configuration::updateValue('LOGIN_SMS_KLOOKVA', Tools::getValue('login_sms'));
            Configuration::updateValue('PASSWORD_SMS_KLOOKVA', Tools::getValue('password_sms'));
            Configuration::updateValue('PHONE_SMS_KLOOKVA', Tools::getValue('phone_sms'));
        }
        if (Tools::isSubmit('savemod_kl_sms_order')) {
            foreach (Language::getLanguages(false) as $lang) {
                Configuration::updateValue('ORDER_SMS_KLOOKVA_'.(int)$lang['id_lang'], Tools::getValue('order_sms_'.(int)$lang['id_lang'], ''));
                Configuration::updateValue('ORDER_SMS_ADMIN_KLOOKVA_'.(int)$lang['id_lang'], Tools::getValue('order_sms_admin_'.(int)$lang['id_lang'], ''));
            }
            Configuration::updateValue('ACTIVE_ORDER_SMS_KLOOKVA', Tools::getValue('active_order'));
            Configuration::updateValue('ACTIVE_ORDER_SMS_ADMIN_KLOOKVA', Tools::getValue('active_order_admin'));
        }
        if (Tools::isSubmit('savemod_kl_sms_tracking')) {
            foreach (Language::getLanguages(false) as $lang) {
            	Configuration::updateValue('TRACKING_SMS_KLOOKVA_'.(int)$lang['id_lang'], Tools::getValue('tracking_sms_'.(int)$lang['id_lang'], ''));
                Configuration::updateValue('TRACKING_SMS_ADMIN_KLOOKVA_'.(int)$lang['id_lang'], Tools::getValue('tracking_sms_admin_'.(int)$lang['id_lang'], ''));
            }
            Configuration::updateValue('ACTIVE_TRACKING_SMS_KLOOKVA', Tools::getValue('active_tracking'));
            Configuration::updateValue('ACTIVE_TRACKING_SMS_ADMIN_KLOOKVA', Tools::getValue('active_tracking_admin'));
        }
        if (Tools::isSubmit('savemod_kl_sms_status')) {
            if ($id_sms = Tools::getValue('id_sms')) {
                $kl_sms = new statusClass((int)$id_sms);
            } else {
                $kl_sms = new statusClass();
            }

            $kl_sms->copyFromPost();
            $kl_sms->id_shop = $this->context->shop->id;
            if ($kl_sms->validateFields(false) && $kl_sms->validateFieldsLang(false)) {
                $kl_sms->save();
                $this->_clearCache('*');
            } else {
                $html .= $this->displayError($this->l('An error occurred while attempting to save.', array(), 'Admin.Notifications.Error'));
            }
        }

        if (Tools::isSubmit('updatemod_kl_sms_status') || Tools::isSubmit('addmod_kl_sms_status')) {
            $helper = $this->initForm_status();
            foreach (Language::getLanguages(false) as $lang) {
                if ($id_sms) {
                    $kl_sms = new statusClass((int)$id_sms);
                    $helper->fields_value['text'][(int)$lang['id_lang']] = $kl_sms->text[(int)$lang['id_lang']];
                    $helper->fields_value['text_admin'][(int)$lang['id_lang']] = $kl_sms->text_admin[(int)$lang['id_lang']];
                } else {
                    $helper->fields_value['text'][(int)$lang['id_lang']] = Tools::getValue('text_'.(int)$lang['id_lang'], '');
                    $helper->fields_value['text_admin'][(int)$lang['id_lang']] = Tools::getValue('text_admin_'.(int)$lang['id_lang'], '');
                }
            }
            $helper->fields_value['is_active'] = (int)$kl_sms->is_active;
            $helper->fields_value['is_active_admin'] = (int)$kl_sms->is_active_admin;
            $helper->fields_value['id_status'] = (int)$kl_sms->id_status;
            return $html.$helper->generateForm($this->fields_form);
        } elseif (Tools::isSubmit('deletemod_kl_sms_status')) {
            $kl_sms = new statusClass((int)$id_sms);
            $kl_sms->delete();
            $this->_clearCache('*');
            Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
        }
        $content = $this->getListContent((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper = $this->initList();
        $helper->listTotal = count($content);
        return $this->context->controller->addCSS('modules/'.$this->name.'/views/css/user_name.css').$html.$this->initForm_login().$this->initForm_order().$this->initForm_tracking().$this->initFormSendSms().$helper->generateList($content, $this->fields_list);
    }

    public function checkbalance() {
        require_once(_PS_MODULE_DIR_.'mod_kl_sms/lib/nusoap.php');
        $site_name = $_SERVER['HTTP_HOST'];
        if (preg_match('/www\.([^\s]*)$/', $site_name, $change))
            $site_name = $change[1];
        $ip = gethostbyname($site_name);
        error_reporting(E_ALL ^ E_WARNING);

        $url = "http://klookva.com.ua/test/shasoapbalance.php?wsdl";

        $client = new nusoap_client($url);

        $err = $client->getError();

        if ($err) {
            echo '<p><b>Error: ' . $err . '</b></p>';
        }

        $args = array('login' => Configuration::get('LOGIN_SMS_KLOOKVA'), 'pasw' => sha1(Configuration::get('PASSWORD_SMS_KLOOKVA')), 'ip' => $ip, 'site_name' => $site_name);

        $return = $client->call('CheckBallanceSha', array($args));
        if ($return['login'] != 0) {
            $msg1 = $this->displayConfirmation($this->l(' Баланс: ') . $return['balance'] . $this->l(" смс"));
        } elseif ($return['login'] == false) {
            $msg1 = $this->displayError($this->l('Неверный логин или пароль!'));
        } else {
            $msg1 = $this->displayError($this->l('Ошибка авторизации!'));
        }
        return $msg1;
    }

    protected function initList()
    {
        $this->fields_list = array(
            'id_status' => array(
                'title' => $this->l('ID status'),
                'width' => 50,
                'type' => 'text',
                'name' => 'id_status',
                'search' => false,
                'orderby' => false
            ),
            'text' => array(
                    'type' => 'text',
                    'title' => $this->l('Text'),
                    'name' => 'text',
                    'lang' => true,
            ),
            'text_admin' => array(
                    'type' => 'text',
                    'title' => $this->l('Text for admin'),
                    'name' => 'text_admin',
                    'lang' => true,
            ),
            'is_active' => array(
                'title' => $this->l('Send sms for customer'),
                'width' => 50,
                'active' => 'is_active',
                'search' => true,
                'orderby' => false,
            ),
            'is_active_admin' => array(
                'title' => $this->l('Send sms for admin'),
                'width' => 50,
                'active' => 'is_active_admin',
                'search' => true,
                'orderby' => false,
            ),
        );

        if (Shop::isFeatureActive()) {
            $this->fields_list['id_shop'] = array(
                'title' => $this->l('ID Shop'),
                'align' => 'center',
                'width' => 25,
                'type' => 'int'
            );
        }

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = false;
        $helper->identifier = 'id_sms';
        $helper->actions = array('edit', 'delete');
        $helper->show_toolbar = true;
        $helper->imageType = 'png';
        $helper->toolbar_btn['new'] =  array(
            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&add'.$this->name.'_status&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Add new', array(), 'Admin.Actions')
        );

        $helper->title = $this->displayName;
        $helper->table = 'mod_kl_sms_status';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        return $helper;
    }

    protected function initForm_status()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $this->fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('New status update text'),
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Status'),
                    'name' => 'id_status',
                    'class' => 'input-lg',
                    'options' => array(
                        'query' => OrderState::getOrderStates($this->context->language->id),
                        'id' => 'id_order_state',
                        'name' => 'name'
                    )
                ),
                array(
                    'type'      => 'switch',                              
                    'label'     => $this->l('Send sms after changed this status to customer?'),        
                    'name'      => 'is_active',                                                         
                    'class'     => 't',                                   
                    'is_bool'   => true,                                                                                     
                    'values'    => array(                                 
                        array(
                            'id'    => 'on',                           
                            'value' => 1,                                       
                            'label' => $this->l('Yes')     
                        ),
                        array(
                            'id'    => 'off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    ),
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('text for customer'),
                    'lang' => true,
                    'name' => 'text',
                    'cols' => 40,
                    'rows' => 10,
                ),
                array(
                    'type'      => 'switch',                              
                    'label'     => $this->l('Send sms after changed this status to admin?'),        
                    'name'      => 'is_active_admin',                                                         
                    'class'     => 't',                                   
                    'is_bool'   => true,                                                                                     
                    'values'    => array(                                 
                        array(
                            'id'    => 'on',                           
                            'value' => 1,                                       
                            'label' => $this->l('Yes')     
                        ),
                        array(
                            'id'    => 'off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    ),
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('text for admin'),
                    'lang' => true,
                    'name' => 'text_admin',
                    'cols' => 40,
                    'rows' => 10,
                )
            ),
            'submit' => array(
                'title' => $this->l('Save', array(), 'Admin.Actions'),
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = 'mod_kl_sms_status';
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        foreach (Language::getLanguages(false) as $lang) {
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );
        }

        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'savemod_kl_sms_status';
        $helper->toolbar_btn =  array(
            'save' =>
            array(
                'desc' => $this->l('Save', array(), 'Admin.Actions'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
            ),
            'back' =>
            array(
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list', array(), 'Admin.Actions'),
            )
        );
        return $helper;
    }

    protected function getUsersOnlyWithPhones()
    {
        $customers = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
            SELECT c.`id_customer` as id, CONCAT(c.`firstname`, \' \', c.`lastname`) as name, a.phone_mobile, a.phone
            FROM `'._DB_PREFIX_.'customer` c
            LEFT JOIN `'._DB_PREFIX_.'address` a ON (c.`id_customer` = a.`id_customer`)
            WHERE 1 '.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER). ' AND c.`active` = 1 AND a.`active` = 1 AND a.`deleted` = 0 AND ((a.phone_mobile IS NOT NULL AND a.phone_mobile <> \'\') OR (a.phone IS NOT NULL AND a.phone <> \'\'))
            ORDER BY c.`firstname` ASC'
        );

        $phones = array();
        foreach ($customers as $key => $customer) {
            if (!in_array($customer['phone_mobile'], $phones) && !in_array($customer['phone'], $phones)) {
                if ($customer['phone_mobile'] !== '') {
                    $customers[$key]['true_phone'] = $customer['phone_mobile'];
                    $phones[] = $customer['phone_mobile'];
                    continue;
                } elseif ($customer['phone'] !== '') {
                    $customers[$key]['true_phone'] = $customer['phone'];
                    $phones[] = $customer['phone'];
                    continue;
                } else {
                    unset($customers[$key]);
                }
            } else {
                unset($customers[$key]);
            }
        }

        return $customers;
    }


    protected function initFormSendSms()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $this->fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Send sms for customers'),
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'multiple' => true,
                    'label' => $this->l('Send to users'),
                    'name' => 'users',
                    'required' => true,
                    'options' => array(
                        'query' => $this->getUsersOnlyWithPhones(),
                        'id' => 'true_phone',
                        'name' => 'name'
                    ),
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Text for sms'),
                    'required' => true,
                    'name' => 'text_sms',
                ),
            ),
            'submit' => array(
                'title' => $this->l('Send sms', array(), 'Admin.Actions'),
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = 'mod_kl_sms';
        $helper->identifier = $this->identifier;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        foreach (Language::getLanguages(false) as $lang) {
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );
        }
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'sendSmsForUsers';

        return $helper->generateForm($this->fields_form);
    }

    protected function initForm_login()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $this->fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Configuration'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Login'),
                    'name' => 'login_sms',
                    'required'  => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Password'),
                    'name' => 'password_sms',
                    'required'  => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Phone admin'),
                    'name' => 'phone_sms',
                ),
            ),
            'buttons' => array(
                    'check-balance' => array(
                    'title' => $this->l('Check Balance'),
                    'name' => $this->name.'_checkbalance',
                    'type' => 'submit',
                    'class' => 'btn btn-default pull-left',
                    'icon' => 'icon-check',
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save', array(), 'Admin.Actions'),
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = 'mod_kl_sms';
        $helper->identifier = $this->identifier;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->toolbar_scroll = true;
        foreach (Language::getLanguages(false) as $lang) {
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );
        }
        $helper->title = $this->displayName;
        $helper->submit_action = 'savemod_kl_sms_login';
        $helper->fields_value['login_sms'] = Configuration::get('LOGIN_SMS_KLOOKVA');
        $helper->fields_value['password_sms'] = Configuration::get('PASSWORD_SMS_KLOOKVA');
        $helper->fields_value['phone_sms'] = Configuration::get('PHONE_SMS_KLOOKVA');
        return $helper->generateForm($this->fields_form);
    }

    protected function initForm_order()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $this->fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Confirm order'),
            ),
            'input' => array(
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Text for customer'),
                    'lang' => true,
                    'name' => 'order_sms',
                    'cols' => 40,
                    'rows' => 10,
                ),
                array(
                    'type'      => 'switch',                              
                    'label'     => $this->l('Send sms after customer confirm order for customer?'),        
                    'name'      => 'active_order',                                                           
                    'class'     => 't',                                   
                    'is_bool'   => true,                                                                                     
                    'values'    => array(                                 
                        array(
                            'id'    => 'on',                           
                            'value' => 1,                                       
                            'label' => $this->l('Yes')     
                        ),
                        array(
                            'id'    => 'off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    ),
                ),
				array(
                    'type' => 'textarea',
                    'label' => $this->l('Text for admin'),
                    'lang' => true,
                    'name' => 'order_sms_admin',
                    'cols' => 40,
                    'rows' => 10,
                ),
				array(
                    'type'      => 'switch',                              
                    'label'     => $this->l('Send sms after customer confirm order for admin?'),        
                    'name'      => 'active_order_admin',                                                         
                    'class'     => 't',                                   
                    'is_bool'   => true,                                                                                     
                    'values'    => array(                                 
                        array(
                            'id'    => 'on',                           
                            'value' => 1,                                       
                            'label' => $this->l('Yes')     
                        ),
                        array(
                            'id'    => 'off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    ),
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save', array(), 'Admin.Actions'),
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = 'mod_kl_sms';
        $helper->identifier = $this->identifier;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        foreach (Language::getLanguages(false) as $lang) {
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );
        }
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'savemod_kl_sms_order';
        foreach (Language::getLanguages(false) as $lang) {
            $helper->fields_value['order_sms'][(int)$lang['id_lang']] = Configuration::get('ORDER_SMS_KLOOKVA_'.(int)$lang['id_lang']);
            $helper->fields_value['order_sms_admin'][(int)$lang['id_lang']] = Configuration::get('ORDER_SMS_ADMIN_KLOOKVA_'.(int)$lang['id_lang']);
        }
        $helper->fields_value['active_order'] = Configuration::get('ACTIVE_ORDER_SMS_KLOOKVA');
        $helper->fields_value['active_order_admin'] = Configuration::get('ACTIVE_ORDER_SMS_ADMIN_KLOOKVA');
        return $helper->generateForm($this->fields_form);
    }

    protected function initForm_tracking()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $this->fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Add tracking number'),
            ),
            'input' => array(
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Text for customer'),
                    'cols' => 40,
                    'rows' => 10,
                    'lang' => true,
                    'name' => 'tracking_sms',
                    'desc' => $this->l('Input %tracking% for show your tracking number'),
                ),
                array(
                    'type'      => 'switch',                              
                    'label'     => $this->l('Send sms after add tracking number for customer?'),        
                    'name'      => 'active_tracking',                                                           
                    'class'     => 't',                                   
                    'is_bool'   => true,                                                                                     
                    'values'    => array(                                 
                        array(
                            'id'    => 'on',                           
                            'value' => 1,                                       
                            'label' => $this->l('Yes')     
                        ),
                        array(
                            'id'    => 'off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    ),
                ),
				array(
                    'type' => 'textarea',
                    'label' => $this->l('Text for admin'),
                    'cols' => 40,
                    'rows' => 10,
                    'lang' => true,
                    'name' => 'tracking_sms_admin',
                    'desc' => $this->l('Input %tracking% for show your tracking number'),
                ),
				array(
                    'type'      => 'switch',                              
                    'label'     => $this->l('Send sms after add tracking number for admin?'),        
                    'name'      => 'active_tracking_admin',                                                           
                    'class'     => 't',                                   
                    'is_bool'   => true,                                                                                     
                    'values'    => array(                                 
                        array(
                            'id'    => 'on',                           
                            'value' => 1,                                       
                            'label' => $this->l('Yes')     
                        ),
                        array(
                            'id'    => 'off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    ),
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save', array(), 'Admin.Actions'),
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = 'mod_kl_sms';
        $helper->identifier = $this->identifier;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        foreach (Language::getLanguages(false) as $lang) {
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );
        }
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'savemod_kl_sms_tracking';
        foreach (Language::getLanguages(false) as $lang) {
            $helper->fields_value['tracking_sms'][(int)$lang['id_lang']] = Configuration::get('TRACKING_SMS_KLOOKVA_'.(int)$lang['id_lang']);
            $helper->fields_value['tracking_sms_admin'][(int)$lang['id_lang']] = Configuration::get('TRACKING_SMS_ADMIN_KLOOKVA_'.(int)$lang['id_lang']);
        }
        $helper->fields_value['active_tracking'] = Configuration::get('ACTIVE_TRACKING_SMS_KLOOKVA');
        $helper->fields_value['active_tracking_admin'] = Configuration::get('ACTIVE_TRACKING_SMS_ADMIN_KLOOKVA');
        return $helper->generateForm($this->fields_form);
    }

    protected function getListContent($id_lang)
    {
        return  Db::getInstance()->executeS('
            SELECT r.`id_sms`, r.`id_status`, r.`id_shop`, r.`is_active`, r.`is_active_admin`, rl.`text`, rl.`text_admin`
            FROM `'._DB_PREFIX_.'kl_sms_status` r
            LEFT JOIN `'._DB_PREFIX_.'kl_sms_status_lang` rl ON (r.`id_sms` = rl.`id_sms`)
            WHERE `id_lang` = '.(int)$id_lang.' '.Shop::addSqlRestrictionOnLang());
    }

    public function getPhone($phone){
    	if (!isset($phone) && (trim($phone) === '' || !Validate::isPhoneNumber($phone)))
    		return Configuration::get('PS_SHOP_PHONE');
    	else
    		return $phone;
    }

    public function hookActionValidateOrder ($params) {
    	$text = str_replace("%order%", $params['order']->reference, Configuration::get('ORDER_SMS_KLOOKVA_'.$this->context->language->id));
        $text = str_replace("%name%", $this->getNameCustomerByOrderId($params['order']->id), $text);
        $text_admin = str_replace("%order%", $params['order']->reference, Configuration::get('ORDER_SMS_ADMIN_KLOOKVA_'.$this->context->language->id));
        $text_admin = str_replace("%name%", $this->getNameCustomerByOrderId($params['order']->id), $text_admin);
        if (Configuration::get('ACTIVE_ORDER_SMS_KLOOKVA') == 1) 
            $this->sendSms($text ,$this->getPhoneByIdCustomer($params['order']->id_customer),$params['order']->id);
        if (Configuration::get('ACTIVE_ORDER_SMS_ADMIN_KLOOKVA') == 1) 
        	$this->sendSms($text_admin, $this->getPhone(Configuration::get('PHONE_SMS_KLOOKVA')), $params['order']->id, true);
    }

    public function hookActionOrderStatusUpdate ($params) {
        $kl_sms = statusClass::getSmsByStatusId($this->context->language->id,$params['id_order_state']);
        $text = str_replace("%order%", $params['order']->reference, $kl_sms->text);
        $text = str_replace("%name%", $this->getNameCustomerByOrderId($params['order']->id), $text);
        $text_admin = str_replace("%order%", $params['order']->reference, $kl_sms->text_admin);
        $text_admin = str_replace("%name%", $this->getNameCustomerByOrderId($params['order']->id), $text_admin);
        if (isset($kl_sms['id_status'])) {
        	if ($kl_sms->is_active == 1)
            	$this->sendSms($text,$this->getPhoneByIdCustomer($params['order']->id_customer),$params['order']->id);
            if ($kl_sms->is_active_admin == 1)
            	$this->sendSms($text_admin,$this->getPhone(Configuration::get('PHONE_SMS_KLOOKVA')),$params['order']->id,true);
        }
    }

    public function hookActionAdminOrdersTrackingNumberUpdate ($params) {
        $text = str_replace("%tracking%", $params['order']->shipping_number, Configuration::get('TRACKING_SMS_KLOOKVA_'.$this->context->language->id));
        $text = str_replace("%order%", $params['order']->reference, $text);
        $text = str_replace("%name%", $this->getNameCustomerByOrderId($params['order']->id), $text);
        $text_admin = str_replace("%tracking%", $params['order']->shipping_number, Configuration::get('TRACKING_SMS_ADMIN_KLOOKVA_'.$this->context->language->id));
        $text_admin = str_replace("%order%", $params['order']->reference, $text_admin);
        $text_admin = str_replace("%name%", $this->getNameCustomerByOrderId($params['order']->id), $text_admin);

        if (Configuration::get('ACTIVE_TRACKING_SMS_KLOOKVA') == 1)
           	$this->sendSms($text,$this->getPhoneByIdCustomer($params['order']->id_customer),$params['order']->id);
        if (Configuration::get('ACTIVE_TRACKING_SMS_ADMIN_KLOOKVA') == 1)
           	$this->sendSms($text_admin,$this->getPhone(Configuration::get('PHONE_SMS_KLOOKVA')),$params['order']->id,true);
    }

    public function getPhoneByIdCustomer ($id_customer) {
        $customer = new Customer($id_customer);
        $addresses = $customer->getAddresses($this->context->language->id);
        $phone = '';
        foreach ($addresses as $address)
        {
            if($phone == ''){
                if ($address['phone_mobile'] != '')
                    $phone = $address['phone_mobile'];
                elseif ($address['phone'] != '')
                    $phone = $address['phone'];
            } else
                break;
        }
        return $phone;
    }

    public function getCustomerIdByPhone($phone) 
    {
        return Db::getInstance()->getValue(
            'SELECT id_customer
            FROM '._DB_PREFIX_.'address
            WHERE phone = "'.pSQL($phone).'" OR phone_mobile = "'.pSQL($phone).'"'
        );
    }

    public function getNameCustomerByOrderId($id_order) {
    	$order = new Order($id_order);
    	return $order->getCustomer()->firstname.' '.$order->getCustomer()->lastname;
    }

    public function hookDisplayAdminOrderTabOrder ($params) {
        $this->context->smarty->assign('urltosave', $this->context->link->getModuleLink('mod_kl_sms','sendSmsInOrder'));
        $this->context->smarty->assign('phone', $this->getPhoneByIdCustomer($params['order']->id_customer));
        $this->context->smarty->assign('id_order', $params['order']->id);
        $this->context->smarty->assign('elements', orderClass::getSmsByOrderId($this->context->language->id, $params['order']->id));
        return $this->display(__FILE__,'views/templates/hook/mod_kl_sms.tpl');
    }

    public function SaveRecord($sms,$id_order,$is_admin){
        $sms_order = new orderClass();
        $sms_order->id_order = $id_order;
        $sms_order->id_shop = $this->context->shop->id;
        $sms_order->date = date("Y-m-d H:i:s");
        if ($is_admin == false)
        	$sms_order->text[$this->context->language->id] = $sms;
        if ($is_admin == true)
        	$sms_order->text_admin[$this->context->language->id] = $sms;
        $sms_order->save();
    }

    public function sendSms($sms,$phones, $id_order=0, $is_admin=false, $ajax = true) 
    {
        require_once(_PS_MODULE_DIR_.'mod_kl_sms/lib/nusoap.php');
        $url = "http://klookva.com.ua/test/shasoapserver.php?wsdl";
        $client = new nusoap_client($url);

        $err = $client->getError();

        if ($err) {
            echo '<p><b>Error: ' . $err . '</b></p>';
        }
        $phones = str_replace(array(' ','-','_','(',')','.'),'',$phones);
        $site_name = $_SERVER['HTTP_HOST'];
        if (preg_match('/www\.([^\s]*)$/', $site_name, $change))
            $site_name = $change[1];
        $ip = gethostbyname($site_name);
        $tmp_phones = explode(',', $phones);
        $i = 0;
        $new_phones = array();
        while ($i < sizeof($tmp_phones)) {
            $string = $tmp_phones[$i];
            $pattern = '/([\s])/';
            $replacement = '';
            $repl_phone=preg_replace($pattern, $replacement, $string);
            if (preg_match('/^\+38[0-9]{10}$/', $tmp_phones[$i]) == TRUE)
                array_push($new_phones, $tmp_phones[$i]);
            elseif (preg_match('/^38[0-9]{10}$/', $tmp_phones[$i]) == TRUE)
                array_push($new_phones, '+' . $tmp_phones[$i]);
            elseif (preg_match('/^8[0-9]{10}$/', $tmp_phones[$i]) == TRUE)
                array_push($new_phones, '+3' . $tmp_phones[$i]);
            elseif (preg_match('/^[0-9]{10}$/', $tmp_phones[$i]) == TRUE)
                array_push($new_phones, '+38' . $tmp_phones[$i]);
            $i++;
        }  
        if ($ajax) {
            if(!empty($new_phones)){
                $args = array('login' => Configuration::get('LOGIN_SMS_KLOOKVA'), 'pass' => sha1(Configuration::get('PASSWORD_SMS_KLOOKVA')), 'smstext' => $sms, 'phones' => implode(',', $new_phones), 'ip' => $ip, 'site_name' => $site_name, 'title' => Tools::getHttpHost(false));
                $return = $client->call('SendMessege', array($args));
                if ($return['sent'] == '1') {
                    $this->SaveRecord($sms, $id_order, $is_admin);
                    $result = array('error' => 0, 'message' => $this->l('Sms send successfull', $this->context->language->id));
                } else {
                    $result = array('error' => 1, 'message' => $this->l('Error', $this->context->language->id));
                } 
            } else {
                $result = array('error' => 1, 'message' => $this->l('Error wrong number', $this->context->language->id));
            }
            if (stristr($_SERVER['REQUEST_URI'],'order-confirmation') === false) {
                echo json_encode($result);
            }
        } else {
            if(!empty($new_phones)) {
                $args = array('login' => Configuration::get('LOGIN_SMS_KLOOKVA'), 'pass' => sha1(Configuration::get('PASSWORD_SMS_KLOOKVA')), 'smstext' => $sms, 'phones' => implode(',', $new_phones), 'ip' => $ip, 'site_name' => $site_name, 'title' => Tools::getHttpHost(false));
                $return = $client->call('SendMessege', array($args));
                if ($return['sent'] == '1') {
                    return true;
                } else {
                    return false;
                } 
            } else {
                return false;
            }
        }
    }
}