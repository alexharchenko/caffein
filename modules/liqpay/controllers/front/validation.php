<?php

class liqpayValidationModuleFrontController extends ModuleFrontController
{
    public function initContent() 
    {
        parent::initContent();

        PrestaShopLogger::addLog('Check validate', 1, null, 'PaymentOptions', 1, true);

        if (!Tools::getValue('signature') || !Tools::getValue('data')) {
            die($this->module->l('No data'));
        }

        $key = Configuration::get('LIQPAY_PRIVATE_KEY') . Tools::getValue('data') . Configuration::get('LIQPAY_PRIVATE_KEY');
        $signature = base64_encode( sha1($key, 1) );
        $parsed_data = json_decode(base64_decode(Tools::getValue('data')));
        if (Tools::getValue('signature') === $signature && $parsed_data->public_key === Configuration::get('LIQPAY_PUBLIC_KEY')) {
            PrestaShopLogger::addLog('Status: '. $parsed_data->status, 1, null, 'PaymentOptions', 1, true);
            $order = Order::getByCartId((int)$parsed_data->order_id);
            if ($parsed_data->status == 'error' || $parsed_data->status == 'failure') {
                $order->setCurrentState(Configuration::get('LIQPAY_ERROR'));
            } elseif ($parsed_data->status == 'success' || $parsed_data->status == 'ok') {
                $order->setCurrentState(Configuration::get('LIQPAY_SUCCESS'));
            } else {
                $order->setCurrentState(Configuration::get('LIQPAY_ETC'));
            }
            echo $this->module->l('Успешная смена статуса');exit;
        } else {
            echo $this->module->l('Неверные ключи');exit;
        }
    }
}
