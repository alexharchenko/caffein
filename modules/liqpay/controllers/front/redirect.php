<?php
/**
 * Liqpay Payment Module
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category        Liqpay
 * @package         Liqpay
 * @version         0.1
 * @author          Liqpay
 * @copyright       Copyright (c) 2014 Liqpay
 * @license         http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 *
 * EXTENSION INFORMATION
 *
 * Prestashop       1.5.6.2
 * LiqPay API       https://www.liqpay.ua/documentation/ru
 *
 */

class liqpayRedirectModuleFrontController extends ModuleFrontController
{
	public $ssl = true;

	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		parent::initContent();
		$id_cart = Tools::getValue('id_cart');
		$cart = New Cart((int)$id_cart);
		$total = $cart->getOrderTotal(true, Cart::BOTH);
		if (!$this->module->validateOrder(
			intval($cart->id),
			Configuration::get('PS_OS_PREPARATION'),
			$total,
			$this->module->displayName
		)) {
			return false;
		}
		$currency = new Currency((int)($cart->id_currency));

		$private_key = Configuration::get('LIQPAY_PRIVATE_KEY');
		$public_key  = Configuration::get('LIQPAY_PUBLIC_KEY');
		$amount = number_format($cart->getOrderTotal(true, Cart::BOTH), 1, '.', '');
		$currency = $currency->iso_code == 'RUR' ? 'RUB' : $currency->iso_code;
		$order_id = $this->module->addZeros($id_cart);
		$description = 'Order #'.$order_id;
		$customer = new Customer($cart->id_customer);

		$result_url = Tools::getHttpHost(true).'/index.php?controller=order-confirmation&id_cart='.(int)$cart->id.'&id_module='.(int)$this->module->id.'&id_order='.$this->module->currentOrder.'&key='.$customer->secure_key;
		$server_url = $this->context->link->getModuleLink($this->module->name, 'validation');
		// $result_url = 'http://5b27f088.ngrok.io/index.php?controller=order-confirmation&id_cart='.(int)$cart->id.'&id_module='.(int)$this->module->id.'&id_order='.$this->module->currentOrder.'&key='.$customer->secure_key;
		// $server_url = 'http://5b27f088.ngrok.io/module/liqpay/validation';
		$type = 'buy';

		$version = '3';
		$language = Configuration::get('PS_LOCALE_LANGUAGE') == 'en' ? 'en' : 'ru';

		$data = base64_encode(
			        json_encode(
						    array('version'     => $version,
						    	  'public_key'  => $public_key,
						    	  'amount'      => $amount,
						    	  'currency'    => $currency,
						 		  'description' => $description,
						    	  'order_id'    => $order_id,
						    	  'type'        => $type,
						    	  'language'    => $language,
						    	  'sandbox'		=> 0,
						    	  'result_url'	=> $result_url,
						    	  'server_url' 	=> $server_url
						    	)
			        		)
		        		);

		$signature = base64_encode(sha1($private_key.$data.$private_key, 1));
        
		$this->context->smarty->assign(compact('data', 'signature'));

		$this->setTemplate('module:liqpay/views/templates/front/redirect.tpl');
	}
}
