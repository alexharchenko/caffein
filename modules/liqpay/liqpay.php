<?php
/**
 * Liqpay Payment Module
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category        Liqpay
 * @package         Liqpay
 * @version         3.0
 * @author          Liqpay
 * @copyright       Copyright (c) 2014 Liqpay
 * @license         http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 *
 * EXTENSION INFORMATION
 *
 * Prestashop       1.5.6.2
 * LiqPay API       https://www.liqpay.ua/documentation/ru
 *
 */


if (!defined('_PS_VERSION_')) { exit; }
use PrestaShop\PrestaShop\Core\Payment\PaymentOption;
include_once _PS_MODULE_DIR_.'liqpay/classes/LiqPaySDK.php';
/**
 * Payment method liqpay form
 *
 * @author      Liqpay <support@liqpay.ua>
 */
class Liqpay extends PaymentModule
{
	protected $supported_currencies = array('EUR','UAH','USD','RUB');

	public $liqpay_public_key = '';
	public $liqpay_private_key = '';

    /**
     * Costructor
     *
     * @return Liqpay
     */
	public function __construct()
	{
		$this->name = 'liqpay';
		$this->tab = 'payments_gateways';
		$this->version = '3.0.1';
		$this->author = 'Liqpay';
		// $this->need_instance = 0;
		$this->controllers = array('payment', 'validation');
		$this->currencies = true;
        $this->currencies_mode = 'checkbox';

        $config = Configuration::getMultiple(array('LIQPAY_PUBLIC_KEY','LIQPAY_PRIVATE_KEY'));
		if (isset($config['LIQPAY_PUBLIC_KEY']) && $config['LIQPAY_PUBLIC_KEY']) {
			$this->liqpay_public_key = $config['LIQPAY_PUBLIC_KEY'];
		}
		if (isset($config['LIQPAY_PRIVATE_KEY']) && $config['LIQPAY_PRIVATE_KEY']) {
			$this->liqpay_private_key = $config['LIQPAY_PRIVATE_KEY'];
		}
		$this->bootstrap = true;
		parent::__construct();
        // $this->page = basename(__FILE__, '.php');
        $this->displayName = 'Liqpay';
        $this->description = $this->l('Accept payments with Liqpay');
        $this->confirmUninstall = $this->l('Are you sure you want to delete your details ?');
        $correctly = !isset($this->liqpay_public_key) OR !isset($this->liqpay_private_key);
        if ($correctly) {
        	$this->warning = $this->l('Your Liqpay account must be set correctly');
        }
        $this->ps_versions_compliancy = array('min' => '1.6.1.0', 'max' => _PS_VERSION_);
	}

    /**
     * Return module web path
     *
     * @return string
     */
	public function getPath()
	{
		return $this->_path;
	}

    /**
     * Install module
     *
     * @return bool
     */
	public function install()
	{
		return parent::install() 
				&& $this->registerHook('payment')
				&& $this->registerHook('paymentOptions')
           		;
	}


    /**
     * Uninstall module
     *
     * @return bool
     */
	public function uninstall()
	{
		return
			parent::uninstall() &&
			Configuration::deleteByName('LIQPAY_PUBLIC_KEY') &&
			Configuration::deleteByName('LIQPAY_SUCCESS') &&
			Configuration::deleteByName('LIQPAY_ERROR') &&
			Configuration::deleteByName('LIQPAY_ETC') &&
			Configuration::deleteByName('LIQPAY_PRIVATE_KEY');
	}


    /**
     * Hook payment
     *
     * @param array $params
     *
     * @return string
     */
	public function hookPaymentOptions($params)
	{
		if (!$this->active) {
		 	PrestaShopLogger::addLog('LIQPAY not active', 3, null, 'PaymentOptions', (int)$id_cart, true);
            return;
        }
        if (!$this->checkCurrency($params['cart'])) {
        	PrestaShopLogger::addLog('LIQPAY currency check failed', 3, null, 'PaymentOptions', (int)$id_cart, true);
            return;
        }

        $this->smarty->assign(
            $this->getTemplateVars()
        );
        $this->smarty->assign(array(
            'this_path' => $this->_path,
            'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->name.'/',
		 	'id' => (int)$params['cart']->id,
        ));
        $newOption = new PaymentOption();
        $newOption->setModuleName($this->name)
                ->setCallToActionText($this->trans('Visa/Mastercard', array(), 'Modules.liqpay.Admin'))
                ->setAction($this->context->link->getModuleLink('liqpay', 'redirect', ['id_cart' => $params['cart']->id]))
                //->setAction($this->context->link->getModuleLink($this->name, 'validation', array(), true))
                ->setAdditionalInformation($this->fetch('module:liqpay/views/templates/hook/payment.tpl'));

        return [$newOption];
	}

	public function getTemplateVars()
    {
        $cart = $this->context->cart;
        $total = $this->trans(
            '%amount% (tax incl.)',
            array(
                '%amount%' => Tools::displayPrice($cart->getOrderTotal(true, Cart::BOTH)),
            ),
            'Modules.Checkpayment.Admin'
        );

        $checkOrder = Configuration::get('CHEQUE_NAME');
        if (!$checkOrder) {
            $checkOrder = '___________';
        }

        $checkAddress = Tools::nl2br(Configuration::get('CHEQUE_ADDRESS'));
        if (!$checkAddress) {
            $checkAddress = '___________';
        }

        return [
            'checkTotal' => $total,
            'checkOrder' => $checkOrder,
            'checkAddress' => $checkAddress,
        ];
    }


    /**
     * Check currency
     *
     * @param  Cart $cart
     *
     * @return bool
     */
	public function checkCurrency($cart)
	{
		$currency_order = new Currency((int)($cart->id_currency));
		$currencies_module = $this->getCurrency((int)$cart->id_currency);
		if (is_array($currencies_module)) {
			foreach ($currencies_module as $currency_module) {
				if ($currency_order->id == $currency_module['id_currency']){
					return true;
				}
			}
		}
		return false;
	}

	private function _displayCheck()
    {
        return $this->display(__FILE__, './views/templates/hook/infos.tpl');
    }

    public function addZeros ($id_cart)
    {
    	$res = '';
        switch (strlen((string)$id_cart)) {
        	case 1:
        		$res = '0000'.$id_cart;
        		break;
        	case 2:
        		$res = '000'.$id_cart;
        		break;
        	case 3:
        		$res = '00'.$id_cart;
        		break;
        	case 1:
        		$res = '0'.$id_cart;
        		break;
        	default:
        		$res = $id_cart;
        		break;
        }
        return $res;
    }

    /**
     * Get a configuration page
     *
     * @return string
     */
	public function getContent()
	{
	    $this->_html = '';
	    if (Tools::isSubmit('submit'.$this->name)) {
			$liqpay_public_key = strval(Tools::getValue('LIQPAY_PUBLIC_KEY'));
			$liqpay_private_key = strval(Tools::getValue('LIQPAY_PRIVATE_KEY'));

            $err = !$liqpay_public_key  || empty($liqpay_public_key)  || !Validate::isGenericName($liqpay_public_key)  ||
                   !$liqpay_private_key || empty($liqpay_private_key) || !Validate::isGenericName($liqpay_private_key);

	        if ($err) {
	            $this->_html .= $this->displayError( $this->l('Invalid Configuration value') );
	        } else {
	            Configuration::updateValue('LIQPAY_PUBLIC_KEY', $liqpay_public_key);
	            Configuration::updateValue('LIQPAY_PRIVATE_KEY', $liqpay_private_key);
	            Configuration::updateValue('LIQPAY_SUCCESS', Tools::getValue('id_status_suc'));
	            Configuration::updateValue('LIQPAY_ERROR', Tools::getValue('id_status_err'));
	            Configuration::updateValue('LIQPAY_ETC', Tools::getValue('id_status_etc'));
				$this->liqpay_public_key = $liqpay_public_key;
				$this->liqpay_private_key = $liqpay_private_key;
	            $this->_html .= $this->displayConfirmation($this->l('Settings updated'));
	        }
	    }
	    return $this->_displayCheck().$this->_html.$this->displayForm();
	}


    /**
     * Generate form
     *
     * @return string
     */
	public function displayForm()
	{
	    $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
	    $fields_form[0]['form'] = array(
	        'legend' => array(
	            'title' => $this->l('Settings'),
	        ),
	        'input' => array(
	            array(
	                'type' => 'text',
	                'label' => $this->l('Public key'),
	                'name' => 'LIQPAY_PUBLIC_KEY',
	                'size' => 20,
	                'required' => true
	            ),
	            array(
	                'type' => 'text',
	                'label' => $this->l('Private key'),
	                'name' => 'LIQPAY_PRIVATE_KEY',
	                'size' => 20,
	                'required' => true
	            ),
	            array(
                    'type' => 'select',
                    'label' => $this->l('Status for success'),
                    'name' => 'id_status_suc',
                    'class' => 'input-lg',
                    'options' => array(
                        'query' => OrderState::getOrderStates($this->context->language->id),
                        'id' => 'id_order_state',
                        'name' => 'name'
                    )
                ),
               	array(
                    'type' => 'select',
                    'label' => $this->l('Status for error'),
                    'name' => 'id_status_err',
                    'class' => 'input-lg',
                    'options' => array(
                        'query' => OrderState::getOrderStates($this->context->language->id),
                        'id' => 'id_order_state',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Status for else'),
                    'name' => 'id_status_etc',
                    'class' => 'input-lg',
                    'options' => array(
                        'query' => OrderState::getOrderStates($this->context->language->id),
                        'id' => 'id_order_state',
                        'name' => 'name'
                    )
                ),
	        ),
	        'submit' => array(
	            'title' => $this->l('Save'),
	            'class' => 'button'
	        )
	    );

	    $helper = new HelperForm();
	    $helper->module = $this;
	    $helper->name_controller = $this->name;
	    $helper->token = Tools::getAdminTokenLite('AdminModules');
	    $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
	    $helper->default_form_language = $default_lang;
	    $helper->allow_employee_form_lang = $default_lang;
	    $helper->title = $this->displayName;
	    $helper->show_toolbar = true;
	    $helper->toolbar_scroll = true;
	    $helper->submit_action = 'submit'.$this->name;
	    $helper->toolbar_btn = array(
	        'save' => array(
	            'desc' => $this->l('Save'),
	            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
	            '&token='.Tools::getAdminTokenLite('AdminModules'),
	        ),
	        'back' => array(
	            'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
	            'desc' => $this->l('Back to list')
	        )
	    );
		$helper->fields_value['LIQPAY_PUBLIC_KEY'] = $this->liqpay_public_key;
		$helper->fields_value['LIQPAY_PRIVATE_KEY'] = $this->liqpay_private_key;
		$helper->fields_value['id_status_suc'] = Configuration::get('LIQPAY_SUCCESS');
		$helper->fields_value['id_status_err'] = Configuration::get('LIQPAY_ERROR');
		$helper->fields_value['id_status_etc'] = Configuration::get('LIQPAY_ETC');
	    return $helper->generateForm($fields_form);
	}

}