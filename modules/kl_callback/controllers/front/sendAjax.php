<?php
class kl_callbacksendAjaxModuleFrontController extends ModuleFrontController
{
	// public $ssl = true;
	public function initContent()
	{
		parent::initContent();
		if($this->recordDataToDatabase(Tools::getValue('name'),Tools::getValue('organisation'),Tools::getValue('phone'),Tools::getValue('email'),Tools::getValue('message'),Tools::getValue('url'))){
			$to = Configuration::get('CALLBACK_EMAIL', $this->context->language->id);
			if (!filter_var($to, FILTER_VALIDATE_EMAIL)) {
				$to = strval(Configuration::get('PS_SHOP_EMAIL'));
			}

			if($this->sendMail($to,Tools::getValue('name'),Tools::getValue('organisation'),Tools::getValue('phone'),Tools::getValue('email'),Tools::getValue('message'))){
				$result = array('error' => 0, 'message' => $this->l('Thank you for your message','sendAjax'));
				echo json_encode($result);
			}
			else{
				$result = array('error' => 1, 'message' => $this->module->l('Error Ocured','sendAjax'));
				echo json_encode($result);
			}
		}
		else{
			$result = array('error' => 1, 'message' => $this->l('System Error Ocured','sendAjax'));
			echo json_encode($result);
		}
	}

	function sendMail($to,$name,$organisation,$phone,$email,$message){
		if (!filter_var($to, FILTER_VALIDATE_EMAIL)) {
			return false;
		}

		$templatePath = dirname(__FILE__).'../../views/templates/mail/';
		/**
	     * Send Email
	     *
	     * @param int    $idLang         Language ID of the email (to translate the template)
	     * @param string $template       Template: the name of template not be a var but a string !
	     * @param string $subject        Subject of the email
	     * @param string $templateVars   Template variables for the email
	     * @param string $to             To email
	     * @param string $toName         To name
	     * @param string $from           From email
	     * @param string $fromName       To email
	     * @param array  $fileAttachment Array with three parameters (content, mime and name). You can use an array of array to attach multiple files
	     * @param bool   $mode_smtp      SMTP mode (deprecated)
	     * @param string $templatePath   Template path
	     * @param bool   $die            Die after error
	     * @param int    $idShop         Shop ID
	     * @param string $bcc            Bcc recipient address. You can use an array of array to send to multiple recipients
	     * @param string $replyTo        Reply-To recipient address
	     * @param string $replyToName    Reply-To recipient name
	     *
	     * @return bool|int Whether sending was successful. If not at all, false, otherwise amount of recipients succeeded.
	     */

		if(!Mail::Send($this->context->language->id,'mail',$this->l('Newsletter confirmation', $this->context->language->id),array('name' => $name,'organisation' => $organisation,'phone' => $phone,'email' => $email,'message' => $message),pSQL($to),null,null,null,null,null,$templatePath,false,$this->context->shop->id)){
			return false;
		}
		return true;
	}
	function recordDataToDatabase($name,$organisation,$phone,$email,$message,$url){
		if (Db::getInstance()->insert('kl_callback', array(
			'name' => pSql($name),
			'organisation' => pSql($organisation),
			'phone' => pSql($phone),
			'email' => pSql($email),
		    'message' => pSql($message),
		    'date' => date('Y-m-d H:i:s'),
		    'page' => pSql($url)
		)))
			return true;
		else 
			return false;
	}
}
