<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Private License.
* Any copies are illegal
*
*
*  @author klookva <contact@klookva.com.ua>
*  @copyright  2007-2017 kLookva
*  International Property of klookva
*/


/*
    Module: ps_kl_callback
    - id
    - phone
    - date
    - page

*/
if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;
class kl_callback extends Module implements WidgetInterface
{
    private $templateFile;
    private $templateFileModal;
    public $dbfiles = '/db/install.php';
	public function __construct()
	{
		$this->name = 'kl_callback';
		$this->version = '1.0.0';
		$this->author = 'kLooKva';
		$this->need_instance = 0;
        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('Tasting entry');
        $this->description = $this->l('Displays a callback on your shop front-end.');
        $this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);
        $this->templateFile = 'module:kl_callback/kl_callback.tpl';
        $this->templateFileModal = 'module:kl_callback/kl_callbackModal.tpl';
    }


    private function initList()
    {
        $where = $this->createWhereSQL();
        $orderby = $this->createOrderSQL();
        $limit = $this->createLimitSQL();

        $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'kl_callback'.$where.$orderby.$limit;
        $result = Db::getInstance()->ExecuteS($sql);

        $sql_all_records = 'SELECT * FROM ' . _DB_PREFIX_ . 'kl_callback'.$where.$orderby;
        $result_all = Db::getInstance()->ExecuteS($sql_all_records);

        $fields_list = array(
            'id' => array(
                'title' => $this->l('Id'),
                'width' => 140,
                'type' => 'text',
            ),
            'date' => array(
                'title' => $this->l('Date'),
                'type' => 'text',
            ),
            'name' => array(
                'title' => $this->l('Name'),
                'type' => 'text',
            ),
            'organisation' => array(
                'title' => $this->l('Organisation'),
                'type' => 'text',
            ),
            'phone' => array(
                'title' => $this->l('Phone'),
                'type' => 'text',
            ),
            'email' => array(
                'title' => $this->l('Email'),
                'type' => 'text',
            ),
            'message' => array(
                'title' => $this->l('Message'),
                'type' => 'text',
            ),
            'page' => array(
                'title' => $this->l('Page Url'),
                'type' => 'text',
            ),
        );
        $helper = new HelperList();
        $helper->simple_header = false;
        $helper->identifier = 'id';
        $helper->show_toolbar = false;
        $helper->title = 'Callback orders';
        $helper->table = 'kl_callback';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->listTotal = count($result_all);
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        return $helper->generateList($result,$fields_list);
    }

    public function install()
    {
        return (parent::install() &&
            $this->registerHook('actionFrontControllerSetMedia') &&
            $this->registerHook('displayHeader') &&
            $this->registerHook('displayCallback') &&
            $this->registerHook('displayCallbackCMSPage') &&
            $this->registerHook('actionObjectLanguageAddAfter') &&
            $this->registerHook('displayBeforeBodyClosingTag') &&
            $this->installSQL());
    }

    public function hookActionObjectLanguageAddAfter($params)
    {
        return $this->installFixture((int)$params['object']->id, Configuration::get('CALLBACK_EMAIL', (int)Configuration::get('PS_LANG_DEFAULT')));
    }

    protected function installFixture($id_lang, $image = null)
    {
        $values['CALLBACK_EMAIL'][(int)$id_lang] = $image;
        $values['CALLBACK_CSS'] = '';
        $values['CALLBACK_JS'] = '';

        Configuration::updateValue('CALLBACK_EMAIL', $values['CALLBACK_EMAIL']);
        Configuration::updateValue('CALLBACK_CSS', $values['CALLBACK_CSS']);
        Configuration::updateValue('CALLBACK_JS', $values['CALLBACK_JS']);
    }

    public function uninstall()
    {
        Configuration::deleteByName('CALLBACK_EMAIL');
        Configuration::deleteByName('CALLBACK_CSS');
        Configuration::deleteByName('CALLBACK_JS');
        $this->unInstallSQL();
        return parent::uninstall();
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitStoreConf')) {
            $languages = Language::getLanguages(false);
            $values = array();
            foreach ($languages as $lang) {
                $values['CALLBACK_EMAIL'][$lang['id_lang']] = Tools::getValue('CALLBACK_EMAIL_'.$lang['id_lang']); 
            }
            Configuration::updateValue('CALLBACK_EMAIL', $values['CALLBACK_EMAIL']);
            Configuration::updateValue('CALLBACK_CSS', Tools::getValue('CALLBACK_CSS_'));
            Configuration::updateValue('CALLBACK_JS', Tools::getValue('CALLBACK_JS_'));
            $this->_clearCache($this->templateFile);
            $this->_clearCache($this->templateFileModal);
            return $this->displayConfirmation($this->l('The settings have been updated.', array(), 'Admin.Notifications.Success'));
        }

        return '';
    }

    public function getContent()
    {
        return $this->postProcess().$this->renderForm().$this->initList();
    }

    public function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings', array(), 'Admin.Global'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Contact Email'),
                        'name' => 'CALLBACK_EMAIL',
                        'desc' => $this->l('Specify email to receive messages or leave it blank to use default Shop email'),
                        'lang' => true,

                    ),
                    array(
                        'type' => 'checkbox',
                        'label' => $this->l('Include CSS'),
                        'name' => 'CALLBACK_CSS',
                        'desc' => $this->l('Include module css to a page. If not - template styles will be used (* using bootstrap styles)'),
                        'values'  => array(
                            'query' => array('id_checkbox_options' => 0),
                            'id'    => 'id_checkbox_options',                                          
                            'name'  => 'NAME',                                       
                          ),
                        'lang' => false,
                        
                    ),
                     array(
                        'type' => 'checkbox',
                        'label' => $this->l('Include JS'),
                        'name' => 'CALLBACK_JS',
                        'desc' => $this->l('Include module js to a page. If not - template JS will be used (* using bootstrap JS)'),
                        'values'  => array(
                            'query' => array('id_checkbox_options' => 0),
                            'id'    => 'id_checkbox_options',                                          
                            'name'  => 'NAME',                                       
                          ),
                        
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save', array(), 'Admin.Actions')
                )
            ),
        );

        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitStoreConf';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $this->getPathUri(),
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }
    public function getConfigFieldsValues()
    {   
        $languages = Language::getLanguages(false);
        $fields = array();
        
        foreach ($languages as $lang) {
            $fields['CALLBACK_EMAIL'][(int)$lang['id_lang']] = Tools::getValue('CALLBACK_EMAIL_'.$lang['id_lang'], Configuration::get('CALLBACK_EMAIL_'.$lang['id_lang'], $lang['id_lang']));
        }
        $fields['CALLBACK_CSS_'] = Tools::getValue('CALLBACK_CSS', Configuration::get('CALLBACK_CSS'));
        $fields['CALLBACK_JS_'] = Tools::getValue('CALLBACK_JS', Configuration::get('CALLBACK_JS'));
        return $fields;
    }

    public function hookActionFrontControllerSetMedia($params){
         if(Tools::getValue('CALLBACK_CSS', Configuration::get('CALLBACK_CSS'))=="on"){
            $this->context->controller->registerStylesheet('kl-callback-style','modules/'.$this->name.'/css/callback-style.css',['media' => 'all','priority' => 200]);
        }
        
        if(Tools::getValue('CALLBACK_JS', Configuration::get('CALLBACK_JS'))=="on")
            $this->context->controller->addJS($this->_path.'js/bootstrap.min.js');

        $this->context->controller->addJS($this->_path.'js/functions.js');
    }

    public function hookDisplayCallbackCMSPage($params)
    {
        $this->context->smarty->assign($this->getWidgetVariables(null,[]));
        
        return $this->display(__FILE__, 'kl_callbackCMSPage.tpl');
    }

    public function renderWidget($hookName, array $params)
    {
        if (!$this->isCached($this->templateFile, $this->getCacheId('kl_callback'))) 
            $this->smarty->assign($this->getWidgetVariables($hookName, $params));
        
        return $this->fetch($this->templateFile, $this->getCacheId('kl_callback'));
    }

    public function hookDisplayBeforeBodyClosingTag()
    {
        $this->context->smarty->assign($this->getWidgetVariables(null,[]));
        
        return $this->display(__FILE__, 'kl_callbackModal.tpl');
    }

    public function getWidgetVariables($hookName, array $params)
    {
        $callback_email = Configuration::get('CALLBACK_EMAIL', $this->context->language->id);
        $this->smarty->assign('callback_email', $callback_email);
        $this->smarty->assign('ajaxurl',$this->context->link->getModuleLink('kl_callback','sendAjax'));

        return array(
            'callback_email' => $callback_email
        );
    }
    public function installSQL()
    {
    	$query = array();
        $query[] = 'CREATE TABLE `'._DB_PREFIX_.'kl_callback` (
		  `id` int(10) NOT NULL,
		  `name` varchar(255) NOT NULL,
		  `organisation` varchar(255),
		  `phone` varchar(255) NOT NULL,
		  `email` varchar(255),
		  `message` varchar(255),
		  `date` datetime NOT NULL,
		  `page` varchar(255) NOT NULL
		) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

		$query[] = 'ALTER TABLE `'._DB_PREFIX_.'kl_callback`
		  ADD PRIMARY KEY (`id`);
		ALTER TABLE `ps_kl_callback`
		  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT';

        if(isset($query) && !empty($query)) {
            foreach($query as $query_single){
                if(!Db::getInstance()->Execute($query_single))
                    return false;
            }
        }
        return true;
    }
     public function unInstallSQL()
    {
		$querydrop = array();
		$querydrop[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'kl_callback`';
	    if(isset($querydrop) && !empty($querydrop)) {
	        foreach($querydrop as $query_single){
	            if(!Db::getInstance()->Execute($query_single))
	                return false;
	        }
	    }
        return true;
    }

    public function createWhereSQL(){
        $filter_id = Tools::getValue('kl_callbackFilter_id');
        $filter_date = Tools::getValue('kl_callbackFilter_date');
        $filter_phone = Tools::getValue('kl_callbackFilter_phone');
        $filter_page = Tools::getValue('kl_callbackFilter_page');

        $firstset = false;
        $where = '';
        if(strlen($filter_id)>0){
            $where = ' WHERE id = '.$filter_id;
            $firstset = true;
        }
        if(strlen($filter_date)>0){
            $where .= $this->addRowLikeSQL('date',$filter_date,$firstset);
            $firstset = true;
        }
        if(strlen($filter_phone)>0){
            $where .= $this->addRowLikeSQL('phone',$filter_phone,$firstset);
            $firstset = true;
        }
        if(strlen($filter_page)>0){
            $where .= $this->addRowLikeSQL('page',$filter_page,$firstset);
            $firstset = true;
        }
        return $where;
    }
    public function addRowLikeSQL($name,$param,$firstset){
        if($firstset===true)
            return " AND ".$name." LIKE '%".$param."%'";
        else
            return " WHERE ".$name." LIKE '%".$param."%'";
    }

    public function createOrderSQL(){
        $orderby = '';
        if (Tools::getValue('kl_callbackOrderby'))
            $orderby = ' ORDER BY '.Tools::getValue('kl_callbackOrderby').' '.Tools::getValue('kl_callbackOrderway', 'ASC');

        return $orderby;
    }
    public function createLimitSQL(){
        $page = intval(Tools::getValue('submitFilterkl_callback',1));
        if($page<1) $page = 1;
        $kl_callback_pagination = Tools::getValue('kl_callback_pagination',50);
        $limit = ' LIMIT '.($page-1)*$kl_callback_pagination.','.$kl_callback_pagination;
        return $limit;
    }
    function sendMail($to,$phone){
        if (!filter_var($to, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        $templatePath = dirname(__FILE__).'../../views/templates/mail/';
        /**
         * Send Email
         *
         * @param int    $idLang         Language ID of the email (to llate the template)
         * @param string $template       Template: the name of template not be a var but a string !
         * @param string $subject        Subject of the email
         * @param string $templateVars   Template variables for the email
         * @param string $to             To email
         * @param string $toName         To name
         * @param string $from           From email
         * @param string $fromName       To email
         * @param array  $fileAttachment Array with three parameters (content, mime and name). You can use an array of array to attach multiple files
         * @param bool   $mode_smtp      SMTP mode (deprecated)
         * @param string $templatePath   Template path
         * @param bool   $die            Die after error
         * @param int    $idShop         Shop ID
         * @param string $bcc            Bcc recipient address. You can use an array of array to send to multiple recipients
         * @param string $replyTo        Reply-To recipient address
         * @param string $replyToName    Reply-To recipient name
         *
         * @return bool|int Whether sending was successful. If not at all, false, otherwise amount of recipients succeeded.
         */

        if(!Mail::Send($this->context->language->id,'mail',$this->l('Newsletter confirmation', $this->context->language->id),array('phone' => '380999999999'),pSQL($to),null,null,null,null,null,$templatePath,false,$this->context->shop->id)){
            return false;
        }
        return true;
    }
}
