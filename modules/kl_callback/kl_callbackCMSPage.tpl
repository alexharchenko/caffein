<p>
	<div class="btn btn-primary" data-toggle="modal" data-target="#callBackPopupCMS">{l s='Подать заявку' mod='kl_callback'}</div>
</p>

<div id="callBackPopupCMS" block-type="callback" class="modal fade big" tabindex="-1" role="dialog" aria-labelledby="writeToUsLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content animated flipInX">
			<div class="modal-header container text-center">
				<div class="h3">{l s='Tasting entry' mod='kl_callback'}</div>
				<p>{l s='Leave your phone number and we will contact you as soon as possible.' mod='kl_callback'}</p>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true"><i class="icon close"><img src="{$urls.img_url}icons/close.png" /></i></span>
		        </button>
		    </div>
		    <div class="modal-body container ">
				<form class="text-left">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group label-floating">
								<label for="i3" class="control-label">{l s='Name' mod='kl_callback'}</label>
							    <input type="text" name="name" class="form-control callback-name js-callback-phone" />
							    <label for="i4" class="control-label">{l s='Organisation' mod='kl_callback'}</label>
							    <input type="text" name="organisation" class="form-control callback-organisation js-callback-phone" />
							    <label for="i5" class="control-label">{l s='Phone' mod='kl_callback'}</label>
							    <input type="text" pattern="{literal}[A-Za-z0-9]{6,15}{/literal}" name="phone" class="form-control callback-phone js-callback-phone" />
							    <label for="i6" class="control-label">{l s='Email' mod='kl_callback'}</label>
							    <input type="email" name="email" class="form-control callback-email js-callback-phone" />
							    <label for="i7" class="control-label">{l s='Message' mod='kl_callback'}</label>
							    <input type="textarea" name="message" class="form-control callback-message js-callback-phone" />
						  	</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 text-center">
							<div class="btn btn-primary submit-form" onclick="callbackClick()">{l s='Call me back please' mod='kl_callback'}</div>
						</div>
					</div>
					<input type="hidden" name="ajaxurl" id="ajaxurl" value="{$ajaxurl}"/>
				</form>
		    </div>
		</div>
	</div>
</div>