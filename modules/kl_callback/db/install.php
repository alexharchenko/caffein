<?php 

$query = array();

$query[] = 'CREATE TABLE `'._DB_PREFIX_.'kl_callback` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `organisation` varchar(255),
  `phone` varchar(255) NOT NULL,
  `email` varchar(255),
  `message` varchar(255),
  `date` datetime NOT NULL,
  `page` varchar(255) NOT NULL
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$query[] = 'ALTER TABLE `'._DB_PREFIX_.'kl_callback`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `ps_kl_callback`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT';

$querydrop = array();
$querydrop[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'kl_callback`';