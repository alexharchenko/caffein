function callbackClick() {
  var self = jQuery('[block-type="callback"] .submit-form');
  var elm = jQuery(self).parents('.modal-dialog');
	if(parseInt(elm.find('.js-callback-phone').val().length)>6){
		$.ajax({
		    url: elm.find('#ajaxurl').val(),
		    type: 'get',
		    data: 'ajax=true&name='+elm.find('.callback-name').val()+'&organisation='+elm.find('.callback-organisation').val()+'&phone='+elm.find('.callback-phone').val()+'&email='+elm.find('.callback-email').val()+'&message='+elm.find('.callback-message').val()+'&url='+window.location.href,
		    success: function(data) {
	    		var result = $.parseJSON(data);
		    	if(result.error == 0)
		    		elm.html('<div class="text-center text-success">'+result.message+'</div>');
		    	else
					elm.html('<div class="text-center text-danger">'+result.message+'</div>');
		    	
		    },
		    error: function(data){
		    	elm.html('<div class="text-center text-danger">'+result.message+'</div>');
		    }
		});
		return false;
	}else{
		elm.find('.form-group').addClass('has-danger');
	}
}