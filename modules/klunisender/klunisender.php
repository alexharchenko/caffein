<?php
/**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

include_once(dirname(__FILE__).'/lib/uniApi.php');

class KlUnisender extends Module
{
    private $post_errors = array();
    private $html = '';

    public function __construct()
    {
        $this->name = 'klunisender';
        $this->tab = 'administration';
        $this->version = '1.0.1';
        $this->author = 'kLooKva Antikov';
        $this->need_instance = 0;

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('Klookva newsletter with unisender');
        $this->description = $this->l('Send subscribers to unisender');
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        return parent::install();
    }

    public function uninstall()
    {
        return  Configuration::deleteByName('COUNTRY_KLOOKVA_UNISENDER') &&
                Configuration::deleteByName('SUSCRIBERS_KLOOKVA_UNISENDER') &&
                Configuration::deleteByName('API_KLOOKVA_UNISENDER') &&
                Configuration::deleteByName('SERVER_KLOOKVA_UNISENDER') &&
                parent::uninstall();
    }

    public function getCustomers()
    {
        $id_shop = false;

        $who = (int)Configuration::get('SUSCRIBERS_KLOOKVA_UNISENDER');

        $country = (int)Configuration::get('COUNTRY_KLOOKVA_UNISENDER');

        if (Context::getContext()->cookie->shopContext) {
            $id_shop = (int) Context::getContext()->shop->id;
        }

        $customers = array();
        if ($who == 1 || $who == 0 || $who == 3) {
            $dbquery = new DbQuery();
            $dbquery->select('c.`id_customer` AS `id`, s.`name` AS `shop_name`, gl.`name` AS `gender`, 
            	c.`lastname`, c.`firstname`, c.`email`, c.`newsletter` AS `subscribed`, c.`newsletter_date_add`');
            $dbquery->from('customer', 'c');
            $dbquery->leftJoin('shop', 's', 's.id_shop = c.id_shop');
            $dbquery->leftJoin('gender', 'g', 'g.id_gender = c.id_gender');
            $dbquery->leftJoin('gender_lang', 'gl', 'g.id_gender = gl.id_gender AND gl.id_lang = '.
                (int)$this->context->employee->id_lang);
            if ($who != 0) {
                $dbquery->where('c.`newsletter` = '.($who == 3 ? 0 : 1));
            }
            if ($country) {
                $dbquery->where('(SELECT COUNT(a.`id_address`) as nb_country
                                                    FROM `'._DB_PREFIX_.'address` a
                                                    WHERE a.deleted = 0
                                                    AND a.`id_customer` = c.`id_customer`
                                                    AND a.`id_country` = '.(int)$country.') >= 1');
            }
            if ($id_shop) {
                $dbquery->where('c.`id_shop` = '.(int)$id_shop);
            }

            $customers = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($dbquery->build());
        }

        $non_customers = array();
        if (($who == 0 || $who == 2) && !$country) {
            $dbquery = new DbQuery();
            $dbquery->select('CONCAT(\'N\', e.`id`) AS `id`, s.`name` AS `shop_name`, NULL AS `gender`, NULL AS
             `lastname`, NULL AS `firstname`, e.`email`, e.`active` AS `subscribed`, e.`newsletter_date_add`');
            $dbquery->from('emailsubscription', 'e');
            $dbquery->leftJoin('shop', 's', 's.id_shop = e.id_shop');
            $dbquery->where('e.`active` = 1');
            if ($id_shop) {
                $dbquery->where('e.`id_shop` = '.$id_shop);
            }
            $non_customers = Db::getInstance()->executeS($dbquery->build());
        }

        $subscribers = array_merge($customers, $non_customers);

        return $subscribers;
    }

    public function getContent()
    {
        $this->html .= '';
        $module_url = Tools::getProtocol(Tools::usingSecureMode()).$_SERVER['HTTP_HOST'].'/module/klunisender/';
        $this->html .= $this->adminDisplayInformation($this->l('Add cron job: ').$module_url.'cron?token='.
            Tools::substr(Tools::encrypt('klunisender/index'), 0, 10));

        if (Tools::isSubmit('klunisender_save')) {
            Configuration::updateValue('COUNTRY_KLOOKVA_UNISENDER', Tools::getValue('country'));
            Configuration::updateValue('SUSCRIBERS_KLOOKVA_UNISENDER', Tools::getValue('subscribers'));
            Configuration::updateValue('API_KLOOKVA_UNISENDER', Tools::getValue('api'));
            Configuration::updateValue('SERVER_KLOOKVA_UNISENDER', Tools::getValue('unisender_server'));
            $this->html .= $this->displayConfirmation($this->l('Settings saved'));
        }

        if (Tools::isSubmit('submitExport')) {
            Configuration::updateValue('COUNTRY_KLOOKVA_UNISENDER', Tools::getValue('country'));
            Configuration::updateValue('SUSCRIBERS_KLOOKVA_UNISENDER', Tools::getValue('subscribers'));
            Configuration::updateValue('API_KLOOKVA_UNISENDER', Tools::getValue('api'));
            Configuration::updateValue('SERVER_KLOOKVA_UNISENDER', Tools::getValue('unisender_server'));
            
            $this->html .= $this->exportCustomersToUnisender();
        }

        $this->html .= $this->renderForm();

        return $this->html;
    }

    public function exportCustomersToUnisender()
    {
        $result = $this->getCustomers();
        if ($result) {
            $uniapi= new uniApi(Configuration::get('API_KLOOKVA_UNISENDER'));
            $result_send = $uniapi->importContacts($result, Configuration::get('SERVER_KLOOKVA_UNISENDER'));
            return $this->displayConfirmation($this->l('Added: ').$result_send['inserted'].$this->l('. Updated: ').
                $result_send['updated'].$this->l('. Need activeted: ').$result_send['new_emails']);
        } else {
            return $this->displayError($this->l('No subscribers.'));
        }
    }

    public function renderForm()
    {
        $countries = Country::getCountries($this->context->language->id);

        $countries_list = array(array('id' => 0, 'name' => $this->l('All countries')));
        foreach ($countries as $country) {
            $countries_list[] = array('id' => $country['id_country'], 'name' => $country['name']);
        }
        $uniapi= new uniApi(Configuration::get('API_KLOOKVA_UNISENDER'));
        $server_lists=$uniapi->getLists();
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Export customers'),
                    'icon' => 'icon-envelope'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Api key unisender'),
                        'name' => 'api',
                        'required' => true,
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Server list unisender'),
                        'name' => 'unisender_server',
                        'options' => array(
                            'query' => $server_lists,
                            'id' => 'id',
                            'name' => 'title',
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Customers\' country'),
                        'desc' => $this->l('Filter customers\' country.'),
                        'name' => 'country',
                        'required' => false,
                        'default_value' => 0,
                        'options' => array(
                            'query' => $countries_list,
                            'id' => 'id',
                            'name' => 'name',
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Newsletter subscribers'),
                        'desc' => $this->l('Filter newsletter subscribers.'),
                        'name' => 'subscribers',
                        'required' => false,
                        'default_value' => 0,
                        'options' => array(
                            'query' => array(
                                array('id' => 0, 'name' => $this->l('All Subscribers')),
                                array('id' => 1, 'name' => $this->l('Subscribers with account')),
                                array('id' => 2, 'name' => $this->l('Subscribers without account')),
                                array('id' => 3, 'name' => $this->l('Non-subscribers'))
                            ),
                            'id' => 'id',
                            'name' => 'name',
                        )
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'action',
                    )
                ),
                'buttons' => array(
                        'submitExport' => array(
                        'title' => $this->l('Export to unisender'),
                        'name' => 'submitExport',
                        'type' => 'submit',
                        'class' => 'btn btn-default pull-right',
                        'icon' => 'icon-send',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save', array(), 'Admin.Actions'),
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ?
        Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->id = (int)Tools::getValue('id_carrier');
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'klunisender_save';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.
        $this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues()
    {
        return array(
            'country' => Configuration::get('COUNTRY_KLOOKVA_UNISENDER'),
            'subscribers' => Configuration::get('SUSCRIBERS_KLOOKVA_UNISENDER'),
            'unisender_server' => Configuration::get('SERVER_KLOOKVA_UNISENDER'),
            'api' => Configuration::get('API_KLOOKVA_UNISENDER'),
            'action' => 'customers',
        );
    }
}
