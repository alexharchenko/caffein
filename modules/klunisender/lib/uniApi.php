<?php
/**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

class uniApi
{
    public $api_key;
    public $lang = 'ru';
    private $langlist = array('ru', 'en', 'it');

    /**
     * Создаем объект с разрешенными языками
     * @param string $key ключ доступа к API
     * @param string $lang язык сообщений сервера API, в данный момент поддерживается ru, en, it
     */
    public function __construct($api_key, $lang = 'ru')
    {
        $this->api_key = $api_key;
        if (in_array($lang, $this->langlist)) {
            $this->lang = $lang;
        }
    }

    /**
     * Отправка запросов и обработка ошибок
     * @param string $method название метода
     * @param array $args аргументы метода, свои для каждого метода
     */
    private function uniRequest($method, $args)
    {
        $post_array = array_merge(array(
        'api_key' => $this->api_key,
        'lang' => $this->lang,
        'platform' => 'prestashop'
    ), $args);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_array);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt(
        $ch,
        CURLOPT_URL,
        'https://api.unisender.com/ru/api/'.$method.'?format=json'
    );
        $result = curl_exec($ch);

        if ($result) {
            // Раскодируем ответ API-сервера
            $jsonObj = json_decode($result);

            if (null === $jsonObj) {
                // Ошибка в полученном ответе
                echo "Invalid JSON";
            } elseif (!empty($jsonObj->error)) {
                // Ошибка добавления пользователя
                echo "An error occured: " . $jsonObj->error . "(code: " . $jsonObj->code . ")";
            }
        } else {
            // Ошибка соединения с API-сервером
            echo "API access error";
        }
        return $jsonObj->result;
    }
    /**
     * Этот метод добавляет контакты (e-mail адрес и/или мобильный телефон) подписчика в один или несколько списков, а также позволяет добавить/поменять значения дополнительных     полей и меток.
     * @param <type> $email
     * @param <type> $list Перечисленные через запятую коды списков, в которые надо добавить подписчика.
     * @param <type> $phone В случае наличия и e-mail, и телефона, подписчик будет включён и в e-mail, и в SMS списки рассылки.
     * @param <type> $ip IP-адрес подписчика
     * @param <type> $first_name
     * @param <type> $last_name
     */
    public function subscribe($email, $list, $phone=null, $ip=null, $first_name=null, $last_name=null)
    {
        $args = array(
      'list_ids' => $list,
      'fields[email]' => $email,
      'fields[Name]' => $first_name.' '.$last_name,
      'request_ip' => $ip,
      'overwrite' => 2,
      'request_time' => date("Y.m.d"),
      'confirm_time' => date("Y.m.d"),
      'double_optin' => 1
    );
        if ($phone) {
            $args['fields[phone]']=$phone;
        }
        $result=$this->uniRequest('subscribe', $args);
        if ($result=='too_many_double_optins') {
            $args['double_optin']=0;
            $result=$this->uniRequest('subscribe', $args);
        }
        return $result;
    }

    /**
     * Метод исключает e-mail или телефон подписчика из одного или нескольких     списков
     * @param <type> $email
     * @param <type> $list Перечисленные через запятую коды списков, из которых     мы исключаем подписчика
     * @return <type>
     */
    public function exclude($email, $list)
    {
        $args = array(
          'contact_type' => 'email',
          'contact' => $email
        );
        if ($list) {
            $args['list_ids']=$list;
        }
        return $this->uniRequest('exclude', $args);
    }

    public function importContacts($users, $email_list_ids, $phone_list_ids=null)
    {
        $args_default = array(
            'field_names[0]' => 'email',
            'field_names[1]' => 'Name',
            'field_names[2]' => 'email_list_ids',
            'field_names[3]' => 'email_status',
            'field_names[4]' => 'email_add_time',
            'field_names[5]' => 'email_request_ip',
            'field_names[6]' => 'email_confirm_ip',
            'field_names[7]' => 'email_confirm_time',
            'field_names[8]' => 'phone',
            'double_optin' => '1'
        );
        $args=$args_default;
        $i=0;
        $inserted=0;
        $updated=0;
        $new_emails=0;
        foreach ($users as $user) {
            if ($i>=500) {
                $result=$this->uniRequest('importContacts', $args);
                $args=$args_default;
                $i=0;
                $inserted+=$result->inserted;
                $updated+=$result->updated;
                $new_emails+=$result->new_emails;
            }
            $args['data[' . $i .'][0]']=$user['email'];
            $args['data[' . $i .'][1]']=$user['firstname'].' '.$user['lastname'];
            $args['data[' . $i .'][2]']=$email_list_ids;
            $args['data[' . $i .'][3]']='active';
            $args['data[' . $i .'][4]']=$user['newsletter_date_add'];
            $args['data[' . $i .'][5]']=$user['ip_registration_newsletter'];
            $args['data[' . $i .'][6]']=$user['ip_registration_newsletter'];
            $args['data[' . $i .'][7]']=$user['newsletter_date_add'];
            $args['data[' . $i .'][8]']=$user['phone'];
            $i++;
        }
        $result=$this->uniRequest('importContacts', $args);
        $inserted+=$result->inserted;
        $updated+=$result->updated;
        $new_emails+=$result->new_emails;

        return array('inserted'=>$inserted,'updated'=>$updated,'new_emails'=>$new_emails);
    }

    public function getLists()
    {
        $args = array(
        );
        return $this->uniRequest('getLists', $args);
    }
}
