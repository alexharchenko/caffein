<?php
/**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

class KlUnisenderCronModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();

        if (Tools::getValue('token') !== Tools::substr(Tools::encrypt('klunisender/index'), 0, 10) ||
            !Module::isInstalled('klunisender')) {
            die($this->module->l('Invalid token', 'cron'));
        }
        
        die($this->module->exportCustomersToUnisender());
    }
}
