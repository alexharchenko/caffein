/**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

jQuery(document).ready(function () {
    jQuery.ajax({
        url: ajaxurl,
        type: 'POST',
        data : {
            ajax : true,
            update_status : update_c_or_w,
            pos : pos,
            method : 'test'
        },
        success: function (data) {
            console.log('success');
        },
        error: function (data) {
            console.log('error');
        }
    });
});