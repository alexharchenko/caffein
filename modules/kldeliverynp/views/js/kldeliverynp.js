/**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

$(document).ready(function () {
  $('button[name=confirmDeliveryOption]').click(function(){
    var delivery = $(this);
    var city = delivery.parents('#js-delivery').find('#js-cities option:selected').text();
    var warehouse = delivery.parents('#js-delivery').find('#js-warehouses option:selected').text();
    $.ajax({
      url: saveCartUrl,
      type: "post",
      dataType: "json",
      data: {
          "city": city,
          "warehouse":  warehouse,
      }
    });
  });
  $('.js-track').each(function(index){
    $(this).find('.js_trackin_'+index).click(function(){
      var button = $(this);
      var shipping_number = button.parents('.js-track').find('.js_shipping_number').val();
      $.ajax({
        url: trackingUrl,
        type: "post",
        dataType: "json",
        data: {
            "shipping_number": shipping_number,
        },
        success: function(data){
          if(data.result == "success"){
            button.parents('.js-track').find('.js_tracking_content').html(data.html);
            button.hide();
          }else{
            button.hide();
          }
        }
      });
    });
  });
  $('button[name=confirmDeliveryOption]').click(function(){
    var delivery = $(this);
    var city = delivery.parents('#js-delivery').find('#js-cities option:selected').text();
    var warehouse = delivery.parents('#js-delivery').find('#js-warehouses option:selected').text();
    $.ajax({
      url: saveCartUrl,
      type: "post",
      dataType: "json",
      data: {
          "city": city,
          "warehouse":  warehouse,
      }
    });
  });

  $('#checkout-personal-information-step').find('input[name=city]').blur(function(){
  	var current_city = $('#select2-js-cities-container').html();
  	var true_city = $(this).val().trim();
  	if ($(this).val().trim() != current_city.trim()) {
  		$.each($('.select-filling > option'), function( index, value) {
  			if(true_city == $(value).val()) {
  				$('.select-filling').val(true_city);
  				$('.select-filling').trigger('change'); 
  				return false;
  			}
  		});
  	}
  });

  setInterval(checkDelivery, 1000);
  $('.select-filling').select2();

});

function checkDelivery() {
  var id_carrier_novaPoshtaArray = $.map(id_carrier_novaPoshta, function(value, index) {
    return [value];
  });
  id_carrier_novaPoshtaArray.forEach(function(item, i, id_carrier_novaPoshtaArray){
    if($('#delivery_option_'+item+':checked').length > 0){
      $('#delivery_option_'+item+':checked').parents('.delivery-options').find('.js_delivery_'+item).parents('.carrier-extra-content').css({ display: "block" });

    } else if ($('label[for=delivery-option-'+item+'].active').length > 0) {
      $('label[for=delivery-option-'+item+'].active').parents('.delivery-options').find('.js_delivery_'+item).parents('.carrier-extra-content').css({ display: "block" });
    }
  });
}
function getWarehouses(elem) {
   var city = jQuery(elem).val();
   var id = jQuery(elem)[0].id;

   var count_click = jQuery(elem)[0].dataset.countClick++;
   count_click = parseInt(count_click);


   var id_lang = jQuery(elem)[0].dataset.idLang;
    if (city && city != 'empty' && id_lang) {

       jQuery.ajax({
           beforeSend: function () {
               jQuery('#warehouses').remove();
           },
           url : ajaxurlget,
           type: 'post',
           data: {'city':city, 'id_lang':id_lang, 'count_click':count_click},
           success: function (data) {
               $('.js-warehouse-insert').html(data);
               $('#js-warehouses').select2();
               $('#js-warehouses').parent().find('span.select2.select2-container.select2-container--default').attr('style','width:100%;');
           },
           error: function (data) {
               console.log('error get warehouse');
           }
       });

   } else {

       jQuery('#warehouses').remove();

   }

}

function setNewDeliveryAddress() {

}