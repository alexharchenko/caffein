{**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}
{foreach $carriers as $carrier}
	<div class="form-group label-floating bmd-form-group">
		<select name="cities" id="js-cities" class="select-filling form-control js_delivery_{$carrier}" onchange="getWarehouses(this)" style="width:100%" data-id-lang="{$id_lang}" data-count-click="0">
			{if isset($current_city) && $current_city!=''}
				<option class="js_current_city_np" value="{$current_city}">{$current_city}</option>
			{else}
				{foreach $customer->getAddresses(Context::getContext()->language->id) as $address}
				    <option class="js_current_city_np" value="{$address.city}">{$address.city}</option>
				    {break}
				{/foreach}
			{/if}
			{foreach from=$cities item=city}
			    <option value="{$city.name}">{$city.name}</option>
			{/foreach}
		</select>
	</div>
	<div class="form-group label-floating bmd-form-group">
		<div class="js-warehouse-insert"></div>
	</div>
{/foreach}
<script type="text/javascript">
window.addEventListener('load', function () {
	getWarehouses('#js-cities');
});
</script>