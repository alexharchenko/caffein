{**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}
<div class="js-track">
	<div class="btn btn-primary js_trackin_{$num}">
		{l s='Отследить' mod='kldeliverynp'}
	</div>
	<div class="js_tracking_content"></div>
	<input class="js_shipping_number form-control" style="display:none" type="text" value="{$shipping_number}" hidden/> 
</div>