{**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}
<div class="row "> 
	<div class="col-md-12 input-line">
		<div class="label">
			 {l s='Статус доставки' mod='kldeliverynp'}
		</div>
		<div class="input-data"> 
	        {if !empty($result.data)}
	            {l s='№ накладной' mod='kldeliverynp'} {$result.data[0].Number} - {$result.data[0].Status}
	        {else}
	            {l s='№ накладной не задан либо задан не верно' mod='kldeliverynp'}
	        {/if}
    	</div> 
	</div>
    
</div>