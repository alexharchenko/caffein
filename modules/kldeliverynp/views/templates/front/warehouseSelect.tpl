{**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}
<select id="js-warehouses" class="select-filling form-control">
	<option value="empty_option">
		{l s='Choose a warehouses...' mod='kldeliverynp'}
	</option>
    {foreach $warehouses as $warehouse}
    	<option value="{$warehouse.name}">
    		{$warehouse.name}
    	</option>	
    {/foreach}
</select>