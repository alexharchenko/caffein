<?php
/**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

include_once _PS_MODULE_DIR_.'kldeliverynp/classes/NovaPoshtaCartClass.php';

class KlDeliveryNPsaveCartNovaPoshtaModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        $this->ajax = true;

        parent::initContent();

       
        $this->saveCart();
    }

    public function saveCart()
    {
        if (Tools::getValue('city') && Tools::getValue('warehouse')) {
            $cartNp = new NovaPoshtaCartClass((int)$this->context->cart->id);
            $cartNp->id_cart = $this->context->cart->id;
            $cartNp->id_customer = $this->context->cart->id_customer;
            $cartNp->city = Tools::getValue('city');
            $cartNp->warehouse = Tools::getValue('warehouse');

            $cartNp->save();
        }
    }
}
