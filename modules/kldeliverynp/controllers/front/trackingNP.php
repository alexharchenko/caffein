<?php
/**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

include_once _PS_MODULE_DIR_.'kldeliverynp/src/NovaPoshtaApi2.php';

class KlDeliveryNPtrackingNPModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        $this->ajax = true;

        parent::initContent();

       
        $this->tracking();
    }

    public function tracking()
    {
        if (Tools::getAllValues() && !empty(Tools::getAllValues())) {
            $np = $this->module->getNPApi2();
            $result = $np->documentsTracking(Tools::getValue('shipping_number'));
            $this->context->smarty->assign('result', $result);
            if (!empty($result)) {
                echo json_encode(array(
                    'result'    => 'success',
                    'html'      => $this->context->smarty->fetch(_PS_MODULE_DIR_.
                        'kldeliverynp/views/templates/hook/tracking.tpl')
                ));
            } else {
                echo json_encode(array(
                    'result'  => 'error'
                ));
            }
        }
    }
}
