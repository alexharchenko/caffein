<?php
/**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

class KlDeliveryNPcheckForUpdatesAjaxModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        $this->ajax = true;

        parent::initContent();
        if (Tools::getValue('update_status') == 'city') {
            $this->module->updateCities(Tools::getValue('pos'));
        } elseif (Tools::getValue('update_status') == 'warehouse') {
            $this->module->updateWarehouses(Tools::getValue('pos'));
        }


        echo 'done';
    }
}
