<?php
/**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

class KlDeliveryNPgetWarehousesModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        $this->ajax = true;

        parent::initContent();

        if (Tools::getValue('city')) {
            $this->actionGetWarehouses();
        }
    }

    public function actionGetWarehouses()
    {
        if (Tools::getValue('city') && Tools::getValue('id_lang')) {
            $city = Tools::getValue('city');
            $id_lang = Tools::getValue('id_lang');
            $count_click = Tools::getValue('count_click');

            $warehouses = $this->getWarehouses($city, $id_lang);

            $this->buildSelectWithOptions($warehouses);

            if ($count_click === 0) {
                $this->module->processPostAddress();
            }
        }
    }

    public function getWarehouses($city, $id_lang)
    {
        return Db::getInstance()->executeS('SELECT `name` FROM `'._DB_PREFIX_.'kl_delivery_nova_poshta_warehouses` 
        	WHERE `id_lang` = '.(int)$id_lang.' AND `city` = "'.pSQL($city).'"');
    }

    public function buildSelectWithOptions(array $warehouses)
    {
        $this->context->smarty->assign('warehouses', $warehouses);

        echo $this->context->smarty->fetch(_PS_MODULE_DIR_.'kldeliverynp/views/templates/front/warehouseSelect.tpl');
    }
}
