<?php
/**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

include_once _PS_MODULE_DIR_.'kldeliverynp/classes/NovaPoshtaCartClass.php';
include_once _PS_MODULE_DIR_.'kldeliverynp/src/NovaPoshtaApi2.php';

class KlDeliveryNP extends CarrierModule implements WidgetInterface
{
    private $templateFile;
    private $errors;

    const PREFIX = 'kldeliverynp_';

    protected $hooksnp = array(
        'actionCarrierUpdate',
        'displayBeforeCarrier',
        'displayCarrierExtraContent',
        'header',
    );

    public function __construct()
    {
        $this->name = 'kldeliverynp';
        $this->tab = 'shipping_logistics';
        $this->version = '1.0.5';
        $this->author = 'Klookva';
        $this->bootstrap = true;

        parent::__construct();
        $this->displayName = $this->l('Nova Poshta Shipping');
        $this->description = $this->l('Shipping with Nova Poshta.');
        $this->templateFile = 'module:kldeliverynp/views/templates/hook/kldeliverynp.tpl';
        $this->module_key = '979a4990561600624bd2094e0984d91c';
    }

    public function install()
    {
        return parent::install()
            && $this->installDB()
            && $this->registerHook('displayNPTracker')
            && $this->registerHook('actionAdminControllerSetMedia')
            && $this->registerHook('displayAdminOrderTabOrder')
            && $this->registerHook('displayCarrierExtraContent')
            && $this->registerHook('header')
            && $this->registerHook('actionValidateOrder')
            && $this->createCarriers()
        ;
    }

    public function uninstall()
    {
        return $this->uninstallDB() &&
            Configuration::deleteByName('API_KEY_NP') &&
            Configuration::deleteByName('WAREHOUSE_NP') &&
            Configuration::deleteByName('BRAKE_NP') &&
            Configuration::deleteByName('DATE_NP') &&
            Configuration::deleteByName('CITY_NP') &&
            $this->deleteCarriers() &&
            parent::uninstall();
    }

    protected function createCarriers()
    {

        $carrier = new Carrier();
        $carrier->name = $this->l('Nova Poshta Shipping');
        $carrier->active = true;
        $carrier->deleted = 0;
        $carrier->shipping_handling = false;
        $carrier->range_behavior = 0;
        $carrier->delay[Configuration::get('PS_LANG_DEFAULT')] = $this->l('Nova Poshta Shipping');
        $carrier->shipping_external = true;
        $carrier->is_module = true;
        $carrier->external_module_name = $this->name;
        $carrier->need_range = true;
        $carrier->is_free = 1;
        if ($carrier->add()) {
            $groupArr = new Group();
            $groups = $groupArr->getGroups((int)Configuration::get('PS_LANG_DEFAULT'));
            
            foreach ($groups as $group) {
                Db::getInstance()->insert('carrier_group', array(
                    'id_carrier' => (int) $carrier->id,
                    'id_group' => (int) $group['id_group']
                ));
            }

            /*$rangePrice = new RangePrice();
            $rangePrice->id_carrier = $carrier->id;
            $rangePrice->delimiter1 = '0';
            $rangePrice->delimiter2 = '1000000';
            $rangePrice->add();*/

            $zones = Zone::getZones(true);
            foreach ($zones as $z) {
                $carrier->addZone($z['id_zone']);
                Db::getInstance()->insert('delivery', array(
                    'id_carrier' => (int) $carrier->id,
                    'id_zone' => (int) $z['id_zone'],
                    'price' => 0
                ));
            }

            copy(dirname(__FILE__) . '/views/img/' . 'novaposhta' . '.png', _PS_SHIP_IMG_DIR_ . '/' . (int) $carrier->id . '.png'); //assign carrier logo

            Configuration::updateValue(self::PREFIX . 'novaposhta', $carrier->id);
            Configuration::updateValue(self::PREFIX . 'novaposhta' . '_reference', $carrier->id);
        }
        

        return true;
    }

    protected function deleteCarriers()
    {
        $tmp_carrier_id = Configuration::get(self::PREFIX . 'novaposhta');
        $carrier = new Carrier((int)$tmp_carrier_id);
        $zones = Zone::getZones(true);
        foreach ($zones as $z) {
            $carrier->deleteZone($z['id_zone']);
        }
        Db::getInstance()->delete('delivery', 'id_carrier = '.(int) $carrier->id);
        Db::getInstance()->delete('carrier_group', 'id_carrier = '.(int) $carrier->id);
        $carrier->delete();

        return true;
    }

    public function installDB()
    {
        $return = true;
        $return &= Db::getInstance()->execute(
            '
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'kl_delivery_nova_poshta_cities` (
                `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `id_lang` INT(10) UNSIGNED NOT NULL,
                `name` VARCHAR(255) NOT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;'
        );

        $return &= Db::getInstance()->execute(
            '
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'kl_delivery_nova_poshta_warehouses` (
                `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `id_lang` INT(10) UNSIGNED NOT NULL,
                `name` VARCHAR(255) NOT NULL,
                `city` VARCHAR(255) NOT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;'
        );

        $return &= Db::getInstance()->execute(
            '
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'kl_delivery_nova_poshta_cart` (
                `id_cart` INT(10) UNSIGNED NOT NULL,
                `id_customer` INT(10) UNSIGNED NOT NULL,
                `city` VARCHAR(255) NOT NULL,
                `warehouse` VARCHAR(255) NOT NULL,
                PRIMARY KEY (`id_cart`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;'
        );

        return $return;
    }

    public function uninstallDB()
    {
        return Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'kl_delivery_nova_poshta_cities`') &&
        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'kl_delivery_nova_poshta_warehouses`') &&
        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'kl_delivery_nova_poshta_cart`');
    }

    public function getOrderShippingCost($params, $shipping_cost)
    {
        $total_cost = $shipping_cost;
        if (!empty($params) && isset($params['cost'])) {
            $total_cost = $params['cost'];
        }
        return $total_cost;
    }

    public function getOrderShippingCostExternal($params = array())
    {
        return $this->getOrderShippingCost($params, 0);
    }
    public function getContent()
    {
        $html = '';
        if (Tools::isSubmit('savekldeliverynp')) {
            Configuration::updateValue('API_KEY_NP', Tools::getValue('apikey'));
            $html = $this->displayConfirmation($this->l('Saved key confirm'));
        }
        return $html . $this->initForm();
    }

    protected function initForm()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $this->fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Klookva Nova Poshta'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Api key for Nova Poshta'),
                    'name' => 'apikey',
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save', array(), 'Admin.Actions'),
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = 'kldeliverynp';
        $helper->identifier = $this->identifier;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'savekldeliverynp';
        $helper->fields_value['apikey'] = Configuration::get('API_KEY_NP');
        return $helper->generateForm($this->fields_form);
    }

    public function hookDisplayNPTracker($params)
    {
        $order = new Order($params['id_order']);
        $cart = Order::getCartIdStatic($params['id_order']);
        $cartNp = new NovaPoshtaCartClass((int)$cart);
        if ($cart == $cartNp->id_cart) {
            $this->context->smarty->assign('num', $params['num']);
            $this->context->smarty->assign('shipping_number', $order->getWsShippingNumber());
            return $this->display(__FILE__, 'views/templates/hook/button.tpl');
        } else {
            return '';
        }
    }

    public function hookActionValidateOrder($params)
    {
        $cart = $params['cart']->id;
        $cartNp = new NovaPoshtaCartClass((int)$cart);
        if ($cart == $cartNp->id_cart) {
            $address = new Address();
            $address->id_country = Customer::getCurrentCountry($this->context->customer->id);
            $address->alias = $this->l('Delivery address for Nova Poshta');
            $address->firstname = $this->context->customer->firstname;
            $address->lastname = $this->context->customer->lastname;
            $address->address1 = ' ';
            $address->city = $cartNp->city;
            $address->other = $cartNp->warehouse;
            $temp_address = new Address($params['order']->id_address_delivery);
            $address->phone = $temp_address->phone;
            if ($address->save()) {
                $params['order']->id_address_delivery = $address->id;
                $params['order']->save();
            }
        }
    }

    public function hookDisplayAdminOrderTabOrder($params)
    {
        $order = new Order(Tools::getValue('id_order'));
        $cart = Order::getCartIdStatic(Tools::getValue('id_order'));
        $cartNp = new NovaPoshtaCartClass((int)$cart);
        if ($cart == $cartNp->id_cart) {
            $this->context->smarty->assign('shipping_number', $order->getWsShippingNumber());
            $this->context->smarty->assign('num', 0);
            return $this->display(__FILE__, 'views/templates/hook/button.tpl');
        } else {
            return '';
        }
    }

    public function checkDateForUpdate()
    {
        $today = date("Y-m-d H:i:s");

        $date_from_file = Configuration::get('DATE_NP');

        if (!empty($date_from_file)) {
            $date_from_file = new DateTime($date_from_file);
            $yesterday = new DateTime('-1 day');

            if ($yesterday > $date_from_file) {
                Configuration::updateValue('DATE_NP', $today);
                return true;
            } else {
                return false;
            }
        } else {
            Configuration::updateValue('DATE_NP', $today);
            return true;
        }
    }

    protected function truncateTable($table)
    {
        return Db::getInstance()->execute('TRUNCATE TABLE `'._DB_PREFIX_. pSQL($table).'`');
    }

    protected function insertCity($id_lang, $name)
    {
        return Db::getInstance()->insert('kl_delivery_nova_poshta_cities', array(
            'id_lang' => (int)$id_lang,
            'name' => pSQL($name)
        ));
    }

    protected function insertWarehouses($id_lang, $name, $city)
    {
        return Db::getInstance()->insert('kl_delivery_nova_poshta_warehouses', array(
            'id_lang' => (int)$id_lang,
            'name' => pSQL($name),
            'city' => pSQL($city)
        ));
    }

    protected function getCities($id_lang)
    {
        return  Db::getInstance()->executeS('SELECT `name` FROM `'._DB_PREFIX_.'kl_delivery_nova_poshta_cities` 
        	WHERE `id_lang` = '.(int)$id_lang.' ');
    }

    protected function getWarehouses($id_lang)
    {
        return  Db::getInstance()->executeS('SELECT `name` FROM `'._DB_PREFIX_.'kl_delivery_nova_poshta_warehouses` 
        	WHERE `id_lang` = '.(int)$id_lang.' ');
    }

    protected function getCity($id_lang)
    {
        return  Db::getInstance()->getValue('SELECT `name` FROM `'._DB_PREFIX_.'kl_delivery_nova_poshta_cities` 
        	WHERE `id_lang` = '.(int)$id_lang.' ');
    }

    protected function getWarehouse($id_lang)
    {
        return  Db::getInstance()->getValue('SELECT `name` FROM `'._DB_PREFIX_.'kl_delivery_nova_poshta_warehouses` 
        	WHERE `id_lang` = '.(int)$id_lang.' ');
    }

    public function getNPApi2()
    {
        foreach (Language::getLanguages(true) as $lang) {
            if ($lang['id_lang'] == $this->context->language->id &&
                ($lang['iso_code'] == 'ru' || $lang['iso_code'] == 'ua' || $lang['iso_code'] == 'en')) {
                $np = new NovaPoshtaApi2(
                    Configuration::get('API_KEY_NP'),
                    $lang['iso_code'],
                    false,
                    'curl'
                );
                return $np;
            } else {
                $np = new NovaPoshtaApi2(
                    Configuration::get('API_KEY_NP'),
                    'en',
                    false,
                    'curl'
                );
                return $np;
            }
        }
    }

    public function updateCities($pos = 0)
    {
        $np = $this->getNPApi2();
        $cities = $np->getCities();
        if ($cities["data"]>0) {
            $this->truncateTable('kl_delivery_nova_poshta_cities');
            Configuration::updateValue('CITY_NP', "begin");
            foreach ($cities["data"] as $key => $city) {
                Configuration::updateValue('CITY_NP', $key);
                if ($key >= $pos) {
                    foreach (Language::getLanguages(true) as $lang) {
                        if ($lang['iso_code'] == 'ru') {
                            $this->insertCity($lang['id_lang'], $city['DescriptionRu']);
                        } elseif ($lang['iso_code'] == 'ua') {
                            $this->insertCity($lang['id_lang'], $city['Description']);
                        } else {
                            $this->insertCity($lang['id_lang'], $city['Description']);
                        }
                    }
                }
            }
            Configuration::updateValue('CITY_NP', "end");
        }
    }

    public function getWarehouseByIdCart($id_cart)
    {
        $np = new NovaPoshtaCartClass($id_cart);
        return $np->warehouse;
    }

    public function getCityByIdCart($id_cart)
    {
        $np = new NovaPoshtaCartClass($id_cart);
        return $np->city;
    }

    public function updateWarehouses($pos = 0)
    {
        $np = $this->getNPApi2();
        $cities = $np->getCities();
        $warehouses_arr = array();
        foreach ($cities["data"] as $key => $city) {
            $warehouses_arr[$key] = $np->getWarehouses($city["Ref"]);
        }
        $this->truncateTable('kl_delivery_nova_poshta_warehouses');
        Configuration::updateValue('WAREHOUSE_NP', "begin");
        $new_key = 0;
        foreach ($warehouses_arr as $warehouses) {
            foreach ($warehouses["data"] as $key => $warehouse) {
                Configuration::updateValue('WAREHOUSE_NP', $new_key);
                if ($new_key >= $pos) {
                    foreach (Language::getLanguages(true) as $lang) {
                        if ($lang['iso_code'] == 'ru') {
                            $this->insertWarehouses(
                                $lang['id_lang'],
                                $warehouse['DescriptionRu'],
                                $warehouse['CityDescriptionRu']
                            );
                        } elseif ($lang['iso_code'] == 'ua') {
                            $this->insertWarehouses(
                                $lang['id_lang'],
                                $warehouse['Description'],
                                $warehouse['CityDescription']
                            );
                        } else {
                            $this->insertWarehouses(
                                $lang['id_lang'],
                                $warehouse['Description'],
                                $warehouse['CityDescription']
                            );
                        }
                    }
                    $new_key++;
                }
            }
        }
        Configuration::updateValue('WAREHOUSE_NP', "end");
    }


    public function hookActionAdminControllerSetMedia()
    {
        $this->addCssAndJs();
    }

    public function addCssAndJs()
    {
        $id_carrier_novaPoshta = array();

        $id_carrier_novaPoshta[$this->l('Nova Poshta Shipping')] = Configuration::get(self::PREFIX . 'novaposhta');
        
        Media::addJsDef(array('id_carrier_novaPoshta' => $id_carrier_novaPoshta));
        Media::addJsDef(array('ajaxurlget' => $this->context->link->getModuleLink(
            'kldeliverynp',
            'getWarehouses'
        )));
        Media::addJsDef(array('saveCartUrl' => $this->context->link->getModuleLink(
            'kldeliverynp',
            'saveCartNovaPoshta'
        )));
        Media::addJsDef(array('trackingUrl' => $this->context->link->getModuleLink(
            'kldeliverynp',
            'trackingNP'
        )));
        $this->context->controller->addJS($this->_path . 'views/js/kldeliverynp.js', 'all');
        $this->context->controller->addJS($this->_path . 'views/js/select2.min.js', 'all');

        //check for update cities and warehouses | background
    }

    public function checkForBrakeQuery($minute)
    {
        $now = date("Y-m-d H:i:s");

        $date_from_file = Configuration::get('BRAKE_NP');

        if ($date_from_file && $date_from_file != '' && $date_from_file != null) {
            $date_from_file = new DateTime($date_from_file);
            $prev = new DateTime('-'.$minute.' minute');

            if ($prev > $date_from_file) {
                Configuration::updateValue('BRAKE_NP', $now);
                return true;
            } else {
                return false;
            }
        } else {
            Configuration::updateValue('BRAKE_NP', $now);
            return true;
        }
    }

    public function hookDisplayHeader()
    {
        $this->addCssAndJs();
        $city = $this->getCity($this->context->language->id);
        $warehouse = $this->getWarehouse($this->context->language->id);
        $city_file = Configuration::get('CITY_NP');
        $warehouse_file = Configuration::get('WAREHOUSE_NP');
        if (is_numeric($city_file)) {
            $pos_city = (int)$city_file;
        } else {
            $pos_city = 0;
        }
        if (is_numeric($warehouse_file)) {
            $pos_warehouse = (int)$warehouse_file;
        } else {
            $pos_warehouse = 0;
        }
        if ($this->checkDateForUpdate() || !isset($city) || trim($city) == '' || !$city) {
            Media::addJsDef(array('ajaxurl' => $this->context->link->getModuleLink(
                'kldeliverynp',
                'checkForUpdatesAjax'
            )));
            Media::addJsDef(array('update_c_or_w' => 'city'));
            Media::addJsDef(array('pos' => $pos_city));
            $this->context->controller->addJS($this->_path . 'views/js/check.js', 'all');
        } elseif ($this->checkDateForUpdate() || !isset($warehouse) || trim($warehouse) == '' || !$warehouse) {
            Media::addJsDef(array('ajaxurl' => $this->context->link->getModuleLink(
                'kldeliverynp',
                'checkForUpdatesAjax'
            )));
            Media::addJsDef(array('update_c_or_w' => 'warehouse'));
            Media::addJsDef(array('pos' => $pos_warehouse));
            $this->context->controller->addJS($this->_path . 'views/js/check.js', 'all');
        } else {
            Media::addJsDef(array('update_c_or_w' => 'none'));
            Media::addJsDef(array('pos' => 0));
        }
        if ($this->checkForBrakeQuery(10)) {
            if ($city_file !== 'end') {
                Media::addJsDef(array('ajaxurl' => $this->context->link->getModuleLink(
                    'kldeliverynp',
                    'checkForUpdatesAjax'
                )));
                Media::addJsDef(array('update_c_or_w' => 'city'));
                Media::addJsDef(array('pos' => $pos_city));
                $this->context->controller->addJS($this->_path . 'views/js/check.js', 'all');
            } elseif ($warehouse_file !== 'end') {
                Media::addJsDef(array('ajaxurl' => $this->context->link->getModuleLink(
                    'kldeliverynp',
                    'checkForUpdatesAjax'
                )));
                Media::addJsDef(array('update_c_or_w' => 'warehouse'));
                Media::addJsDef(array('pos' => $pos_warehouse));
                $this->context->controller->addJS($this->_path . 'views/js/check.js', 'all');
            } else {
                Media::addJsDef(array('update_c_or_w' => 'none'));
                Media::addJsDef(array('pos' => 0));
            }
        }
    }

    public function renderWidget($hookName, array $params)
    {
        if (!$this->isCached($this->templateFile, $this->getCacheId('kldeliverynp'))) {
            $this->smarty->assign($this->getWidgetVariables($hookName, $params));
        }

        return $this->fetch($this->templateFile, $this->getCacheId('kldeliverynp'));
    }

    public function getWidgetVariables($hookName, array $params)
    {
        $hookVar = '';
        if (isset($hookName)) {
            $hookVar = $hookName;
        }
        $paramsArr = array();
        if (!empty($params)) {
            $paramsArr = $params;
        }
        $current_id_lang = $this->context->language->id;
        $cities = $this->getCities($current_id_lang);
        $current_city = $this->getCityByIdCart($this->context->cart->id);
        $id_carriers = array();
        $id_carriers[$this->l('Nova Poshta Shipping')] = Configuration::get(self::PREFIX . 'novaposhta');
        return array(
            'cities' => $cities,
            'id_lang' => $current_id_lang,
            'current_city' => $current_city,
            'carriers' => $id_carriers,
            'customer' => $this->context->customer,
            'hookVar' => $hookVar,
            'paramsArr' => $paramsArr
        );
    }
}
