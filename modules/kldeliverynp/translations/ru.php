<?php
/**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

global $_MODULE;
$_MODULE = array();
$_MODULE['<{kldeliverynp}prestashop>kldeliverynp_69d4ada6f8996b52ed7d6cee6da6b3f5'] = 'Новая почта';
$_MODULE['<{kldeliverynp}prestashop>kldeliverynp_98900b7eaa25ab2c93cab56540e0d2dd'] = 'Доставка с помощью новой почты';
$_MODULE['<{kldeliverynp}prestashop>kldeliverynp_c9cc8cce247e49bae79f15173ce97354'] = 'Сохранить';
