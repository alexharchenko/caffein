<?php

class Redirect extends ObjectModel
{
	public $id;
    public $redirect_status;
	public $source_url;
	public $target_url;
	public $title;
    public $page_title;
	public $description;

	public static $definition = array(
        'table' => 'kl_seo_redirect',
        'primary' => 'id',
        'multilang' => false,
        'fields' => array(
            'id' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'redirect_status' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'source_url' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
            'target_url' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
            'title' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
            'page_title' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
            'description' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
        )
    );


	public function __construct($id = null)
    {
        parent::__construct($id);
    }


    public function setValues($values)
    {
        foreach ($values as $key => $v) {
            if (array_key_exists($key, $this)) {
                $this->{$key} = $v;
            }
        }
    }

    public function all()
    {
        $sql = "SELECT * FROM " . _DB_PREFIX_ . self::$definition['table'];
        return Db::getInstance()->executeS($sql);
    }

    public static function where(...$conditions)
    {
        $queries = [];
        $result = [];
        foreach ($conditions as $c) {
            if(isset($c[2])) {
                array_push($queries, _DB_PREFIX_ . self::$definition['table'] . "." . $c[0] . $c[1] . " '" . addcslashes($c[2], "'") . "'");
            }
            else {
                array_push($queries, _DB_PREFIX_ . self::$definition['table'] . "." . $c[0] . " = '" . addcslashes($c[1], "'") . "'");
            }
        }
        $sql = "SELECT * FROM "._DB_PREFIX_ . self::$definition['table'] . " WHERE " . implode(' AND ', $queries);
        return Db::getInstance()->executeS($sql);
    }

}