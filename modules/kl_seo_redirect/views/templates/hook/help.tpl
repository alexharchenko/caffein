<div class="panel">
	<code>
		<br>{l s='Генерация - это текста по шаблону где части текста заменяются на реальные названия товара или категории' mod='kl_seo_redirect'}
		<br>
		<br>{l s='Константы которые доступны в генерации товара:' mod='kl_seo_redirect'}
		<br>{l s='%product% - название продукта' mod='kl_seo_redirect'}
		<br>{l s='%price% - цена продукта с валютой' mod='kl_seo_redirect'}
		<br>{l s='%product-category% - категория продукта (нижний уровень в дереве категорий )' mod='kl_seo_redirect'}
		<br>{l s='%product-category-single% - категория продукта (нижний уровень в дереве категорий) с переводом в винительный падеж единичного числа' mod='kl_seo_redirect'}
		<br>{l s='%product-brand% - бренд продукта' mod='kl_seo_redirect'}
		<br>{l s='%shop% - название магазина' mod='kl_seo_redirect'}
		
		<br>{l s='Константы которые доступны в генерации категории: ' mod='kl_seo_redirect'}
		<br>{l s='%category% - название категории' mod='kl_seo_redirect'}
		<br>{l s='%min-price% - минимальная цена (среди активных продуктов) категории с валютой' mod='kl_seo_redirect'}
		<br>{l s='%max-price% - максимальная цена (среди активных продуктов) категории с валютой' mod='kl_seo_redirect'}
		<br>{l s='%shop% - название магазина' mod='kl_seo_redirect'}
		<br>{l s='%quantity% - количество товаров в категории' mod='kl_seo_redirect'}
		<br>{l s='Константы, которые доступны в генерации бренда: ' mod='kl_seo_redirect'}
	</code>
		<br>Для того, что б работала замена h1 на странице бренда, нужно в файле <strong>themes/&lt;theme_name&gt;/templates/catalog/listing/manufacturer.php</strong>  заменить
		<br>
{literal}&lt;h1&gt;{l s='List of products by brand %brand_name%' sprintf=['%brand_name%' => $manufacturer.name] d='Shop.Theme.Catalog'}&lt;/h1&gt;{/literal}
<br>на
<br>{literal}&lt;h1&gt;{if isset($header1) }
    {$header1}
  {else}
    {l s='List of products by brand %s' sprintf=[$manufacturer.name] d='Shop.Theme.Catalog'}
  {/if}&lt;/h1&gt;{/literal}
  <code>
		<br>{l s='%manufacturer% - бренд' mod='kl_seo_redirect'}
		<br>{l s='%shop% - название магазина' mod='kl_seo_redirect'}
		<br>
		<br>{l s='Например генерация title для страницы товара задан шаблон:' mod='kl_seo_redirect'}
		<br>{l s='Купить %product-category%,  %product% за %price% в магазине %shop% ' mod='kl_seo_redirect'}
		<br>{l s='В итоге будет: Купить смартфоны, Google Pixel 3 за 20000 грн. в магазине Caffein.' mod='kl_seo_redirect'}
	</code>
</div>
