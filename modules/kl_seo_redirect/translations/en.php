<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{kl_seo_redirect}prestashop>help_7e71721121b46c1b78fb724706212fc4'] = 'Generation is a text from a template where parts of the text are replaced by real product names or categories';
$_MODULE['<{kl_seo_redirect}prestashop>help_4506f7a3a2873ac85ead6d86c1828f73'] = 'Constants that are available in the generation of products:';
$_MODULE['<{kl_seo_redirect}prestashop>help_59055e5e45fe5136610ab11670d11747'] = '%product% - product name';
$_MODULE['<{kl_seo_redirect}prestashop>help_7559bc8e58cac0e89fd41dedf1ddf1b0'] = '%price% - product price with currency';
$_MODULE['<{kl_seo_redirect}prestashop>help_8412b1d7bfb04969a978aeed21e95a87'] = '%product-category% - product category name (lowest level in category tree)';
$_MODULE['<{kl_seo_redirect}prestashop>help_f93da15e41dad4f1ce4711d9ac9e4ed0'] = '%product-brand% - product brand name';
$_MODULE['<{kl_seo_redirect}prestashop>help_1780cb7ba3047ec1e057ac2882a46f01'] = '%shop% - shop name';
$_MODULE['<{kl_seo_redirect}prestashop>help_12493c0fd9e414b318949a6293beb32f'] = 'Constants that are available in the category generation:';
$_MODULE['<{kl_seo_redirect}prestashop>help_75a8f232f06c408c039c5d9aa3999537'] = '%category% - category name';
$_MODULE['<{kl_seo_redirect}prestashop>help_5d3ff5cd743ea28b31a05bc49b3658a9'] = '%min-price% -  lowest price within active products';
$_MODULE['<{kl_seo_redirect}prestashop>help_92149a5b87fd0d52e280913b4d2d4b61'] = '%max-price% -  highest price within active products';
$_MODULE['<{kl_seo_redirect}prestashop>help_0111467c065353fd098bd3fa996c929b'] = '%quantity% - product quantity in category';
$_MODULE['<{kl_seo_redirect}prestashop>help_df9db0c8306d8b0265d322251b4cc92a'] = 'For example, the title generation for the product page:';
$_MODULE['<{kl_seo_redirect}prestashop>help_c5aff83c589de420f716f4658d9097ad'] = 'Buy %product-category%, %product% per %price% in shop %shop%';
$_MODULE['<{kl_seo_redirect}prestashop>help_f894d8d9af07fbbc5f656796468f5307'] = 'The result will be: Buy smartphones, Google Pixel 3 for 20000 hryvnas in the shop Caffein.';
