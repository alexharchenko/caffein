<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{mod_kl_productcustomblock}prestashop>mod_kl_productcustomblock_88f1379ce9f66c49af3fcad8f967383b'] = 'Пользовательский блок продукта Klookva';
$_MODULE['<{mod_kl_productcustomblock}prestashop>mod_kl_productcustomblock_ddf2607a200baa039a1eff0b25fe4468'] = 'Отображение пользовательского блока продукта.';
$_MODULE['<{mod_kl_productcustomblock}prestashop>mod_kl_productcustomblock_071f407afc05610fca7a3ec72bcb9977'] = 'Text1 для пользовательского места (для ссылки)';
$_MODULE['<{mod_kl_productcustomblock}prestashop>mod_kl_productcustomblock_0f47cca420169bd92d2fbc7f5bee8212'] = '%link% - для URL-адреса продукта %city% - для пользовательского города';
$_MODULE['<{mod_kl_productcustomblock}prestashop>mod_kl_productcustomblock_197906105678982444ce553259892e29'] = 'Text2 для пользовательского места (для города)';
$_MODULE['<{mod_kl_productcustomblock}prestashop>mod_kl_productcustomblock_c250729b50e204fa16bf0e07b4aab908'] = '%link% и %city% могут использоваться во всех полях';
$_MODULE['<{mod_kl_productcustomblock}prestashop>mod_kl_productcustomblock_1ebfcf39480ea3a252d73fe2c0f0edb6'] = 'Текст для места товара';
$_MODULE['<{mod_kl_productcustomblock}prestashop>mod_kl_productcustomblock_c9cc8cce247e49bae79f15173ce97354'] = 'Сохранить';
