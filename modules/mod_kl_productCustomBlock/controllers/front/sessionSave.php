<?php

if (!class_exists('Geo')) {
    require_once _PS_MODULE_DIR_.'mod_kl_productCustomBlock/lib/geo.php';
}

class mod_kl_productCustomBlocksessionSaveModuleFrontController extends ModuleFrontController {

    public function initContent() {

        $this->ajax = true;

        parent::initContent();

       
        $this->save();

    }

    public function save() {
        global $cookie;
        if(isset($_POST) && !empty($_POST)){
            $o['ip'] = $_POST['ip'];
            // $o['ip'] = '94.232.181.245'; // Біла Церква
            // $o['ip'] = '195.22.108.110'; // Кам'янець-Подільський
            // $o['ip'] = '212.1.118.18'; // Вінниця
            // $o['ip'] = '192.162.142.150'; // Запоріжжя
            $geo = new Geo($o);
            $city = $geo->get_value('city');
            if(is_null($city)) {
                $city = 'ваш город';
            } else {
                $city = $this->getWordSingleForm($city);
            }
            $cookie->city = $city;
            $response = array('status' => true, 'city' => $city);
            $json = Tools::jsonEncode($response);
            echo $json;
            exit;
        }
    }



    private function getWordSingleForm($wordOriginal)
    {
         if (!class_exists('phpMorphy')) {
             require_once _PS_MODULE_DIR_.'mod_kl_productCustomBlock/lib/src/common.php';
         }
        // set some options
        $opts = array(
            // storage type, follow types supported
            // PHPMORPHY_STORAGE_FILE - use file operations(fread, fseek) for dictionary access, this is very slow...
            // PHPMORPHY_STORAGE_SHM - load dictionary in shared memory(using shmop php extension), this is preferred mode
            // PHPMORPHY_STORAGE_MEM - load dict to memory each time when phpMorphy intialized, this useful when shmop ext. not activated. Speed same as for PHPMORPHY_STORAGE_SHM type
            'storage' => PHPMORPHY_STORAGE_FILE,
            // Enable prediction by suffix
            'predict_by_suffix' => true, 
            // Enable prediction by prefix
            'predict_by_db' => true,
            // TODO: comment this
            'graminfo_as_text' => false,
        );

        // Path to directory where dictionaries located
        $dir = _PS_MODULE_DIR_. 'mod_kl_productCustomBlock/lib/dicts';
        $lang = 'ru_RU';

        // Create phpMorphy instance
        try { 
            $morphy = new phpMorphy($dir, $lang, $opts);
        } catch(phpMorphy_Exception $e) {
            return $wordOriginal;
        }

        $myCustomCategoryName = $wordOriginal;
        $upperCaseName = mb_strtoupper($wordOriginal);
        $words = explode(' ', $upperCaseName);
 
        try {
            $gender = null;
            foreach($words as $word) {
                $partOfSpeech = $morphy->getPartOfSpeech($word);
                foreach ($partOfSpeech as $part) {
                    if ($part == PMY_RP_NOUN) {
                        $gramInfo = $morphy->getGramInfoMergeForms($word);
                        foreach ($gramInfo as $key => $info) {
                            if (in_array(PMY_RG_NOMINATIV, $info["grammems"])) {
                                if (in_array(PMY_RG_MASCULINUM, $info["grammems"])) {
                                    $gender = PMY_RG_MASCULINUM;
                                } elseif (in_array(PMY_RG_FEMINUM, $info["grammems"])) {
                                    $gender = PMY_RG_FEMINUM;
                                } elseif (in_array(PMY_RG_NEUTRUM, $info["grammems"])) {
                                    $gender = PMY_RG_NEUTRUM;
                                } elseif (in_array(PMY_RG_MASC_FEM, $info["grammems"])) {
                                    $gender = PMY_RG_MASC_FEM;
                                }
                                $newWord = array_pop($morphy->castFormByGramInfo($word, null, array(PMY_RG_ACCUSATIV, PMY_RG_SINGULAR)))["form"];
                                $myCustomCategoryName = preg_replace("/".$word."/iu", mb_strtolower($newWord), $myCustomCategoryName);
                                continue 2;
                            }
                        }
                    }
                }
            }
            foreach($words as $word) {
                $partOfSpeech = $morphy->getPartOfSpeech($word);
                foreach ($partOfSpeech as $part) {
                    if ($part == PMY_RP_ADJ_FULL) {
                        $gramInfo = $morphy->getGramInfoMergeForms($word);
                        foreach ($gramInfo as $key => $info) {
                            if (in_array(PMY_RG_NOMINATIV, $info["grammems"])) {
                                if (is_null($gender)) {
                                    $newWord = $morphy->castFormByGramInfo($word, null, array(PMY_RG_ACCUSATIV, PMY_RG_SINGULAR))[0]["form"];
                                } else {
                                    $newWord = $morphy->castFormByGramInfo($word, null, array(PMY_RG_ACCUSATIV, PMY_RG_SINGULAR, $gender))[0]["form"];
                                }
                                $myCustomCategoryName = preg_replace("/".$word."/iu", mb_strtolower($newWord), $myCustomCategoryName);
                                continue 2;
                            }
                        }
                    }
                }
            }
        } catch(phpMorphy_Exception $e) {
            return $wordOriginal;
        }
        return  $this->mb_ucwords($myCustomCategoryName);
    }

    private function mb_ucwords($str)
    {
        return mb_convert_case($str, MB_CASE_TITLE, "UTF-8");
    }
}