if(typeof cookieCity === "undefined" || cookieCity === false)
{
    $.getJSON('https://api.ipify.org?format=json', function(data1){
        $.ajax({
            url: ajaxurl,
            type: "post",
            dataType: "json",
            data: {
                "ip": data1.ip,
            },  
            success: function(data){
                myfunc(data.city);
            },
            error: function(data){
                myfunc('ваш город');
            },
        });
    });
} else {
   myfunc(cookieCity); 
}

function myfunc(str){ 
    $('.js_custom_bl').each(function (){
        var new_str = $(this).html().replace('%city%',str);
        $(this).html(new_str);
    });
}
