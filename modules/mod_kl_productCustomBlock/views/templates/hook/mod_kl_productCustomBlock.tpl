{if isset($product_image) || isset($text1) || isset($text2)}
  <a href="{$link_product}">
  {if isset($product_image)}
    <img src="{$product_image}" class="img-fluid">
  {/if}
  </a>
  <span class="js_custom_bl">{$text1 nofilter}</span>
  <br />
  <span class="js_custom_bl">{$text2 nofilter}</span>
{/if}