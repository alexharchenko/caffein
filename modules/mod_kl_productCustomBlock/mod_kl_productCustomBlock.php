<?php

if (!defined('_CAN_LOAD_FILES_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

use PrestaShop\PrestaShop\Adapter\Category\CategoryProductSearchProvider;
use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;
use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;
use PrestaShop\PrestaShop\Core\Product\ProductListingPresenter;
use PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchContext;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery;
use PrestaShop\PrestaShop\Core\Product\Search\SortOrder;

class mod_kl_productCustomBlock extends Module implements WidgetInterface
{
    private $templateFile;

    public function __construct()
    {
        $this->name = 'mod_kl_productCustomBlock';
        $this->version = '2.0.0';
        $this->author = 'Klookva Antikov Posternak';
        $this->need_instance = 0;
        
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Klookva Proposal to deliver the product to the client\'s locality');
        $this->description = $this->l('Offers to deliver the product to the client\'s locality (Works only for the Russian language)');


        $this->templateFile = 'module:mod_kl_productCustomBlock/views/templates/hook/mod_kl_productCustomBlock.tpl';
    }

    public function install()
    {
        return parent::install()
            && $this->registerHook('displayCustomPlaceBlock')
            && $this->registerHook('displayProductPlaceBlock')
            && $this->registerHook('displayHeader')
            && Configuration::updateValue('KLOOKVA_ID_PRODUCT', 0)
            && Configuration::updateValue('KLOOKVA_TEXT_CUSTOM1', '')
            && Configuration::updateValue('KLOOKVA_TEXT_CUSTOM2', '')
            && Configuration::updateValue('KLOOKVA_TEXT_PRODUCT', '')
        ;
    }

    public function hookDisplayHeader() {
        global $cookie;
        Media::addJsDef(array('ajaxurl' => $this->context->link->getModuleLink('mod_kl_productCustomBlock','sessionSave')));
        Media::addJsDef(array('cookieCity' => $cookie->city));
        $this->context->controller->registerJavascript('modules-productCustomBlock-city', 'modules/'.$this->name.'/js/city.js', ['position' => 'bottom', 'priority' => 150]);
    }

    public function uninstall()
    {
        return
            Configuration::deleteByName('KLOOKVA_ID_PRODUCT') &&
            Configuration::deleteByName('KLOOKVA_TEXT_CUSTOM1') &&
            Configuration::deleteByName('KLOOKVA_TEXT_CUSTOM2') &&
            Configuration::deleteByName('KLOOKVA_TEXT_PRODUCT') &&
            Configuration::deleteByName('KLOOKVA_ALLPRODUCTS_CB') &&
            parent::uninstall();
    }

    public function hookDisplayProductPlaceBlock($params)
    {
        global $link;
        $product = new Product((int)Configuration::get('KLOOKVA_ID_PRODUCT'));
        $this->context->smarty->assign('text', str_replace("%link%", '<a href="'.$product->getLink().'">'.$product->name[$this->context->language->id].'</a>', Configuration::get('KLOOKVA_TEXT_PRODUCT')));
        if (Configuration::get('KLOOKVA_ALLPRODUCTS_CB') == 1 || (Configuration::get('KLOOKVA_ALLPRODUCTS_CB') == 0 && $params['id_product']==Configuration::get('KLOOKVA_ID_PRODUCT')))
            return $this->display(__FILE__, 'views/templates/hook/productBlock.tpl');
    }

    public function getContent()
    {
        if (Tools::isSubmit('savemod_kl_productCustomBlock')) {
            Configuration::updateValue('KLOOKVA_ID_PRODUCT', (int)Tools::getValue('id_product'));
            Configuration::updateValue('KLOOKVA_TEXT_CUSTOM1', Tools::getValue('text_custom1_'.$this->context->language->id));
            Configuration::updateValue('KLOOKVA_TEXT_CUSTOM2', Tools::getValue('text_custom2_'.$this->context->language->id));
            Configuration::updateValue('KLOOKVA_TEXT_PRODUCT', Tools::getValue('text_product_'.$this->context->language->id));
            Configuration::updateValue('KLOOKVA_ALLPRODUCTS_CB', Tools::getValue('all_products'));
        }
        return $this->initForm();
    }

    protected function initForm()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $this->fields_form[0]['form'] = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->l('Klookva продукт'),
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->trans('Product', array(), 'Admin.Global'),
                    'name' => 'id_product',
                    'class' => 'input-lg',
                    'options' => array(
                        'query' => $this->getDisplayProductsForHelper(),
                        'id' => 'id',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Text1 for custom place(for link)'),
                    'lang' => true,
                    'name' => 'text_custom1',
                    'desc' => $this->l('%link% - for url to product %city% - for city user'),
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Text2 for custom place(for city)'),
                    'lang' => true,
                    'name' => 'text_custom2',
                    'desc' => $this->l('%link% and %city% can used in all fields'),
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Text for product place'),
                    'lang' => true,
                    'name' => 'text_product',
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show for all products?'),
                    'name' => 'all_products',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'all_products_on',
                            'value' => 1,
                            'label' => $this->l('Yes')),
                        array(
                            'id' => 'all_products_off',
                            'value' => 0,
                            'label' => $this->l('No')),
                    ),
                )
            ),
            'submit' => array(
                'title' => $this->l('Save', array(), 'Admin.Actions'),
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = 'mod_kl_productCustomBlock';
        $helper->identifier = $this->identifier;
        foreach (Language::getLanguages(false) as $lang) {
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );
        }
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'savemod_kl_productCustomBlock';
        $helper->fields_value['id_product'] = Configuration::get('KLOOKVA_ID_PRODUCT');
        $helper->fields_value['text_custom1'][(int)Configuration::get('PS_LANG_DEFAULT')] = Configuration::get('KLOOKVA_TEXT_CUSTOM1');
        $helper->fields_value['text_custom2'][(int)Configuration::get('PS_LANG_DEFAULT')] = Configuration::get('KLOOKVA_TEXT_CUSTOM2');
        $helper->fields_value['text_product'][(int)Configuration::get('PS_LANG_DEFAULT')] = Configuration::get('KLOOKVA_TEXT_PRODUCT');
        $helper->fields_value['all_products'] = Configuration::get('KLOOKVA_ALLPRODUCTS_CB');
        return $helper->generateForm($this->fields_form);
    }
    public function getDisplayProductsForHelper()
    {
        $sql = "SELECT h.`id_product` as id, h.`name` as name
                FROM "._DB_PREFIX_."product_lang h
                WHERE `id_lang`= ".(int)Configuration::get('PS_LANG_DEFAULT')."
                ORDER BY h.`name` ASC
            ";
        $products = Db::getInstance()->executeS($sql);

        foreach ($products as $key => $product) {
            if (preg_match('/admin/i', $product['name'])
                || preg_match('/backoffice/i', $product['name'])) {
                unset($products[$key]);
            }
        }
        return $products;
    }

    protected function _clearCache($template, $cacheId = null, $compileId = null)
    {
        parent::_clearCache($this->templateFile);
    }
    public function renderWidget($hookName = null, array $configuration = [])
    {
        if (!$this->isCached($this->templateFile, $this->getCacheId('mod_kl_productCustomBlock'))) {
            $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));
        }

        return $this->fetch($this->templateFile, $this->getCacheId('mod_kl_productCustomBlock'));
    }

    public function getWidgetVariables($hookName = null, array $configuration = [])
    {
        global $link;
        $product = new Product((int)Configuration::get('KLOOKVA_ID_PRODUCT'), true, $this->context->language->id);
        $text1 = str_replace("%link%", '<a href="'.$product->getLink().'">'.$product->name[$this->context->language->id].'</a>', Configuration::get('KLOOKVA_TEXT_CUSTOM1'));
        $text2 = str_replace("%link%", '<a href="'.$product->getLink().'">'.$product->name[$this->context->language->id].'</a>', Configuration::get('KLOOKVA_TEXT_CUSTOM2'));
        return array(
            'product_image' => $link->getImageLink($product->link_rewrite, $product->getCoverWs(), 'home_default'),
            'text1' => $text1,
            'text2' => $text2,
            'link_product' => $product->getLink(),
        );
    
    }
}
