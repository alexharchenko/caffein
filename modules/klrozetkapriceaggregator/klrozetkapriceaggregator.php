<?php
/**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class KlRozetkaPriceAggregator extends Module
{
    protected $params = array();

    public function __construct()
    {
        $this->name = 'klrozetkapriceaggregator';
        $this->tab = 'others';
        $this->version = '1.0.2';
        $this->author = 'klookva';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Rozetka Price Aggregator');
        $this->description = $this->l('Provide Rozetka with XML-file with your product`s prices');
        $this->confirmUninstall = $this->l('Are you sure you want to delete Rozetka Price Aggregator module?');
        if ($params = Configuration::get($this->name)) {
            $this->params = unserialize($params);
        } else {
            $this->warning = $this->l('Some error occured during installation');
        }
    }

    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }
        if (parent::install() && Configuration::updateValue($this->name, serialize(array(
            'use_template_name' => 0,
            'company_name' => '',
            'id_lang' => $this->context->language->id,
        )))) {
            return true;
        }
        return false;
    }

    public function uninstall()
    {
        if (parent::uninstall()
            && Configuration::deleteByName($this->name)) {
            return true;
        }
        return false;
    }

    public function getContent()
    {
        $output = null;
        if (Tools::isSubmit('save_' . $this->name)) {
            $output .= $this->saveSettings();
        }
        return  $output . $this->displaySettingsForm();
    }

    private function saveSettings()
    {
        if (Configuration::updateValue($this->name, serialize(array(
            'use_template_name' => Tools::getValue('use_template_name'),
            'company_name' => Tools::getValue('company_name'),
            'id_lang' => Tools::getValue('id_lang'),
            'product_id[]' => Tools::getValue('items'),
            
        )))) {
            $this->params = unserialize(Configuration::get($this->name));
            return $this->displayConfirmation($this->l('Settings updated'));
        }
        return $this->displayError($this->l("Error occured during data saving"));
    }

    private function displaySettingsForm()
    {
        $availableItems = $this->renderMultiselect(
            $this->formatProducts($this->getAllProductsID()),
            'availableItems',
            'id_product'
        );
        if (array_key_exists('product_id[]', $this->params)) {
            $enabledItems = $this->renderMultiselect(
                $this->formatProducts($this->params['product_id[]']),
                'items',
                'id_product'
            );
        } else {
            $enabledItems = $this->renderMultiselect(
                $this->formatProducts(array()),
                'items',
                'id_product'
            );
        }
        $data = $this->params;
        $data['link'] = $this->context->link->getModuleLink($this->name, 'display').
        '?token='.Tools::substr(Tools::encrypt('klrozetkapriceaggregator/index'), 0, 10);
        $default_lang = Configuration::get('PS_LANG_DEFAULT');
        $this->fields_form[0]['form'] = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->l("Settings"),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Full company name'),
                    'lang' => false,
                    'name' => 'company_name',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Link for Rozetka'),
                    'lang' => false,
                    'name' => 'link',
                    'disabled' => 'true'
                ),
                array(
                    'type' => 'multichoise',
                    'label' => $this->l('Products', array(), 'Admin.Global'),
                    'name' => 'items',
                    'lang' => true,
                ),
                array(
                    'label' => $this->l('Language to format'),
                    'name' => 'id_lang',
                    'type' => 'select',
                    'options' => array(
                        'query' => Language::getLanguages(),
                        'id' => 'id_lang',
                        'name' => 'name',
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Use template name?'),
                    'name' => 'use_template_name',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'use_template_name_on',
                            'value' => 1,
                            'label' => $this->trans('Enabled', array(), 'Admin.Global')
                        ),
                        array(
                            'id' => 'use_template_name_off',
                            'value' => 0,
                            'label' => $this->trans('Disabled', array(), 'Admin.Global')
                        )
                    )
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save', array(), 'Admin.Actions'),
            ),
        );
        $helper = new HelperForm();
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $data,
            'available' => $availableItems,
            'enabled' => $enabledItems
        );
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->title = $this->displayName;
        $helper->submit_action = 'save_' . $this->name;
        $helper->toolbar_btn = array(
            'save' => array(
                    'desc' => $this->l('Save', array(), 'Admin.Actions'),
                    'href' => AdminController::$currentIndex . '&configure=' . $this->name .
                     '&save_' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules'),
                ),
            'back' => array(
                    'desc' => $this->l('Back to list', array(), 'Admin.Actions'),
                    'href' => AdminController::$currentIndex . '&configure=' . $this->name .
                     '&token='.Tools::getAdminTokenLite('AdminModules'),
                )
        );
        return $helper->generateForm($this->fields_form);
    }

    private function formatProducts($productsID)
    {
        $prods = array();

        if (empty($productsID)) {
            return;
        }

        foreach ($productsID as $i) {
            $index = explode("-", $i);
            $id = $index[0];
            $p = new Product($id, false, Configuration::get('PS_LANG_DEFAULT'));
            if (isset($index[1])) {
                if ($p->getAttributesResume(Configuration::get('PS_LANG_DEFAULT'))) {
                    $array_attr = $p->getAttributeCombinationsById($index[1], Configuration::get('PS_LANG_DEFAULT'));
                } else {
                    $array_attr = array();
                }
                foreach ($array_attr as $comb) {
                    $p->name .= " " . $comb['attribute_name'];
                }
                $prods[] = array(
                        'id_product' => $i,
                        'name' => $p->name,
                    );
            } else {
                if ($p->getAttributesResume(Configuration::get('PS_LANG_DEFAULT'))) {
                    $array_attr = $p->getAttributesResume(Configuration::get('PS_LANG_DEFAULT'));
                    foreach ($array_attr as $comb) {
                        $prods[] = array(
                            'id_product' => $comb['id_product'] . "-" . $comb['id_product_attribute'],
                            'name' => $p->name . " " . $comb['attribute_designation']
                        );
                    }
                } else {
                    $prods[] = array(
                        'id_product' => $id,
                        'name' => $p->name
                    );
                }
            }
        }
        return $prods;
    }

    private function getAllProductsID()
    {
        $id_shop = (int)Shop::getContextShopID();
        $sql = 'SELECT id_product
                FROM `'._DB_PREFIX_.'product`
                WHERE `'._DB_PREFIX_.'product`.id_shop_default=' . (int)$id_shop;
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        return $this->extractSubArray($result, 'id_product');
    }

    private function extractSubArray($array, $fieldNAme)
    {
        $res = array();
        foreach ($array as $a) {
            $res[] = $a[$fieldNAme];
        }
        return $res;
    }
    public function renderMultiselect($elements, $selector_id, $id_field = "id", $name_field = "name")
    {
        $this->context->smarty->assign(array(
                   'elements' => $elements,
                   'selector_id' => $selector_id,
                   'id_field' => $id_field,
                   'name_field' => $name_field
               ));
        return $this->display(__FILE__, 'views/templates/admin/multiselect.tpl');
    }
}
