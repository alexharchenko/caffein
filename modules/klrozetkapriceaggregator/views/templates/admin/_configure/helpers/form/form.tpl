{**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}
{extends file="helpers/form/form.tpl"}

{block name="script"}
    $(document).ready(function(){

        $("#items").closest('form').on('submit', function(e) {
        	$("#items option").prop('selected', true);
        });

        $("#addItem").click(add);
        $("#availableItems").dblclick(add);
        $("#removeItem").click(remove);
        $("#items").dblclick(remove);

        function add()
        {
        	$("#availableItems option:selected").each(function(i){
        		var val = $(this).val();
        		var text = $(this).text();
        		text = text.replace(/(^\s*)|(\s*$)/gi,"");
        		$("#items").append('<option value="'+val+'" selected="selected">'+text+'</option>');
        	});
        	serialize();
        	return false;
        }

        function remove()
        {
        	$("#items option:selected").each(function(i){
        		$(this).remove();
        	});
        	serialize();
        	return false;
        }

        function serialize()
        {
        	var options = "";
        	$("#items option").each(function(i){
        		options += $(this).val()+",";
        	});
        }
    });
{/block}

{block name="input"}
    {if $input.type == 'multichoise'}
	    <div class="row">
	    	<div class="col-lg-6">
				<h4 style="margin-top:5px;">{l s='Selected items' d='Modules.Mainmenu.Admin'}</h4>
				{$enabled}
			</div>
			<div class="col-lg-6">
				<h4 style="margin-top:5px;">{l s='Available items' d='Modules.Mainmenu.Admin'}</h4>
				{$available}
	    	</div>
	    </div>
	    <br/>
	    <div class="row">
	    	<div class="col-lg-1"></div>
	    	<div class="col-lg-5"><a href="#" id="removeItem" class="btn btn-default"><i class="icon-arrow-right"></i> {l s='Remove' d='Modules.Mainmenu.Admin'}</a></div>
		<div class="col-lg-6"><a href="#" id="addItem" class="btn btn-default"><i class="icon-arrow-left"></i> {l s='Add' d='Admin.Actions'}</a></div>
	    </div>
	{else}
		{$smarty.block.parent}
    {/if}
{/block}
