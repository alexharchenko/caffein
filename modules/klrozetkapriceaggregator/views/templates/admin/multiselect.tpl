{**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}
<select multiple="multiple" name="{$selector_id}[]" id="{$selector_id}" style="width: 600px; height: 160px;">
        {foreach $elements as $e}
            <option value="{$e[$id_field]}">{$e[$name_field]}</option>
        {/foreach}
</select>