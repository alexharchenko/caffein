{**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}
<!DOCTYPE yml_catalog SYSTEM "shops.dtd">
<yml_catalog date="{$date}">
    <shop>
        <name>{$sitename}</name>
        <company>{$company}</company>
        <url>{$main_url}</url>
        {if !empty($currencies)}
            <currencies>
                {foreach $currencies as $cur}
                    <currency id="{$cur.iso_code}" rate="{$cur.conversion_rate}"/>
                {/foreach}
            </currencies>
        {/if}
        {if !empty($categories)}
            <categories>
                {foreach $categories as $c}
                    <category id="{$c.id_category}" {if in_array($c.id_parent, $categoryIdList)}parentId="{$c.id_parent}"{/if}>{$c.name}</category>
                {/foreach}
            </categories>
        {/if}
        {if !empty($products)}
            <offers>
                {foreach $products as $p}
                    <offer id="{$p.id_offer}" available="{if $p.available == 1}true{else}false{/if}">
                        <url>{$p.url}</url>
                        <price>{$p.price}</price>
                        <currencyId>{$p.iso_currency}</currencyId>
                        <categoryId>{$p.id_category_default}</categoryId>
                        {if (!empty($p.photos))}
                            {foreach $p.photos as $photo}
                                <picture>{$photo}</picture>
                            {/foreach}
                        {/if}
                        <vendor>{$p.manufacturer_name_only}</vendor>
                        <stock_quantity>{$p.quantity}</stock_quantity>
                        <name>{$p.name}{if $use_template_name == 1} {$p.manufacturer_name_only}{if $p.reference && $p.reference != ''} ({$p.reference}){/if}{/if}</name>
                        <description><![CDATA[{if $p.description_short && trim($p.description_short) != ''}{$p.description_short}{else}{$p.description}{/if}]]></description>
                        {if (!empty($p.params))}
                            {foreach $p.params as $name => $param}
                                {if $param != ''}
                                    <param name="{$name}">{$param}</param>
                                {/if}
                            {/foreach}
                        {/if}
                    </offer>
                {/foreach}
            </offers>
        {/if}
    </shop>
</yml_catalog>