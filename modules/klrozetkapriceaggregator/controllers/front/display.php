<?php
/**
* @author    Antikov Evgeniy
* @copyright 2017-2018 kLooKva
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

class KlRozetkaPriceAggregatorDisplayModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        $encrypt_f = Tools::encrypt('klrozetkapriceaggregator/index');
        if (Tools::getValue('token') != Tools::substr($encrypt_f, 0, 10)) {
            die($this->module->l('Invalid token'));
        }

        $xml = $this->generateXml();
        ob_start();

        echo $xml;

        $string = ob_get_clean();
        $filename = 'rozetka_export_' . date("Y-m-d H:i:s");
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . $filename . '.xml";');
        header('Content-Transfer-Encoding: binary');
        exit($string);
    }

    private function generateXml()
    {
        $sitename = Configuration::get('PS_SHOP_NAME');
        $globals = unserialize(Configuration::get("klrozetkapriceaggregator"));
        $use_template_name = $globals['use_template_name'];
        if (!$globals['company_name'] || $globals['company_name'] == '') {
            $company = $sitename;
        } else {
            $company = $globals['company_name'];
        }
        $currencies = Currency::getCurrencies();
        $products = $this->getProductList();
        $categoryIdList = array();
        $categories = array();
        if (!empty($products)) {
            $categoryIdList = $this->extractSubArray($products, 'id_category_default');
            $categories = $this->getCategoriesList($categoryIdList);
        }
        $main_url = $this->context->link->getBaseLink();
        $header = '<?xml version="1.0" encoding="UTF-8" ?>';
        $this->context->smarty->assign(array(
                   'sitename' => $sitename,
                   'categories' => $categories,
                   'categoryIdList' => $this->extractSubArray($categories, 'id_category'),
                   'products' => $products,
                   'currencies' => $currencies,
                   'company' => $company,
                   'main_url' => $main_url,
                   'use_template_name' => $use_template_name,
                   'date' => date('Y-m-d H:i')
               ));
        return $header . $this->context->smarty->fetch(_PS_MODULE_DIR_.$this->module->name.
            '/views/templates/front/xml.tpl');
    }

    private function getCategoriesList($ids)
    {
        $ids = implode(", ", $ids);
        $langId = (int)Configuration::get('PS_LANG_DEFAULT');
        $sql = "SELECT "._DB_PREFIX_."category.id_category, 
                        "._DB_PREFIX_."category_lang.name, 
                        "._DB_PREFIX_."category.id_parent
                FROM "._DB_PREFIX_."category_lang 
                RIGHT JOIN "._DB_PREFIX_."category
                ON "._DB_PREFIX_."category.id_category = "._DB_PREFIX_."category_lang.id_category
                WHERE "._DB_PREFIX_."category_lang.id_lang = $langId
                AND "._DB_PREFIX_."category.id_category IN (" . pSQL($ids) . ");";
        return Db::getInstance()->executeS($sql);
    }

    private function getProductList()
    {
        $globals = unserialize(Configuration::get("klrozetkapriceaggregator"));

        if (array_key_exists('product_id[]', $globals) && $globals['product_id[]']) {
            $id_products = $globals['product_id[]'];
        } else {
            return array();
        }

        $products = array();
        foreach ($id_products as $id) {
            $index = explode("-", $id);
            $id = $index[0];
            $p = new Product($id, true, $globals['id_lang']);
            $p->params = array();
            $p->id_product = $id;
            $p->photo = Product::getCover($p->id_product)['id_image'];
            if ($p->id_manufacturer == 0) {
                $p->manufacturer_name_only = Configuration::get('PS_SHOP_NAME');
            } else {
                $manufacturer = new Manufacturer($p->id_manufacturer);
                $p->manufacturer_name_only = $manufacturer->name;
            }

            if (!array_key_exists('id_currency', $p->specificPrice)
                || $p->specificPrice['id_currency'] == 0
            ) {
                $tmp_cur = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));
            } else {
                $tmp_cur = new Currency($p->specificPrice['id_currency']);
            }
            $p->iso_currency = $tmp_cur->iso_code;

            $allow_oosp = Product::isAvailableWhenOutOfStock($p->out_of_stock);
            if (!Configuration::get('PS_STOCK_MANAGEMENT')
                || ($p->quantity <= 0 && !$p->available_later && $allow_oosp)
                || ($p->quantity > 0 && !$p->available_now)
                || !$p->available_for_order
                || Configuration::get('PS_CATALOG_MODE')) {
                $p->available = 1;
            } else {
                $p->available = 0;
            }

            $features = $p->getFeatures();
            if ($features) {
                foreach ($features as $feature) {
                    $tmp_feature = new Feature($feature['id_feature']);
                    $value_f = $this->getFeatureValue(
                        $feature['id_feature'],
                        $feature['id_product'],
                        $feature['id_feature_value'],
                        $feature['custom'],
                        $globals['id_lang']
                    );
                    $p->params[$tmp_feature->name[$globals['id_lang']]] = $value_f;
                }
            }
   
            if (isset($index[1])) {
                $p->name = $this->getProductName($id, $index[1]);
                $p->price = $p->getPrice(true, $index[1]);
                $photos_id = $this->getCombinationImagesById($index[1], $id);
                $p->photos = array();
                $p->id_offer = $index[0].$index[1];
                foreach ($photos_id as $photo_id) {
                    $p->photos[] = $this->context->link->getImageLink(
                        $p->link_rewrite,
                        $photo_id['id_image']
                    );
                }

                $p->url = $this->context->link->getProductLink(
                    $p,
                    null,
                    null,
                    null,
                    null,
                    null,
                    $index[1],
                    false,
                    false,
                    false
                );

                $attributes = Product::getAttributesParams($index[0], $index[1]);
                if ($attributes) {
                    foreach ($attributes as $attribute) {
                        $p->params[$attribute['group']] = $attribute['name'];
                    }
                }

                if ($attributes[0]['reference'] &&
                    isset($attributes[0]['reference']) &&
                    trim($attributes[0]['reference']) != ''
                ) {
                    $reference = $attributes[0]['reference'];
                } else {
                    $reference = $p->reference;
                }
                
                $p->reference = $reference;
                $p->params[$this->trans('Reference', array(), 'Admin.Global')] = $reference;
            } else {
                $p->name = $this->getProductName($id);
                $p->price = $p->getPrice();
                $p->params[$this->trans('Reference', array(), 'Admin.Global')] = $p->reference;
                $photos_id = $p->getImages($globals['id_lang']);
                $p->photos = array();
                $p->id_offer = $index[0].'000';
                foreach ($photos_id as $id_ph => $photo_id) {
                    if ($id_ph == 8) {
                        break;
                    }
                    $p->photos[] = $this->context->link->getImageLink(
                        $p->link_rewrite,
                        $photo_id['id_image']
                    );
                }
                $p->url = $this->context->link->getProductLink($p);
            }
            $products[] = (array)$p;
        }
        return $products;
    }

    public function getProductName(
        $id_product,
        $id_product_attribute = null,
        $id_lang = null
    ) {

        if (!$id_lang) {
            $id_lang = (int)Context::getContext()->language->id;
        }

        $query = new DbQuery();

        if ($id_product_attribute) {
            $query->select('IFNULL(CONCAT(pl.name, \' \', GROUP_CONCAT(
                DISTINCT al.name SEPARATOR \' \')),pl.name) as name');
        } else {
            $query->select('DISTINCT pl.name as name');
        }

        // adds joins & where clauses for combinations
        if ($id_product_attribute) {
            $query->from('product_attribute', 'pa');
            $query->join(Shop::addSqlAssociation('product_attribute', 'pa'));
            $query->innerJoin('product_lang', 'pl', 'pl.id_product = pa.id_product 
                AND pl.id_lang = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl'));
            $query->leftJoin(
                'product_attribute_combination',
                'pac',
                'pac.id_product_attribute = pa.id_product_attribute'
            );
            $query->leftJoin('attribute', 'atr', 'atr.id_attribute = pac.id_attribute');
            $query->leftJoin(
                'attribute_lang',
                'al',
                'al.id_attribute = atr.id_attribute AND al.id_lang = '.(int)$id_lang
            );
            $query->leftJoin(
                'attribute_group_lang',
                'agl',
                'agl.id_attribute_group = atr.id_attribute_group
                AND agl.id_lang = '.(int)$id_lang
            );
            $query->where('pa.id_product = '.(int)$id_product.'
                AND pa.id_product_attribute = '.(int)$id_product_attribute);
        } else {
            // or just adds a 'where' clause for a simple product

            $query->from('product_lang', 'pl');
            $query->where('pl.id_product = '.(int)$id_product);
            $query->where('pl.id_lang =
                '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl'));
        }

        return Db::getInstance()->getValue($query);
    }

    private function getCombinationImagesById(
        $id_product_attribute,
        $id_product = null,
        $limit = 8
    ) {
        $sql = 'SELECT pai.`id_image` 
            FROM `'._DB_PREFIX_.'product_attribute_image` pai
            WHERE pai.`id_product_attribute` = '.(int)$id_product_attribute.'
            LIMIT '.(int)$limit;
  
        $result = Db::getInstance()->executeS($sql);

        if (!$result && isset($id_product)) {
            $sql = 'SELECT i.`id_image`
            FROM `'._DB_PREFIX_.'image` i
            '.Shop::addSqlAssociation('image', 'i').'
            WHERE i.`id_product` = '.(int)$id_product.'
            ORDER BY i.`position`
            LIMIT '.(int)$limit;

            $result = Db::getInstance()->executeS($sql);

            if (!$result) {
                return array();
            }
        }

        return $result;
    }

    private function getFeatureValue($id_feature, $id_product, $id_feature_value, $custom, $id_lang)
    {
        $sql = 'SELECT fvl.`value` 
            FROM `'._DB_PREFIX_.'feature` f
            LEFT JOIN `'._DB_PREFIX_.'feature_product` fp ON (f.id_feature = fp.id_feature)
            LEFT JOIN `'._DB_PREFIX_.'feature_value` fv ON (f.id_feature = fv.id_feature)
            LEFT JOIN `'._DB_PREFIX_.'feature_value_lang` fvl 
            ON (fv.id_feature_value = fvl.id_feature_value)
            WHERE f.`id_feature` = '.(int)$id_feature.' 
            AND fp.`id_product` = '.(int)$id_product.' 
            AND fv.`custom` = '.(int)$custom.' 
            AND fv.`id_feature_value` = '.(int)$id_feature_value.' 
            AND fvl.`id_lang` = '.(int)$id_lang;

        $result = Db::getInstance()->getValue($sql);

        if (!$result) {
            return '';
        }

        return $result;
    }

    private function extractSubArray($items, $fieldname)
    {
        $values = array();
        foreach ($items as $i) {
            $values[] = $i[$fieldname];
        }
        return $values;
    }
}
