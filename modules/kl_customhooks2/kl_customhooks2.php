<?php

class kl_customhooks2 extends Module
{
    public function __construct()
    {
        $this->name = 'kl_customhooks2';
        $this->version = '1.0.0';
        $this->author = 'kLooKva Antikov';
        $this->need_instance = 0;
        $this->bootstrap = true; 
        parent::__construct();

        $this->displayName = $this->trans('Klookva custom hooks2', array(), 'Modules.kl_customhooks.Admin');
        $this->description = $this->trans('custom hooks for us2', array(), 'Modules.kl_customhooks.Admin');
        $this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        if (parent::install() == false or !$this->registerHook('displayBackOfficeHeader'))
        {
            return false;
        }
        return true;
    }

    public function uninstall()
    {
        $this->unregisterHook('displayBackOfficeHeader');
        return parent::uninstall();
    }

    public function hookDisplayBackOfficeHeader($params)
    {
        $this->context->controller->addJS($this->_path.'new.tinymce.inc.js', 'all');
    }

}