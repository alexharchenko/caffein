function newTinySetup(config) {
 
  if (typeof tinyMCE === 'undefined') {
    setTimeout(function () {
      newTinySetup(config);
    }, 100);
    return;
  }  
  if (!config) {
    config = {};
  }
 
  if (typeof config.editor_selector != 'undefined') {
    config.selector = '.' + config.editor_selector;
  }
 
  var default_config = {
    selector: ".rte",
    content_css : '/forTinyMCE.css',
    style_formats: [
      { title: 'Header 1', block: 'div', classes: 'h1' },
      { title: 'Header 2', block: 'div', classes: 'h2' },
      { title: 'Header 3', block: 'div', classes: 'h3' },
      { title: 'Header 4', block: 'div', classes: 'h4' },
      { title: 'Параграф', inline: 'p', classes: '' },
    ],
    height : "400",
    browser_spellcheck: true,
    plugins : "visualblocks, preview searchreplace print insertdatetime, hr charmap colorpicker anchor code link image paste pagebreak table contextmenu filemanager table code media textcolor emoticons",
    toolbar2 : "newdocument,print,|,bold,italic,underline,|,strikethrough,superscript,subscript,|,forecolor,colorpicker,backcolor,|,bullist,numlist,outdent,indent",
    toolbar1 : "styleselect,|,fontselect,|,fontsizeselect,", 
    toolbar3 : "code,|,table,|,cut,copy,paste,searchreplace,|,blockquote,|,undo,redo,|,link,unlink,anchor,|,image,emoticons,media,|,inserttime,|,preview ",
    toolbar4 : "visualblocks,|,charmap,|,hr,",
    external_filemanager_path: baseAdminDir + "filemanager/",
    filemanager_title: "File manager",
    external_plugins: {"filemanager": baseAdminDir + "filemanager/plugin.min.js"},
    language: iso_user,
    skin: "prestashop",
    menubar: false,
    statusbar: false,
    relative_urls: false,
    convert_urls: false,
    entity_encoding: "raw",
    custom_elements: "style",
    extended_valid_elements: "pre[*],style[*]",
    valid_children: "+body[style|section],pre[section|div|p|br|span|img|style|h1|h2|h3|h4|h5],*[*]",
    valid_elements : '*[*]', 
    force_p_newlines : false, 
    cleanup: false,
    forced_root_block : false, 
    force_br_newlines : true,  
    convert_urls:true,
    relative_urls:false,
    remove_script_host:false,
    init_instance_callback: "changeToMaterial"
  };
 
  $.each(default_config, function (index, el) {
    if (config[index] === undefined)
      config[index] = el;
  });
 
  // Change icons in popups
  $('body').on('click', '.mce-btn, .mce-open, .mce-menu-item', function () {
    changeToMaterial();
  });
 // tinyMCE.get("content_5").setContent('5');
  tinyMCE.init(config);
}

$(document).ready(function() {
  setTimeout(function () {
    $("textarea").each(function(indx){
      textareaId = $(this).attr("id");
      if (typeof tinyMCE !== 'undefined' && typeof tinyMCE.get(textareaId) !== 'undefined') {
        tinyMCE.get(textareaId).remove();
      }
      newTinySetup({init_instance_callback : function(editor) {
    editor.setContent($('#'+textareaId).text());
  }});
    });
  }, 200);
});