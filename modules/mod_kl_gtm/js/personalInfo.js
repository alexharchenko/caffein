if ($('#checkout-personal-information-step').hasClass('js-current-step'))
	gtmStep2();
else if ($('#checkout-addresses-step').hasClass('js-current-step'))
	gtmStep3();

function gtmStep2() {
	dataLayer.push({
	   "ecommerce": {
	   	 	"currencyCode":gtm_currency_iso,
		    "checkout": {
		       "actionField": {"step":2},
		       "products": gtm_products
		    }
	  },
	  'event': 'gtm-ee-event',
 	  'gtm-ee-event-category': 'Enhanced Ecommerce',
 	  'gtm-ee-event-action': 'Checkout Step 2',
 	  'gtm-ee-event-non-interaction': false,
	});
}

function gtmStep3() {
	dataLayer.push({
	   "ecommerce": {
	   	 	"currencyCode":gtm_currency_iso,
		    "checkout": {
		       "actionField": {"step":3},
		       "products": gtm_products
		    }
	  },
	  'event': 'gtm-ee-event',
 	  'gtm-ee-event-category': 'Enhanced Ecommerce',
 	  'gtm-ee-event-action': 'Checkout Step 3',
 	  'gtm-ee-event-non-interaction': false,
	});
}

function gtmStep4() {
	carrierId = $("#js-delivery input[type='radio']:checked").val();
	carrierId = carrierId.substring(0, carrierId.length - 1);
	carrierName = $("#js-carrier-name-" + carrierId).text();
	dataLayer.push({
	   "ecommerce": {
	   	 	"currencyCode":gtm_currency_iso,
		    "checkout": {
		       'actionField': {'step': 4, 'option': carrierName},
		       "products": gtm_products
		    }
	  },
	  'event': 'gtm-ee-event',
 	  'gtm-ee-event-category': 'Enhanced Ecommerce',
 	  'gtm-ee-event-action': 'Checkout Step 4',
 	  'gtm-ee-event-non-interaction': false,
	});
}

function gtmStep5() {
	id = $("#checkout-payment-step input[type='radio']:checked").attr('id');
	name = $('label[for='+id+']').text();
	dataLayer.push({
	   "ecommerce": {
	   	 	"currencyCode":gtm_currency_iso,
		    "checkout": {
		       "actionField": {"step":5, 'option': name},
		       "products": gtm_products
		    }
	  },
	  'event': 'gtm-ee-event',
 	  'gtm-ee-event-category': 'Enhanced Ecommerce',
 	  'gtm-ee-event-action': 'Checkout Step 5',
 	  'gtm-ee-event-non-interaction': false,
	});
}