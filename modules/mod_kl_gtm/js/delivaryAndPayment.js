if ($('.js_delivery_and_payment').hasClass('active')) {
	if (!$(this).hasClass('active')) {
		if ($('a[href="#newuser"]').hasClass('active'))
			gtmDAP('Новый');
		else
			gtmDAP('Постоянный');
	}
}
$('.js_delivery_and_payment').click(function () {
	if (!$(this).hasClass('active')) {
		if ($('a[href="#newuser"]').hasClass('active'))
			gtmDAP('Новый');
		else
			gtmDAP('Постоянный');
	}
});

$('#payment-confirmation').find('button[type=submit]').click(function () { 
	dataLayer.push({
	    'ecommerce': {
	      'checkout_option': {
	        'actionField': {'step': 3, 'option': $('.payment-option.active').find('.js_payment_name').html()}
	      }
	    },
	    'event': 'gtm-ee-event',
		'gtm-ee-event-category': 'Enhanced Ecommerce',
		'gtm-ee-event-action': 'Payment Type',
		'gtm-ee-event-non-interaction': 'False',
	});
});

function gtmDAP(user) {
  dataLayer.push({
  	'event': 'gtm-ee-event',
 	'gtm-ee-event-category': 'Enhanced Ecommerce',
 	'gtm-ee-event-action': 'Customer Type',
 	'gtm-ee-event-non-interaction': 'False', //отправляется событие, влияющее на показатель отказов: переменная gtm-ee-event-non-interaction принимает значение False.
    'ecommerce': {
      'checkout_option': {
        'actionField': {'step': 2, 'option': user}
      }
    }
  });
    dataLayer.push({
	   "ecommerce": {
	   	 "currencyCode":gtm_currency_iso,
	     "checkout": {
	       "actionField": {"step":3}
	    }
	  },
	  'event': 'gtm-ee-event', //Custom event. Required
	  'gtm-ee-event-category': 'Enhanced Ecommerce',
	  'gtm-ee-event-action': 'Checkout Step 3',
	  'gtm-ee-event-non-interaction': 'False',
	});
}