function EEproductClick (id) {
	$.ajax({
        url: sendAjaxGTM,
        type: "post",
        dataType: "json",
        data: {
            "id_product_gtm": id,
            'controller_gtm': 'product-click'
        },
        success: function(data){
        	responseProductClick(data.gtm_referer, data.gtm_id_product, data.gtm_name, data.gtm_price, data.gtm_manufacturer, data.gtm_category, data.gtm_variant);
        }
    });
}

function EEaddToCart (id, quantity) {
	$.ajax({
        url: sendAjaxGTM,
        type: "post",
        dataType: "json",
        data: {
            "id_product_gtm": id,
            'quantity_gtm': quantity,
            'controller_gtm': 'add-to-cart'
        },
        success: function(data){
        	responseAddToCartClick(data.gtm_id_product, data.gtm_name, data.gtm_price, data.gtm_manufacturer, data.gtm_category, data.gtm_variant, data.gtm_quantity);
        }
    });
}

function EEremoveFromCart (id) {
	quantity = parseInt($('#js-cart-line-product-quantity-' + id).val());
	$.ajax({
        url: sendAjaxGTM,
        type: "post",
        dataType: "json",
        data: {
            "id_product_gtm": id,
            'quantity_gtm': quantity,
            'controller_gtm': 'add-to-cart'
        },
        success: function(data){
        	responseRemoveFromCart(data.gtm_id_product, data.gtm_name, data.gtm_price, data.gtm_manufacturer, data.gtm_category, data.gtm_variant, data.gtm_quantity);
        }
    });
}

function EEtabList (ids, list) {
	$.ajax({
        url: sendAjaxGTM,
        type: "post",
        dataType: "json",
        data: {
            "id_products_gtm": ids,
            'list_gtm': list,
            'controller_gtm': 'tab-list'
        },
        success: function(data){
        	responseTabListClick(data.gtm_products);
        }
    });
}

function responseTabListClick(gtm_products) {
	dataLayer.push({
		   "ecommerce": {
		   	 "currencyCode":gtm_currency_iso,
		     "impressions": gtm_products
		   },
		   'event': 'gtm-ee-event',
		   'gtm-ee-event-category': 'Enhanced Ecommerce',
		   'gtm-ee-event-action': 'Product Impressions',
		   'gtm-ee-event-non-interaction': true,
	});
}

function responseProductClick(gtm_referer, gtm_id_product, gtm_name, gtm_price, gtm_manufacturer, gtm_category, gtm_variant) {
	dataLayer.push({
	  "event":"EEproductClick", //Custom event. Required
	   "ecommerce": {
	     "currencyCode":gtm_currency_iso,
	     "click": {
	       "actionField": {"list":gtm_referer}, 
	       "products": [{
	         "id":gtm_id_product,
	         "name":gtm_name,
	         //'sku': gtm_sku,
	         "price":gtm_price.toString(),
	         "brand":gtm_manufacturer,
	         "category":gtm_category,
	         "variant":gtm_variant //optional, if there is a variant of the product
	      }]
	    }
	  },
	   'event': 'gtm-ee-event', //Custom event. Required
	   'gtm-ee-event-category': 'Enhanced Ecommerce',
	   'gtm-ee-event-action': 'Product Clicks',
	   'gtm-ee-event-non-interaction': false,
	});
}

function responseAddToCartClick(gtm_id_product, gtm_name, gtm_price, gtm_manufacturer, gtm_category, gtm_variant, gtm_quantity) {
	dataLayer.push({
	   "ecommerce": {
	     "currencyCode":gtm_currency_iso,
	     "add": {
	       "products": [{
	         "id":gtm_id_product,
	         "name":gtm_name,
	         "price":gtm_price.toString(),
	         "brand":gtm_manufacturer,
	         "category":gtm_category,
	         "variant":gtm_variant, //optional, if there is a variant of the product
	         "quantity":gtm_quantity
	      }]
	    }
	  },
	  'event': 'gtm-ee-event', //Custom event. Required
 	  'gtm-ee-event-category': 'Enhanced Ecommerce',
 	  'gtm-ee-event-action': 'Adding a Product to a Shopping Cart',
 	  'gtm-ee-event-non-interaction': false,
	});
}

function responseRemoveFromCart(gtm_id_product, gtm_name, gtm_price, gtm_manufacturer, gtm_category, gtm_variant, gtm_quantity) {
	dataLayer.push({
	   "ecommerce": {
	     "currencyCode":gtm_currency_iso,
	     "remove": {
	       "products": [{
	         "id":gtm_id_product,
	         "name":gtm_name,
	         "price":gtm_price.toString(),
	         "brand":gtm_manufacturer,
	         "category":gtm_category,
	         "variant":gtm_variant, //optional, if there is a variant of the product
	         "quantity":gtm_quantity
	      }]
	    }
	  },
	  'event': 'gtm-ee-event', //Custom event. Required
	  'gtm-ee-event-category': 'Enhanced Ecommerce',
	  'gtm-ee-event-action': 'Removing a Product from a Shopping Cart',
	  'gtm-ee-event-non-interaction': false,
	});
}