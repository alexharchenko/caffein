dataLayer.push({
   "ecommerce": {
   	 "currencyCode":gtm_currency_iso,
     "checkout": {
		"actionField": {"step":1},
       	"products": gtm_products
	 }
  	},
  	'event': 'gtm-ee-event', //Custom event. Required
	'gtm-ee-event-category': 'Enhanced Ecommerce',
 	'gtm-ee-event-action': 'Checkout Step 1',
 	'gtm-ee-event-non-interaction': false,
});