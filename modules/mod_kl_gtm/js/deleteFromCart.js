dataLayer.push({
   "ecommerce": {
     "currencyCode":gtm_currency_iso,
     "remove": {
       "products": [{
         "id":gtm_id_product,
         "name":gtm_name,
         "price":gtm_price.toString(),
         "brand":gtm_manufacturer,
         "category":gtm_category,
         "variant":gtm_variant, //optional, if there is a variant of the product
         "quantity":gtm_quantity
      }]
    }
  },
  'event': 'gtm-ee-event', //Custom event. Required
  'gtm-ee-event-category': 'Enhanced Ecommerce',
  'gtm-ee-event-action': 'Removing a Product from a Shopping Cart',
  'gtm-ee-event-non-interaction': 'False',
});