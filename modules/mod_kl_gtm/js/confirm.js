if (document.getElementById('content-hook_order_confirmation') !== null) {
  dataLayer.push({
     "ecommerce": {
     		"currencyCode":gtm_currency_iso,
       	"purchase": {
         		"actionField": {
  		         "id":gtm_id_order, //transaction ID - mandatory. Required
  		         "revenue":gtm_revenue, //total including tax and shipping. Required
  		         "tax":gtm_tax, // Not Required
  		         "shipping":gtm_shipping, // Not Required
  		         "coupon":gtm_coupon //if a coupon code was used for this order
  		    },
         		"products": gtm_products
      	}
    	},
    	'event': 'gtm-ee-event',
   	'gtm-ee-event-category': 'Enhanced Ecommerce',
   	'gtm-ee-event-action': 'Purchase',
   	'gtm-ee-event-non-interaction': false,
  });
}