dataLayer.push({
   	"ecommerce": {
     	"currencyCode":gtm_currency_iso,
     	"detail": {
       		"actionField": {"list":gtm_referer}, 
       		"products": [{
		        "id":gtm_id_product,
		        "name":gtm_name,
		        //'sku': gtm_sku,
		        "price":gtm_price.toString(),
		        "brand":gtm_manufacturer,
		        "category":gtm_category,
		        "variant":gtm_variant //optional, if there is a variant of the product
		    }]
    	},
    	'impressions': gtm_accessories
  	},
  	'event': 'gtm-ee-event',
 	'gtm-ee-event-category': 'Enhanced Ecommerce',
 	'gtm-ee-event-action': 'Product Details',
 	'gtm-ee-event-non-interaction': true,
});
