<?php

if (!defined('_PS_VERSION_'))
    exit;

class mod_kl_gtm extends Module{

    private $templateFile;

    public function __construct() {
        $this->name = 'mod_kl_gtm';
        $this->version = '1.0.2';
        $this->author = 'Klookva Antikov';
        $this->bootstrap = TRUE;

        parent::__construct();

        $this->displayName = $this->l('Klookva GTM');
        $this->description = $this->l('Add google tag manager events on pages');
    }

    public function install()
    {
        return parent::install()
            && $this->registerHook('header')
        ;
    }

    public function uninstall()
    {
        return parent::uninstall();
    }

    public function hookDisplayHeader($params) {

    	Media::addJsDef(array('gtm_currency_iso' => $this->context->currency->iso_code));
		if (Tools::getValue('controller') === 'product') {
			$product = new Product(Tools::getValue('id_product'), true, Configuration::get('PS_LANG_DEFAULT'));
			$category = new Category($product->id_category_default, Configuration::get('PS_LANG_DEFAULT'));
			$combs = $product->getAttributeCombinationsById(Tools::getValue('id_product_attribute'), Configuration::get('PS_LANG_DEFAULT'));
			$variants = '';
			$accessories = $product->getAccessories(Configuration::get('PS_LANG_DEFAULT'));
			foreach ($combs as $comb) {
				$variants .= $comb['group_name']. ' - '. $comb['attribute_name'];
				if (end($combs) !== $comb)
					$variants .=', ';
			}

			$gtm_accessories = [];
			foreach($accessories as $key => $accessorie) {
				if ($accessorie['quantity'] > 0) {
					$product_tmp = new Product($accessorie['id_product'], true, Configuration::get('PS_LANG_DEFAULT'));
					$category_accessorie = new Category($product_tmp->id_category_default, Configuration::get('PS_LANG_DEFAULT'));
					$combs_accessorie = $product_tmp->getAttributeCombinationsById($accessorie['id_product_attribute'], Configuration::get('PS_LANG_DEFAULT'));
					$variants_accessorie = '';
					foreach ($combs_accessorie as $comb) {
						$variants_accessorie .= $comb['group_name']. ' - '. $comb['attribute_name'];
						if (end($combs_accessorie) !== $comb)
							$variants_accessorie .=', ';
					}
					$gtm_accessories[$key]['id'] = $accessorie['id_product'];
					$gtm_accessories[$key]['name'] = $accessorie['name'];
					$gtm_accessories[$key]['price'] = (string)$accessorie['price'];
					$gtm_accessories[$key]['brand'] = $accessorie['manufacturer_name'];
					$gtm_accessories[$key]['category'] = $category_accessorie->name;
					$gtm_accessories[$key]['position'] = $key;
					$gtm_accessories[$key]['list'] = 'Похожие товары';
					$gtm_accessories[$key]['variant'] = $variants_accessorie;
				}
			}
			Media::addJsDef(array('gtm_referer' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Tools::getHttpHost(true) . $_SERVER['REQUEST_URI'] ));
			Media::addJsDef(array('gtm_id_product' => Tools::getValue('id_product')));
			Media::addJsDef(array('gtm_name' => $product->name));
			//Media::addJsDef(array('gtm_sku' => $product->reference));
			Media::addJsDef(array('gtm_price' => (string)$product->price));
			Media::addJsDef(array('gtm_manufacturer' => $product->getWsManufacturerName()));
			Media::addJsDef(array('gtm_category' => $category->name));
			Media::addJsDef(array('gtm_variant' => $variants));
			Media::addJsDef(array('gtm_accessories' => $gtm_accessories));
			$this->context->controller->registerJavascript('modules-gtm-product', 'modules/'.$this->name.'/js/product.js', ['position' => 'bottom', 'priority' => 150]);

		} elseif (Tools::getValue('controller') === 'search') {
		    
	    	$search = Search::find(Configuration::get('PS_LANG_DEFAULT'),Tools::getValue('s'), 1, Configuration::get('PS_PRODUCTS_PER_PAGE'));
	    	$products = $search['result'];
		    $gtm_products = [];
			foreach($products as $key => $product) {
				$product_tmp = new Product($product['id_product'], true, Configuration::get('PS_LANG_DEFAULT'));
				$category = new Category($product_tmp->id_category_default, Configuration::get('PS_LANG_DEFAULT'));
				$combs = $product_tmp->getAttributeCombinationsById($product['id_product_attribute'], Configuration::get('PS_LANG_DEFAULT'));
				$variants = '';
				foreach ($combs as $comb) {
					$variants .= $comb['group_name']. ' - '. $comb['attribute_name'];
					if (end($combs) !== $comb)
						$variants .=', ';
				}
				$gtm_products[$key]['id'] = $product['id_product'];
				$gtm_products[$key]['name'] = $product['name'];
				$gtm_products[$key]['price'] = (string)$product['price'];
				$gtm_products[$key]['brand'] = $product['manufacturer_name'];
				$gtm_products[$key]['category'] = $category->name;
				$gtm_products[$key]['position'] = $key;
				$gtm_products[$key]['variant'] = $variants;
				$gtm_products[$key]['list'] = 'Search page';
			}

    		Media::addJsDef(array('gtm_products' => $gtm_products));
    		$this->context->controller->registerJavascript('modules-gtm-search', 'modules/'.$this->name.'/js/search.js', ['position' => 'bottom', 'priority' => 150]);

		// } elseif (Tools::getValue('controller') === 'cart' && Tools::getValue('delete') !== false) {

		// 	$product = new Product(Tools::getValue('id_product'), true, Configuration::get('PS_LANG_DEFAULT'));
		// 	$category = new Category($product->id_category_default, Configuration::get('PS_LANG_DEFAULT'));
		// 	$combs = $product->getAttributeCombinationsById(Tools::getValue('id_product_attribute'), Configuration::get('PS_LANG_DEFAULT'));
		// 	$variants = '';
		// 	foreach ($combs as $comb) {
		// 		$variants .= $comb['group_name']. ' - '. $comb['attribute_name'];
		// 		if (end($combs) !== $comb)
		// 			$variants .=', ';
		// 	}
		// 	Media::addJsDef(array('gtm_id_product' => Tools::getValue('id_product')));
		// 	Media::addJsDef(array('gtm_name' => $product->name));
		// 	Media::addJsDef(array('gtm_price' => (string)$product->price));
		// 	Media::addJsDef(array('gtm_manufacturer' => $product->getWsManufacturerName()));
		// 	Media::addJsDef(array('gtm_category' => $category->name));
		// 	Media::addJsDef(array('gtm_variant' => $variants));
	 //    	Media::addJsDef(array('gtm_quantity' => intval(Tools::getValue('delete'))));
	 //    	$this->context->controller->registerJavascript('modules-gtm-delete', 'modules/'.$this->name.'/js/deleteFromCart.js', ['position' => 'bottom', 'priority' => 150]);
		    
    	} elseif (Tools::getValue('controller') === 'order' || Tools::getValue('controller') === 'orderconfirmation' || Tools::getValue('controller') === 'cart') {
    		
    		if (Tools::getValue('controller') === 'order' || Tools::getValue('controller') === 'cart')
    			$products = $params['cart']->getProducts();
			elseif (Tools::getValue('controller') === 'orderconfirmation') {
				$temp_cart = new Cart(Tools::getValue('id_cart'));
				$products = $temp_cart->getProducts();
			}

			$gtm_products = [];
			foreach($products as $key => $product) {
				$product_tmp = new Product($product['id_product'], true, Configuration::get('PS_LANG_DEFAULT'));
				$category = new Category($product_tmp->id_category_default, Configuration::get('PS_LANG_DEFAULT'));
				$combs = $product_tmp->getAttributeCombinationsById($product['id_product_attribute'], Configuration::get('PS_LANG_DEFAULT'));
				$variants = '';
				foreach ($combs as $comb) {
					$variants .= $comb['group_name']. ' - '. $comb['attribute_name'];
					if (end($combs) !== $comb)
						$variants .=', ';
				}
				$gtm_products[$key]['id'] = $product['id_product'];
				$gtm_products[$key]['name'] = $product['name'];
				$gtm_products[$key]['price'] = (string)$product['price'];
				$gtm_products[$key]['brand'] = $product['manufacturer_name'];
				$gtm_products[$key]['category'] = $category->name;
				$gtm_products[$key]['position'] = $key;
				$gtm_products[$key]['variant'] = $variants;
				$gtm_products[$key]['quantity'] = $product['quantity'];
			}

    		Media::addJsDef(array('gtm_products' => $gtm_products));
    		if (Tools::getValue('controller') === 'order') {
		        // $this->context->controller->registerJavascript('modules-gtm-dap', 'modules/'.$this->name.'/js/delivaryAndPayment.js', ['position' => 'bottom', 'priority' => 150]);
		        $this->context->controller->registerJavascript('modules-gtm-pi', 'modules/'.$this->name.'/js/personalInfo.js', ['position' => 'bottom', 'priority' => 150]);
		    } elseif (Tools::getValue('controller') === 'orderconfirmation') {

		    	$order = Order::getByCartId(Tools::getValue('id_cart'));

		    	$discounts = $order->getCartRules();
		    	$all_coupon = '';
		    	foreach($discounts as $discount) {
		    		if ($discount === end($discounts))
		    			$all_coupon .= $discount['name'];
		    		else
		    			$all_coupon .= $discount['name'].'|';
		    	}
		        Media::addJsDef(array('gtm_id_order' => $order->reference));
				Media::addJsDef(array('gtm_revenue' => $order->total_paid));
				Media::addJsDef(array('gtm_tax' => $order->total_wrapping_tax_incl));
				Media::addJsDef(array('gtm_shipping' => $order->getShipping()[0]['shipping_cost_tax_excl']));
				Media::addJsDef(array('gtm_coupon' => $all_coupon));
		    	$this->context->controller->registerJavascript('modules-gtm-confirm', 'modules/'.$this->name.'/js/confirm.js', ['position' => 'bottom', 'priority' => 150]);
		    } elseif (Tools::getValue('controller') === 'cart') {
		    	$this->context->controller->registerJavascript('modules-gtm-cart', 'modules/'.$this->name.'/js/cart.js', ['position' => 'bottom', 'priority' => 150]);
		    }
	    }
	    Media::addJsDef(array('sendAjaxGTM' => $this->context->link->getModuleLink('mod_kl_gtm','getAjax')));
	    $this->context->controller->registerJavascript('modules-gtm-controller', 'modules/'.$this->name.'/js/controller.js', ['position' => 'bottom', 'priority' => 150]);
    }

   

}
