<?php

class mod_kl_gtmgetAjaxModuleFrontController extends ModuleFrontController {

    public function initContent() {

        $this->ajax = true;

        parent::initContent();
       	if (Tools::getValue('controller_gtm') === 'product-click') {
           $this->sendProductClick();
       	} elseif (Tools::getValue('controller_gtm') === 'add-to-cart') {
       		$this->sendAddToCart();
       	} elseif (Tools::getValue('controller_gtm') === 'tab-list') {
       		$this->sendTabList();
       	}

    }

    public function sendAddToCart() {

    	if (Tools::getValue('id_product_gtm') === false || Tools::getValue('id_product_gtm') === '') {
    		echo $this->module->l('Error: no product id');
    		return;
    	}

    	$product = new Product(Tools::getValue('id_product_gtm'), true, Configuration::get('PS_LANG_DEFAULT'));
		$category = new Category($product->id_category_default, Configuration::get('PS_LANG_DEFAULT'));
		$combs = $product->getAttributeCombinationsById($product->cache_default_attribute, Configuration::get('PS_LANG_DEFAULT'));
		$variants = '';
		foreach ($combs as $comb) {
			$variants .= $comb['group_name']. ' - '. $comb['attribute_name'];
			if (end($combs) !== $comb)
				$variants .=', ';
		}
		$gtm_id_product = Tools::getValue('id_product_gtm');
		$gtm_name = $product->name;
		$gtm_price = (string)$product->price;
		$gtm_manufacturer = $product->getWsManufacturerName();
		$gtm_category = $category->name;
		$gtm_variant = $variants;
		$gtm_quantity = Tools::getValue('quantity_gtm') ? intval(Tools::getValue('quantity_gtm')) : 1;

		echo json_encode(array('gtm_id_product' => $gtm_id_product, 'gtm_name' => $gtm_name, 'gtm_price' => $gtm_price, 'gtm_manufacturer' => $gtm_manufacturer, 'gtm_category' => $gtm_category, 'gtm_variant' => $gtm_variant, 'gtm_quantity' => $gtm_quantity));

    }

    public function sendTabList() {

    	if (Tools::getValue('id_products_gtm') === false || Tools::getValue('id_products_gtm') === '') {
    		echo $this->module->l('Error: no product id');
    		return;
    	}
    	$products = is_array(Tools::getValue('id_products_gtm')) ? Tools::getValue('id_products_gtm') : json_decode(Tools::getValue('id_products_gtm'));

	    $gtm_products = [];
		foreach($products as $key => $product) {
			$product_tmp = new Product($product, true, Configuration::get('PS_LANG_DEFAULT'));
			$category = new Category($product_tmp->id_category_default, Configuration::get('PS_LANG_DEFAULT'));
			$combs = $product_tmp->getAttributeCombinationsById($product_tmp->cache_default_attribute, Configuration::get('PS_LANG_DEFAULT'));
			$variants = '';
			foreach ($combs as $comb) {
				$variants .= $comb['group_name']. ' - '. $comb['attribute_name'];
				if (end($combs) !== $comb)
					$variants .=', ';
			}
			$gtm_products[$key]['id'] = $product;
			$gtm_products[$key]['name'] = $product_tmp->name;
			$gtm_products[$key]['price'] = (string)$product_tmp->price;
			$gtm_products[$key]['brand'] = $product_tmp->manufacturer_name;
			$gtm_products[$key]['category'] = $category->name;
			$gtm_products[$key]['position'] = $key;
			$gtm_products[$key]['variant'] = $variants;
			$gtm_products[$key]['list'] = Tools::getValue('list_gtm');
		}

		echo json_encode(array('gtm_products' => $gtm_products));

    }

    public function sendProductClick() {

    	if (Tools::getValue('id_product_gtm') === false || Tools::getValue('id_product_gtm') === '') {
    		echo $this->module->l('Error: no product id');
    		return;
    	}
    	$product = new Product(Tools::getValue('id_product_gtm'), true, Configuration::get('PS_LANG_DEFAULT'));
		$category = new Category($product->id_category_default, Configuration::get('PS_LANG_DEFAULT'));
		$combs = $product->getAttributeCombinationsById($product->cache_default_attribute, Configuration::get('PS_LANG_DEFAULT'));
		$variants = '';
		foreach ($combs as $comb) {
			$variants .= $comb['group_name']. ' - '. $comb['attribute_name'];
			if (end($combs) !== $comb)
				$variants .=', ';
		}
		$gtm_referer = $_SERVER['HTTP_REFERER'];
		$gtm_id_product = Tools::getValue('id_product_gtm');
		$gtm_name = $product->name;
		//$gtm_sku = $product->reference;
		$gtm_price = (string)$product->price;
		$gtm_manufacturer = $product->getWsManufacturerName();
		$gtm_category = $category->name;
		$gtm_variant = $variants;

		echo json_encode(array('gtm_referer' => $gtm_referer, 'gtm_id_product' => $gtm_id_product, 'gtm_name' => $gtm_name, 'gtm_price' => $gtm_price, 'gtm_manufacturer' => $gtm_manufacturer, 'gtm_category' => $gtm_category, 'gtm_variant' => $gtm_variant));
    }

}