<?php
/**
 * 2002-2016 TemplateMonster
 *
 * TM Google Map
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 *  @author    TemplateMonster
 *  @copyright 2002-2016 TemplateMonster
 *  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class TMGoogleMapSettingsRepository
{
    private $db;
    private $shop;
    private $language;
    private $db_prefix;
    private $table = 'tmgooglemap_settings';
    public $settingsList = array(
        'TMGOOGLE_API_KEY' => '',
        'TMGOOGLE_STYLE' => 'shift_worker',
        'TMGOOGLE_TYPE' => 'roadmap',
        'TMGOOGLE_ZOOM' => 9,
        'TMGOOGLE_SCROLL' => 0,
        'TMGOOGLE_TYPE_CONTROL' => 0,
        'TMGOOGLE_STREET_VIEW' => 1,
        'TMGOOGLE_ANIMATION' => 0,
        'TMGOOGLE_POPUP' => 1,
    );

    public function __construct(Db $db, Shop $shop, Language $language)
    {
        $this->db = $db;
        $this->shop = $shop;
        $this->language = $language;
        $this->db_prefix = $db->getPrefix();
    }

    public function createTables()
    {
        $engine = _MYSQL_ENGINE_;
        $success = true;
        $this->dropTables();

        $queries = [
            "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}{$this->table}`(
    			`id_setting` int(10) unsigned NOT NULL auto_increment,
    			`hook_name` VARCHAR (100),
    			`id_shop` int(10) unsigned,
    			`setting_name` VARCHAR (100),
    			`value` VARCHAR (100),
    			PRIMARY KEY (`id_setting`, `hook_name`, `id_shop`)
            ) ENGINE=$engine DEFAULT CHARSET=utf8",
            "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}tmgooglemap`(
    			`id_tab` int(11) NOT NULL AUTO_INCREMENT,
                `hook_name` VARCHAR(100) NOT NULL,
                `default` int(11) NOT NULL,
                `id_store` int(11) NOT NULL,
                `id_shop` int(11) NOT NULL,
                `status` int(11) NOT NULL,
                `marker` VARCHAR(100) NOT NULL,
                 PRIMARY KEY  (`id_tab`)
            ) ENGINE=$engine DEFAULT CHARSET=utf8",
            "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}tmgooglemap_lang`(
    			`id_tab` int(11) NOT NULL,
                `id_lang` int(11) NOT NULL,
                `content` text NOT NULL
            ) ENGINE=$engine DEFAULT CHARSET=utf8"
        ];

        foreach ($queries as $query) {
            $success &= $this->db->execute($query);
        }

        return $success;
    }

    public function dropTables()
    {
        $sql = "DROP TABLE IF EXISTS
			`{$this->db_prefix}{$this->table}`,
			`{$this->db_prefix}tmgooglemap`,
			`{$this->db_prefix}tmgooglemap_lang`";

        return Db::getInstance()->execute($sql);
    }

    public function setDefaultSettings($hookName) {
        $result = true;
        foreach ($this->settingsList as $name => $value) {
            $result &= $this->db->insert($this->table, array('hook_name' => $hookName, 'id_shop' => $this->shop->id, 'setting_name' => $name, 'value' => $value));
        }

        return $result;
    }

    public function getSetting($hookName, $settingName)
    {
        return $this->db->getValue(
            "SELECT `value`
            FROM `{$this->db_prefix}{$this->table}`
            WHERE `hook_name` = '".$hookName."'
            AND `setting_name` = '".$settingName."'
            AND `id_shop` = ".$this->shop->id
        );
    }

    public function getSettings($hookName, array $result = [])
    {
        $settings = $this->db->executeS(
            "SELECT `setting_name`, `value`
            FROM `{$this->db_prefix}{$this->table}`
            WHERE `hook_name` = '".$hookName."'
            AND `id_shop` = ".$this->shop->id
        );

        if ($settings) {
            foreach ($settings as $setting) {
                $result[$setting['setting_name']] = $setting['value'];
            }
        }

        return $result;
    }

    public function checkSetting($hookName, $settingName)
    {
        return $this->db->getValue(
            "SELECT `id_setting`
            FROM `{$this->db_prefix}{$this->table}`
            WHERE `hook_name` = '".$hookName."'
            AND `setting_name` = '".$settingName."'
            AND `id_shop` = ".$this->shop->id
        );
    }

    public function insertSetting($hookName, $settingName, $value)
    {
        return $this->db->insert($this->table, array('hook_name' => $hookName, 'id_shop' => $this->shop->id, 'setting_name' => $settingName, 'value' => $value));
    }

    public function updateSetting($hookName, $settingName, $value)
    {
        if ($this->checkSetting($hookName, $settingName)) {
            return $this->db->update(
                $this->table, array('value' => $value),
                '`hook_name` = "'.$hookName.'" AND `setting_name` = "'.$settingName.'"'
            );
        } else {
            return $this->insertSetting($hookName, $settingName, $value);
        }
    }

    public function deleteHookSettings($hookName)
    {
        return $this->db->delete($this->table, '`hook_name` = "'.$hookName.'" AND `id_shop` = '.$this->shop->id);
    }

    public function getStyles()
    {
        return $this->db->executeS(
            "SELECT `value`
            FROM `{$this->db_prefix}{$this->table}`
            WHERE `id_shop` = ".$this->shop->id."
            AND `setting_name` = 'TMGOOGLE_STYLE'"
        );
    }

    /**
     * Get list with store id
     * return bool $result if invalid or false
     */
    public function getStoresListIds()
    {
        return $this->db->executeS("SELECT s.`id_store`, s.`name`
				FROM {$this->db_prefix}store s
				LEFT JOIN {$this->db_prefix}store_shop ss
				ON(s.`id_store` = ss.`id_store`)
				WHERE ss.`id_shop` = ".$this->shop->id);
    }

    /**
     * Get all active store data for list table
     *
     * @return array $result
     */
    public function getTabList($hookName)
    {
        return $this->db->executeS("SELECT tmg.*, s.`name`
            FROM {$this->db_prefix}tmgooglemap tmg
			LEFT JOIN {$this->db_prefix}store s
			ON(tmg.`id_store` = s.`id_store`)
			WHERE tmg.`id_shop` = ".$this->shop->id."
			AND tmg.`hook_name` = '".$hookName."'
			ORDER BY tmg.`id_tab`");
    }

    /**
     * Get all active store data
     *
     * @return array $result
     */
    public function getStoreContactsData($hookName)
    {
        return $this->db->executeS("SELECT tmg.*, sl.`name` , tmgl.`content`
            FROM {$this->db_prefix}tmgooglemap tmg
            LEFT JOIN {$this->db_prefix}store s
            ON(tmg.`id_store` = s.`id_store`)
            LEFT JOIN {$this->db_prefix}store_lang sl
            ON(sl.`id_store` = s.`id_store`)
            LEFT JOIN {$this->db_prefix}tmgooglemap_lang tmgl
            ON(tmg.`id_tab` = tmgl.`id_tab`)
            WHERE tmg.`id_shop` = ".$this->shop->id."
            AND tmg.`hook_name` = '".$hookName."'
            AND tmgl.`id_lang` = ".$this->language->id."
            AND sl.`id_lang` = ".$this->language->id."
            AND tmg.`status` = 1
            AND s.`active` = 1
            ORDER BY tmg.`id_tab`");
    }

    /**
     * Get shop by id store,
     * check if store already exists with same id store
     *
     * @param int $id_store
     * @return bool|array id store info or false
     */

    public function getShopByIdStore($hookName, $id_store)
    {
        return $this->db->executeS("SELECT *
                FROM {$this->db_prefix}tmgooglemap
                WHERE `id_store` = '".pSql($id_store)."'
                AND `hook_name` = '".$hookName."'");
    }
}
