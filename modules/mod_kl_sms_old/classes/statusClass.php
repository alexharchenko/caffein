<?php

class statusClass extends ObjectModel
{
    public $id;

    public $id_status;

    public $id_shop;

    public $is_active;

    public $text;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'kl_sms_status',
        'primary' => 'id_sms',
        'multilang' => true,
        'fields' => array(
            'id_shop' =>            array('type' => self::TYPE_NOTHING, 'validate' => 'isUnsignedId'),
            'id_status' =>          array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
            'is_active' =>          array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
            // Lang fields
            'text' =>               array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString', 'required' => true),
        )
    );

    public static function getSmsByStatusId($id_lang,$id_status)
    {
        return  Db::getInstance()->executeS('
            SELECT r.`id_sms`, r.`id_status`, r.`id_shop`, r.`is_active`, rl.`text`
            FROM `'._DB_PREFIX_.'kl_sms_order` r
            LEFT JOIN `'._DB_PREFIX_.'kl_sms_order_lang` rl ON (r.`id_sms` = rl.`id_sms`)
            WHERE `id_status` = '.(int)$id_status.' AND `id_lang` = '.(int)$id_lang.' '.Shop::addSqlRestrictionOnLang());
    }

    public function copyFromPost()
    {
        /* Classical fields */
        foreach ($_POST as $key => $value) {
            if (array_key_exists($key, $this) and $key != 'id_'.$this->table) {
                $this->{$key} = $value;
            }
        }

        /* Multilingual fields */
        if (sizeof($this->fieldsValidateLang)) {
            $languages = Language::getLanguages(false);
            foreach ($languages as $language) {
                foreach ($this->fieldsValidateLang as $field => $validation) {
                    if (isset($_POST[$field.'_'.(int)($language['id_lang'])])) {
                        $this->{$field}[(int)($language['id_lang'])] = $_POST[$field.'_'.(int)($language['id_lang'])];
                    }
                }
            }
        }
    }
    
}
