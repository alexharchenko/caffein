{if $phone != ''}
	<form action="javascript:void(null);" method="post">
		<label for="textar">{l s='Put your message' mod='mod_kl_sms'}</label>
		<textarea id="textar" class="form-control" name="text_sms" required></textarea>
		<input class="form-control" name="phone" value="{$phone}" hidden style="display:none;">
		<input class="form-control" name="id_order" value="{$id_order}" hidden style="display:none;">
		<button class="btn btn-primary js_submitbutton" type="submit">{l s='Send sms' mod='mod_kl_sms'}</button>
	</form>
{else}
	{l s='Please add phone to customer' mod='mod_kl_sms'}
{/if}
{if !empty($elements)}
	<table class="table">
		<thead>
			<tr class="nodrag nodrop">
				<th class="">
					<span class="title_box">{l s='ID sms' mod='mod_kl_sms'}</span>
				</th>
				<th class="">
					<span class="title_box">{l s='Text' mod='mod_kl_sms'}</span>
				</th>
				<th class="">
					<span class="title_box">{l s='Date' mod='mod_kl_sms'}</span>
				</th>
			</tr>
		</thead>
		<tbody>
			{foreach $elements as $element}
				<tr class=" odd">			
					<td class="pointer" >
						{$element.id_sms}
					</td>
					<td class="pointer" >
						{$element.text}
					</td>
					<td class="pointer" >
						{$element.date}
					</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
{/if}
<script type="text/javascript">
	window.addEventListener('load', function () {
		$('.js_submitbutton').click(function(){
			var form = $(this).parent();
			var msg   = $(this).parent().serialize();
	        $.ajax({
				type: 'POST',
				url: '{$urltosave}',
				data: msg,
				success: function(data) {
					s = data.split("}")[0];
					s += "}";
		    		var result = $.parseJSON(s);

			    	if(result.error == 1)
			    		form.html('<div class="text-center text-danger">'+result.message+'</div>');
			    	else
						form.html('<div class="text-center text-success">'+result.message+'</div>');      	
	          	}
	        });
		});
		
	});
</script>