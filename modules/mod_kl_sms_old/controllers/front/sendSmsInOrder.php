<?php

class mod_kl_smssendSmsInOrderModuleFrontController extends ModuleFrontController {

    public function initContent() {

        $this->ajax = true;

        parent::initContent();

        $this->sendSmsInOrder_method();
    }

    public function sendSmsInOrder_method() {
        
        if(isset($_POST['text_sms']) && isset($_POST['phone']) && isset($_POST['id_order']))
        {
            $this->module->sendSms($_POST['text_sms'],$_POST['phone'], $_POST['id_order']);
        }
    }
}