<?php

if (!defined('_PS_VERSION_'))
    exit;

include_once _PS_MODULE_DIR_.'mod_kl_sms/classes/statusClass.php';
include_once _PS_MODULE_DIR_.'mod_kl_sms/classes/orderClass.php';

class mod_kl_sms extends Module {

    private $templateFile;

    public function __construct() {
        $this->name = 'mod_kl_sms';
        $this->version = '1.0.0';
        $this->author = 'Klookva Antikov';
        $this->bootstrap = TRUE;

        parent::__construct();

        $this->displayName = $this->l('Sms Sender');
        $this->description = $this->l('Send sms after confirm order,tracking number update and status update');
        $this->templateFile = 'module:mod_kl_sms/views/templates/hook/mod_kl_sms.tpl';
    }

    public function install()
    {
        return parent::install()
            && $this->installDB()
            && $this->registerHook('displayAdminOrderTabOrder')
            && $this->registerHook('displayOrderConfirmation')
            && $this->registerHook('actionOrderStatusUpdate')
            && $this->registerHook('actionAdminOrdersTrackingNumberUpdate')
        ;
    }

    public function installDB()
    {
        $return = true;
        $return &= Db::getInstance()->execute('
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'kl_sms_status` (
                `id_sms` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                `id_status` int(10) unsigned DEFAULT NULL,
                `is_active` tinyint(1) unsigned NOT NULL default \'0\',
                `id_shop` int(10) unsigned DEFAULT NULL,
                UNIQUE (`id_status`),
                PRIMARY KEY (`id_sms`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;'
        );

        $return &= Db::getInstance()->execute('
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'kl_sms_status_lang` (
                `id_sms` INT UNSIGNED NOT NULL,
                `id_lang` int(10) unsigned NOT NULL ,
                `text` text NOT NULL,
                PRIMARY KEY (`id_sms`, `id_lang`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;'
        );

        $return &= Db::getInstance()->execute('
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'kl_sms_order` (
                `id_sms` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                `id_order` int(10) unsigned DEFAULT NULL,
                `id_shop` int(10) unsigned DEFAULT NULL,
                `date` datetime NOT NULL,
                PRIMARY KEY (`id_sms`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;'
        );

        $return &= Db::getInstance()->execute('
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'kl_sms_order_lang` (
                `id_sms` INT UNSIGNED NOT NULL,
                `id_lang` int(10) unsigned NOT NULL ,
                `text` text NOT NULL,
                PRIMARY KEY (`id_sms`, `id_lang`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;'
        );

        return $return;
    }

    public function uninstall()
    {
        foreach (Language::getLanguages(false) as $lang) {
            Configuration::deleteByName('ORDER_SMS_KLOOKVA_'.(int)$lang['id_lang']);
            Configuration::deleteByName('TRACKING_SMS_KLOOKVA_'.(int)$lang['id_lang']);
        }
        return 
            $this->uninstallDB() &&
            Configuration::deleteByName('LOGIN_SMS_KLOOKVA') &&
            Configuration::deleteByName('PASSWORD_SMS_KLOOKVA') &&
            Configuration::deleteByName('SENDTOADMIN_SMS_KLOOKVA') &&
            Configuration::deleteByName('ACTIVE_ORDER_SMS_KLOOKVA') &&
            Configuration::deleteByName('ACTIVE_TRACKING_SMS_KLOOKVA') &&
            parent::uninstall();
    }

    public function uninstallDB()
    {
        return Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'kl_sms_status`') 
            && Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'kl_sms_status_lang`') 
            && Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'kl_sms_order`') 
            && Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'kl_sms_order_lang`');
    }

    public function getContent()
    {
        $html = '';
        $id_sms = (int)Tools::getValue('id_sms');
        if (Tools::isSubmit('mod_kl_sms_checkbalance')) {
            $html .= $this->checkbalance();
        }
        if (Tools::isSubmit('savemod_kl_sms_login')) {
            Configuration::updateValue('LOGIN_SMS_KLOOKVA', Tools::getValue('login_sms'));
            Configuration::updateValue('PASSWORD_SMS_KLOOKVA', Tools::getValue('password_sms'));
            Configuration::updateValue('SENDTOADMIN_SMS_KLOOKVA', Tools::getValue('send_to_admin'));  
        }
        if (Tools::isSubmit('savemod_kl_sms_order')) {
            foreach (Language::getLanguages(false) as $lang) {
                Configuration::updateValue('ORDER_SMS_KLOOKVA_'.(int)$lang['id_lang'], Tools::getValue('order_sms_'.(int)$lang['id_lang'], ''));
            }
            Configuration::updateValue('ACTIVE_ORDER_SMS_KLOOKVA', Tools::getValue('active_order'));
        }
        if (Tools::isSubmit('savemod_kl_sms_tracking')) {
            foreach (Language::getLanguages(false) as $lang) {
                Configuration::updateValue('TRACKING_SMS_KLOOKVA_'.(int)$lang['id_lang'], Tools::getValue('tracking_sms_'.(int)$lang['id_lang'], ''));
            }
            Configuration::updateValue('ACTIVE_TRACKING_SMS_KLOOKVA', Tools::getValue('active_tracking'));
        }
        if (Tools::isSubmit('savemod_kl_sms_status')) {
            if ($id_sms = Tools::getValue('id_sms')) {
                $kl_sms = new statusClass((int)$id_sms);
            } else {
                $kl_sms = new statusClass();
            }

            $kl_sms->copyFromPost();
            $kl_sms->id_shop = $this->context->shop->id;
            if ($kl_sms->validateFields(false) && $kl_sms->validateFieldsLang(false)) {
                $kl_sms->save();
                $this->_clearCache('*');
            } else {
                $html .= $this->displayError($this->l('An error occurred while attempting to save.', array(), 'Admin.Notifications.Error'));
            }
        }

        if (Tools::isSubmit('updatemod_kl_sms_status') || Tools::isSubmit('addmod_kl_sms_status')) {
            $helper = $this->initForm_status();
            foreach (Language::getLanguages(false) as $lang) {
                if ($id_sms) {
                    $kl_sms = new statusClass((int)$id_sms);
                    $helper->fields_value['text'][(int)$lang['id_lang']] = $kl_sms->text[(int)$lang['id_lang']];
                } else {
                    $helper->fields_value['text'][(int)$lang['id_lang']] = Tools::getValue('text_'.(int)$lang['id_lang'], '');
                }
            }
            $helper->fields_value['is_active'] = (int)$kl_sms->is_active;
            $helper->fields_value['id_status'] = (int)$kl_sms->id_status;
            return $html.$helper->generateForm($this->fields_form);
        } elseif (Tools::isSubmit('deletemod_kl_sms_status')) {
            $kl_sms = new statusClass((int)$id_sms);
            $kl_sms->delete();
            $this->_clearCache('*');
            Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
        }
        $content = $this->getListContent((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper = $this->initList();
        $helper->listTotal = count($content);
        return $html.$this->initForm_login().$this->initForm_order().$this->initForm_tracking().$helper->generateList($content, $this->fields_list);
    }

    public function checkbalance() {
        require_once(_PS_MODULE_DIR_.'mod_kl_sms/lib/nusoap.php');
        $site_name = $_SERVER['HTTP_HOST'];
        if (preg_match('/www\.([^\s]*)$/', $site_name, $change))
            $site_name = $change[1];
        $ip = gethostbyname($site_name);
        error_reporting(E_ALL ^ E_WARNING);

        $url = "http://klookva.com.ua/test/shasoapbalance.php?wsdl";

        $client = new nusoap_client($url);

        $err = $client->getError();

        if ($err) {
            echo '<p><b>Error: ' . $err . '</b></p>';
        }

        $args = array('login' => Configuration::get('LOGIN_SMS_KLOOKVA'), 'pasw' => sha1(Configuration::get('PASSWORD_SMS_KLOOKVA')), 'ip' => $ip, 'site_name' => $site_name);

        $return = $client->call('CheckBallanceSha', array($args));
        if ($return['login'] != 0) {
            $msg1 = $this->displayConfirmation($this->l(' Баланс: ') . $return['balance'] . $this->l(" смс"));
        } elseif ($return['login'] == false) {
            $msg1 = $this->displayError($this->l('Неверный логин или пароль!'));
        } else {
            $msg1 = $this->displayError($this->l('Ошибка авторизации!'));
        }
        return $msg1;
    }

    protected function initList()
    {
        $this->fields_list = array(
            'id_status' => array(
                'title' => $this->l('ID status'),
                'width' => 50,
                'type' => 'text',
                'name' => 'id_status',
                'search' => false,
                'orderby' => false
            ),
            'text' => array(
                    'type' => 'text',
                    'title' => $this->l('Text'),
                    'name' => 'text',
                    'lang' => true,
                    'name' => 'text',
            ),
            'is_active' => array(
                'title' => $this->l('Send sms'),
                'width' => 50,
                'active' => 'is_active',
                'search' => true,
                'orderby' => false,
            ),
        );

        if (Shop::isFeatureActive()) {
            $this->fields_list['id_shop'] = array(
                'title' => $this->l('ID Shop'),
                'align' => 'center',
                'width' => 25,
                'type' => 'int'
            );
        }

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = false;
        $helper->identifier = 'id_sms';
        $helper->actions = array('edit', 'delete');
        $helper->show_toolbar = true;
        $helper->imageType = 'png';
        $helper->toolbar_btn['new'] =  array(
            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&add'.$this->name.'_status&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Add new', array(), 'Admin.Actions')
        );

        $helper->title = $this->displayName;
        $helper->table = 'mod_kl_sms_status';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        return $helper;
    }

    protected function initForm_status()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $this->fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('New status update text'),
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Status'),
                    'name' => 'id_status',
                    'class' => 'input-lg',
                    'options' => array(
                        'query' => OrderState::getOrderStates($this->context->language->id),
                        'id' => 'id_order_state',
                        'name' => 'name'
                    )
                ),
                array(
                    'type'      => 'switch',                              
                    'label'     => $this->l('Send sms after changed this status?'),        
                    'name'      => 'is_active',                            
                    'required'  => true,                                
                    'class'     => 't',                                   
                    'is_bool'   => true,                                                                                     
                    'values'    => array(                                 
                        array(
                            'id'    => 'on',                           
                            'value' => 1,                                       
                            'label' => $this->l('Yes')     
                        ),
                        array(
                            'id'    => 'off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    ),
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('text'),
                    'lang' => true,
                    'required'  => true,
                    'name' => 'text',
                    'cols' => 40,
                    'rows' => 10,
                )
            ),
            'submit' => array(
                'title' => $this->l('Save', array(), 'Admin.Actions'),
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = 'mod_kl_sms_status';
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        foreach (Language::getLanguages(false) as $lang) {
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );
        }

        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'savemod_kl_sms_status';
        $helper->toolbar_btn =  array(
            'save' =>
            array(
                'desc' => $this->l('Save', array(), 'Admin.Actions'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
            ),
            'back' =>
            array(
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list', array(), 'Admin.Actions'),
            )
        );
        return $helper;
    }

    protected function initForm_login()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $this->fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Configuration'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Login'),
                    'name' => 'login_sms',
                    'required'  => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Password'),
                    'name' => 'password_sms',
                    'required'  => true,
                ),
                array(
                    'type'      => 'switch',                              
                    'label'     => $this->l('Send sms to admin?'),        
                    'name'      => 'send_to_admin',                            
                    'required'  => true,                                
                    'class'     => 't',                                   
                    'is_bool'   => true,                                                                                     
                    'values'    => array(                                 
                        array(
                            'id'    => 'on',                           
                            'value' => 1,                                       
                            'label' => $this->l('Yes')     
                        ),
                        array(
                            'id'    => 'off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    ),
                ),
            ),
            'buttons' => array(
                    'check-balance' => array(
                    'title' => $this->l('Check Balance'),
                    'name' => $this->name.'_checkbalance',
                    'type' => 'submit',
                    'class' => 'btn btn-default pull-left',
                    'icon' => 'icon-check',
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save', array(), 'Admin.Actions'),
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = 'mod_kl_sms';
        $helper->identifier = $this->identifier;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->toolbar_scroll = true;
        foreach (Language::getLanguages(false) as $lang) {
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );
        }
        $helper->title = $this->displayName;
        $helper->submit_action = 'savemod_kl_sms_login';
        $helper->fields_value['login_sms'] = Configuration::get('LOGIN_SMS_KLOOKVA');
        $helper->fields_value['password_sms'] = Configuration::get('PASSWORD_SMS_KLOOKVA');
        $helper->fields_value['send_to_admin'] = Configuration::get('SENDTOADMIN_SMS_KLOOKVA');
        return $helper->generateForm($this->fields_form);
    }

    protected function initForm_order()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $this->fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Confirm order'),
            ),
            'input' => array(
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Text'),
                    'lang' => true,
                    'name' => 'order_sms',
                    'required'  => true,
                    'cols' => 40,
                    'rows' => 10,
                ),
                array(
                    'type'      => 'switch',                              
                    'label'     => $this->l('Send sms after customer confirm order?'),        
                    'name'      => 'active_order',                            
                    'required'  => true,                                
                    'class'     => 't',                                   
                    'is_bool'   => true,                                                                                     
                    'values'    => array(                                 
                        array(
                            'id'    => 'on',                           
                            'value' => 1,                                       
                            'label' => $this->l('Yes')     
                        ),
                        array(
                            'id'    => 'off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    ),
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save', array(), 'Admin.Actions'),
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = 'mod_kl_sms';
        $helper->identifier = $this->identifier;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        foreach (Language::getLanguages(false) as $lang) {
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );
        }
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'savemod_kl_sms_order';
        foreach (Language::getLanguages(false) as $lang) {
            $helper->fields_value['order_sms'][(int)$lang['id_lang']] = Configuration::get('ORDER_SMS_KLOOKVA_'.(int)$lang['id_lang']);
        }
        $helper->fields_value['active_order'] = Configuration::get('ACTIVE_ORDER_SMS_KLOOKVA');
        return $helper->generateForm($this->fields_form);
    }

    protected function initForm_tracking()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $this->fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Add tracking number'),
            ),
            'input' => array(
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Text'),
                    'cols' => 40,
                    'rows' => 10,
                    'lang' => true,
                    'name' => 'tracking_sms',
                    'required'  => true,
                    'desc' => $this->l('Input %tracking% for show your tracking number'),
                ),
                array(
                    'type'      => 'switch',                              
                    'label'     => $this->l('Send sms after add tracking number?'),        
                    'name'      => 'active_tracking',                            
                    'required'  => true,                                
                    'class'     => 't',                                   
                    'is_bool'   => true,                                                                                     
                    'values'    => array(                                 
                        array(
                            'id'    => 'on',                           
                            'value' => 1,                                       
                            'label' => $this->l('Yes')     
                        ),
                        array(
                            'id'    => 'off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    ),
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save', array(), 'Admin.Actions'),
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = 'mod_kl_sms';
        $helper->identifier = $this->identifier;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        foreach (Language::getLanguages(false) as $lang) {
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );
        }
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'savemod_kl_sms_tracking';
        foreach (Language::getLanguages(false) as $lang) {
            $helper->fields_value['tracking_sms'][(int)$lang['id_lang']] = Configuration::get('TRACKING_SMS_KLOOKVA_'.(int)$lang['id_lang']);
        }
        $helper->fields_value['active_tracking'] = Configuration::get('ACTIVE_TRACKING_SMS_KLOOKVA');
        return $helper->generateForm($this->fields_form);
    }

    protected function getListContent($id_lang)
    {
        return  Db::getInstance()->executeS('
            SELECT r.`id_sms`, r.`id_status`, r.`id_shop`, r.`is_active`, rl.`text`
            FROM `'._DB_PREFIX_.'kl_sms_status` r
            LEFT JOIN `'._DB_PREFIX_.'kl_sms_status_lang` rl ON (r.`id_sms` = rl.`id_sms`)
            WHERE `id_lang` = '.(int)$id_lang.' '.Shop::addSqlRestrictionOnLang());
    }

    public function hookDisplayOrderConfirmation ($params) {
        if (Configuration::get('ACTIVE_ORDER_SMS_KLOOKVA') == 1) {
            if (Configuration::get('SENDTOADMIN_SMS_KLOOKVA') == 1)
                $this->sendSms(Configuration::get('ORDER_SMS_KLOOKVA_'.$this->context->language->id),Configuration::get('PS_SHOP_PHONE').','.$this->getPhoneByIdCustomer($params['order']->id_customer),$params['order']->id);
            else
                $this->sendSms(Configuration::get('ORDER_SMS_KLOOKVA_'.$this->context->language->id),$this->getPhoneByIdCustomer($params['order']->id_customer),$params['order']->id);
        }
    }

    public function hookActionOrderStatusUpdate ($params) {
        $kl_sms = statusClass::getSmsByStatusId($this->context->language->id,$params['id_order_state']);
        if (isset($kl_sms['id_status']) && $kl_sms->is_active == 1) {
            if (Configuration::get('SENDTOADMIN_SMS_KLOOKVA') == 1)
                $this->sendSms($kl_sms->text,Configuration::get('PS_SHOP_PHONE').','.$this->getPhoneByIdCustomer($params['order']->id_customer),$params['order']->id);
            else
                $this->sendSms($kl_sms->text,$this->getPhoneByIdCustomer($params['order']->id_customer),$params['order']->id);
        }
    }

    public function hookActionAdminOrdersTrackingNumberUpdate ($params) {
        $text = str_replace("%tracking%", $params['order']->shipping_number, Configuration::get('TRACKING_SMS_KLOOKVA_'.$this->context->language->id));

        if (Configuration::get('ACTIVE_TRACKING_SMS_KLOOKVA') == 1){ 
            if (Configuration::get('SENDTOADMIN_SMS_KLOOKVA') == 1)
                $this->sendSms($text,Configuration::get('PS_SHOP_PHONE').','.$this->getPhoneByIdCustomer($params['order']->id_customer),$params['order']->id);
            else
                $this->sendSms($text,$this->getPhoneByIdCustomer($params['order']->id_customer),$params['order']->id);
        }
    }

    public function getPhoneByIdCustomer ($id_customer) {
        $customer = new Customer($id_customer);
        $addresses = $customer->getAddresses($this->context->language->id);
        $phone = '';
        foreach ($addresses as $address)
        {
            if($phone == ''){
                if ($address['phone_mobile'] != '')
                    $phone = $address['phone_mobile'];
                elseif ($address['phone'] != '')
                    $phone = $address['phone'];
            } else
                break;
        }
        return $phone;
    }

    public function hookDisplayAdminOrderTabOrder ($params) {
        $this->context->smarty->assign('urltosave', $this->context->link->getModuleLink('mod_kl_sms','sendSmsInOrder'));
        $this->context->smarty->assign('phone', $this->getPhoneByIdCustomer($params['order']->id_customer));
        $this->context->smarty->assign('id_order', $params['order']->id);
        $this->context->smarty->assign('elements', orderClass::getSmsByOrderId($this->context->language->id, $params['order']->id));
        return $this->display(__FILE__,'views/templates/hook/mod_kl_sms.tpl');
    }

    public function SaveRecord($sms,$id_order){
        $sms_order = new orderClass();
        $sms_order->id_order = $id_order;
        $sms_order->id_shop = $this->context->shop->id;
        $sms_order->date = date("Y-m-d H:i:s");
        $sms_order->text[$this->context->language->id] = $sms;
        $sms_order->save();
    }

    public function sendSms($sms,$phones, $id_order=0) {
        require_once(_PS_MODULE_DIR_.'mod_kl_sms/lib/nusoap.php');
        $url = "http://klookva.com.ua/test/shasoapserver.php?wsdl";
        $client = new nusoap_client($url);

        $err = $client->getError();

        if ($err) {
            echo '<p><b>Error: ' . $err . '</b></p>';
        }
        $phones = str_replace(array(' ','-','_','(',')','.'),'',$phones);
        $site_name = $_SERVER['HTTP_HOST'];
        if (preg_match('/www\.([^\s]*)$/', $site_name, $change))
            $site_name = $change[1];
        $ip = gethostbyname($site_name);
        $tmp_phones = explode(',', $phones);
        $i = 0;
        $new_phones = array();
        while ($i < sizeof($tmp_phones)) {
            $string = $tmp_phones[$i];
            $pattern = '/([\s])/';
            $replacement = '';
            $repl_phone=preg_replace($pattern, $replacement, $string);
            if (preg_match('/^\+38[0-9]{10}$/', $tmp_phones[$i]) == TRUE)
                array_push($new_phones, $tmp_phones[$i]);
            elseif (preg_match('/^38[0-9]{10}$/', $tmp_phones[$i]) == TRUE)
                array_push($new_phones, '+' . $tmp_phones[$i]);
            elseif (preg_match('/^8[0-9]{10}$/', $tmp_phones[$i]) == TRUE)
                array_push($new_phones, '+3' . $tmp_phones[$i]);
            elseif (preg_match('/^[0-9]{10}$/', $tmp_phones[$i]) == TRUE)
                array_push($new_phones, '+38' . $tmp_phones[$i]);
            $i++;
        }  
        if(!empty($new_phones)){
            $args = array('login' => Configuration::get('LOGIN_SMS_KLOOKVA'), 'pass' => sha1(Configuration::get('PASSWORD_SMS_KLOOKVA')), 'smstext' => $sms, 'phones' => implode(',', $new_phones), 'ip' => $ip, 'site_name' => $site_name, 'title' => Configuration::get('PS_SHOP_NAME'));
            $return = $client->call('SendMessege', array($args));
            if ($return['sent'] == '1') {
                $this->SaveRecord($sms, $id_order);
                $result = array('error' => 0, 'message' => $this->l('Sms send successfull', $this->context->language->id));
            } else {
                $result = array('error' => 1, 'message' => $this->l('Error', $this->context->language->id));
            } 
        } else {
            $result = array('error' => 1, 'message' => $this->l('Error wrong number', $this->context->language->id));
        }
        echo json_encode($result);
    }
}