{*
* 2002-2017 TemplateMonster
*
* TM Products Slider
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
* @author     TemplateMonster
* @copyright  2002-2017 TemplateMonster
* @license    http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{*-------Information list-------*}

{$settings.online_only = false}                 {*display "Online only" in slide info*}
{$settings.reference = false}                   {*display "Reference name" in slide info*}
{$settings.new_sale_labels = false}             {*display "New Sale labels" in slide info*}
{$settings.condition = false}                   {*display "Condition" in slide info*}
{$settings.product_name = true}                 {*display "Product name" in slide info*}
{$settings.description_short = false}            {*display "Description short" in slide info*}
{$settings.description = false}                 {*display "Description" in slide info*}
{$settings.manufacturer = false}                 {*display "Manufacturer" in slide info*}
{$settings.supplier = false}                     {*display "Supplier" in slide info*}
{$settings.features = false}                    {*display "Features" in slide info*}
{$settings.prices = true}                       {*display "Prices" in slide info*}
{$settings.quantity = false}                    {*display "Quantity" in slide info*}
{$settings.cart_button = true}                  {*display "Add to cart button" in slide info*}
{$settings.more_button = true}                  {*display "Read more button" in slide info*}

{*-------and Information list------*}

{if isset($slides) && $slides}
<div class="container">
    <div id="tm-products-slider" class="grid">
        <div class="slider_container" id="slider_container_3" style="width: {$settings.grid_slider_width|escape:'htmlall':'UTF-8'}px;">
            <div u="slides" class="main-slides" style="width: {$settings.grid_slider_width|escape:'htmlall':'UTF-8'/2}px; height: {$settings.grid_slider_height|escape:'htmlall':'UTF-8'}px;">
                {foreach from=$slides item=slide}
                    <div class="slide-inner">
                        <div u="thumb">
                            {if isset($slide.info_array.images) && $slide.info_array.images}
                                <img class="img-fluid" src="{$slide.info_array.cover.bySize.medium_default.url|escape:'htmlall':'UTF-8'}" alt="{$slide.info_array.name|escape:'htmlall':'UTF-8'}" />
                            {else}
                                <div class="thumb-info without-image">
                                    <p class="thumb-name">{$slide.info_array.name|escape:'htmlall':'UTF-8'}</p>
                                    {if $slide.info_array.price && $slide.info_array.show_price && $settings.prices && !isset($restricted_country_mode)}
                                        <span class="thumb-price">{$slide.info_array.price|escape:'htmlall':'UTF-8'}</span>
                                    {/if}
                                </div>
                            {/if}
                        </div>
                        <div class="slide-image col-xs-6{if !$slide.info_array.images} without-slide-image{/if}">
                            <div class="slide-image-wrap">
                                {if isset($slide.info_array.images) && $slide.info_array.images}
                                    {if $slide.info_array.new && $settings.new_sale_labels}
                                        <span class="new-box">
                                            <span class="new-label">{l s='New' mod='tmproductsslider'}</span>
                                        </span>
                                    {/if}
                                    {if $slide.info_array.on_sale && $settings.new_sale_labels}
                                        <span class="sale-box no-print">
                                            <span class="sale-label">{l s='Sale!' mod='tmproductsslider'}</span>
                                        </span>
                                    {/if}
                                    {if ($settings.grid_extended_settings && $settings.grid_images_gallery)}
                                        {if isset($slide.info_array.images) && $slide.info_array.images}
                                            <div id="inner-slider-{$slide.info_array.id_product|escape:'htmlall':'UTF-8'}" class="sliders-inner" style="width: {$settings.grid_slider_width|escape:'htmlall':'UTF-8'/2}px">
                                                <div u="slides" class="inner-slides" style="width: {$settings.grid_slider_width|escape:'htmlall':'UTF-8'/2}px; height: {$settings.grid_slider_height|escape:'htmlall':'UTF-8'}px;">
                                                    {foreach from=$slide.images item=img}
                                                        <div>
                                                            <img u="image" class="img-fluid" src="{$link->getImageLink($slide.info_array.name, $img.id_image, 'large_default')|escape:'htmlall':'UTF-8'}" alt="{$slide.info_array.name|escape:'htmlall':'UTF-8'}" />
                                                            <img u="thumb" src="{$link->getImageLink($slide.info_array.name, $img.id_image, 'small_default')|escape:'htmlall':'UTF-8'}" alt="{$slide.info_array.name|escape:'htmlall':'UTF-8'}" />
                                                        </div>
                                                    {/foreach}
                                                </div>
                                                <div u="thumbnavigator" class="inner-thumbnail-buttons">
                                                    <div u="slides">
                                                        <div u="prototype" class="p">
                                                            <div class=w><div u="thumbnailtemplate" class="t"></div></div>
                                                            <div class=c></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        {/if}
                                    {else}
                                        <a class="slide-image" href="{$slide.info_array.url|escape:'htmlall':'UTF-8'}" title="{$slide.info_array.name|escape:'htmlall':'UTF-8'}">
                                            <img class="img-fluid" src="{$slide.info_array.cover.bySize.large_default.url|escape:'htmlall':'UTF-8'}" alt="{$slide.info_array.name|escape:'htmlall':'UTF-8'}" />
                                        </a>
                                    {/if}
                                {/if}
                                <div class="slide-info">
                                    {if $slide.info_array.online_only && $settings.online_only}
                                        <p class="online_only">{l s='Online only' mod='tmproductsslider'}</p>
                                    {/if}
                                    {if (!empty($slide.info_array.reference) || $slide.info_array.reference) && $settings.reference}
                                        <p id="product_reference">
                                            <label>{l s='Reference:' mod='tmproductsslider'} </label>
                                            <span class="editable" {if !empty($slide.info_array.reference) && $slide.info_array.reference} content="{$slide.info_array.reference|escape:'htmlall':'UTF-8'}"{/if}>{if !isset($groups)}{$slide.info_array.reference|escape:'htmlall':'UTF-8'}{/if}</span>
                                        </p>
                                    {/if}
                                    {if !$slide.info_array.is_virtual && $slide.info_array.embedded_attributes.condition && $settings.condition}
                                        <p id="product_condition">
                                            <label>{l s='Condition:' mod='tmproductsslider'} </label>
                                            {if $slide.info_array.embedded_attributes.condition == 'new'}
                                                <span class="editable">{l s='New product' mod='tmproductsslider'}</span>
                                            {elseif $slide.info_array.embedded_attributes.condition == 'used'}
                                                <span class="editable">{l s='Used' mod='tmproductsslider'}</span>
                                            {elseif $slide.info_array.embedded_attributes.condition == 'refurbished'}
                                                <span class="editable">{l s='Refurbished' mod='tmproductsslider'}</span>
                                            {/if}
                                        </p>
                                    {/if}
                                    {if $settings.product_name}<h3><a href="{$slide.info_array.url|escape:'htmlall':'UTF-8'}">{$slide.info_array.name|escape:'htmlall':'UTF-8'}</a></h3>{/if}
                                    {if $slide.info_array.description_short && $settings.description_short}
                                        <p class="slide-description des-short">{$slide.info_array.description_short|strip_tags:true|truncate:130:'...':false|escape:'htmlall':'UTF-8'}</p>
                                    {/if}
                                    {if $slide.info_array.description && $settings.description}
                                        <p class="slide-description">{$slide.info_array.description|strip_tags:true|truncate:230:'...':false|escape:'htmlall':'UTF-8'}</p>
                                    {/if}
                                    {if $slide.info_array.manufacturer_name && $settings.manufacturer}
                                        <div class="slide-manufacturer">
                                            <span>{l s='Brand:' mod='tmproductsslider'}</span>
                                            {$slide.info_array.manufacturer_name|escape:'htmlall':'UTF-8'}
                                        </div>
                                    {/if}
                                    {if isset($slide.info_array.features) && $slide.info_array.features && $settings.features}
                                        <div class="product-features">
                                            {foreach from=$slide.info_array.features item=feature}
                                                <div><span>{$feature.name|escape:'htmlall':'UTF-8'}:</span> {$feature.value|escape:'htmlall':'UTF-8'}</div>
                                            {/foreach}
                                        </div>
                                    {/if}
                                    {if $slide.info_array.price && $slide.info_array.show_price && $settings.prices && !isset($restricted_country_mode)}
                                        <div class="product-price">
                                            {block name='product_price_and_shipping'}
                                                <div class="product-price-and-shipping">
                                                    {if $slide.info_array.has_discount}
                                                        {hook h='displayProductPriceBlock' product=$slide.info_array type="old_price"}
                                                        <span class="product-price product-price-old">{$slide.info_array.regular_price}</span>
                                                        {if $slide.info_array.discount_type === 'percentage'}
                                                            <span class="product-price product-price-reduction">{$slide.info_array.discount_percentage}</span>
                                                        {/if}
                                                    {/if}
                                                    {hook h='displayProductPriceBlock' product=$slide.info_array type="before_price"}
                                                    <span class="product-price">{$slide.info_array.price}</span>
                                                    {hook h='displayProductPriceBlock' product=$slide.info_array type='unit_price'}
                                                    {hook h='displayProductPriceBlock' product=$slide.info_array type='weight'}
                                                </div>
                                            {/block}
                                            {if ($slide.info_array.embedded_attributes.available_for_order && !$slide.info_array.embedded_attributes.quantity_all_versions <= 0 && $settings.quantity)}
                                                <!-- number of item in stock -->
                                                <p id="product-quantity">
                                                    <span>{$slide.info_array.embedded_attributes.quantity_all_versions|intval}</span>
                                                    {if $slide.info_array.embedded_attributes.quantity_all_versions == 1}
                                                        <span>{l s='Item' mod='tmproductsslider'}</span>
                                                    {else}
                                                        <span>{l s='Items' mod='tmproductsslider'}</span>
                                                    {/if}
                                                </p>
                                            {/if}
                                        </div>
                                    {/if}
                                    {if $settings.more_button || $settings.cart_button}
                                        <div class="buttons-container">
                                            {if $slide.info_array.embedded_attributes.available_for_order && !isset($restricted_country_mode) && $settings.cart_button}
                                                {if (!isset($slide.info_array.embedded_attributes.customization_required) || !$slide.info_array.embedded_attributes.customization_required) && $slide.info_array.embedded_attributes.quantity_all_versions > 0}
                                                    <a class="ajax_add_to_cart_button btn btn-md btn-default cart-button" href="{$link->getPageLink('cart', true, NULL, "qty=1&amp;id_product={$slide.info_array.id_product|intval}&amp;token={$static_token}&amp;add", false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart' mod='tmproductsslider'}" data-id-product="{$slide.info_array.id_product|intval}" data-minimal_quantity="{$slide.info_array.embedded_attributes.minimal_quantity|intval}">
                                                        <span>{l s='Add to cart' mod='tmproductsslider'}</span>
                                                    </a>
                                                {else}
                                                    <a href="{$slide.info_array.url|escape:'htmlall':'UTF-8'}" class="btn btn-md btn-default cart-button">
                                                        <span>{l s='Add to cart' mod='tmproductsslider'}</span>
                                                    </a>
                                                {/if}
                                            {/if}
                                            {if $settings.more_button}
                                                <a href="{$slide.info_array.url|escape:'htmlall':'UTF-8'}" class="btn lnk_view btn btn-default">
                                                    <span>{l s='Read More' mod='tmproductsslider'}</span>
                                                </a>
                                            {/if}
                                        </div>
                                    {/if}
                                </div>
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
            {if $settings.grid_extended_settings && $settings.grid_slider_navigation}
                <span u="arrowleft" class="prev-btn"></span>
                <span u="arrowright" class="next-btn"></span>
            {/if}
            {if $settings.grid_extended_settings && $settings.grid_slider_pagination}
                <div u="navigator" class="pagers">
                    <div u="prototype"><div u="numbertemplate"></div></div>
                </div>
            {/if}
            {if ($settings.grid_extended_settings && $settings.grid_slider_thumbnails) || !$settings.grid_extended_settings}
                <div u="thumbnavigator" class="thumbnail-buttons">
                    <div u="slides">
                        <div u="prototype" class="p">
                            <div class=w><div u="thumbnailtemplate" class="t"></div></div>
                            <div class=c></div>
                        </div>
                    </div>
                </div>
            {/if}
        </div>
    </div>
</div>
{/if}

