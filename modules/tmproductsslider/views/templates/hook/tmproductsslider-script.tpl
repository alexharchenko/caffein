{*
* 2002-2017 TemplateMonster
*
* TM Products Slider
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
* @author     TemplateMonster
* @copyright  2002-2017 TemplateMonster
* @license    http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{if $settings.slider_type == standard}
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            {literal}
                var jssor_1_SlideoTransitions = [
                    [{b:0,d:600,y:-290,e:{y:27}}],
                    [{b:0,d:1000,y:185},{b:1000,d:500,o:-1},{b:1500,d:500,o:1},{b:2000,d:1500,r:360},{b:3500,d:1000,rX:30},{b:4500,d:500,rX:-30},{b:5000,d:1000,rY:30},{b:6000,d:500,rY:-30},{b:6500,d:500,sX:1},{b:7000,d:500,sX:-1},{b:7500,d:500,sY:1},{b:8000,d:500,sY:-1},{b:8500,d:500,kX:30},{b:9000,d:500,kX:-30},{b:9500,d:500,kY:30},{b:10000,d:500,kY:-30},{b:10500,d:500,c:{x:87.50,t:-87.50}},{b:11000,d:500,c:{x:-87.50,t:87.50}}],
                    [{b:0,d:600,x:410,e:{x:27}}],
                    [{b:-1,d:1,o:-1},{b:0,d:600,o:1,e:{o:5}}],
                    [{b:-1,d:1,c:{x:175.0,t:-175.0}},{b:0,d:800,c:{x:-175.0,t:175.0},e:{c:{x:7,t:7}}}],
                    [{b:-1,d:1,o:-1},{b:0,d:600,x:-570,o:1,e:{x:6}}],
                    [{b:-1,d:1,o:-1,r:-180},{b:0,d:800,o:1,r:180,e:{r:7}}],
                    [{b:0,d:1000,y:80,e:{y:24}},{b:1000,d:1100,x:570,y:170,o:-1,r:30,sX:9,sY:9,e:{x:2,y:6,r:1,sX:5,sY:5}}],
                    [{b:2000,d:600,rY:30}],
                    [{b:0,d:500,x:-105},{b:500,d:500,x:230},{b:1000,d:500,y:-120},{b:1500,d:500,x:-70,y:120},{b:2600,d:500,y:-80},{b:3100,d:900,y:160,e:{y:24}}],
                    [{b:0,d:1000,o:-0.4,rX:2,rY:1},{b:1000,d:1000,rY:1},{b:2000,d:1000,rX:-1},{b:3000,d:1000,rY:-1},{b:4000,d:1000,o:0.4,rX:-1,rY:-1}]
                ];
            {/literal}
            var options = {
                $AutoPlay: {if ($settings.standard_extended_settings && $settings.standard_slider_autoplay) || !$settings.standard_extended_settings}true{else}false{/if},                //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlaySteps: 1,                                              //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
                $AutoPlayInterval: {$settings.standard_slider_interval|escape:'htmlall':'UTF-8'},        //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1
                $CaptionSliderOptions: {
                    $Class: $JssorCaptionSlideo$,
                    $Transitions: jssor_1_SlideoTransitions,
                    $Breaks: {literal}[
                        [{d:2000,b:1000}]
                    ]{/literal}
                },
                $Loop: {if ($settings.standard_extended_settings && $settings.standard_slider_loop) || !$settings.standard_extended_settings}true{else}false{/if},

                $ArrowKeyNavigation: true,   			                        //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideEasing: $JssorEasing$.$EaseOutQuint,                      //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
                $SlideDuration: {$settings.standard_slider_duration|escape:'htmlall':'UTF-8'},           //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                                      //[Optional] Minimum drag offset to trigger slide , default value is 20
                $SlideWidth: {$settings.standard_slider_width|escape:'htmlall':'UTF-8'},                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                $SlideSpacing: 0, 					                            //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 1,                                              //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                            //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 0,                                               //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                            //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation: 3,                                            //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

                $ArrowNavigatorOptions: {                                       //[Optional] Options to specify and enable arrow navigator or not
                    $Class: $JssorArrowNavigator$,                              //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 2,                                           //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $Steps: 1                                                   //[Optional] Steps to go for each navigation request, default value is 1
                },

                $ThumbnailNavigatorOptions: {                                   //[Optional] Options to specify and enable thumbnail navigator or not
                    $Class: $JssorThumbnailNavigator$,                          //[Required] Class to create thumbnail navigator instance
                    $ChanceToShow: 2,                                           //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $ActionMode: 1,                                             //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
                    $SpacingX: 30,                                              //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
                    $Cols: 3,                                                   //[Optional] Number of slides to display in the 'slides' container (slideshow would be disabled if the value is greater than 1)
                    $ParkingPosition: 0,                                        //[Optional] The offset position to park thumbnail
                    $Orientation: 1,
                    $Loop: {if ($settings.standard_extended_settings && $settings.standard_slider_loop) || !$settings.standard_extended_settings}true{else}false{/if},
                    $AutoCenter: 0
                },

                $BulletNavigatorOptions: {                                      //[Optional] Options to specify and enable navigator or not
                    $Class: $JssorBulletNavigator$,                             //[Required] Class to create navigator instance
                    $ChanceToShow: 2,                                           //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 0,                                             //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1,                                                  //[Optional] Steps to go for each navigation request, default value is 1
                    $Lanes: 1,                                                  //[Optional] Specify lanes to arrange items, default value is 1
                    $SpacingX: 5,                                               //[Optional] Horizontal space between each item in pixel, default value is 0
                    $SpacingY: 0,                                               //[Optional] Vertical space between each item in pixel, default value is 0
                    $Orientation: 1                                             //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                }
            };

            var nestedSliders = [];

            var nestedSlidersOptions = {
                $AutoPlay: false,                                               //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false

                $ArrowKeyNavigation: true,   			                              //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideEasing: $JssorEasing$.$EaseOutQuint,                      //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
                $SlideDuration: 300,                                            //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                                      //[Optional] Minimum drag offset to trigger slide , default value is 20
                $SlideSpacing: 0, 					                                    //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 1,                                              //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                            //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 0,                                               //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                            //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation: 3,                                            //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

                $ThumbnailNavigatorOptions: {                                   //[Optional] Options to specify and enable thumbnail navigator or not
                    $Class: $JssorThumbnailNavigator$,                          //[Required] Class to create thumbnail navigator instance
                    $ChanceToShow: 2,                                           //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $ActionMode: 1,                                             //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
                    $SpacingX: 12,                                              //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
                    $Cols: 5,                                                   //[Optional] Number of slides to display in the 'slides' container (slideshow would be disabled if the value is greater than 1)
                    $ParkingPosition: 0,                                        //[Optional] The offset position to park thumbnail
                    $AutoCenter: 1
                }
            };

            var jssor_slider = new $JssorSlider$("slider_container_1", options);
            {if ($settings.standard_extended_settings && $settings.standard_images_gallery) || !$settings.standard_extended_settings}
            {foreach from=$slides item=product}
            {if isset($product.image) && $product.image}
            var jssor_slider{$product.info->id|escape:'htmlall':'UTF-8'} = new $JssorSlider$("inner-slider-{$product.info->id|escape:'htmlall':'UTF-8'}-s", nestedSlidersOptions);
            nestedSliders.push(jssor_slider{$product.info->id|escape:'htmlall':'UTF-8'});
            {/if}
            {/foreach}
            {/if}
            function ScaleSlider() {
                var parentWidth = jssor_slider.$Elmt.parentNode.clientWidth;

                if (parentWidth)
                    jssor_slider.$ScaleWidth(Math.max(Math.min(parentWidth, {$settings.standard_slider_width|escape:'htmlall':'UTF-8'}), 30));
                else
                    window.setTimeout(ScaleSlider, 30);
            }
            ScaleSlider();

            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
        });
    </script>
{elseif $settings.slider_type == list}
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            var options = {
                $AutoPlay         : {if ($settings.list_extended_settings && $settings.list_slider_autoplay) || !$settings.list_extended_settings}true{else}false{/if},                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlaySteps    : 1,                                              //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
                $AutoPlayInterval : {$settings.list_slider_interval|escape:'htmlall':'UTF-8'},            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover     : 1,                                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1
                $Loop             : {if ($settings.list_extended_settings && $settings.list_slider_loop) || !$settings.list_extended_settings}true{else}false{/if},
                $ArrowKeyNavigation   : true,   			                              //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideEasing          : $JssorEasing$.$EaseOutQuint,                      //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
                $SlideDuration        : {$settings.list_slider_duration|escape:'htmlall':'UTF-8'},               //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide : 20,                                      //[Optional] Minimum drag offset to trigger slide , default value is 20
                $SlideWidth           : {$settings.list_slider_width|escape:'htmlall':'UTF-8'},                     //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                $SlideSpacing         : 0, 					                                    //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces        : 1,                                              //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition      : 0,                                            //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode         : 0,                                               //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation      : 2,                                            //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation      : 3,                                            //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
                $ArrowNavigatorOptions : {                                       //[Optional] Options to specify and enable arrow navigator or not
                    $Class        : $JssorArrowNavigator$,                              //[Requried] Class to create arrow navigator instance
                    $ChanceToShow : 2,                                           //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $Steps        : 1                                                   //[Optional] Steps to go for each navigation request, default value is 1
                },
                $ThumbnailNavigatorOptions : {                                   //[Optional] Options to specify and enable thumbnail navigator or not
                    $Class           : $JssorThumbnailNavigator$,                          //[Required] Class to create thumbnail navigator instance
                    $ChanceToShow    : 2,                                           //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $ActionMode      : 1,                                             //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
                    $SpacingX        : 0,                                               //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
                    $SpacingY        : 10,                                              //[Optional] Vertical space between each thumbnail in pixel, default value is 0
                    $Cols            : 3,
                    $ParkingPosition : 0,                                        //[Optional] The offset position to park thumbnail
                    $Orientation     : 2,                                            //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                    $Loop            : {if ($settings.list_extended_settings && $settings.list_slider_loop) || !$settings.list_extended_settings}true{else}false{/if},
                    $AutoCenter      : 0,
                    $ArrowNavigatorOptions : {
                        $Class        : $JssorArrowNavigator$,                  //[Requried] Class to create arrow navigator instance
                        $ChanceToShow : 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                        $Steps        : 3                                       //[Optional] Steps to go for each navigation request, default value is 1
                    }
                },
                $BulletNavigatorOptions : {                                      //[Optional] Options to specify and enable navigator or not
                    $Class        : $JssorBulletNavigator$,                             //[Required] Class to create navigator instance
                    $ChanceToShow : 2,                                           //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter   : 0,                                             //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps        : 1,                                                  //[Optional] Steps to go for each navigation request, default value is 1
                    $Lanes        : 1,                                                  //[Optional] Specify lanes to arrange items, default value is 1
                    $SpacingX     : 5,                                               //[Optional] Horizontal space between each item in pixel, default value is 0
                    $SpacingY     : 0,                                               //[Optional] Vertical space between each item in pixel, default value is 0
                    $Orientation  : 1                                             //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                }
            };
            var nestedSliders = [];
            var nestedSlidersOptions = {
                $AutoPlay : false,                                               //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $ArrowKeyNavigation   : true,   			                              //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideEasing          : $JssorEasing$.$EaseOutQuint,                      //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
                $SlideDuration        : 300,                                            //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide : 20,                                      //[Optional] Minimum drag offset to trigger slide , default value is 20
                $SlideSpacing         : 0, 					                                    //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces        : 1,                                              //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition      : 0,                                            //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode         : 0,                                               //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation      : 1,                                            //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation      : 3,                                            //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
                $ThumbnailNavigatorOptions : {                                   //[Optional] Options to specify and enable thumbnail navigator or not
                    $Class           : $JssorThumbnailNavigator$,                          //[Required] Class to create thumbnail navigator instance
                    $ChanceToShow    : 2,                                           //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $ActionMode      : 1,                                             //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
                    $SpacingX        : 10,                                              //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
                    $Cols            : 5,                                                   //[Optional] Number of slides to display in the 'slides' container (slideshow would be disabled if the value is greater than 1)
                    $ParkingPosition : 0,                                        //[Optional] The offset position to park thumbnail
                    $AutoCenter      : 1
                }
            };

            var jssor_slider = new $JssorSlider$("slider_container_2", options);
            {if $settings.list_extended_settings && $settings.list_images_gallery}
            {foreach from=$slides item=product}
                {if isset($product.image) && $product.image}
                    var jssor_slider{$product.info->id|escape:'htmlall':'UTF-8'} = new $JssorSlider$("inner-slider-{$product.info->id|escape:'htmlall':'UTF-8'}", nestedSlidersOptions);
                    nestedSliders.push(jssor_slider{$product.info->id|escape:'htmlall':'UTF-8'});
                {/if}
            {/foreach}
            {/if}
            function ScaleSlider() {
                var parentWidth = jssor_slider.$Elmt.parentNode.clientWidth;
                if (parentWidth)
                    jssor_slider.$ScaleWidth(Math.max(Math.min(parentWidth, {$settings.list_slider_width|escape:'htmlall':'UTF-8'}), 30)); else
                    window.setTimeout(ScaleSlider, 30);
            }

            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);

            //responsive code end
        });
    </script>
{elseif $settings.slider_type == grid}
    <script type="text/javascript">
        jQuery(document).ready(function($) {

            var _SlideshowTransitions = [
                //Fade
                { $Duration: 700, $Opacity: 2, $Brother: { $Duration: 1000, $Opacity: 2} }
            ];

            var options = {
                $AutoPlay: {if ($settings.grid_extended_settings && $settings.grid_slider_autoplay) || !$settings.grid_extended_settings}true{else}false{/if},                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlaySteps: 1,                                              //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
                $AutoPlayInterval: {$settings.grid_slider_interval|escape:'htmlall':'UTF-8'},            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1
                $Loop: {if ($settings.grid_extended_settings && $settings.grid_slider_loop) || !$settings.grid_extended_settings}true{else}false{/if},

                $ArrowKeyNavigation: true,   			                              //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideEasing: $JssorEasing$.$EaseOutQuint,                      //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
                $SlideDuration: {$settings.grid_slider_duration|escape:'htmlall':'UTF-8'},               //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                                      //[Optional] Minimum drag offset to trigger slide , default value is 20
                $SlideWidth: {$settings.grid_slider_width|escape:'htmlall':'UTF-8'},                     //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                $SlideSpacing: 0, 					                                    //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 1,                                              //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                            //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 0,                                               //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 2,                                            //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation: 3,                                            //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

                $SlideshowOptions: {                                            //[Optional] Options to specify and enable slideshow or not
                    $Class: $JssorSlideshowRunner$,                             //[Required] Class to create instance of slideshow
                    $Transitions: _SlideshowTransitions,                        //[Required] An array of slideshow transitions to play slideshow
                    $TransitionsOrder: 1,                                       //[Optional] The way to choose transition to play slide, 1 Sequence, 0 Random
                    $ShowLink: true                                             //[Optional] Whether to bring slide link on top of the slider when slideshow is running, default value is false
                },

                $ArrowNavigatorOptions: {                                       //[Optional] Options to specify and enable arrow navigator or not
                    $Class: $JssorArrowNavigator$,                              //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 2,                                           //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $Steps: 1                                                   //[Optional] Steps to go for each navigation request, default value is 1
                },

                $ThumbnailNavigatorOptions: {                                   //[Optional] Options to specify and enable thumbnail navigator or not
                    $Class: $JssorThumbnailNavigator$,                          //[Required] Class to create thumbnail navigator instance
                    $ChanceToShow: 2,                                           //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $ActionMode: 2,                                             //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
                    $SpacingX: 0,                                               //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
                    $SpacingY: 0,                                               //[Optional] Vertical space between each thumbnail in pixel, default value is 0
                    $Cols: 3,                                                   //[Optional] Number of items to display in the thumbnail navigator container
                    $ParkingPosition: 0,                                        //[Optional] The offset position to park thumbnail
                    $Orientation: 2,                                            //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                    $Rows: 3,                                                   //[Optional] Specify lanes to arrange thumbnails
                    $Loop: {if ($settings.grid_extended_settings && $settings.grid_slider_loop) || !$settings.grid_extended_settings}true{else}false{/if},
                    $AutoCenter: 0
                },

                $BulletNavigatorOptions: {                                      //[Optional] Options to specify and enable navigator or not
                    $Class: $JssorBulletNavigator$,                             //[Required] Class to create navigator instance
                    $ChanceToShow: 2,                                           //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 0,                                             //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1,                                                  //[Optional] Steps to go for each navigation request, default value is 1
                    $SpacingX: 0,                                               //[Optional] Horizontal space between each item in pixel, default value is 0
                    $SpacingY: 5,                                               //[Optional] Vertical space between each item in pixel, default value is 0
                    $Orientation: 2                                             //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                }
            };

            var nestedSliders = [];

            var nestedSlidersOptions = {
                $AutoPlay: false,                                               //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $ArrowKeyNavigation: true,   			                              //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideEasing: $JssorEasing$.$EaseOutQuint,                      //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
                $SlideDuration: 300,                                            //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                                      //[Optional] Minimum drag offset to trigger slide , default value is 20
                $SlideSpacing: 0, 					                                    //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 1,                                              //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                            //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 0,                                               //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                            //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation: 3,                                            //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

                $ThumbnailNavigatorOptions: {                                   //[Optional] Options to specify and enable thumbnail navigator or not
                    $Class: $JssorThumbnailNavigator$,                          //[Required] Class to create thumbnail navigator instance
                    $ChanceToShow: 2,                                           //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $Orientation: 2,                                            //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                    $ActionMode: 1,                                             //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
                    $SpacingX: 0,                                               //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
                    $SpacingY: 10,                                              //[Optional] Vertical space between each thumbnail in pixel, default value is 0
                    $Cols: 5,                                                   //[Optional] Number of pieces to display, default value is 1
                    $ParkingPosition: 0,                                        //[Optional] The offset position to park thumbnail
                    $AutoCenter: 0
                }
            };

            var jssor_slider = new $JssorSlider$("slider_container_3", options);
            {if $settings.grid_extended_settings && $settings.grid_images_gallery}
            {foreach from=$slides item=product}
            {if isset($product.image) && $product.image}
            var jssor_slider{$product.info->id|escape:'htmlall':'UTF-8'} = new $JssorSlider$("inner-slider-{$product.info->id|escape:'htmlall':'UTF-8'}", nestedSlidersOptions);
            nestedSliders.push(jssor_slider{$product.info->id|escape:'htmlall':'UTF-8'});
            {/if}
            {/foreach}
            {/if}
            function ScaleSlider() {
                var parentWidth = jssor_slider.$Elmt.parentNode.clientWidth;
                if (parentWidth)
                    jssor_slider.$ScaleWidth(Math.max(Math.min(parentWidth, {$settings.grid_slider_width|escape:'htmlall':'UTF-8'}), 30));
                else
                    window.setTimeout(ScaleSlider, 30);
            }
            ScaleSlider();

            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
        });
    </script>
{elseif $settings.slider_type == fullwidth}
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            var options = {
                $AutoPlay: {if ($settings.fullwidth_extended_settings && $settings.fullwidth_slider_autoplay) || !$settings.fullwidth_extended_settings}true{else}false{/if},                //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlaySteps: 1,                                              //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
                $AutoPlayInterval: {$settings.fullwidth_slider_interval|escape:'htmlall':'UTF-8'},       //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1
                $Loop: {if ($settings.fullwidth_extended_settings && $settings.fullwidth_slider_loop) || !$settings.fullwidth_extended_settings}true{else}false{/if},

                $ArrowKeyNavigation: true,   			                              //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideEasing: $JssorEasing$.$EaseOutQuint,                      //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
                $SlideDuration: {$settings.fullwidth_slider_duration|escape:'htmlall':'UTF-8'},          //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                                      //[Optional] Minimum drag offset to trigger slide , default value is 20
                $SlideWidth: {$settings.fullwidth_slider_width|escape:'htmlall':'UTF-8'},                //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                $SlideSpacing: 0, 					                                    //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 1,                                              //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                            //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 0,                                               //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                            //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation: 3,                                            //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
                $AutoCenter: 1,

                $ArrowNavigatorOptions: {                                       //[Optional] Options to specify and enable arrow navigator or not
                    $Class: $JssorArrowNavigator$,                              //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 2,                                           //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $Steps: 1                                                   //[Optional] Steps to go for each navigation request, default value is 1
                },

                $ThumbnailNavigatorOptions: {                                   //[Optional] Options to specify and enable thumbnail navigator or not
                    $Class: $JssorThumbnailNavigator$,                          //[Required] Class to create thumbnail navigator instance
                    $ChanceToShow: 2,                                           //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $ActionMode: 1,                                             //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
                    $SpacingX: 12,                                              //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
                    $Cols: 5,                                                   //[Optional] Number of pieces to display, default value is 1
                    $ParkingPosition: 0,                                        //[Optional] The offset position to park thumbnail
                    $Orientation: 1,
                    $Loop: {if ($settings.fullwidth_extended_settings && $settings.fullwidth_slider_loop) || !$settings.fullwidth_extended_settings}true{else}false{/if},
                    $AutoCenter: 1
                },

                $BulletNavigatorOptions: {                                      //[Optional] Options to specify and enable navigator or not
                    $Class: $JssorBulletNavigator$,                             //[Required] Class to create navigator instance
                    $ChanceToShow: 2,                                           //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 0,                                             //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1,                                                  //[Optional] Steps to go for each navigation request, default value is 1
                    $Lanes: 1,                                                  //[Optional] Specify lanes to arrange items, default value is 1
                    $SpacingX: 5,                                               //[Optional] Horizontal space between each item in pixel, default value is 0
                    $SpacingY: 0,                                               //[Optional] Vertical space between each item in pixel, default value is 0
                    $Orientation: 1                                             //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                }
            };

            var nestedSliders = [];

            var nestedSlidersOptions = {
                $AutoPlay: false,                                               //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false

                $ArrowKeyNavigation: true,   			                              //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideEasing: $JssorEasing$.$EaseOutQuint,                      //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
                $SlideDuration: 300,                                            //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                                      //[Optional] Minimum drag offset to trigger slide , default value is 20
                $SlideSpacing: 0, 					                                    //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 1,                                              //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                            //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 0,                                               //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                            //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation: 3,                                            //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

                $ThumbnailNavigatorOptions: {                                   //[Optional] Options to specify and enable thumbnail navigator or not
                    $Class: $JssorThumbnailNavigator$,                          //[Required] Class to create thumbnail navigator instance
                    $ChanceToShow: 2,                                           //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $ActionMode: 1,                                             //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
                    $SpacingY: 12,                                              //[Optional] Vertical space between each thumbnail in pixel, default value is 0
                    $Cols: 5,                                                   //[Optional] Number of pieces to display, default value is 1
                    $ParkingPosition: 0,                                        //[Optional] The offset position to park thumbnail
                    $AutoCenter: 1,
                    $Orientation: 2
                }
            };

            var jssor_slider = new $JssorSlider$("slider_container_4", options);
            {if ($settings.fullwidth_extended_settings && $settings.fullwidth_images_gallery) || !$settings.fullwidth_extended_settings}
            {foreach from=$slides item=product}
            {if isset($product.image) && $product.image}
            var jssor_slider{$product.info->id|escape:'htmlall':'UTF-8'} = new $JssorSlider$("inner-slider-{$product.info->id|escape:'htmlall':'UTF-8'}-f", nestedSlidersOptions);
            nestedSliders.push(jssor_slider{$product.info->id|escape:'htmlall':'UTF-8'});
            {/if}
            {/foreach}
            {/if}
            function ScaleSlider() {
                var parentWidth = jssor_slider.$Elmt.parentNode.clientWidth;
                if (parentWidth)
                    jssor_slider.$ScaleWidth(Math.max(Math.min(parentWidth, {$settings.fullwidth_slider_width|escape:'htmlall':'UTF-8'}), 30));
                else
                    window.setTimeout(ScaleSlider, 30);
            }
            ScaleSlider();

            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
        });
    </script>
{/if}