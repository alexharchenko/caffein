<?php

class KlAutoRegistration extends Module
{
    public function __construct()
    {
        $this->name = 'klautoregistration';
        $this->version = '1.0.0';
        $this->author = 'kLooKva Antikov';
        $this->need_instance = 0;
        $this->bootstrap = true; 
        parent::__construct();

        $this->displayName = $this->l('Klookva auto registration');
        $this->description = $this->l('Auto registration when do order');
        $this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        if (parent::install() == false or !$this->registerHook('actionValidateOrder'))
        {
            return false;
        }
        return true;
    }

    public function uninstall()
    {
        $this->unregisterHook('actionValidateOrder');
        return parent::uninstall();
    }

    public function hookActionValidateOrder($params)
    {
        $customer = new Customer($params['cart']->id_customer);
        $customer->transformToCustomer($this->context->language->id);
    }
}