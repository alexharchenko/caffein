<?php

class kl_customblockClass extends ObjectModel
{

    public $id;

    public $id_shop;

    public $id_hook;

    public $text;

    public $file_name;
    
    public $title;

    public $button_title;

    public $url;
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'kl_customblock',
        'primary' => 'id_block',
        'multilang' => true,
        'fields' => array(
            'id_shop' =>            array('type' => self::TYPE_NOTHING, 'validate' => 'isUnsignedId'),
            'file_name' => array('type' => self::TYPE_STRING, 'validate' => 'isFileName'),
            'id_hook' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
            // Lang fields
            'text' =>               array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml', 'required' => true),
            'title' =>              array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'button_title' =>               array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'url' =>                array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
        )
    );

    public function copyFromPost()
    {
        /* Classical fields */
        foreach ($_POST as $key => $value) {
            if (array_key_exists($key, $this) and $key != 'id_'.$this->table) {
                $this->{$key} = $value;
            }
        }

        /* Multilingual fields */
        if (sizeof($this->fieldsValidateLang)) {
            $languages = Language::getLanguages(false);
            foreach ($languages as $language) {
                foreach ($this->fieldsValidateLang as $field => $validation) {
                    if (isset($_POST[$field.'_'.(int)($language['id_lang'])])) {
                        $this->{$field}[(int)($language['id_lang'])] = $_POST[$field.'_'.(int)($language['id_lang'])];
                    }
                }
            }
        }
    }
    
}
