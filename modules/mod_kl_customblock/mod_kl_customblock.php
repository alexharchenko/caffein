<?php

if (!defined('_CAN_LOAD_FILES_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

include_once _PS_MODULE_DIR_.'mod_kl_customblock/classes/kl_customblockClass.php';

class mod_kl_customblock extends Module implements WidgetInterface
{
    private $templateFile;

    public function __construct()
    {
        $this->name = 'mod_kl_customblock';
        $this->version = '1.9.0';
        $this->author = 'Klookva Antikov';
        $this->need_instance = 0;
        $this->bootstrap = true;
        parent::__construct();
        $this->hookName = 'displayProductList';
        $this->displayName = $this->l('Klookva custom block', array(), 'Modules.KLcustomblock.Admin');
        $this->description = $this->l('Display custom block with img, title, description and button url .', array(), 'Modules.KLcustomblock.Admin');
        $this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);

        $this->templateFile = 'module:mod_kl_customblock/views/templates/hook/mod_kl_customblock.tpl';
    }

    public function install()
    {
        return parent::install()
            && $this->installDB()
            && Configuration::updateValue('VALUE_kl_customblock', 5)
            && $this->registerHook($this->hookName)
        ;
    }

    public function installDB()
    {
        $return = true;
        $return &= Db::getInstance()->execute('
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'kl_customblock` (
                `id_block` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                `id_shop` int(10) unsigned DEFAULT NULL,
                `file_name` varchar(300),
                `id_hook` int(10) UNSIGNED,
                PRIMARY KEY (`id_block`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;'
        );

        $return &= Db::getInstance()->execute('
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'kl_customblock_lang` (
                `id_block` INT UNSIGNED NOT NULL,
                `id_lang` int(10) unsigned NOT NULL ,
                `text` text NOT NULL,
                `title` varchar(50) NOT NULL,
                `button_title` varchar(50) NOT NULL,
                `url` varchar(300) NOT NULL,
                PRIMARY KEY (`id_block`, `id_lang`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;'
        );

        return $return;
    }

    public function uninstall()
    {
        return Configuration::deleteByName('VALUE_kl_customblock') &&
            $this->uninstallDB() &&
            $this->unregisterHook($this->hookName) &&
            parent::uninstall();
    }

    public function uninstallDB()
    {
        return Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'kl_customblock`') && Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'kl_customblock_lang`');
    }

    public function getContent()
    {
        $html = '';
        $id_block = (int)Tools::getValue('id_block');

        if (Tools::isSubmit('savemod_kl_customblock')) {
            if ($id_block = Tools::getValue('id_block')) {
                $kl_customblock = new kl_customblockClass((int)$id_block);
            } else {
                $kl_customblock = new kl_customblockClass();
            }
            Configuration::updateValue($this->name . '_position', Tools::getValue('position'));
            $kl_customblock->copyFromPost();
            $kl_customblock->id_shop = $this->context->shop->id;
            if ($kl_customblock->validateFields(false) && $kl_customblock->validateFieldsLang(false)) {
                $kl_customblock->save();
                //var_dump($kl_customblock);exit;
                if (isset($_FILES['image']) && isset($_FILES['image']['tmp_name']) && !empty($_FILES['image']['tmp_name'])) {
                    if ($error = ImageManager::validateUpload($_FILES['image'])) {
                        return false;
                    } elseif (!($tmpName = tempnam(_PS_TMP_IMG_DIR_, 'PS')) || !move_uploaded_file($_FILES['image']['tmp_name'], $tmpName)) {
                        return false;
                    } elseif (!ImageManager::resize($tmpName, dirname(__FILE__).'/img/kl_customblock-'.(int)$kl_customblock->id.'-'.(int)$kl_customblock->id_shop.'.png')) {
                        return false;
                    }
                    unlink($tmpName);
                    $kl_customblock->file_name = 'kl_customblock-'.(int)$kl_customblock->id.'-'.(int)$kl_customblock->id_shop.'.png';
                    $kl_customblock->save();
                }
                $this->_clearCache('*');
            } else {
                $html .= '<div class="alert alert-danger conf error">'.$this->l('An error occurred while attempting to save.', array(), 'Admin.Notifications.Error').'</div>';
            }
        }

        if (Tools::isSubmit('updatemod_kl_customblock') || Tools::isSubmit('addmod_kl_customblock')) {
            $helper = $this->initForm();
            foreach (Language::getLanguages(false) as $lang) {
                if ($id_block) {
                    $kl_customblock = new kl_customblockClass((int)$id_block);
                    $helper->fields_value['text'][(int)$lang['id_lang']] = $kl_customblock->text[(int)$lang['id_lang']];
                    $helper->fields_value['title'][(int)$lang['id_lang']] = $kl_customblock->title[(int)$lang['id_lang']];
                    $helper->fields_value['button_title'][(int)$lang['id_lang']] = $kl_customblock->button_title[(int)$lang['id_lang']];
                    $helper->fields_value['url'][(int)$lang['id_lang']] = $kl_customblock->url[(int)$lang['id_lang']];
                    $image = dirname(__FILE__).DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$kl_customblock->file_name;
                    $this->fields_form[0]['form']['input'][0]['image'] = '<img src="'.$this->getImageURL($kl_customblock->file_name).'" />';
                } else {
                    $helper->fields_value['text'][(int)$lang['id_lang']] = Tools::getValue('text_'.(int)$lang['id_lang'], '');
                    $helper->fields_value['title'][(int)$lang['id_lang']] = Tools::getValue('title_'.(int)$lang['id_lang'], '');
                    $helper->fields_value['button_title'][(int)$lang['id_lang']] = Tools::getValue('button_title_'.(int)$lang['id_lang'], '');
                    $helper->fields_value['url'][(int)$lang['id_lang']] = Tools::getValue('url_'.(int)$lang['id_lang'], '');
                }
            }
            $blocks = $this->getListContent_All((int)Configuration::get('PS_LANG_DEFAULT'));
            foreach($blocks as $block)
            {
                $name_hook = Hook::getNameById($block['id_hook']);
                $this->registerHook($name_hook);
            }
            $helper->fields_value['id_hook'] = $kl_customblock->id_hook ?? Hook::getIdByName($this->hookName);
            $helper->fields_value['position'] = (int)Configuration::get($this->name . '_position') !== 0 ? (int)Configuration::get($this->name . '_position') : 2;

            if ($id_block = Tools::getValue('id_block')) {
                $this->fields_form[0]['form']['input'][] = array('type' => 'hidden', 'name' => 'id_block');
                $helper->fields_value['id_block'] = (int)$id_block;

            }

            return $html.$helper->generateForm($this->fields_form);
        } elseif (Tools::isSubmit('deletemod_kl_customblock')) {
            $kl_customblock = new kl_customblockClass((int)$id_block);
            if (file_exists(dirname(__FILE__).'/img/'.$kl_customblock->file_name)) {
                unlink(dirname(__FILE__).'/img/'.$kl_customblock->file_name);
            }
            $kl_customblock->delete();
            $this->_clearCache('*');
            Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
        } else {
            $content = $this->getListContent_All((int)Configuration::get('PS_LANG_DEFAULT'));
            $helper = $this->initList();
            $helper->listTotal = count($content);
            return $html.$helper->generateList($content, $this->fields_list);
        }
    }
    protected function getListContent_All($id_lang)
    {
        return  Db::getInstance()->executeS('
            SELECT r.`id_block`, r.`id_shop`, r.`file_name`, r.`id_hook`, rl.`text`,rl.`title`,rl.`button_title`,rl.`url`
            FROM `'._DB_PREFIX_.'kl_customblock` r
            LEFT JOIN `'._DB_PREFIX_.'kl_customblock_lang` rl ON (r.`id_block` = rl.`id_block`)
            WHERE `id_lang` = '.(int)$id_lang.' '.Shop::addSqlRestrictionOnLang());
    }
    protected function getListContent($id_lang,$id_hook, $randOrder, $blockNmbr)
    {
        $query = 'SELECT r.`id_block`, r.`id_shop`, r.`file_name`, rl.`text`,rl.`title`,rl.`button_title`,rl.`url`
            FROM `'._DB_PREFIX_.'kl_customblock` r
            LEFT JOIN `'._DB_PREFIX_.'kl_customblock_lang` rl ON (r.`id_block` = rl.`id_block`)
            WHERE `id_hook` = '.$id_hook.' AND `id_lang` = '.(int)$id_lang.' '.Shop::addSqlRestrictionOnLang();
        if($randOrder) {
            $query .= "ORDER BY RAND()";
        }
        if($blockNmbr) {
            $query .= "LIMIT " . $blockNmbr;
        }
        return  Db::getInstance()->executeS($query);
    }

    protected function initForm()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $this->fields_form[0]['form'] = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->l('New kl_customblock block', array(), 'Modules.KLcustomblock.Admin'),
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Hook', array(), 'Admin.Global'),
                    'name' => 'id_hook',
                    'class' => 'input-lg',
                    'options' => array(
                        'query' => $this->getDisplayHooksForHelper(),
                        'id' => 'id',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Title', array(), 'Modules.KLcustomblock.Admin'),
                    'lang' => true,
                    'name' => 'title',
                ),
                array(
                    'type' => 'file',
                    'label' => $this->l('Image', array(), 'Admin.Global'),
                    'name' => 'image',
                    'value' => true,
                    'display_image' => true,
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Text block', array(), 'Modules.KLcustomblock.Admin'),
                    'lang' => true,
                    'name' => 'text',
                    'cols' => 40,
                    'rows' => 10,
                    'class' => 'rte',
                    'autoload_rte' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Title Button', array(), 'Modules.KLcustomblock.Admin'),
                    'lang' => true,
                    'name' => 'button_title',
                ), 
                array(
                    'type' => 'text',
                    'label' => $this->l('Url Button', array(), 'Modules.KLcustomblock.Admin'),
                    'lang' => true,
                    'name' => 'url',
                ), 
                array(
                    'type' => 'text',
                    'label' => $this->l('Number of block (for all blocks)', array(), 'Modules.KLcustomblock.Admin'),
                    'name' => 'position',
                )
            ),
            'submit' => array(
                'title' => $this->l('Save', array(), 'Admin.Actions'),
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = 'mod_kl_customblock';
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        foreach (Language::getLanguages(false) as $lang) {
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );
        }

        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'savemod_kl_customblock';
        $helper->toolbar_btn =  array(
            'save' =>
            array(
                'desc' => $this->l('Save', array(), 'Admin.Actions'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
            ),
            'back' =>
            array(
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list', array(), 'Admin.Actions'),
            )
        );
        return $helper;
    }
    public function getDisplayHooksForHelper()
    {
        $sql = "SELECT h.`id_hook` as id, h.`name` as name
                FROM "._DB_PREFIX_."hook h
                ORDER BY h.`name` ASC
            ";
        $hooks = Db::getInstance()->executeS($sql);

        foreach ($hooks as $key => $hook) {
            if (preg_match('/admin/i', $hook['name'])
                || preg_match('/backoffice/i', $hook['name'])) {
                unset($hooks[$key]);
            }
        }
        return $hooks;
    }

    protected function initList()
    {
        $this->fields_list = array(
            'id_block' => array(
                'title' => $this->l('ID', array(), 'Admin.Global'),
                'width' => 120,
                'type' => 'text',
                'search' => false,
                'orderby' => false
            ),
            'title' => array(
                    'type' => 'text',
                    'title' => $this->l('Title', array(), 'Modules.KLcustomblock.Admin'),
                    'lang' => true,
                    'name' => 'title',
                ),
            'text' => array(
                'title' => $this->l('Text', array(), 'Admin.Global'),
                'width' => 140,
                'type' => 'text',
                'search' => false,
                'orderby' => false
            ),
        );

        if (Shop::isFeatureActive()) {
            $this->fields_list['id_shop'] = array(
                'title' => $this->l('ID Shop', array(), 'Modules.KLcustomblock.Admin'),
                'align' => 'center',
                'width' => 25,
                'type' => 'int'
            );
        }

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = false;
        $helper->identifier = 'id_block';
        $helper->actions = array('edit', 'delete');
        $helper->show_toolbar = true;
        $helper->imageType = 'png';
        $helper->toolbar_btn['new'] =  array(
            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&add'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Add new', array(), 'Admin.Actions')
        );

        $helper->title = $this->displayName;
        $helper->table = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        return $helper;
    }

    protected function _clearCache($template, $cacheId = null, $compileId = null)
    {
        parent::_clearCache($this->templateFile);
    }

    public function renderWidget($hookName, array $configuration)
    {
        $key = 'mod_kl_customblock|' . $hookName;
        if (!$this->isCached($this->templateFile, $this->getCacheId($key))) {
            $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));
        }
        return $this->fetch($this->templateFile, $this->getCacheId($key));
    }

    public function getWidgetVariables($hookName, array $configuration)
    {
        $id_hook = Hook::getIdByName($hookName);
        $randOrder = isset($configuration['randOrder']) ? $configuration['randOrder'] : false;
        // $blockNmbr - количество кастомных блоков, которые будут выведены
        $blockNmbr = isset($configuration['blockNmbr']) ? $configuration['blockNmbr'] : null;

        $elements = $this->getListContent($this->context->language->id,$id_hook, $randOrder, $blockNmbr);

        foreach ($elements as &$element) {
            if(isset($element['file_name']) && $element['file_name'] !== '')
                $element['image'] = $this->getImageURL($element['file_name']);
        }
        return array(
            'elements' => $elements,
        );
    }

    private function getImageURL($image)
    {
        return $this->context->link->getMediaLink(__PS_BASE_URI__.'modules/'.$this->name.'/img/'.$image);
    }
}
