{if $elements}
	{foreach from=$elements item=element}
		<div class="col-xs-12">
			<div class="bg-gray-lightest">
				<div class="container">
					<div class="row content-module">
						<div class="col-md-8">
							<div class="h3">{$element.title}</div>
							{$element.text nofilter}
							<p><a class="btn-primary btn" href="{$element.url}">{$element.button_title}</a></p>	
						</div>
						{if isset($element.image)}
						<div class="col-md-4 text-right"><img src="{$element.image}" alt=" "></div>
						{/if}
					</div>
				</div>
			</div>
		</div>
	{/foreach}
{/if}