<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{kl_productsocialpoll}prestashop>kl_productsocialpoll_c888438d14855d7d96a2724ee9c306bd'] = 'Настройки сохранены';
$_MODULE['<{kl_productsocialpoll}prestashop>display_e1bf4c83fcacfde3d52cbad814507571'] = 'Спасибо за Ваш голос';
$_MODULE['<{kl_productsocialpoll}prestashop>display_dde114a3af7c7228f08ac3404b8ef2b8'] = 'Ошибка';
$_MODULE['<{kl_productsocialpoll}prestashop>display_6260cd54bfa40f151c10a28a5e4b7900'] = 'Вы не можете проголосовать. Возможно, вы уже проголосовали ранее';
