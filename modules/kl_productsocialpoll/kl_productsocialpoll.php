<?php
if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;
require_once _PS_MODULE_DIR_.'kl_productsocialpoll/classes/pollsClass.php';
require_once _PS_MODULE_DIR_.'kl_productsocialpoll/classes/VotesClass.php';
require_once _PS_MODULE_DIR_.'kl_productsocialpoll/classes/productsToPollsClass.php';


class kl_ProductSocialPoll extends Module implements WidgetInterface
{
    public function __construct()
    {
        $this->name = 'kl_productsocialpoll';
        $this->tab = 'others';
        $this->version = '1.1.1';
        $this->author = 'Peter Shcherbatiuk';
        $this->need_instance = 1;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Product Social Polls');
        $this->description = $this->l('Description test');
        $this->confirmUninstall = $this->l('Are you sure you want to delete ProductSocialPoll module?');
        if ($params = Configuration::get('PRODUCTSOCIALPOLL')) {
            $this->params = unserialize($params);
        }
        else {
            $this->warning = $this->l('Some error occured during installation');
        }
        $this->templateFile = "module:" . $this->name . "/views/templates/front/productPoll.tpl";   
    }

    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        } 
        if (parent::install() 
            && Configuration::updateValue('PRODUCTSOCIALPOLL', serialize(['FbAppToken' => '371296339970703', 'currentPoll' => '1']))
            && $this->registerHook('displayProductPoll')
            && $this->registerHook('actionFrontControllerSetMedia')
            && $this->createTables()) {
            return true;
        }
        return false;
    }

    private function createTables()
    {
        return Db::getInstance()->execute('
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'kl_productsocialpoll_polls` ( 
                `id` INT UNSIGNED NOT NULL AUTO_INCREMENT , 
                `expiration_date` TIMESTAMP NOT NULL , 
                `poll_photo` VARCHAR(100) NULL , 
                `winner_id` INT NULL , 
                PRIMARY KEY (`id`)) 
                ENGINE = '._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;') &&
            Db::getInstance()->execute('
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'kl_productsocialpoll_votes` ( 
                `fb_id` BIGINT(20) NOT NULL , 
                `poll_id` INT NOT NULL , 
                `voted_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , 
                `product_id` INT NOT NULL ,
                PRIMARY KEY (`fb_id`, `poll_id`)) 
                ENGINE = '._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;') &&
            Db::getInstance()->execute('
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'kl_productsocialpoll_products_to_polls` ( 
                `poll_id` INT NOT NULL , 
                `product_id` INT NOT NULL ,
                PRIMARY KEY (`product_id`, `poll_id`)) 
                ENGINE = '._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;') &&
            Db::getInstance()->execute('
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'kl_productsocialpoll_polls_lang` ( 
                `id` INT NOT NULL , 
                `id_lang` INT NOT NULL , 
                `name` VARCHAR(150) NULL ,
                `description` VARCHAR(500) NULL ,
                `fb_post_text` VARCHAR(500) NULL , 
                PRIMARY KEY (`id`, `id_lang`)) 
                ENGINE = '._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');
    }

    private function dropTables() 
    {
        return Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'kl_productsocialpoll_polls`') 
            && Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'kl_productsocialpoll_votes`')
            && Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'kl_productsocialpoll_products_to_polls`')
            && Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'kl_productsocialpoll_polls_lang`');
    }

    public function uninstall()
    {
        if (parent::uninstall() 
            && Configuration::deleteByName('PRODUCTSOCIALPOLL') 
            && $this->unregisterHook('displayProductPoll')
            && $this->unregisterHook('actionFrontControllerSetMedia')
            && $this->dropTables()) {
            return true;
        }
        return false;
    }

    public function getContent()
    {
        $output = null;
        if (Tools::isSubmit('delete' . $this->name)) {
            $output .= $this->deleteEntity($_GET['id']);
        }
        elseif (Tools::isSubmit('add'.$this->name) || Tools::isSubmit('edit'.$this->name)) {
            return $this->displayPollForm() . $this->displayVoteList();
        }
        elseif (Tools::isSubmit('save' . $this->name)) {
            $output .= $this->savePoll($_REQUEST);
        }
        elseif (Tools::isSubmit('settings' . $this->name)) {
            return $this->displaySettingsForm();
        }
        elseif (Tools::isSubmit('saveSettings' . $this->name)) {
            $output .= $this->updateSettings();
        }

        return $output.$this->displayPollsList();
    }

    private function deleteEntity($id)
    {
        $entity = new Polls((int)$id);
        if ($entity->delete()) {
            return $this->displayConfirmation($this->l('Poll successfully deleted'));
        }
        else {
            return $this->displayError($this->l("Error occured during deleting"));
        }
    }

    private function savePoll($values)
    {
        $poll = new Polls($values['id']);
        $poll->setValues($values);

        if ($poll->validateFields(false) && $poll->validateFieldsLang(false)) {

            $poll->save($values);
            ProductsToPolls::bulkUpdate($values['product_id'], $poll->id);

            return $this->displayConfirmation($this->l('Poll successfully created/updated'));
        }
        else {
            return $this->displayError($this->l("Error occured during data saving"));
        }
    }

    private function updateSettings()
    {
        $params = [
            'currentPoll' => Tools::getValue('currentPoll'),
            'FbAppToken' => Tools::getValue('FbAppToken'),
        ];
        Configuration::updateValue('PRODUCTSOCIALPOLL', serialize($params));
        return $this->displayConfirmation($this->l('Settings updated'));
    }

    private function displayVoteList()
    {
        $this->fields_list = array(
            'name' => array(
                'type' => 'text',
                'title' => $this->l('Product Name', array(), 'Modules.kl_ProductSocialPoll.Admin'),
                'lang' => true,
                'name' => 'name',
                'search' => false,
            ),
            'number' => array(
                'type' => 'text',
                'title' => $this->l('Number of Votes', array(), 'Admin.Global'),
                'name' => 'number',
                'search' => false,
                'orderby' => true
            ),
        );
        if (Shop::isFeatureActive()) {
            $this->fields_list['id'] = array(
                'title' => $this->l('ID Shop', array(), 'Modules.kl_ProductSocialPoll.Admin'),
                'align' => 'center',
                'width' => 25,
                'type' => 'int'
            );
        }
        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = true;
        $helper->identifier = 'name';
        $helper->actions = [];
        $helper->title = "Votes";
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name."&edit".$this->name . "&id=" . $_GET['id'];
        $content = Votes::getVotesNumber($_GET['id']);
        $helper->listTotal = count($content);
        return $helper->generateList($content, $this->fields_list);
    }

    private function displaySettingsForm()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $pollsList = Polls::all();
        array_push($pollsList, ['id' => 0, 'name' => $this->l("No active poll")]);

        $this->fields_form[0]['form'] = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->l('Settings'),
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Current poll', array(), 'Modules.kl_ProductSocialPoll.Admin'),
                    'lang' => false,
                    'name' => 'currentPoll',
                    'class' => 'input-lg',
                    'options' => array(
                        'query' => $pollsList,
                        'id' => 'id',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Facebook App Token', array(), 'Modules.kl_ProductSocialPoll.Admin'),
                    'lang' => false,
                    'name' => 'FbAppToken',
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save', array(), 'Admin.Actions'),
            )
        );
        $helper = new HelperForm();
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = [
            'fields_value' => $this->params
        ];
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'saveSettings' . $this->name;
        $helper->toolbar_btn = array(
            'save' => [
                    'desc' => $this->l('Save', array(), 'Admin.Actions'),
                    'href' => AdminController::$currentIndex.'&configure='.$this->name.'&submit'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                ],
            'back' => [
                    'desc' => $this->l('Back to list', array(), 'Admin.Actions'),
                    'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                ]
        );
        return $helper->generateForm($this->fields_form);
    }

    public function displayPollsList()
    {
        $this->fields_list = array(
            'id' => array(
                'title' => $this->l('ID', array(), 'Admin.Global'),
                'width' => 120,
                'type' => 'text',
                'search' => false,
                'orderby' => false
            ),
            'name' => array(
                    'type' => 'text',
                    'title' => $this->l('Name', array(), 'Modules.kl_ProductSocialPoll.Admin'),
                    'lang' => true,
                    'name' => 'title',
                ),
            'expiration_date' => array(
                'title' => $this->l('Expiration date', array(), 'Admin.Global'),
                'width' => 140,
                'type' => 'datetime',
                'search' => false,
            ),
        );
        if (Shop::isFeatureActive()) {
            $this->fields_list['id'] = array(
                'title' => $this->l('ID Shop', array(), 'Modules.kl_ProductSocialPoll.Admin'),
                'align' => 'center',
                'width' => 25,
                'type' => 'int'
            );
        }
        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = false;
        $helper->identifier = 'id';
        $helper->actions = array('edit', 'delete');
        $helper->show_toolbar = true;
        $helper->toolbar_btn['new'] =  array(
            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&add'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Add new', array(), 'Admin.Actions')
        );
        $helper->toolbar_btn['edit'] =  array(
            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&settings'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Edit', array(), 'Admin.Actions')
        );
        $helper->title = $this->displayName;
        $helper->table = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name."&edit".$this->name;
        $content = Polls::all();
        $helper->listTotal = count($content);
        return $helper->generateList($content, $this->fields_list);
    }

    public function displayPollForm()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        if ($id = Tools::getValue('id')) {
            $data = Polls::where(['id', $id]);
            $products = ProductsToPolls::select(['poll_id', $id]);
            $data['product_id[]'] = [];
            foreach ($products as $p) {
                array_push($data['product_id[]'], $p['product_id']);
            }
        }
        // die(var_dump($data));
        $this->fields_form[0]['form'] = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->l($data ? $data['name'][$default_lang] : 'New poll' , array(), 'Modules.kl_ProductSocialPoll.Admin'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Name', array(), 'Modules.kl_ProductSocialPoll.Admin'),
                    'lang' => true,
                    'name' => 'name',
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Description', array(), 'Modules.kl_ProductSocialPoll.Admin'),
                    'lang' => true,
                    'name' => 'description',
                ),
                 array(
                    'type' => 'textarea',
                    'label' => $this->l('Facebook post text', array(), 'Modules.kl_ProductSocialPoll.Admin'),
                    'lang' => true,
                    'name' => 'fb_post_text',
                    'desc' => $this->l('%productname% - формирует название товара'),
                ),
                array(
                    'type' => 'datetime',
                    'label' => $this->l('Expiration date', array(), 'Modules.kl_ProductSocialPoll.Admin'),
                    'lang' => true,
                    'name' => 'expiration_date',
                ),
                [
                    'type' => 'file',
                    'label' => $this->l('Photo', array(), 'Modules.kl_ProductSocialPoll.Admin'),
                    'name' => 'poll_photo',
                    'image' => $data['poll_photo'],
                    'value' => true,
                    'display_image' => true,
                ],
                array(
                    'type' => 'select',
                    'multiple' => 'true',
                    'label' => $this->l('Products', array(), 'Admin.Global'),
                    'name' => 'product_id[]',
                    'class' => 'input-lg',
                    'options' => array(
                        'query' => Product::getProducts(Configuration::get('PS_LANG_DEFAULT'), 0, PHP_INT_MAX, 'id_product', 'asc'),
                        'id' => 'id_product',
                        'name' => 'name'
                    )
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save', array(), 'Admin.Actions'),
            )
        );
        $helper = new HelperForm();
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        foreach (Language::getLanguages(false) as $lang) {
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );
        }
        $helper->tpl_vars = [
            'fields_value' => $data
        ];  
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name.'&id='.$_REQUEST['id'];
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'save' . $this->name;
        $helper->toolbar_btn = [
            'save' => [
                    'desc' => $this->l('Save', array(), 'Admin.Actions'),
                    'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                ],
            'back' => [
                    'desc' => $this->l('Back to list', array(), 'Admin.Actions'),
                    'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                ]
        ];
        return $helper->generateForm($this->fields_form);
    }

    public function renderWidget($hookName = "displayProductPoll", array $configuration)
    {
        $key = 'kl_productsocialpoll|' . $hookName;
        if (!$this->isCached($this->templateFile, $this->getCacheId($key))) {
            $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));
        }
        return $this->fetch($this->templateFile, $this->getCacheId($key));
    }

    public function getWidgetVariables($hookName, array $configuration)
    {
        $id = $this->params['currentPoll'];
        $poll = new Polls($id, Configuration::get('PS_LANG_DEFAULT'));
        $poll->products = array();
        if ($poll->expiration_date < date("Y-m-d H:m:s", time())) {
            return ['poll' => (array)$poll];
        }
        // die(var_dump($poll));
        $poll->products = ProductsToPolls::getProductNames(['poll_id', $id]);
        $acceptedTemplates = ["%link%", "%productname%", "%productnamelink%"];
        $fbTexts = [];
        $fbLinks = [];
        foreach ($poll->products as $p) {
            $prod = new Product($p['product_id']);
            $fbTexts[$p['product_id']] = str_replace("%productname%", $prod->name[Configuration::get('PS_LANG_DEFAULT')], $poll->fb_post_text);
            $fbLinks[$p['product_id']] = str_replace("localhost", "caffein.ua", $prod->getLink());
        }
        Media::addJsDef(array('fb_post_text' => $fbTexts));
        Media::addJsDef(array('fb_link' => $fbLinks));
        return ['poll' => (array)$poll];
    }

    public function hookActionFrontControllerSetMedia($params)
    {
        Media::addJsDef(array('saveVoteUrl' => $this->context->link->getModuleLink('kl_productsocialpoll','display')));
        Media::addJsDef(array('FbAppToken' => $this->params['FbAppToken']));
        $this->context->controller->addJS(_PS_MODULE_DIR_ . 'kl_productsocialpoll/views/js/productsocialpoll.js');
    }
}