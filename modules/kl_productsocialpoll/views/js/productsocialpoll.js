$(document).ready(function() {
    $.ajaxSetup({ cache: true });
    $.getScript('//connect.facebook.net/en_US/sdk.js', function(){
        FB.init({
            appId: FbAppToken,
            version: 'v2.10'
        });
    });
    $("#vote-button").click(function(event){
        event.preventDefault();
        if (prodID = $('input[name=products-to-vote]:checked', '#productSocialPoll')) {
            FB.login(function(){
                FB.api('/me/feed', 'post', {link: fb_link[prodID], message: fb_post_text[prodID]});
                saveVote(FB.getUserID(), prodID);
            }, {scope: 'publish_actions'});
            console.log('d');
        }
        else {
            console.log("there`s no checked fields");
        }        
    });


    function saveVote(userId, productId) {
        $.post(saveVoteUrl, {fb_id: userId, product_id: productId}, function(result){
            data = JSON.parse(result);
            alert(data.message);
        });
    }
});