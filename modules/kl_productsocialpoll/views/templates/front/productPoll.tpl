{if $poll.products}
	<form id="productSocialPoll">
		<div class="h3">{$poll.name}</div>
		{$poll.description}
		{if $poll.poll_photo}
			<img src="{$poll.poll_photo}"/>
		{/if}
		<div>
			{foreach from=$poll.products item=prod}
				<input type="radio" name="products-to-vote" value="{$prod.product_id}" id="{$prod.name}">
				<label for="{$prod.name}">{$prod.name}</label>
			{/foreach}
		</div>
		<div>
		    <button type="submit" id="vote-button">{l s='Submit' mod='Modules.kl_productsocialpoll'}</button>
		</div>
	</form>
{/if}