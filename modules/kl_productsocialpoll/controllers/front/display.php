<?php

require_once _PS_MODULE_DIR_ . 'kl_productsocialpoll/classes/VotesClass.php';
require_once _PS_MODULE_DIR_ . 'kl_productsocialpoll/kl_productsocialpoll.php';

class kl_productsocialpolldisplayModuleFrontController extends ModuleFrontController
{
	public function initContent()
    {
    	echo $this->saveVote();
        exit();
    }

    private function saveVote()
    {
        if (Votes::isUserCanVote($_POST['fb_id'])) {
            $vote = new Votes();
            if ($params = Configuration::get('PRODUCTSOCIALPOLL')) {
                $vote->poll_id = unserialize($params)['currentPoll'];
                $vote->setValues($_POST);
                if ($vote->validateFields(false) && $vote->save()) {
                    return json_encode(['status' => true, 'message' => $this->module->l('Thank you for your vote', 'display')]);
                }
            }
            return json_encode(['status' => false, 'message' => $this->module->l('Some technical problems had occured, please try later', 'display')]);
        }
        else {
            return json_encode(['status' => false, 'message' => $this->module->l('You can\'t vote. Maybe you already took part in this poll before', 'display')]);
        }
    }
}