<?php

class ProductsToPolls extends ObjectModel
{
    public $poll_id;
    public $product_id;

    public static $definition = array(
        'table' => "kl_productsocialpoll_products_to_polls",
        'primary' => 'poll_id',
        'multilang' => false,
        'fields' => array(
            'poll_id' => array('type' => self::TYPE_NOTHING, 'validate' => 'isUnsignedId'),
            'product_id' => array('type' => self::TYPE_NOTHING, 'validate' => 'isUnsignedId'),
        )
    );

    public static function bulkInsert($prods, $poll_id)
    {
        if (!isset($prods)) {
            return false;
        }
        $result = true;
        foreach ($prods as $id) {
            $tmp = new ProductsToPolls();
            $tmp->poll_id = $poll_id;
            $tmp->product_id = $id;
            $result = $result && $tmp->save();
            unset($tmp);
        }
        return $result;
    }

    public static function bulkUpdate($prods, $poll_id)
    {
        $prodsToPolls = new ProductsToPolls();
        if ($prodsToPolls->deleteSelection([$poll_id])) {
            return self::bulkInsert($prods, $poll_id);
        }
    }

    public static function getProductNames(...$conditions)
    {
        $queries = [];
        $result = [];
        foreach ($conditions as $c) {
            if(isset($c[2])) {
                array_push($queries, _DB_PREFIX_ . self::$definition['table'] . "." . $c[0] . $c[1] . " '" . addcslashes($c[2], "'") . "'");
            }
            else {
                array_push($queries, _DB_PREFIX_ . self::$definition['table'] . "." . $c[0] . " = '" . addcslashes($c[1], "'") . "'");
            }
        }
        array_push($queries, _DB_PREFIX_ . "product_lang.id_lang = '" . Configuration::get('PS_LANG_DEFAULT') . "'");
        $sql = "SELECT "._DB_PREFIX_."product_lang.name, "._DB_PREFIX_."kl_productsocialpoll_products_to_polls.product_id
                FROM "._DB_PREFIX_."kl_productsocialpoll_products_to_polls
                LEFT JOIN "._DB_PREFIX_."product_lang
                ON "._DB_PREFIX_."product_lang.id_product = "._DB_PREFIX_."kl_productsocialpoll_products_to_polls.product_id
                WHERE " . implode(' AND ', $queries);
        return Db::getInstance()->executeS($sql);
    }

    public static function select(...$conditions)
    {
        $queries = [];
        $result = [];
        foreach ($conditions as $c) {
            if(isset($c[2])) {
                array_push($queries, _DB_PREFIX_ . self::$definition['table'] . "." . $c[0] . $c[1] . " '" . addcslashes($c[2], "'") . "'");
            }
            else {
                array_push($queries, _DB_PREFIX_ . self::$definition['table'] . "." . $c[0] . " = '" . addcslashes($c[1], "'") . "'");
            }
        }
        $sql = "SELECT * FROM "._DB_PREFIX_ . self::$definition['table'] . " WHERE " . implode(' AND ', $queries);
        return Db::getInstance()->executeS($sql);
    } 
}
