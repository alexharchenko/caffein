<?php

class Polls extends ObjectModel
{
    public $id;
    public $name;
    public $expiration_date;
    public $winner_id;
    public $products;
    public $description;
    public $fb_post_text;
    public $poll_photo;

    public static $definition = array(
        'table' => 'kl_productsocialpoll_polls',
        'primary' => 'id',
        'multilang' => true,
        'fields' => array(
            'id' => array('type' => self::TYPE_NOTHING, 'validate' => 'isUnsignedId'),
            'expiration_date' => array('type' => self::TYPE_NOTHING, 'validate' => 'isString'),
            'winner_id' => array('type' => self::TYPE_NOTHING, 'validate' => 'isUnsignedId'),
            'poll_photo' => array('type' => self::TYPE_HTML, 'validate' => 'isString'),
            // Lang fields
            'name' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString'),
            'description' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString'),
            'fb_post_text' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString'),
        )
    );

    public function setValues($values)
    {
        /* Classical fields */
        foreach ($values as $key => $v) {
            if (array_key_exists($key, $this) && $v != "") {
                $this->{$key} = $v;
            }
        }

        /* Multilingual fields */
        if (sizeof($this->fieldsValidateLang)) {
            $languages = Language::getLanguages(false);
            foreach ($languages as $language) {
                foreach ($this->fieldsValidateLang as $field => $validation) {
                    if (isset($values[$field.'_'.(int)($language['id_lang'])])) {
                        $this->{$field}[(int)($language['id_lang'])] = $values[$field.'_'.(int)($language['id_lang'])];
                    }
                }
            }
        }
        
        if (Tools::getValue('poll_photo') !== "") {
            return $this->saveImage('poll_photo');
        }
    }

    private function saveImage($fieldName)
    {
        if ($error = ImageManager::validateUpload($_FILES[$fieldName])) {
            return false;
        }
        $filename = uniqid().basename($_FILES[$fieldName]['name']);
        $filename = str_replace(' ', '-', $filename);
        $filename = strtolower($filename);
        $filename = filter_var($filename, FILTER_SANITIZE_STRING);
        $_FILES[$fieldName]['name'] = $filename;
        // $uploader = new UploaderCore();
        // $result = $uploader->upload($_FILES[$fieldName], "upload/kl_productsocialpoll");
        $result = move_uploaded_file($_FILES[$fieldName]['tmp_name'], explode("modules", __FILE__)[0] . "/upload/kl_productsocialpoll/" . $filename);
        if (result['error'] !== 0) {
            $this->$fieldName = "upload/kl_productsocialpoll/" . $filename;
            return true;
        }
        return false;
    }

    public static function all($lang_id = null)
    {
        if (!intval($lang_id)) {
           $lang_id  = (int)Configuration::get('PS_LANG_DEFAULT');
        }
        $sql = "SELECT "._DB_PREFIX_ . self::$definition['table'] .".*, "._DB_PREFIX_ . self::$definition['table'] . "_lang.name 
                FROM "._DB_PREFIX_ . self::$definition['table'] . "
                LEFT JOIN "._DB_PREFIX_ . self::$definition['table'] . "_lang
                ON "._DB_PREFIX_ . self::$definition['table'] . "_lang.id = "._DB_PREFIX_ . self::$definition['table'] . ".id
                WHERE "._DB_PREFIX_ . self::$definition['table'] . "_lang.id_lang=".$lang_id;
        return Db::getInstance()->executeS($sql);
    }

    public static function where(...$conditions)
    {
        $queries = [];
        $result = [];
        foreach ($conditions as $c) {
            if(isset($c[2])) {
                array_push($queries, _DB_PREFIX_ . self::$definition['table'] . "." . $c[0] . $c[1] . " '" . addcslashes($c[2], "'") . "'");
            }
            else {
                array_push($queries, _DB_PREFIX_ . self::$definition['table'] . "." . $c[0] . " = '" . addcslashes($c[1], "'") . "'");
            }
        }
        $sql = "SELECT * FROM "._DB_PREFIX_ . self::$definition['table'] . " WHERE " . implode(' AND ', $queries);
        if ($result = Db::getInstance()->executeS($sql)[0]) {
            foreach (self::getNames($result['id']) as $id => $lang) {
                $result['name'][$id] = $lang['name'];
                $result['description'][$id] = $lang['description'];
                $result['fb_post_text'][$id] = $lang['fb_post_text'];
            }
        }

        return $result;
    }

    public static function getNames($prod_id)
    {
        $languages = [];
        $sql = "SELECT name, description, fb_post_text FROM "._DB_PREFIX_ . self::$definition['table'] . "_lang WHERE id=" . $prod_id;
        if ($result = Db::getInstance()->executeS($sql)) {
            for ($i = 0; $i < count($result); $i++) {
                $languages[$i+1] = $result[$i];
            }
        }

        return $languages;
    }
}