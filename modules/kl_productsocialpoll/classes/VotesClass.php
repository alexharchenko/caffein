<?php 

class Votes extends ObjectModel
{
    public $poll_id;
    public $fb_id;
    public $voted_at;
    public $product_id;

    public function __construct()
    {
        parent::__construct();
    }

    public static $definition = array(
        'table' => 'kl_productsocialpoll_votes',
        'primary' => 'fb_id',
        'multilang' => false,
        'fields' => array(
            'poll_id' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'fb_id' => array('type' => self::TYPE_INT),
            'voted_at' => array('type' => self::TYPE_NOTHING),
            'product_id' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId')
        )
    );

    public static function isUserCanVote($id)
    {
    	if(!intval($id)) {
    		return false;
    	}
        if ($params = Configuration::get('PRODUCTSOCIALPOLL')) {
            $poll_id = intval(unserialize($params)['currentPoll']);
            $sql = "SELECT count(*) AS number FROM " . _DB_PREFIX_ . self::$definition['table'] . " WHERE fb_id = $id AND poll_id = " . $poll_id;
            if ($result = Db::getInstance()->executeS($sql)) {
                if ($result[0]['number'] > 0) {
                    return false;
                }
            }
            return true;
        }
    	return false;
    }

    public function setValues($values)
    {
        foreach ($values as $key => $v) {
            if (array_key_exists($key, $this)) {
                $this->{$key} = $v;
            }
        }
    }

    public static function getVotesNumber($poll_id)
    {
        if (!intval($poll_id)) {
            return [];
        }
        $sql = "SELECT count(" . _DB_PREFIX_ . self::$definition['table'] . ".product_id) as number, " . _DB_PREFIX_ . "product_lang.name 
                FROM `" . _DB_PREFIX_ . self::$definition['table'] . "` 
                LEFT JOIN " . _DB_PREFIX_ . "product_lang
                ON " . _DB_PREFIX_ . "product_lang.id_product = " . _DB_PREFIX_ . self::$definition['table'] . ".product_id
                WHERE " . _DB_PREFIX_ . "product_lang.id_lang = " . Configuration::get('PS_LANG_DEFAULT') . "
                AND " . _DB_PREFIX_ . self::$definition['table'] . ".poll_id = " . $poll_id ."
                GROUP BY " . _DB_PREFIX_ . self::$definition['table'] . ".product_id, " . _DB_PREFIX_ . "product_lang.name
                ORDER BY number DESC;";
        return Db::getInstance()->executeS($sql);
    }
}