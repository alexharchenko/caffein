<?php

/**
 * PrestaShop PS IT Solution Module
 *
 * @author    PS IT Solution http://www.psitsolution.com/
 * @copyright 2010-2017 PS IT Solution
 * @license   This software is distributed under MIT License
 *
 * CONTACT US http://psitsolution.com
 * info@psitsolution.com
 */

 // ----------- //
 // ----------- //
 // VERSION 203 //
 // ----------- //
 // ----------- //

class psitclean extends Module
{
    public function __construct()
    {
        @ini_set("display_errors", 0);
        @error_reporting(0);
        $this->bootstrap = true;
        $this->name = 'psitclean';
        $this->version = '2.0.3';
        $this->author = 'PS IT Solution';
        $this->bootstrap = 1;
        $this->psitsolution_link = 'http://psitsolution.com/prestashop-seo-clean-urls/';
        $this->tab = 'seo';
        $this->displayName = $this->l('PS IT Clean URLs');
        $this->description = $this->l('This module generates clean urls and increases the SEO value of your Prestashop Store.');
        parent::__construct();
        $this->checkIfOverrideDirExists();
    }

    private function checkIfOverrideDirExists(){
        if(!file_exists('../override/controllers/front/listing')){
            var_dump(mkdir('../override/controllers/front/listing',0777, true));
        }
    }



    public static function psversion($part = 1)
    {
        $version = _PS_VERSION_;
        $exp = explode('.', $version);
        if ($part == 1)
        {
            return $exp[1];
        }
        if ($part == 2)
        {
            return $exp[2];
        }
        if ($part == 3)
        {
            return $exp[3];
        }
    }

    //User Interface

    public function getContent()
    {
        $output = '<div class="panel"><h3><i class="icon-wrench"></i> PS IT Clean Urls Setting</h3>
        <p class="info">'
            .$this->l('On some versions you could have to disable Cache, save, open your shop home page, than go back and enable it:').'<br>'
            .'</p>
          <p class="info"><h4>'
            .$this->l('After installation, follow these simple steps:').'</h4>'
            .sprintf('%s -> %s -> %s', $this->l('Advanced Parameters'), $this->l('Performance'), $this->l('Clear Smarty cache')).'</strong><br>'
            .sprintf('%s -> %s -> %s -> %s', $this->l('Shop Parameters'), $this->l('SEO and URLs'), $this->l('Set userfriendly URL off'), $this->l('Save')).'<br>'
            .sprintf('%s -> %s -> %s -> %s', $this->l('Shop Parameters'), $this->l('SEO and URLs'), $this->l('Set userfriendly URL on'), $this->l('Save')).'<br>'
            .'</p>
            </div>';

        $sql = 'SELECT * FROM `'._DB_PREFIX_.'product_lang`
            WHERE `link_rewrite`
            IN (SELECT `link_rewrite` FROM `'._DB_PREFIX_.'product_lang`
            GROUP BY `link_rewrite`, `id_lang`
            HAVING count(`link_rewrite`) > 1)';
        if (Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_SHOP) {
            $sql .= ' AND `id_shop` = '.(int) Shop::getContextShopID();
        }

        if ($res = Db::getInstance()->ExecuteS($sql)) {
            $err = $this->l('You need to fix duplicate URL entries:').'<br>';
            foreach ($res as $row) {
                $lang = $this->context->language->getLanguage($row['id_lang']);
                $err .= $row['name'].' ('.$row['id_product'].') - '.$row['link_rewrite'].'<br>';

                $shop = $this->context->shop->getShop($lang['id_shop']);
                $err .= $this->l('Language: ').$lang['name'].'<br>'.$this->l('Shop: ').$shop['name'].'<br><br>';
            }
            $output .= $this->displayWarning($err);
        } else {
            $output .= $this->displayConfirmation($this->l('Nice. You have no duplicate URL entry.'));
        }

        return
        '<div class="panel"><div class="panel">
          <span style="width: 100%;font-size: 28px;"><a href ="http://www.psitsolution.com/prestashop-seo-clean-urls/" target="_blank"><img style="width: 100px;height: 100px;background: rgba(64,127,161,0.9);padding: 10px;border-radius: 50%;margin: 0 0 20px 0;" src="http://www.psitsolution.com/prestashop-seo-clean-urls/en/assets/images/intro.png"></a> Thanks for choosing PS IT Prestashop Modules<img style="width: 15px;height: 15px;vertical-align: 30%;" src="http://www.psitsolution.com/it/dist/img/logo.png"/>. <br />Do you need Support? <a href="http://www.psitsolution.com/prestashop-seo-clean-urls/en/" target="_blank">Book Now our <i class="icon-support"></i> Support Service!</a></span>
        </div>'.$output.'</div>';
    }




    //Uninstall

    public function inconsistency($var)
    {
        return true;
    }

    public function uninstall()
    {
        return parent::uninstall();
    }

    public function install()
    {
        return parent:: install();
    }




}
