<?php
/**
 * PrestaShop PS IT Solution Module
 *
 * @author    PS IT Solution http://www.psitsolution.com/
 * @copyright 2010-2017 PS IT Solution
 * @license   This software is distributed under MIT License
 *
 * CONTACT US http://psitsolution.com
 * info@psitsolution.com
 */
class Dispatcher extends DispatcherCore
{


    protected function loadRoutes($id_shop = null)
    {
        /*
         * @var array List of default routes
         */
        $this->default_routes = array(
            'category_rule' => array(
                'controller' => 'category',
                'rule' => '{categories:/}{rewrite}/',
                'keywords' => array(
                    'id' => array('regexp' => '[0-9]+'),
                    'categories' => array('regexp' => '[/_a-zA-Z0-9\pL\pS-]*'),
                    'rewrite' => array('regexp' => '[_a-zA-Z0-9\pL\pS-]*', 'param' => 'category_rewrite'),
                    'meta_keywords' => array('regexp' => '[_a-zA-Z0-9\pL-]*'),
                    'meta_title' => array('regexp' => '[_a-zA-Z0-9\pL-]*'),
                ),
            ),
            'supplier_rule' => array(
                'controller' => 'supplier',
                'rule' => 'supplier/{rewrite}/',
                'keywords' => array(
                    'id' => array('regexp' => '[0-9]+'),
                    'rewrite' => array('regexp' => '[_a-zA-Z0-9\pL\pS-]*', 'param' => 'supplier_rewrite'),
                    'meta_keywords' => array('regexp' => '[_a-zA-Z0-9\pL-]*'),
                    'meta_title' => array('regexp' => '[_a-zA-Z0-9\pL-]*'),
                ),
            ),
            'manufacturer_rule' => array(
                'controller' => 'manufacturer',
                'rule' => 'manufacturer/{rewrite}/',
                'keywords' => array(
                    'id' => array('regexp' => '[0-9]+'),
                    'rewrite' => array('regexp' => '[/_a-zA-Z0-9\pL\pS-]*', 'param' => 'manufacturer_rewrite'),
                    'meta_keywords' => array('regexp' => '[/_a-zA-Z0-9\pL\pL-]*'),
                    'meta_title' => array('regexp' => '[/_a-zA-Z0-9\pL\pS-]*'),
                ),
            ),
            'cms_rule' => array(
                'controller' => 'cms',
                'rule' => 'info/{rewrite}',
                'keywords' => array(
                    'id' => array('regexp' => '[0-9]+'),
                    'rewrite' => array('regexp' => '[_a-zA-Z0-9\pL\pS-]*', 'param' => 'cms_rewrite'),
                    'meta_keywords' => array('regexp' => '[_a-zA-Z0-9\pL-]*'),
                    'meta_title' => array('regexp' => '[_a-zA-Z0-9\pL-]*'),
                ),
            ),
            'cms_category_rule' => array(
                'controller' => 'cms',
                'rule' => 'info/{rewrite}/',
                'keywords' => array(
                    'id' => array('regexp' => '[0-9]+'),
                    'rewrite' => array('regexp' => '[_a-zA-Z0-9\pL\pS-]*', 'param' => 'cms_category_rewrite'),
                    'meta_keywords' => array('regexp' => '[_a-zA-Z0-9\pL-]*'),
                    'meta_title' => array('regexp' => '[_a-zA-Z0-9\pL-]*'),
                ),
            ),
            'module' => array(
                'controller' => null,
                'rule' => 'module/{module}/{controller}',
                'keywords' => array(
                    'module' => array('regexp' => '[_a-zA-Z0-9-]+', 'param' => 'module'),
                    'controller' => array('regexp' => '[_a-zA-Z0-9-]+', 'param' => 'controller'),
                ),
                'params' => array(
                    'fc' => 'module',
                ),
            ),
            'product_rule' => array(
                'controller' => 'product',
                'rule' => '{category:/}{rewrite}',
                'keywords' => array(
                    'id' => array('regexp' => '[0-9]+'),
                    'id_product_attribute' => array('regexp' => '[0-9]+'),
                    'rewrite' => array('regexp' => '[_a-zA-Z0-9\pL\pS-]*', 'param' => 'product_rewrite'),
                    'quantity_wanted' => array('regexp' => '[0-9\pL]*'),
                    'ean13' => array('regexp' => '[0-9\pL]*'),
                    'category' => array('regexp' => '[/_a-zA-Z0-9\pL\pS-]*'),
                    'categories' => array('regexp' => '[/_a-zA-Z0-9-\pL]*'),
                    'reference' => array('regexp' => '[/_a-zA-Z0-9\pL\pS-]*'),
                    'meta_keywords' => array('regexp' => '[/_a-zA-Z0-9\pL\pS-]*'),
                    'meta_title' => array('regexp' => '[/_a-zA-Z0-9\pL\pS-]*'),
                    'manufacturer' => array('regexp' => '[/_a-zA-Z0-9\pL\pS-]*'),
                    'supplier' => array('regexp' => '[/_a-zA-Z0-9\pL\pS-]*'),
                    'price' => array('regexp' => '[0-9\.,]*'),
                    'tags' => array('regexp' => '[a-zA-Z0-9-\pL]*'),
                ),
            ),
            'layered_rule' => array(
                'controller' => 'category',
                'rule' => '{rewrite}/f/{selected_filters}',
                'keywords' => array(
                    'id' => array('regexp' => '[0-9]+'),
                    'selected_filters' => array('regexp' => '.*', 'param' => 'selected_filters'),
                    'rewrite' => array('regexp' => '[_a-zA-Z0-9\pL-]*', 'param' => 'category_rewrite'),
                    'meta_keywords' => array('regexp' => '[_a-zA-Z0-9\pL-]*'),
                    'meta_title' => array('regexp' => '[_a-zA-Z0-9\pL-]*'),
                ),
            ),
        );

        parent::loadRoutes($id_shop);
    }
    /**
     * @param string $route_id   Name of the route (need to be uniq, a second route with same name will override the first)
     * @param string $rule       Url rule
     * @param string $controller Controller to call if request uri match the rule
     * @param int    $id_lang
     * @param int    $id_shop
     */

    public function addRoute($route_id, $rule, $controller, $id_lang = null, array $keywords = array(), array $params = array(), $id_shop = null)
    {
        if (isset(Context::getContext()->language) && $id_lang === null) {
            $id_lang = (int) Context::getContext()->language->id;
        }
        if (isset(Context::getContext()->shop) && $id_shop === null) {
            $id_shop = (int) Context::getContext()->shop->id;
        }
        $regexp = preg_quote($rule, '#');
        if ($keywords) {
            $transform_keywords = array();
            preg_match_all('#\\\{(([^{}]*)\\\:)?('.implode('|', array_keys($keywords)).')(\\\:([^{}]*))?\\\}#', $regexp, $m);
            for ($i = 0, $total = count($m[0]); $i < $total; ++$i) {
                $prepend = $m[2][$i];
                $keyword = $m[3][$i];
                $append = $m[5][$i];
                $transform_keywords[$keyword] = array(
                    'required' => isset($keywords[$keyword]['param']),
                    'prepend' => stripslashes($prepend),
                    'append' => stripslashes($append),
                );
                $prepend_regexp = $append_regexp = '';
                if ($prepend || $append) {
                    $prepend_regexp = '('.$prepend;
                    $append_regexp = $append.')??'; // fix greediness (step 1)
                }
                if (isset($keywords[$keyword]['param'])) {
                    $regexp = str_replace($m[0][$i], $prepend_regexp.'(?P<'.$keywords[$keyword]['param'].'>'.$keywords[$keyword]['regexp'].')'.$append_regexp, $regexp);
                } else {
                    $regexp = str_replace($m[0][$i], $prepend_regexp.'('.$keywords[$keyword]['regexp'].')'.$append_regexp, $regexp);
                }
            }
            $keywords = $transform_keywords;
        }
        $regexp = '#^/'.$regexp.'$#uU'; // fix greediness (step 2)
        if (!isset($this->routes[$id_shop])) {
            $this->routes[$id_shop] = array();
        }
        if (!isset($this->routes[$id_shop][$id_lang])) {
            $this->routes[$id_shop][$id_lang] = array();
        }
        $this->routes[$id_shop][$id_lang][$route_id] = array(
            'rule' => $rule,
            'regexp' => $regexp,
            'controller' => $controller,
            'keywords' => $keywords,
            'params' => $params,
        );
    }
    /**
     * Retrieve the controller from url or request uri if routes are activated.
     *
     * @param int $id_shop, defaults null
     *
     * @return string
     */

    public function getController($id_shop = null)
    {
        if (defined('_PS_ADMIN_DIR_')) {
            $_GET['controllerUri'] = Tools::getvalue('controller');
        }
        if ($this->controller) {
            $_GET['controller'] = $this->controller;
            return $this->controller;
        }
        if (null === $id_shop) {
            $id_shop = (int) Context::getContext()->shop->id;
        }
        $controller = Tools::getValue('controller');
        $curr_lang_id = Context::getContext()->language->id;
        if (isset($controller) && is_string($controller) && preg_match('/^([0-9a-z_-]+)\?(.*)=(.*)$/Ui', $controller, $m)) {
            $controller = $m[1];
            if (isset($_GET['controller'])) {
                $_GET[$m[2]] = $m[3];
            } elseif (isset($_POST['controller'])) {
                $_POST[$m[2]] = $m[3];
            }
        }
        if (!Validate::isControllerName($controller)) {
            $controller = false;
        }
        if ($this->use_routes && !$controller && !defined('_PS_ADMIN_DIR_')) {
            if (!$this->request_uri) {
                return strtolower($this->controller_not_found);
            }
            $controller = $this->controller_not_found;
            if (!preg_match('/\.(gif|jpe?g|png|css|js|ico)$/i', $this->request_uri)) {
                if ($this->empty_route) {
                    $this->addRoute($this->empty_route['routeID'], $this->empty_route['rule'], $this->empty_route['controller'], $curr_lang_id, array(), array(), $id_shop);
                }
                list($uri) = explode('?', $this->request_uri);
                if (isset($this->routes[$id_shop][$curr_lang_id])) {
                    $route = array();
                    foreach ($this->routes[$id_shop][$curr_lang_id] as $k => $r) {
                        if (preg_match($r['regexp'], $uri, $m)) {
                            $isTemplate = false;
                            $module = isset($r['params']['module']) ? $r['params']['module'] : '';
                            switch ($r['controller'].$module) { // Avoid name collision between core and modules' controllers
                                case 'supplier':
                                case 'manufacturer':
                                    if (false !== strpos($r['rule'], '{')) {
                                        $isTemplate = true;
                                    }
                                    break;
                                case 'cms':
                                case 'product':
                                    $isTemplate = true;
                                    break;
                                    case 'category':
                                        // category can be processed in two ways
                                        if (false === strpos($r['rule'], 'selected_filters')) {
                                            $isTemplate = true;
                                        }
                                        break;
                                }

                                if (!$isTemplate) {
                                    $route = $r;
                                    break;
                                }
                            }
                        }
                        // if route is not found, we have to find rewrite link in database
                        if (empty($route)) {
                            // get the path from requested URI, and remove "/" at the beginning
                            $short_link = ltrim(parse_url($uri, PHP_URL_PATH), '/');

                            $route = $this->routes[$id_shop][$curr_lang_id]['product_rule'];
                            if (!self::isProductLink($short_link, $route)) {
                                $route = $this->routes[$id_shop][$curr_lang_id]['category_rule'];
                                if (!self::isCategoryLink($short_link, $route)) {
                                    $route = $this->routes[$id_shop][$curr_lang_id]['cms_rule'];
                                    if (!self::isCmsLink($short_link, $route)) {
                                        $route = $this->routes[$id_shop][$curr_lang_id]['cms_category_rule'];
                                        if (!self::isCmsCategoryLink($short_link, $route)) {
                                            $route = $this->routes[$id_shop][$curr_lang_id]['manufacturer_rule'];
                                            if (!self::isManufacturerLink($short_link, $route)) {
                                                $route = $this->routes[$id_shop][$curr_lang_id]['supplier_rule'];
                                                if (!self::isSupplierLink($short_link, $route)) {
                                                    // no route found
                                                    $route = array();
                                                    $controller = $this->controller_not_found;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (!empty($route['controller'])) {
                                $controller = $route['controller'];
                            }
                        }

                        if (!empty($route)) {
                            if (preg_match($route['regexp'], $uri, $m)) {
                                // Route found! Now fill $_GET with parameters of uri
                                foreach ($m as $k => $v) {
                                    if (!is_numeric($k)) {
                                        $_GET[$k] = $v;
                                    }
                                }

                                $controller = $route['controller'] ? $route['controller'] : $_GET['controller'];
                                if (!empty($route['params'])) {
                                    foreach ($route['params'] as $k => $v) {
                                        $_GET[$k] = $v;
                                    }
                                }

                                // A patch for module friendly urls
                            if (preg_match('#module-([a-z0-9_-]+)-([a-z0-9]+)$#i', $controller, $m)) {
                                $_GET['module'] = $m[1];
                                $_GET['fc'] = 'module';
                                $controller = $m[2];
                            }
                            if (isset($_GET['fc']) && $_GET['fc'] == 'module') {
                                $this->front_controller = self::FC_MODULE;
                            }
                        }
                    }
                }
            }
            if ($controller == 'index' || $this->request_uri == '/index.php') {
                $controller = $this->default_controller;
            }
            $this->controller = $controller;
        } else { // Default mode, take controller from url
            $this->controller = $controller;
        }
        $this->controller = str_replace('-', '', $this->controller);
        $_GET['controller'] = $this->controller;
        return $this->controller;
    }
    /**
     * Check if $short_link is a Product Link.
     *
     * @param string $short_link: requested url without '?' part and without '/' on begining
     *
     * @return bool true: it's a link to product, false: it isn't
     */

    private static function isProductLink($short_link, $route)
    {
        $short_link = preg_replace('#\.html?$#', '', '/'.$short_link);
        $regexp = preg_replace('!\\\.html?\\$#!', '$#', $route['regexp']);
        preg_match($regexp, $short_link, $kw);
        if (empty($kw['product_rewrite'])) {
            return false;
        }
        $sql = 'SELECT `id_product`
            FROM `'._DB_PREFIX_.'product_lang`
            WHERE `link_rewrite` = \''.pSQL($kw['product_rewrite']).'\' AND `id_lang` = '.(int) Context::getContext()->language->id;
        if (Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_SHOP) {
            $sql .= ' AND `id_shop` = '.(int) Shop::getContextShopID();
        }
        $id_product = (int) Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        return $id_product > 0;
    }
    /**
     * Check if $short_link is a Category Link.
     *
     * @param string $short_link: requested url without '?' part and without '/' on begining
     *
     * @return bool true: it's a link to category, false: it isn't
     */

    private static function isCategoryLink($short_link, $route)
    {
     $category = basename($short_link);
     $tree = explode("/", trim($short_link, '/'));
//echo '<pre>Dispatcher tree:'.print_r($tree,1).'</pre>';
     $short_link = preg_replace('#\.html?$#', '', '/'.$short_link);
     $regexp = preg_replace('!\\\.html?\\$#!', '$#', $route['regexp']);
     preg_match($regexp, $short_link, $kw);
     if (empty($kw['category_rewrite'])) {
         return false;
     }

     $categories = Dispatcher::getSimpleCategories((int)Context::getContext()->language->id);
$id_category = 0;
foreach ($categories as $candidate){
  $cloneTree = $tree;
  if ($candidate['link_rewrite'] == array_pop($cloneTree)){
    if (empty($cloneTree) || $candidate['is_root_category'] ||  Dispatcher::checkParentsLoop($candidate, $categories, $cloneTree)){
      $id_category = (int)$candidate['id_category'];
      break;
    }
  }
}

     return ($id_category > 0);
 }

public static function checkParentsLoop($candidate, $categories, $tree){
foreach  ($categories as $cat){
  $cloneTree = $tree;
  if ($candidate['id_parent'] == $cat['id_category'] && $cat['link_rewrite'] == array_pop($cloneTree)){
    if (empty($cloneTree) || $cat['is_root_category'] || Dispatcher::checkParentsLoop($cat, $categories, $cloneTree)){
      return $cat['id_category'];
      break;
    }
  }
}
return false;

}

 public static function getSimpleCategories($id_lang)
 {
     return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
       SELECT c.`id_category`, c.`id_parent`, c.`is_root_category`, cl.`name`, cl.`link_rewrite`
       FROM `'._DB_PREFIX_.'category` c
       LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (c.`id_category` = cl.`id_category`'.Shop::addSqlRestrictionOnLang('cl').')
       '.Shop::addSqlAssociation('category', 'c').'
       WHERE cl.`id_lang` = '.(int)$id_lang.'
       AND c.`id_category` != '.Configuration::get('PS_ROOT_CATEGORY').'
       GROUP BY c.id_category');
 }
    /**
     * Check if $short_link is a Cms Link.
     *
     * @param string $short_link: requested url without '?' part and without '/' on begining
     *
     * @return bool true: it's a link to cms page, false: it isn't
     */

    private static function isCmsLink($short_link, $route)
    {
        $short_link = preg_replace('#\.html?$#', '', '/'.$short_link);
        $regexp = preg_replace('!\\\.html?\\$#!', '$#', $route['regexp']);
        preg_match($regexp, $short_link, $kw);
        if (empty($kw['cms_rewrite'])) {
            return false;
        }
        $sql = 'SELECT l.`id_cms`
            FROM `'._DB_PREFIX_.'cms_lang` l
            LEFT JOIN `'._DB_PREFIX_.'cms_shop` s ON (l.`id_cms` = s.`id_cms`)
            WHERE l.`link_rewrite` = \''.pSQL($kw['cms_rewrite']).'\'';
        if (Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_SHOP) {
            $sql .= ' AND s.`id_shop` = '.(int) Shop::getContextShopID();
        }
        $id_cms = (int) Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        return $id_cms > 0;
    }
    /**
     * Check if $short_link is a Cms Category Link.
     *
     * @param string $short_link: requested url without '?' part and without '/' on begining
     *
     * @return bool true: it's a link to cms page, false: it isn't
     */

    private static function isCmsCategoryLink($short_link, $route)
    {
        $short_link = preg_replace('#\.html?$#', '', '/'.$short_link);
        $regexp = preg_replace('!\\\.html?\\$#!', '$#', $route['regexp']);
        preg_match($regexp, $short_link, $kw);
        if (empty($kw['cms_category_rewrite'])) {
            if (0 === strpos('/'.$route['rule'], $short_link)) {
                return true;
            }
            return false;
        }
        $sql = 'SELECT l.`id_cms_category`
            FROM `'._DB_PREFIX_.'cms_category_lang` l
            LEFT JOIN `'._DB_PREFIX_.'cms_category_shop` s ON (l.`id_cms_category` = s.`id_cms_category`)
            WHERE l.`link_rewrite` = \''.$kw['cms_category_rewrite'].'\'';
        if (Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_SHOP) {
            $sql .= ' AND s.`id_shop` = '.(int) Shop::getContextShopID();
        }
        $id_cms_cat = (int) Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        return $id_cms_cat > 0;
    }
    /**
     * Check if $short_link is a Manufacturer Link.
     *
     * @param string $short_link: requested url without '?' part and without '/' on begining
     *
     * @return bool true: it's a link to manufacturer, false: it isn't
     */

    private static function isManufacturerLink($short_link, $route)
    {
      $id_manufacturer = 0;
      {
          $explode_manufacturer_link = explode("/", $short_link);
          if ($explode_manufacturer_link[0] == "supplier")
          {
              return false;
          }
          $count = count($explode_manufacturer_link);
          if (isset($explode_manufacturer_link[1]))
          {
              $name_manufacturer = $explode_manufacturer_link[1];
          }
          $sqlall = 'SELECT * FROM `' . _DB_PREFIX_ . 'manufacturer` m
      LEFT JOIN `' . _DB_PREFIX_ . 'manufacturer_shop` s ON (m.`id_manufacturer` = s.`id_manufacturer`) WHERE 1=1 ';
          if (Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_SHOP){
              $sqlall .= ' AND s.`id_shop` = ' . (int)Shop::getContextShopID();
          }
          $allmanufacturers = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sqlall);
          if (isset($explode_manufacturer_link[1]))
          {
              foreach ($allmanufacturers as $key => $manufacturer)
              {
                  if ($name_manufacturer == Tools::str2url($manufacturer['name']))
                  {
                      $id_manufacturer = $manufacturer['id_manufacturer'];
                  }
              }
          }
          if (isset($id_manufacturer))
          {
              return ($id_manufacturer > 0) ? true : false;
          }
          else
          {
              return false;
          }
        }
  }
    /**
     * Check if $short_link is a Supplier Link.
     *
     * @param string $short_link: requested url without '?' part and without '/' on begining
     *
     * @return bool true: it's a link to supplier, false: it isn't
     */

    private static function isSupplierLink($short_link, $route)
    {
      $id_supplier = 0;
      {
          $explode_supplier_link = explode("/", $short_link);
          $count = count($explode_supplier_link);
          if (isset($explode_supplier_link[1]))
          {
              $name_supplier = $explode_supplier_link[1];
          }
          $sql = 'SELECT *
      FROM `' . _DB_PREFIX_ . 'supplier` sp
      LEFT JOIN `' . _DB_PREFIX_ . 'supplier_shop` s ON (sp.`id_supplier` = s.`id_supplier`) WHERE 1=1';
          if (Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_SHOP){
              $sql .= ' AND s.`id_shop` = ' . (int)Shop::getContextShopID();
          }
          $allsuppliers = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sql);
          if (isset($explode_supplier_link[1]))
          {
              foreach ($allsuppliers as $key => $supplier)
              {
                  if ($name_supplier == Tools::str2url($supplier['name']))
                  {
                      $id_supplier = $supplier['id_supplier'];
                  }
              }
          }
          if (isset($id_supplier))
          {
              return ($id_supplier > 0) ? true : false;
          }
          else
          {
              return false;
          }
      }
    }
}
