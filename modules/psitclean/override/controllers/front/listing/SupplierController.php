<?php

/**
 * PrestaShop PS IT Solution Module
 *
 * @author    PS IT Solution http://www.psitsolution.com/
 * @copyright 2010-2017 PS IT Solution
 * @license   This software is distributed under MIT License
 *
 * CONTACT US http://psitsolution.com
 * info@psitsolution.com
 */
 use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery;
 use PrestaShop\PrestaShop\Core\Product\Search\SortOrder;
 use PrestaShop\PrestaShop\Adapter\Supplier\SupplierProductSearchProvider;


class SupplierController extends SupplierControllerCore
{
    public function init()
    {

        {
            //SUPPLIER PATTERN
            $link_pattern = Tools::safeOutput(urldecode(Tools::getValue('supplier_rewrite')));
            $supplier_pattern = '@.*?([0-9]+)\_\_([/_a-zA-Z0-9\pL\pS-]*)@';
            preg_match($supplier_pattern, $_SERVER['REQUEST_URI'], $Array);
            if (isset($Array[2]) && $Array[2] != "")
            {
                $link_pattern = $Array[2];
            }
            if ($link_pattern)
            {
            $sql = 'SELECT *
        		FROM `' . _DB_PREFIX_ . 'supplier` sp
        		LEFT JOIN `' . _DB_PREFIX_ . 'supplier_shop` s ON (sp.`id_supplier` = s.`id_supplier`) WHERE 1=1';
                if (Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_SHOP)
                {
                    $sql .= ' AND s.`id_shop` = ' . (int)Shop::getContextShopID();
                }
                $allsuppliers = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sql);
                foreach ($allsuppliers as $key => $supplier)
                {
                    if ($link_pattern == Tools::str2url($supplier['name']))
                    {
                        $id_supplier = $supplier['id_supplier'];
                    }
                }
                if ($id_supplier != "")
                {
                    $_POST['id_supplier'] = $id_supplier;
                    $_GET['id_supplier'] = $id_supplier;
                    $_GET['supplier_rewrite'] = '';
                }
            }
            parent::init();
        }

    }
}
