<?php
/**
 * PrestaShop module by PSIT Solution
 *
 * @author    PS IT Solution http://psitsolution.com/
 * @copyright 2010-2017 PS IT Solution
 * @license   This softaware is distributed under MIT License
 *
 * Contact http://psitsolution.com info@psitsolution.com
 * Support: http://www.psitsolution.com/prestashop-seo-clean-urls/en/
 */
 class CategoryController extends CategoryControllerCore
 {

   public function init()
   {
      if ($category_rewrite = Tools::getValue('category_rewrite')) {
          $cat = explode("/", trim($_SERVER['REQUEST_URI'], '/'));
          $root_cat = CategoryCore::getRootCategory();
          $sql = 'SELECT `'._DB_PREFIX_.'category_lang`.`id_category` FROM `'._DB_PREFIX_.'category_lang` JOIN `'._DB_PREFIX_.'category` on `'._DB_PREFIX_.'category_lang`.`id_category` = `'._DB_PREFIX_.'category`.`id_category`
          WHERE `link_rewrite` = \''.pSQL(basename($category_rewrite, '.html')).'\' AND '
              . '((SELECT `link_rewrite` FROM `'._DB_PREFIX_.'category_lang` WHERE `id_category`=`id_parent` AND `id_lang` = '.(int)Context::getContext()->language->id.' LIMIT 1) = \''.$cat[count($cat)-3].'\''
              . ' OR (SELECT `link_rewrite` FROM `'._DB_PREFIX_.'category_lang` WHERE `id_category`=`id_parent` AND `id_lang` = '.(int)Context::getContext()->language->id.' LIMIT 1) = \''.$root_cat->link_rewrite.'\')'
              . ' AND `id_lang` = '.(int)Context::getContext()->language->id;
          if (Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_SHOP) {
              $sql .= ' AND `id_shop` = '.(int)Shop::getContextShopID();
          }
          $id_category = (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
          if ($category_rewrite = Tools::getValue('category_rewrite')) {
              $sql = 'SELECT `id_category` FROM `'._DB_PREFIX_.'category_lang`
                  WHERE `link_rewrite` = \''.pSQL(str_replace('.html', '', $category_rewrite)).'\' AND `id_lang` = '.Context::getContext()->language->id;
              if (Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_SHOP) {
                  $sql .= ' AND `id_shop` = '.(int) Shop::getContextShopID();
              }
              $id_category = (int) Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
              if ($id_category > 0) {
                  $_GET['id_category'] = $id_category;
              }
          }
      }
      if ($category_rewrite = Tools::getValue('category_rewrite')) {
       $_base_url = str_replace(Tools::getHttpHost(true), '', $this->context->link->getPageLink('index',true));
       $uri = str_replace($_base_url, '', $_SERVER['REQUEST_URI']);
       $tree = explode("/", trim($uri,'/'));
       $categories = Dispatcher::getSimpleCategories((int)Context::getContext()->language->id);
       $id_category = 0;
        foreach ($categories as $candidate){
            $cloneTree = $tree;
              if ($candidate['link_rewrite'] == array_pop($cloneTree)){
                   if (empty($cloneTree) || $candidate['is_root_category'] ||  Dispatcher::checkParentsLoop($candidate, $categories, $cloneTree)){
                         $id_category = (int)$candidate['id_category'];
                            break;
                             }
                             }
                             }
                             if ($id_category > 0) {
                               $_GET['id_category'] = $id_category;
                             }
                           }
      parent::init();
   }
  }
