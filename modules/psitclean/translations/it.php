<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{psitclean}prestashop>psitclean_9ed87a6b540a0af9c87bbba89dce0247'] = 'Questo modulo ottimizza gli urls ed aumenta l\'efficienza SEO del tuo Negozio Prestashop.';
$_MODULE['<{psitclean}prestashop>psitclean_8aa6176f15ae0b8ec0f7f7797bc129cb'] = 'Su alcune versioni potrebbe essere necessario disattivare la cache, salvare, aprire la home page del negozio, tornare indietro e abilitarlo.';
$_MODULE['<{psitclean}prestashop>psitclean_8a02fb71e23168f2804c7e693011c06d'] = 'Dopo l\'installazione, segui i seguenti semplici passi:';
$_MODULE['<{psitclean}prestashop>psitclean_c9d7eedc8be4380c02106619824b8449'] = 'Parametri Avanzati';
$_MODULE['<{psitclean}prestashop>psitclean_9446a98ad14416153cc4d45ab8b531bf'] = 'Prestazioni';
$_MODULE['<{psitclean}prestashop>psitclean_341be761f9f11e9b06dac0071fb8a5ed'] = 'Pulisci la Smarty Cache';
$_MODULE['<{psitclean}prestashop>psitclean_cf39aaa3dadb081e301236a4abf5e5f4'] = 'Parametri Negozio';
$_MODULE['<{psitclean}prestashop>psitclean_c9cc8cce247e49bae79f15173ce97354'] = 'Salva';
