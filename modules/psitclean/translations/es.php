<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{psitclean}prestashop>psitclean_9ed87a6b540a0af9c87bbba89dce0247'] = 'Este módulo optimiza las urls amigable y mejora el SEO de tu Tienda Virtual Prestashop..';
$_MODULE['<{psitclean}prestashop>psitclean_8aa6176f15ae0b8ec0f7f7797bc129cb'] = 'En algunas versiones puede ser pobile que tendrá que deshabilitar la caché, guardar, ir a la página principal de la tienda, volver atrás y habilitarlo.';
$_MODULE['<{psitclean}prestashop>psitclean_8a02fb71e23168f2804c7e693011c06d'] = 'Despues de la instalación, segue los siguientes pasos:';
$_MODULE['<{psitclean}prestashop>psitclean_c9d7eedc8be4380c02106619824b8449'] = 'Parámetros Avanzados';
$_MODULE['<{psitclean}prestashop>psitclean_9446a98ad14416153cc4d45ab8b531bf'] = 'Rendimiento';
$_MODULE['<{psitclean}prestashop>psitclean_341be761f9f11e9b06dac0071fb8a5ed'] = 'Limpiar la Smarty Cache';
$_MODULE['<{psitclean}prestashop>psitclean_cf39aaa3dadb081e301236a4abf5e5f4'] = 'Parámetros Tienda';
$_MODULE['<{psitclean}prestashop>psitclean_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
